package id.code.service.model.remote.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;

import static id.code.master_data.AliasName._EMAIL;
import static id.code.service.AliasName.*;

@Table(name = _VIEW_NAME_USERS)
public class RemoteUserModel {

    @JsonProperty(_ABSENCE_CARD_NO)
    @TableColumn(value = _ABSENCE_CARD_NO, primaryKey = true)
    private String absenceCardNo;

    @JsonProperty(_EMPLOYEE_NAME)
    @TableColumn(value = _EMPLOYEE_NAME)
    private String employeeName;

    @JsonProperty(_EMPLOYEE_STATUS)
    @TableColumn(value = _EMPLOYEE_STATUS)
    private Integer employeeStatus;

    @JsonProperty(_POSITION_TITLE)
    @TableColumn(_POSITION_TITLE)
    private String positionTitle;

    @JsonProperty(_BRANCH_CODE)
    @TableColumn(_ADDRESS_ID)
    private String branchCode;

    @JsonProperty(_EMAIL)
    @TableColumn(_CONTACT_VALUE)
    private String email;

    public String getEmail() {
        return email;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getAbsenceCardNo() {
        return absenceCardNo;
    }
    public String getEmployeeName() {
        return employeeName;
    }
    public String getPositionTitle() {
        return positionTitle;
    }
    public Integer getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setAbsenceCardNo(String absenceCardNo) {
        this.absenceCardNo = absenceCardNo;
    }
    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
    public void setPositionTitle(String positionTitle) {
        this.positionTitle = positionTitle;
    }
    public void setEmployeeStatus(Integer employeeStatus) {
        this.employeeStatus = employeeStatus;
    }
}
