package id.code.service.model.remote.branch;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.service.AliasName;

import static id.code.service.AliasName.*;

/**
 * Created by CODE.ID on 8/29/2017.
 */
@Table(name = AliasName._VIEW_NAME_SUB_BRANCHES)
public class RemoteSubBranchModel {
    @JsonProperty(value = _LOCATION_CODE)
    @TableColumn(name = _LOCATION_CODE, primaryKey = true)
    private String locationCode;

    @JsonProperty(value = _CABANG)
    @TableColumn(name = _CABANG)
    private String cabang;

    @JsonProperty(value = _DESCRIPTION)
    @TableColumn(name = _DESCRIPTION)
    private String description;

    public String getCabang() {
        return cabang;
    }
    public String getDescription() {
        return description;
    }
    public String getLocationCode() {
        return locationCode;
    }


    public void setCabang(String cabang) {
        this.cabang = cabang;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

}
