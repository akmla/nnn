package id.code.service.model.remote.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;

import static id.code.service.AliasName.*;

/**
 * Created by CODE.ID on 9/6/2017.
 */
@Table(name = _VIEW_NAME_PRODUCTS_EXTERNAL)
public class RemoteProductExternalModel {

    @JsonProperty(value = _PRINCIPAL)
    @TableColumn(name = _PRINCIPAL)
    private String principal;

    @JsonProperty(value = _PRODUCT_CODE)
    @TableColumn(name = _PRODUCT_CODE, primaryKey = true)
    private String productCode;

    @JsonProperty(value = _PRODUCT_DESCRIPTION)
    @TableColumn(name = _PRODUCT_DESCRIPTION)
    private String productDescription;

    @JsonProperty(value = _PRODUCT_GROUP)
    @TableColumn(name = _PRODUCT_GROUP)
    private String productGroup;

    public String getPrincipal() {
        return principal;
    }
    public String getProductCode() {
        return productCode;
    }
    public String getProductDescription() {
        return productDescription;
    }
    public String getProductGroup() {
        return productGroup;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }
}
