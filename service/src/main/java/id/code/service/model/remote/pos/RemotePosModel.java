package id.code.service.model.remote.pos;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;

import static id.code.service.AliasName.*;

@Table(name = _VIEW_NAME_POS)
public class RemotePosModel {
    @JsonProperty(value = _LOCATION_CODE)
    @TableColumn(name = _LOCATION_CODE, primaryKey = true)
    private String locationCode;

    @JsonProperty(value = _DESCRIPTION)
    @TableColumn(name = _DESCRIPTION)
    private String description;

    @JsonProperty(value = _CABANG)
    @TableColumn(name = _CABANG)
    private String cabang;

    @JsonProperty(value = _REGIONAL)
    @TableColumn(name = _REGIONAL)
    private String regional;

    public String getCabang() {
        return cabang;
    }
    public String getDescription() {
        return description;
    }
    public String getLocationCode() {
        return locationCode;
    }
    public String getRegional() {
        return regional;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }
    public void setRegional(String regional) {
        this.regional = regional;
    }
}
