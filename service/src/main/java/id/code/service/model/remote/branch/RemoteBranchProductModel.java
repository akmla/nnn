package id.code.service.model.remote.branch;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;

import static id.code.service.AliasName.*;

@Table(name = _VIEW_NAME_BRANCH_PRODUCTS)
public class RemoteBranchProductModel {
    @JsonProperty(value = _LOCATION_CODE)
    @TableColumn(name = _LOCATION_CODE, primaryKey = true)
    private String locationCode;

    @JsonProperty(value = _CROSS_REFERENCE)
    @TableColumn(name = _CROSS_REFERENCE, primaryKey = true)
    private String crossReference;

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getCrossReference() {
        return crossReference;
    }

    public void setCrossReference(String crossReference) {
        this.crossReference = crossReference;
    }
}
