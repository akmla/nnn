package id.code.service.model.remote.sio;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;

import static id.code.service.AliasName.*;

@Table(name = _VIEW_NAME_SIO_PROMOTION_TYPES)
public class RemoteSioTypePromotionModel {

    @JsonProperty(value = _SIO_TYPE)
    @TableColumn(name = _SIO_TYPE, primaryKey = true)
    private String sioType;

    @JsonProperty(value = _FLEX_VALUE)
    @TableColumn(name = _FLEX_VALUE, primaryKey = true)
    private String flexValue;

    @JsonProperty(value = _DESCRIPTION)
    @TableColumn(name = _DESCRIPTION)
    private String description;

    @JsonProperty(value = _KAT_ITEM_PROMOSI)
    @TableColumn(name = _KAT_ITEM_PROMOSI)
    private String katItemPromosi;

    public String getSioType() {
        return sioType;
    }
    public String getFlexValue() {
        return flexValue;
    }
    public String getDescription() {
        return description;
    }
    public String getKatItemPromosi() {
        return katItemPromosi;
    }

    public void setSioType(String sioType) {
        this.sioType = sioType;
    }
    public void setFlexValue(String flexValue) {
        this.flexValue = flexValue;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setKatItemPromosi(String katItemPromosi) {
        this.katItemPromosi = katItemPromosi;
    }
}
