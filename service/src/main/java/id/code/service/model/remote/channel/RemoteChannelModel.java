package id.code.service.model.remote.channel;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.service.AliasName;

import static id.code.service.AliasName._LOOKUP_CODE;
import static id.code.service.AliasName._MEANING;

/**
 * Created by CODE.ID on 8/22/2017.
 */
@Table(name = AliasName._VIEW_NAME_CHANNELS)
public class RemoteChannelModel {
    @JsonProperty(value = _LOOKUP_CODE)
    @TableColumn(name = _LOOKUP_CODE, primaryKey = true)
    private String lookupCode;

    @JsonProperty(value = _MEANING)
    @TableColumn(name = _MEANING)
    private String meaning;

    public String getLookupCode() {
        return lookupCode;
    }
    public String getMeaning() {
        return meaning;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }
    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }
}
