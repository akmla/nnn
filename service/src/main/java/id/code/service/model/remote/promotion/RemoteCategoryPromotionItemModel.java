package id.code.service.model.remote.promotion;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;

import static id.code.service.AliasName.*;

@Table(name = _VIEW_NAME_CATEGORY_PROMOTION_ITEMS)
public class RemoteCategoryPromotionItemModel {

    @JsonProperty(value = _FLEX_VALUE_MEANING)
    @TableColumn(name = _FLEX_VALUE_MEANING, primaryKey = true)
    private String flexValueMeaning;

    @JsonProperty(value = _DESCRIPTION)
    @TableColumn(name = _DESCRIPTION)
    private String description;

    public String getFlexValueMeaning() {
        return flexValueMeaning;
    }
    public String getDescription() {
        return description;
    }

    public void setFlexValueMeaning(String flexValueMeaning) {
        this.flexValueMeaning = flexValueMeaning;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
