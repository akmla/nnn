package id.code.service.model.remote.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.service.AliasName;

import static id.code.service.AliasName.*;

/**
 * Created by CODE.ID on 8/22/2017.
 */
@Table(name = AliasName._VIEW_NAME_PRODUCTS)
public class RemoteProductModel {
    @JsonProperty(value = _CROSS_REFERENCE)
    @TableColumn(name = _CROSS_REFERENCE, primaryKey = true)
    private String crossReference;

    @JsonProperty(value = _DESCRIPTION)
    @TableColumn(name = _DESCRIPTION)
    private String description;

    @JsonProperty(value = _KATEGORI_ROKOK)
    @TableColumn(name = _KATEGORI_ROKOK)
    private String kategoriRokok;

//    @JsonProperty(value = _BRANCH_CODE)
//    @TableColumn(name = _BRANCH_CODE)
//    private String branchCode;

//    @JsonProperty(value = _TYPE)
//    @TableColumn(name = _TYPE)
//    private String type;

    public String getCrossReference() {
        return crossReference;
    }
    public String getKategoriRokok() {
        return kategoriRokok;
    }
    public String getDescription() {
        return description;
    }

//    public String getBranches() {
//        return branchCode;
//    }
//    public String getType() {
//        return type;
//    }

    public void setCrossReference(String crossReference) {
        this.crossReference = crossReference;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setKategoriRokok(String kategoriRokok) {
        this.kategoriRokok = kategoriRokok;
    }

//    public void setBranches(String branchCode) {
//        this.branchCode = branchCode;
//    }
//    public void setType(String type) {
//        this.type = type;
//    }
}
