package id.code.service.facade;

import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.region.RegionModel;
import id.code.service.model.remote.region.RemoteRegionModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName._FLEX_VALUE_MEANING;
import static id.code.service.AliasName._INI_SYSTEM_SERVICE;

/**
 * Created by CODE.ID on 8/29/2017.
 */
public class RegionServiceFacade extends BaseServiceFacade {
    private List<RemoteRegionModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteRegionModel.class).orderBy(_FLEX_VALUE_MEANING).asc().limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteRegionModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteRegionModel> data = this.getData(rows);
                for (final RemoteRegionModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(RegionModel.class)
                            .where(_CODE).isEqual(aData.getFlexValueMeaning());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final RegionModel region = result.getItem(RegionModel.class);
                            region.setCode(aData.getFlexValueMeaning());
                            region.setName(aData.getDescription());
                            region.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(region, _MODIFIED, _MODIFIED_BY, _NAME)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final RegionModel region = new RegionModel();
                            region.setCode(aData.getFlexValueMeaning());
                            region.setName(aData.getDescription());
                            region.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(region).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;
                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }

}
