package id.code.service.facade;

import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.branch.BranchProductModel;
import id.code.service.model.remote.branch.RemoteBranchProductModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName._MODIFIED;
import static id.code.master_data.AliasName._MODIFIED_BY;
import static id.code.service.AliasName.*;

public class BranchProductServiceFacade extends BaseServiceFacade {
    private List<RemoteBranchProductModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteBranchProductModel.class)
                .orderBy(_LOCATION_CODE).asc()
                .orderBy(_CROSS_REFERENCE).asc()
                .limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteBranchProductModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteBranchProductModel> data = this.getData(rows);
                for (final RemoteBranchProductModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(BranchProductModel.class)
                            .where(_BRANCH_CODE).isEqual(aData.getLocationCode())
                            .where(_PRODUCT_CODE).isEqual(aData.getCrossReference());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final BranchProductModel branchProduct = result.getItem(BranchProductModel.class);
                            branchProduct.setBranchCode(aData.getLocationCode());
                            branchProduct.setProductCode(aData.getCrossReference());
                            branchProduct.setType(_INTERNAL);
                            branchProduct.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(branchProduct, _MODIFIED, _MODIFIED_BY)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final BranchProductModel branchProduct = new BranchProductModel();
                            branchProduct.newModel(_INI_SYSTEM_SERVICE);
                            branchProduct.setBranchCode(aData.getLocationCode());
                            branchProduct.setProductCode(aData.getCrossReference());
                            branchProduct.setType(_INTERNAL);
                            final UpdateResult insert = QueryBuilder.insert(branchProduct).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;

                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                    sqlTransaction.beginTransaction();
                }
            }
        }
    }

}
