package id.code.service.facade;

import id.code.apollo.model.promotion.PromotionItemModel;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.product.ProductModel;
import id.code.service.model.remote.promotion.RemotePromotionItemModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName._BRAND;
import static id.code.service.AliasName._CATEGORY_CODE;
import static id.code.service.AliasName._DESCRIPTION;
import static id.code.service.AliasName.*;
import static id.code.service.AliasName._ITEM_CODE;
import static id.code.service.AliasName._PRODUCT_CODE;

public class PromotionItemServiceFacade extends BaseServiceFacade {
    private List<RemotePromotionItemModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        return QueryBuilder.select(_PROMOTION_ITEM, RemotePromotionItemModel.class)
                .includeAllJoin()
                .join(_PRODUCT, ProductModel.class).on(_PROMOTION_ITEM, _BRAND).isEqual(_PRODUCT, _NAME)
                .orderBy(_PROMOTION_ITEM, _BRAND).asc()
                .orderBy(_PROMOTION_ITEM, _KAT_ITEM).asc()
                .orderBy(_PROMOTION_ITEM, _ITEM_CODE).asc()
                .orderBy(_PROMOTION_ITEM, _SEGMENT2).asc()
                .orderBy(_PROMOTION_ITEM, _DESCRIPTION).asc()
                .orderBy(_PROMOTION_ITEM, _UKURAN).asc()
                .limit(100).offset(offset).getResult(super.openRemoteConnection())
                .executeItems(RemotePromotionItemModel.class, (result, promotion) -> {
                    final ProductModel product = result.getItem(_PRODUCT, ProductModel.class);
                    promotion.setBrand(product.getCode());
                });
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemotePromotionItemModel> data = this.getData(rows);
                for (final RemotePromotionItemModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(PromotionItemModel.class)
                            .where(_CODE).isEqual(aData.getItemCode());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final PromotionItemModel promotionItem = result.getItem(PromotionItemModel.class);
                            promotionItem.setCategoryCode(aData.getSegment2());
                            promotionItem.setProductCode(aData.getBrand());
                            promotionItem.setItemSize(aData.getUkuran());
                            promotionItem.setCode(aData.getItemCode());
                            promotionItem.setName(aData.getDescription());
                            promotionItem.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(promotionItem, _MODIFIED, _MODIFIED_BY,
                                    _NAME, _PRODUCT_CODE, _CATEGORY_CODE, _ITEM_SIZE)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final PromotionItemModel promotionItem = new PromotionItemModel();
                            promotionItem.setCategoryCode(aData.getKatItem());
                            promotionItem.setProductCode(aData.getBrand());
                            promotionItem.setItemSize(aData.getUkuran());
                            promotionItem.setCode(aData.getItemCode());
                            promotionItem.setName(aData.getDescription());
                            promotionItem.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(promotionItem).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;

                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }
}
