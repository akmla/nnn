package id.code.service.facade;

import id.code.apollo.model.sio.SioTypePromotionModel;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.service.model.remote.sio.RemoteSioTypePromotionModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName._DESCRIPTION;
import static id.code.service.AliasName.*;
import static id.code.service.AliasName._SIO_TYPE;
import static id.code.service.AliasName._SIO_TYPE_CODE;

public class SioTypePromotionServiceFacade extends BaseServiceFacade {
    private List<RemoteSioTypePromotionModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteSioTypePromotionModel.class)
                .orderBy(_KAT_ITEM_PROMOSI).asc()
                .orderBy(_SIO_TYPE).asc()
                .orderBy(_FLEX_VALUE).asc()
                .orderBy(_DESCRIPTION).asc()
                .limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteSioTypePromotionModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 0;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteSioTypePromotionModel> data = this.getData(rows);
                for (final RemoteSioTypePromotionModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(SioTypePromotionModel.class)
                            .where(_CODE).isEqual(aData.getFlexValue())
                            .where(_SIO_TYPE_CODE).isEqual(aData.getSioType())
                            .where(_CATEGORY_ITEM_CODE).isEqual(aData.getKatItemPromosi());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final SioTypePromotionModel sioPromotionType = result.getItem(SioTypePromotionModel.class);
                            sioPromotionType.setCategoryItemCode(aData.getKatItemPromosi());
                            sioPromotionType.setSioTypeCode(aData.getSioType());
                            sioPromotionType.setCode(aData.getFlexValue());
                            sioPromotionType.setName(aData.getDescription());
                            sioPromotionType.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(sioPromotionType, _MODIFIED, _MODIFIED_BY, _NAME)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final SioTypePromotionModel sioPromotionType = new SioTypePromotionModel();
                            sioPromotionType.setCategoryItemCode(aData.getKatItemPromosi());
                            sioPromotionType.setSioTypeCode(aData.getSioType());
                            sioPromotionType.setCode(aData.getFlexValue());
                            sioPromotionType.setName(aData.getDescription());
                            sioPromotionType.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(sioPromotionType).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
//
//                                // Update Outlet Item Summary
//
//                                // Get All Outlet
//
//                                final List<OutletModel> outlets = QueryBuilder.select(_OUTLET, OutletModel.class)
//                                        .join(_CONTRACT, ContractModel.class).on(_CONTRACT, _SIO_TYPE_CODE).isEqual(_SIO_TYPE, _CODE)
//                                        .join(_SIO_TYPE_PROMOTION, SioTypePromotionModel.class).on(_CONTRACT, _SIO_TYPE_CODE).isEqual(_SIO_TYPE_PROMOTION, _SIO_TYPE_CODE)
//                                        .getResult(sqlTransaction).executeItems(OutletModel.class);
//
//                                for (final OutletModel outlet : outlets) {
//
//                                    // Get All Sio Type Promotions
//
//                                    final List<SioTypePromotionModel> listSioTypePromotions = QueryBuilder.select(_SIO_TYPE_PROMOTION, SioTypePromotionModel.class)
//                                            .join(_SIO_TYPE, SioTypeModel.class).on(_SIO_TYPE, _CODE).isEqual(_SIO_TYPE_PROMOTION, _SIO_TYPE_CODE)
//                                            .join(_CONTRACT, ContractModel.class).on(_CONTRACT, _SIO_TYPE_CODE).isEqual(_SIO_TYPE, _CODE)
//                                            .where(_CONTRACT, _CODE).isEqual(outlet.getContractCode())
//                                            .getResult(sqlTransaction)
//                                            .executeItems(_SIO_TYPE_PROMOTION, SioTypePromotionModel.class);
//
//                                    for (final SioTypePromotionModel sioTypePromotion : listSioTypePromotions) {
//
//                                        final OutletItemSummaryModel outletItemSummary = QueryBuilder.select(OutletItemSummaryModel.class)
//                                                .where(_OUTLET_ID).isEqual(outlet.getId())
//                                                .where(_ITEM_CODE).isEqual(sioTypePromotion.getCategoryItemCode())
//                                                .where(_CATEGORY_CODE).isEqual(sioTypePromotion.getCategoryItemCode())
//                                                .getResult(sqlTransaction)
//                                                .executeItem(OutletItemSummaryModel.class);
//
//                                        if (outletItemSummary == null) {
//                                            outletItemSummary.newModel(outlet.getModifiedBy());
//                                            outletItemSummary.setOutletId(outlet.getId());
//                                            outletItemSummary.setLastItemCondition(_DEFAULT_LAST_CONDITION);
//                                            outletItemSummary.setLastActionToItem(_DEFAULT_ACTION);
//                                            outletItemSummary.setItemCode(sioTypePromotion.getCategoryItemCode());
//                                            outletItemSummary.setCategoryCode(sioTypePromotion.getCategoryItemCode());
//
//                                            final UpdateResult resultOutletItemSummary = QueryBuilder.insert(outletItemSummary).execute(sqlTransaction);
//
//                                            if (!resultOutletItemSummary.isModified()) {
//                                                sqlTransaction.rollbackTransaction();
//                                            }
//                                        }
//                                    }
//                                }
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;

                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                    sqlTransaction.beginTransaction();
                }
            }
        }
    }
}
