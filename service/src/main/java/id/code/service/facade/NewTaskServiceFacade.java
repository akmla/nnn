package id.code.service.facade;

import id.code.apollo.model.schedule.ScheduleOutletModel;
import id.code.apollo.model.summary.TaskSummaryModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.component.utility.DateUtility;
import id.code.component.utility.StringUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.builder.UpdateResult;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName.*;

public class NewTaskServiceFacade extends BaseServiceFacade {
    private Long today;

    public NewTaskServiceFacade() {
        final Properties properties = new Properties();
        try (final FileInputStream inputStream = new FileInputStream("config.properties")) {
            properties.load(inputStream);
            final String todayProperties = properties.getProperty("CURRENT_DATE");
            today = StringUtility.isNullOrWhiteSpace(todayProperties) ? DateUtility.toUnixMillis(DateUtility.toLocalDate(System.currentTimeMillis())) :
                    StringUtility.getLong(todayProperties);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private List<ScheduleOutletModel> getItems(SimpleTransaction sqlTransaction, long scheduleId, long cycleSeq) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(ScheduleOutletModel.class)
                .where(_SCHEDULE_ID).isEqual(scheduleId)
                .where(_CYCLE_SEQ).isEqual(cycleSeq)
                .orderBy(_DAY).asc();

        try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
            return result.getItems(ScheduleOutletModel.class);
        }
    }

    public boolean generateTask() throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {

            final List<TaskModel> listFinishTasks = QueryBuilder.select(TaskModel.class)
                    .where(_FINISH_DATE).isEqual(today)
                    .getResult(sqlTransaction)
                    .executeItems(TaskModel.class);

            for (final TaskModel finishTask : listFinishTasks) {

                final List<TaskItemModel> taskItems = QueryBuilder.select(TaskItemModel.class)
                        .where(_TASK_ID).isEqual(finishTask.getId())
                        .where(_TASK_DATE).greaterThan(today)
                        .getResult(sqlTransaction)
                        .executeItems(TaskItemModel.class);

                for (final TaskItemModel taskItem : taskItems) {
                    taskItem.modify(_MY_APPLICATION);

                    if (!QueryBuilder.delete(taskItem).execute(sqlTransaction).isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }
            }


            final List<TaskModel> listActiveTasks = QueryBuilder.select(TaskModel.class)
                    .where(_START_DATE).equalsLessThan(today + _UNIX_MILLIS_ONE_DAY_VALUE)
                    .where(_FINISH_DATE).greaterThanOr(today, _FINISH_DATE).is(null)
                    .getResult(sqlTransaction)
                    .executeItems(TaskModel.class);

            for (final TaskModel currentTask : listActiveTasks) {
                int currentSeq = 1;

                final TaskSummaryModel taskSummary = QueryBuilder.select(TaskSummaryModel.class)
                        .where(_USER_ID).isEqual(currentTask.getUserId())
                        .limit(1)
                        .getResult(sqlTransaction)
                        .executeItem(TaskSummaryModel.class);

                if (taskSummary != null) {
                    while (currentSeq <= taskSummary.getCycleNum()) {
                        final List<ScheduleOutletModel> listScheduleOutlets = QueryBuilder.select(ScheduleOutletModel.class)
                                .where(_SCHEDULE_ID).isEqual(currentTask.getScheduleId())
                                .where(_CYCLE_SEQ).isEqual(currentSeq)
                                .orderBy(_CYCLE_SEQ).asc()
                                .orderBy(_DAY).asc()
                                .orderBy(_OUTLET_ID).asc()
                                .getResult(sqlTransaction)
                                .executeItems(ScheduleOutletModel.class);

                        for (final ScheduleOutletModel scheduleOutlet : listScheduleOutlets) {
                            final long days = ((currentSeq - 1) + taskSummary.getTotalWeek()) * _DAYS_ONE_WEEK_VALUE;
                            final Long taskDate = (((scheduleOutlet.getDay() - 1) + days) * _DAYS_ONE_DAY_UNIX_MILLIS) + currentTask.getStartDate();

                            final TaskItemModel taskItem = new TaskItemModel();
                            taskItem.newModel(_INI_SYSTEM_SERVICE);
                            taskItem.setTaskId(currentTask.getId());
                            taskItem.setOutletId(scheduleOutlet.getOutletId());
                            taskItem.setStatus(_OPEN);
                            taskItem.setRemark(scheduleOutlet.getRemark());
                            taskItem.setTaskDate(taskDate);

                            final UpdateResult resultInsertTaskItem = QueryBuilder.insert(taskItem).execute(sqlTransaction);

                            if (!resultInsertTaskItem.isModified()) {
                                sqlTransaction.rollbackTransaction();
                                return false;
                            }
                        }
                        currentSeq += 1;
                    }

                    taskSummary.modify(_INI_SYSTEM_SERVICE);
                    taskSummary.setTotalWeek((taskSummary.getScheduleId() == currentTask.getScheduleId()) ? taskSummary.getTotalWeek() + currentTask.getCycleNum() : currentTask.getCycleNum());
                    taskSummary.setScheduleId(currentTask.getScheduleId());
                    taskSummary.setCycleNum(currentTask.getCycleNum());
                    taskSummary.setNextCycleSeq(_FIRST_CYCLE);

                    if (!QueryBuilder.update(taskSummary).execute(sqlTransaction).isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }
            }
            sqlTransaction.commitTransaction();
            return true;
        }
    }
}
