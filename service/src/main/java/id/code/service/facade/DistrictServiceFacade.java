package id.code.service.facade;

import id.code.component.utility.StringUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.district.DistrictModel;
import id.code.service.model.remote.district.RemoteDistrictModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName._BRANCH_CODE;
import static id.code.master_data.AliasName._CITY_CODE;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName.*;

/**
 * Created by CODE.ID on 8/22/2017.
 */
public class DistrictServiceFacade extends BaseServiceFacade {
    private List<RemoteDistrictModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteDistrictModel.class)
                .orderBy(_KABUPATEN_CODE).asc()
                .orderBy(_KECAMATAN_CODE).asc()
                .limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteDistrictModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteDistrictModel> data = this.getData(rows);
                for (final RemoteDistrictModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(DistrictModel.class)
                            .where(_BRANCH_CODE).isEqual(aData.getBranchCode())
                            .where(_CITY_CODE).isEqual(aData.getKabupatenCode())
                            .where(_CODE).isEqual(StringUtility.trimNotNull(aData.getKabupatenCode()) +
                                    StringUtility.trimNotNull(aData.getKecamatanCode()));

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final DistrictModel district = result.getItem(DistrictModel.class);
                            district.setBranchCode(aData.getBranchCode());
                            district.setCode(StringUtility.trimNotNull(aData.getKabupatenCode()) +
                                    StringUtility.trimNotNull(aData.getKecamatanCode()));
                            district.setName(aData.getKecamatanDescription() == null ? "-" : aData.getKecamatanDescription());
                            district.setCityCode(aData.getKabupatenCode());
                            district.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(district, _MODIFIED, _MODIFIED_BY, _NAME)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final DistrictModel district = new DistrictModel();
                            district.setBranchCode(aData.getBranchCode());
                            district.setCode(StringUtility.trimNotNull(aData.getKabupatenCode()) +
                                    StringUtility.trimNotNull(aData.getKecamatanCode()));
                            district.setName(aData.getKecamatanDescription() == null ? "-" : aData.getKecamatanDescription());
                            district.setCityCode(aData.getKabupatenCode());
                            district.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(district).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;

                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }
}
