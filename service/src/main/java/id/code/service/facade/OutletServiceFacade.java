package id.code.service.facade;

import id.code.apollo.model.contract.ContractHistoryModel;
import id.code.apollo.model.contract.ContractItemModel;
import id.code.apollo.model.contract.ContractModel;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.sio.SioTypeModel;
import id.code.apollo.model.sio.SioTypePromotionModel;
import id.code.apollo.model.summary.OutletItemSummaryModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.component.utility.DateUtility;
import id.code.component.utility.StringUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.QueryExecutionException;
import id.code.database.builder.UpdateResult;
import id.code.master_data.AliasName;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.channel.ChannelViewModel;
import id.code.master_data.model.city.CityViewModel;
import id.code.master_data.model.district.DistrictViewModel;
import id.code.master_data.model.region.RegionViewModel;
import id.code.service.model.remote.outlet.RemoteOutletModel;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName._BRANCH_CODE;
import static id.code.service.AliasName.*;
import static id.code.service.AliasName._CITY_CODE;
import static id.code.service.AliasName._REGION_CODE;
import static id.code.service.AliasName._SIO_TYPE;
import static id.code.service.AliasName._SIO_TYPE_CODE;

public class OutletServiceFacade extends BaseServiceFacade {
    private static final String _ALIAS_OUTLET = "O";

    private List<RemoteOutletModel> getData(SimpleTransaction transaction, long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        return QueryBuilder.select(_ALIAS_OUTLET, RemoteOutletModel.class)
                .includeAllJoin()
                .leftJoin(_BRANCH, BranchViewModel.class).on(_BRANCH, _NAME).isEqual(_ALIAS_OUTLET, _CABANG)
                .leftJoin(_CITY, CityViewModel.class).on(_CITY, _BRANCH_CODE).isEqualChain(_BRANCH, _CODE)
                .and(_CITY, _NAME).isEqual(_ALIAS_OUTLET, _KABUPATEN)
                .leftJoin(_DISTRICT, DistrictViewModel.class).on(_DISTRICT, _NAME).isEqualChain(_ALIAS_OUTLET, _KECAMATAN)
                .and(_BRANCH, _CODE).isEqualChain(_DISTRICT, _BRANCH_CODE)
                .and(_CITY, _CODE).isEqual(_DISTRICT, _CITY_CODE)
                .orderBy(_ALIAS_OUTLET, _ID_CUSTOMER).asc()
                .limit(100).offset(offset)
                .getResult(transaction)
                .executeItems(RemoteOutletModel.class, (resultOutlet, remoteOutlet) -> {
                    remoteOutlet.setBranchCode(resultOutlet.getItem(BranchViewModel.class).getCode());
                    remoteOutlet.setDistrictCode(resultOutlet.getItem(DistrictViewModel.class).getCode());
                });
    }

    public void doBackupOutlet() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException, ParseException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(_FORMAT_DATE_STAGING);
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteOutletModel> data = this.getData(sqlTransaction, rows);
                for (final RemoteOutletModel aData : data) {
                    lastRow += 1;
                    try {
                        final Long startDate = simpleDateFormat.parse(aData.getStartDate()).toInstant().toEpochMilli();
                        final Long finishDate = simpleDateFormat.parse(aData.getFinishDate()).toInstant().toEpochMilli();
                        final OutletModel outlet = new OutletModel();
                        outlet.setCode(aData.getIdCustomer());
                        outlet.newModel(_INI_SYSTEM_SERVICE);
                        outlet.setChannelCode(aData.getChannelOutlet());
                        outlet.setDistrictCode(aData.getDistrictCode()); // need district code
                        outlet.setBranchCode(aData.getBranchCode()); // need branch code
                        outlet.setContractCode(aData.getNoKontrak());
                        outlet.setName(aData.getNama());
                        outlet.setDimension(aData.getLebarToko());
                        outlet.setAddress(aData.getAlamat());
                        outlet.setArea(aData.getKawasan());
                        outlet.setPhone(aData.getNoTelp());
                        outlet.setWsBns(aData.getWsBns());
                        outlet.setLokasi(aData.getLokasi());
                        outlet.setPosisi(aData.getPosisi());
                        outlet.setJumlahSku(aData.getJumlahSku());
                        outlet.setJumlahStock(aData.getJumlahStock());
                        outlet.setVerified(_STATUS_NOT_TAGGED);
                        outlet.setStatus(_STATUS_TRUE);

                        final UpdateResult insert = QueryBuilder.insert(outlet).execute(sqlTransaction);
                        if (insert.isModified()) {
                            inserted += 1;
                        }

                        final OutletSummaryModel outletSummary = new OutletSummaryModel();
                        outletSummary.newModel(outlet.getCreatedBy());
                        outletSummary.setOutletId(outlet.getId());

                        QueryBuilder.select(_DISTRICT, DistrictViewModel.class)
                                .includeAllJoin()
                                .join(_BRANCH, BranchViewModel.class).on(_DISTRICT, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                                .join(_CITY, CityViewModel.class).on(_DISTRICT, _CITY_CODE).isEqual(_CITY, _CODE)
                                .join(_REGION, RegionViewModel.class).on(_BRANCH, _REGION_CODE).isEqual(_REGION, _CODE)
                                .where(_DISTRICT, _CODE).isEqual(outlet.getDistrictCode())
                                .getResult(sqlTransaction)
                                .executeItem(BranchViewModel.class, (resultLocation, branch) -> {
                                    final CityViewModel city = resultLocation.getItem(_CITY, CityViewModel.class);
                                    final DistrictViewModel district = resultLocation.getItem(_DISTRICT, DistrictViewModel.class);
                                    final RegionViewModel region = resultLocation.getItem(_REGION, RegionViewModel.class);
                                    outletSummary.setBranchCode(branch.getCode());
                                    outletSummary.setBranchName(branch.getName());
                                    outletSummary.setDistrictCode(district.getCode());
                                    outletSummary.setDistrictName(district.getName());
                                    outletSummary.setRegionCode(region.getCode());
                                    outletSummary.setRegionName(region.getName());
                                    outletSummary.setCityCode(city.getCode());
                                    outletSummary.setCityName(city.getName());
                                });


                        final ChannelViewModel channel = QueryBuilder.select(ChannelViewModel.class)
                                .where(_CODE).isEqual(outlet.getChannelCode())
                                .getResult(sqlTransaction).executeItem(ChannelViewModel.class);

                        outletSummary.setChannelCode(channel.getCode());
                        outletSummary.setChannelName(channel.getName());
                        outletSummary.modify(_MY_APPLICATION);

                        if (!StringUtility.isNullOrWhiteSpace(outlet.getContractCode())) {
                            QueryBuilder.select(AliasName._SIO_TYPE, SioTypeModel.class)
                                    .join(_CONTRACT, ContractModel.class).on(_CONTRACT, AliasName._SIO_TYPE_CODE).isEqual(AliasName._SIO_TYPE, _CODE)
                                    .where(_CONTRACT, _CODE).isEqual(outlet.getContractCode()).getResult(sqlTransaction)
                                    .executeItems(SioTypeModel.class, (result, sioType) -> {
                                        outletSummary.setSioTypeCode(sioType.getCode());
                                        outletSummary.setSioTypeName(sioType.getName());
                                    });
                        }

                        if (!QueryBuilder.insert(outletSummary).execute(sqlTransaction).isModified()) {
                            return;
                        }

                        final ContractHistoryModel historyNewContract = new ContractHistoryModel();
                        historyNewContract.newModel(outlet.getModifiedBy());
                        historyNewContract.setStartDate(startDate);
                        historyNewContract.setFinishDate(finishDate);
                        historyNewContract.setContractCode(outlet.getContractCode());
                        historyNewContract.setOutletId(outlet.getId());

                        if (!QueryBuilder.insert(historyNewContract).execute(sqlTransaction).isModified()) {
                            return;
                        }

                        final List<SioTypePromotionModel> listSioTypePromotions = QueryBuilder.select(_SIO_TYPE_PROMOTION, SioTypePromotionModel.class)
                                .join(_SIO_TYPE, SioTypeModel.class).on(_SIO_TYPE, _CODE).isEqual(_SIO_TYPE_PROMOTION, _SIO_TYPE_CODE)
                                .join(_CONTRACT, ContractModel.class).on(_CONTRACT, _SIO_TYPE_CODE).isEqual(_SIO_TYPE, _CODE)
                                .where(_CONTRACT, _CODE).isEqual(outlet.getContractCode())
                                .getResult(sqlTransaction)
                                .executeItems(_SIO_TYPE_PROMOTION, SioTypePromotionModel.class);

                        for (final SioTypePromotionModel sioTypePromotion : listSioTypePromotions) {
                            final OutletItemSummaryModel outletItemSummary = new OutletItemSummaryModel();
                            outletItemSummary.newModel(outlet.getModifiedBy());
                            outletItemSummary.setOutletId(outlet.getId());
                            outletItemSummary.setLastItemCondition(_DEFAULT_LAST_CONDITION);
                            outletItemSummary.setLastActionToItem(_DEFAULT_ACTION);
                            outletItemSummary.setItemCode(sioTypePromotion.getCategoryItemCode());
                            outletItemSummary.setCategoryCode(sioTypePromotion.getCategoryItemCode());

                            if (!QueryBuilder.insert(outletItemSummary).execute(sqlTransaction).isModified()) {
                                return;
                            }
                        }
//                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;

                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }

    public void doBackupContract() throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(_FORMAT_DATE_STAGING);
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteOutletModel> data = this.getData(sqlTransaction, rows);
                for (final RemoteOutletModel aData : data) {
                    lastRow += 1;

                    try {
                        final Long startDate = simpleDateFormat.parse(aData.getStartDate()).toInstant().toEpochMilli();
                        final Long finishDate = simpleDateFormat.parse(aData.getFinishDate()).toInstant().toEpochMilli();
                        final ContractModel contract = new ContractModel();
                        contract.newModel(_INI_SYSTEM_SERVICE);
                        contract.setSioTypeCode(aData.getJenisInvestasi());
                        contract.setBrandCode(aData.getBrand());
                        contract.setCode(aData.getNoKontrak());
                        contract.setRealCode(aData.getNoKontrak());
                        contract.setInsentif(aData.getInsentif());
                        contract.setPajak(aData.getPajak());
                        contract.setProduksi(aData.getProduksiDanPasang());
                        contract.setMaintenance(aData.getMaintenance());
                        contract.setStartDate(startDate);
                        contract.setFinishDate(finishDate);
                        contract.setNamaPemilik(aData.getNama());
                        contract.setIjinDanKoordinasi(aData.getBiayaIjinDanKoordinasi());
                        contract.setRelokasi(aData.getBiayaRelokasi());
                        contract.setContractYear(DateUtility.toLocalDate(startDate).getYear());
                        contract.setIsUsed(_STATUS_TRUE);
                        contract.setStatus(_STATUS_ACTIVE);
                        contract.setTop(aData.getTop());

                        // NEEDED BUT NOT AVAILABLE

                        contract.setNoKtp(!(StringUtility.isNullOrWhiteSpace(aData.getNoKtp())) ? aData.getNoKtp() : _DEFAULT_NULL_STRING);
                        contract.setTotal(_DEFAULT_LONG);
                        contract.setPaymentAmount(_DEFAULT_DOUBLE);
                        contract.setInstallment(_DEFAULT_DOUBLE);

                        final UpdateResult insert = QueryBuilder.insert(contract).execute(sqlTransaction);
                        if (insert.isModified()) {
                            inserted += 1;
                        }

                        final ContractItemModel contractItem = new ContractItemModel();
                        contractItem.setContractCode(contract.getCode());
                        LocalDate date = LocalDate.parse(aData.getStartDate(), DateTimeFormatter.ofPattern(_FORMAT_DATE_STAGING));

                        for (int i = 1; i <= aData.getTop(); i++) {
                            // TOP 3
                            // 1 01-01-2018
                            // 2 01-05-2018
                            // 3 01-09-2018

                            contractItem.setSeq(i);
                            contractItem.setDueDate(DateUtility.toUnixMillis(date));
                            contractItem.setStatus(_UNPAID);
                            contractItem.setInsentif(aData.getInsentif());
                            contractItem.newModel(_INI_SYSTEM_SERVICE);
                            if (!QueryBuilder.insert(contractItem).execute(sqlTransaction).isModified()) {
                                return;
                            }
                            date = date.plusMonths(12 / i);
                        }


                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;

                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }
}
