package id.code.service.facade;

import id.code.component.utility.StringUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.master_data.model.channel.ChannelModel;
import id.code.service.model.remote.channel.RemoteChannelModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.component.AppLogger.writeError;
import static id.code.component.AppLogger.writeInfo;
import static id.code.master_data.AliasName.*;
import static id.code.service.AliasName._INI_SYSTEM_SERVICE;
import static id.code.service.AliasName._LOOKUP_CODE;

/**
 * Created by CODE.ID on 8/22/2017.
 */
public class ChannelServiceFacade extends BaseServiceFacade {

    private List<RemoteChannelModel> getData(long offset) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(RemoteChannelModel.class)
                .orderBy(_LOOKUP_CODE).asc()
                .limit(100).offset(offset);
        try (ResultBuilder result = sqlSelect.execute(super.openRemoteConnection())) {
            return result.getItems(RemoteChannelModel.class);
        }
    }

    public void doBackup() throws SQLException, IllegalAccessException, QueryBuilderException, InstantiationException {
        try (final SimpleTransaction sqlTransaction = super.openLocalTransaction()) {
            boolean isDone = false;
            int lastRow = 1;
            long rows = 1;
            long inserted = 0;
            long updated = 0;
            long failed = 0;
            while (!isDone) {
                final List<RemoteChannelModel> data = this.getData(rows);
                for (final RemoteChannelModel aData : data) {
                    lastRow += 1;

                    final SelectBuilder sqlSelect = QueryBuilder.select(ChannelModel.class)
                            .where(_CODE).isEqual(aData.getLookupCode());

                    try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
                        if (result.moveNext()) {
                            final ChannelModel channel = result.getItem(ChannelModel.class);
                            channel.setCode(StringUtility.trimNotNull(aData.getLookupCode()).toUpperCase());
                            channel.setName(StringUtility.trimNotNull(aData.getMeaning()).toUpperCase());
                            channel.modify(_INI_SYSTEM_SERVICE);
                            final UpdateResult update = QueryBuilder.update(channel, _MODIFIED, _MODIFIED_BY, _NAME)
                                    .execute(sqlTransaction);
                            if (update.isModified()) {
                                updated += 1;
                            }
                        } else {
                            final ChannelModel channel = new ChannelModel();
                            channel.setCode(StringUtility.trimNotNull(aData.getLookupCode()).toUpperCase());
                            channel.setName(StringUtility.trimNotNull(aData.getMeaning()).toUpperCase());
                            channel.newModel(_INI_SYSTEM_SERVICE);
                            final UpdateResult insert = QueryBuilder.insert(channel).execute(sqlTransaction);
                            if (insert.isModified()) {
                                inserted += 1;
                            }
                        }
                    } catch (Exception e) {
                        failed += 1;
                        if (e instanceof QueryExecutionException) {
                            QueryExecutionException err = ((QueryExecutionException) e);
                            err.getQuery();
                            err.getParameters();
                            writeError("Query : " + err.getQuery() +
                                    "\nValue : " + err.getParameters().toString() +
                                    "\nMessage : " + e.getMessage(), null);
                        } else {
                            throw e;
                        }
                        break;
                    }
                }

                if (lastRow == 100)
                    rows += 100;
                else
                    rows = lastRow;
                if (data.size() < 1) {
                    isDone = true;
                    sqlTransaction.commitTransaction();
                    writeInfo("Inserted :" + inserted + " record.");
                    writeInfo("Updated :" + updated + " record.");
                    writeInfo("Failed :" + failed + " record.");
                }
            }
        }
    }
}
