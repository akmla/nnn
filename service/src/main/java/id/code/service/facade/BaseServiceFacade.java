package id.code.service.facade;

import id.code.component.DatabasePool;
import id.code.database.SimpleTransaction;
import id.code.master_data.facade.BaseFacade;
import id.code.service.Main;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by CODE.ID on 8/22/2017.
 */
public class BaseServiceFacade extends BaseFacade {

    private Connection openLocalConnection() throws SQLException {
        return DatabasePool.getDataSource().getConnection();
    }

    Connection openRemoteConnection() throws SQLException {
        return Main.getDataSourceRemote().getConnection();
    }

    SimpleTransaction openLocalTransaction() throws SQLException {
        return new SimpleTransaction(this.openLocalConnection());
    }

    public SimpleTransaction openRemoteTransaction() throws SQLException {
        return new SimpleTransaction(this.openRemoteConnection());
    }

}
