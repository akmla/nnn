package id.code.service;

/**
 * Created by CODE.ID on 8/22/2017.
 */
public class AliasName {

    public static final String _INI_SYSTEM_SERVICE = "Service Apollo 1.0";

    public static final String _BRANCH_CODE = "BRANCH_CODE";
    public static final String _BRAND_CODE = "BRAND_CODE";
    public static final String _CITY_CODE = "CITY_CODE";
    public static final String _CITY_NAME = "CITY_NAME";
    public static final String _EVENT_CODE = "EVENT_CODE";
    public static final String _LOCATION_CODE = "LOCATION_CODE";
    public static final String _PACKAGE_CODE = "PACKAGE_CODE";
    public static final String _PRODUCT_CODE = "PRODUCT_CODE";
    public static final String _REGION_CODE = "REGION_CODE";

    // REMOTE

    public static final String _VIEW_NAME_BRANCHES = "NNA_BRANCH_V";
    public static final String _VIEW_NAME_BRANCH_PRODUCTS = "NNA_BRANCH_ROKOK_V";
    public static final String _VIEW_NAME_BRANDS = "NNA_INV_KATEGORI_ROKOK_V";
    public static final String _VIEW_NAME_CHANNELS = "NNA_CHANNEL_V";
    public static final String _VIEW_NAME_CITIES = "NNA_KABUPATEN_V";
    public static final String _VIEW_NAME_DISTRICTS = "NNA_KECAMATAN_V";
    public static final String _TABLE_NAME_STAGING_OUTLETS = "STAGING_OUTLETS";
    public static final String _VIEW_NAME_POS = "NNA_POS_V";
    public static final String _VIEW_NAME_PRODUCTS = "NNA_JENIS_ROKOK_V";
    public static final String _VIEW_NAME_PRODUCTS_EXTERNAL = "NNA_ROKOK_EXTERNAL_V";
    public static final String _VIEW_NAME_PROVINCES = "NNA_PROVINCE_V";
    public static final String _VIEW_NAME_REGIONS = "NNA_REGIONAL_V";
    public static final String _VIEW_NAME_SUB_BRANCHES = "NNA_SUBBRANCH_V";
    public static final String _VIEW_NAME_USERS = "NNA_USER_APOLLO_V";

    // Apollo

    public static final String _VIEW_NAME_CATEGORY_PROMOTION_ITEMS = "NNA_KATEGORI_ITEM_PROMOSI_V";
    public static final String _VIEW_NAME_PROMOTION_ITEMS = "NNA_PROMOTION_ITEM_V";
    public static final String _VIEW_NAME_SIO_PROMOTION_TYPES = "NNA_SIO_PROMOTION_TYPE_V";
    public static final String _VIEW_NAME_SIO_TYPES = "NNA_SIO_TYPE_V";
    public static final String _VIEW_NAME_THEMATICS = "NNA_THEMATIC_NAME_V";

    // Channels

    public static final String _LOOKUP_CODE = "LOOKUP_CODE";
    public static final String _MEANING = "MEANING";

    // Provinces

    public static final String _PROVINCE_CODE = "PROVINCE_CODE";
    public static final String _PROVINCE_NAME = "PROVINCE_NAME";

    // Cities / Kabupaten

    public static final String _KABUPATEN_CODE = "KABUPATEN_CODE";
    public static final String _KABUPATEN_KODE_BPS = "KABUPATEN_KODE_BPS";
    public static final String _KABUPATEN_DESCRIPTION = "KABUPATEN_DESCRIPTION";

    // Districts / Kecamatan

    public static final String _KECAMATAN_CODE = "KECAMATAN_CODE";
    public static final String _KECAMATAN_DESCRIPTION = "KECAMATAN_DESCRIPTION";

    // Branches

    public static final String _DESCRIPTION = "DESCRIPTION";
    public static final String _REGIONAL = "REGIONAL";
    public static final String _CABANG = "CABANG";

    // Brand

    public static final String _FLEX_VALUE_MEANING = "FLEX_VALUE_MEANING";
    public static final String _INTERNAL = "INTERNAL";
    public static final String _PRINCIPAL = "PRINCIPAL";

    // Product

    public static final String _CROSS_REFERENCE = "CROSS_REFERENCE";
    public static final String _KATEGORI_ROKOK = "KATEGORI_ROKOK";
    public static final String _PRODUCT_DESCRIPTION = "PRODUCT_DESCRIPTION";
    public static final String _PRODUCT_GROUP = "PRODUCT_GROUP";

    // Category Promotion Item

    public static final String _BRAND = "BRAND";
    public static final String _ITEM_CODE = "ITEM_CODE";
    public static final String _KAT_ITEM = "KAT_ITEM";
    public static final String _SEGMENT2 = "SEGMENT2";
    public static final String _UKURAN = "UKURAN";

    // Sio Promotion Type

    public static final String _SIO_TYPE = "SIO_TYPE";
    public static final String _FLEX_VALUE = "FLEX_VALUE";
    public static final String _KAT_ITEM_PROMOSI = "KAT_ITEM_PROMOSI";

    // Promotion Item Sio

    public static final String _CATEGORY_CODE = "CATEGORY_CODE";
    public static final String _CATEGORY_PROMOTION_ITEM_CODE = "CATEGORY_PROMOTION_ITEM_CODE";
    public static final String _PROMOTION_ITEM_CODE = "PROMOTION_ITEM_CODE";
    public static final String _SIO_TYPE_CODE = "SIO_TYPE_CODE";

    // User / Employee

    public static final String _EMPLOYEE_ID = "EMPLOYEE_ID";
    public static final String _EMPLOYEE_NAME = "EMPLOYEE_NAME";
    public static final String _EMPLOYEE_STATUS = "EMPLOYEE_STATUS";
    public static final String _POSITION_TITLE = "POSITION_TITLE";
    public static final String _CONTACT_VALUE = "CONTACT_VALUE";
    public static final String _ADDRESS_ID = "ADDRESS_ID";
    public static final String _ABSENCE_CARD_NO = "ABSENCE_CARD_NO";

    public static final String _MERCHANDISER = "MERCHANDISER";
    public static final String _POSITION_TITLE_MERCHANDISER = "PROMOTION MERCHANDISER";
    public static final String _POSITION_TITLE_TEAM_LEADER = "PROMOTION TEAM LEADER";
    public static final String _POSITION_TITLE_SUPERVISOR = "PROMOTION SUPERVISOR";

    // Outlet

    public static final String _ID_CUSTOMER = "ID_CUSTOMER";
    public static final String _CHANNEL_OUTLET = "CHANNEL_OUTLET";
    public static final String _JENIS_INVESTASI = "JENIS_INVESTASI";
    public static final String _KABUPATEN = "KABUPATEN";
    public static final String _NAMA = "NAMA";
    public static final String _ALAMAT = "ALAMAT";
    public static final String _LEBAR_TOKO = "LEBAR_TOKO";
    public static final String _NO_KONTRAK = "NO_KONTRAK";
    public static final String _BIAYA_IJIN_DAN_KOORDINASI = "BIAYA_IJIN_DAN_KOORDINASI";
    public static final String _BIAYA_RELOKASI = "BIAYA_RELOKASI";
    public static final String _PRODUKSI_DAN_PASANG = "PRODUKSI_DAN_PASANG";

    // Param Value

    public static final String _OPEN = "OPEN";

    public static final long _DAYS_ONE_DAY_UNIX_MILLIS = 86400000;
    public static final long _DAYS_ONE_DAY_VALUE = 1;

    // Default Value

    public static final int _DEFAULT_INT = 0;
    public static final long _DEFAULT_LONG = 0;
    public static final double _DEFAULT_DOUBLE = 0;
    public static final int _DEFAULT_TOP = 0;
    public static final String _DEFAULT_EMPTY = "-";
    public static final String _DEFAULT_GENDER = "-";
    public static final String _DEFAULT_PHONE = "0123456789";
    public static final String _DEFAULT_NULL_STRING = "UNDEFINED";
    public static final String _FORMAT_DATE_STAGING = "d-MMM-yy";
    public static final String _SUFFIX_EMAIL = "@LIMAMAIL.NET";
}