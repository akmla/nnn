package id.code.service;

import com.zaxxer.hikari.HikariDataSource;
import id.code.component.AppLogger;
import id.code.component.DefaultComponent;
import id.code.database.DefaultConverter;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.service.facade.TaskServiceFacade;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by CODE.ID on 8/22/2017.
 */
public class Main {
    private static Properties properties;
    private static HikariDataSource dataRemote;

    public static void main(String[] args) throws IOException, IllegalAccessException, SQLException, InstantiationException, QueryBuilderException {
        try {
            readProperties();

            DefaultComponent.initialize(properties);
            QueryBuilder.initialize(properties, new DefaultConverter());

//            // Set Remote Connection
//            HikariConfig configHikari = new HikariConfig();
//            configHikari.setUsername(properties.getProperty("dbRemote.user"));
//            configHikari.setPassword(properties.getProperty("dbRemote.password"));
//            configHikari.setJdbcUrl(properties.getProperty("dbRemote.url"));
//            configHikari.addDataSourceProperty("cachePrepStmts", "true");
//            configHikari.addDataSourceProperty("prepStmtCacheSize", "250");
//            configHikari.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
//            dataRemote = new HikariDataSource(configHikari);


            AppLogger.writeInfo("=========================================== Start ===========================================");

//            // sync
//            final PosServiceFacade posServiceFacade = new PosServiceFacade();
//            AppLogger.writeInfo("Sync Pos");
//            posServiceFacade.doBackup();
//
//            final RegionServiceFacade regionServiceFacade = new RegionServiceFacade();
//            AppLogger.writeInfo("Sync Regions");
//            regionServiceFacade.doBackup();
//
//            final BranchServiceFacade branchServiceFacade = new BranchServiceFacade();
//            AppLogger.writeInfo("Sync Branches");
//            branchServiceFacade.doBackup();
//
//            final SubBranchServiceFacade subBranchServiceFacade = new SubBranchServiceFacade();
//            AppLogger.writeInfo("Sync Sub Branches");
//            subBranchServiceFacade.doBackup();
//
//            final BrandServiceFacade brandServiceFacade = new BrandServiceFacade();
//            AppLogger.writeInfo("Sync Brands");
//            brandServiceFacade.doBackup();
//
//            final ChannelServiceFacade channelServiceFacade = new ChannelServiceFacade();
//            AppLogger.writeInfo("Sync Channels");
//            channelServiceFacade.doBackup();
//
//            final ProvinceServiceFacade provinceServiceFacade = new ProvinceServiceFacade();
//            AppLogger.writeInfo("Sync Provinces");
//            provinceServiceFacade.doBackup();
//
//            final CityServiceFacade cityServiceFacade = new CityServiceFacade();
//            AppLogger.writeInfo("Sync Cities");
//            cityServiceFacade.doBackup();
//
//            final DistrictServiceFacade districtServiceFacade = new DistrictServiceFacade();
//            AppLogger.writeInfo("Sync Districts");
//            districtServiceFacade.doBackup();
//
//            final ProductServiceFacade productServiceFacade = new ProductServiceFacade();
//            AppLogger.writeInfo("Sync Products Internal");
//            productServiceFacade.doBackupInternal();
//            AppLogger.writeInfo("Sync Products External");
//            productServiceFacade.doBackupExternal();
//
//            final BranchProductServiceFacade branchProductServiceFacade = new BranchProductServiceFacade();
//            AppLogger.writeInfo("Sync Branch Products");
//            branchProductServiceFacade.doBackup();
//
//            final ThematicFacadeService thematicFacade = new ThematicFacadeService();
//            AppLogger.writeInfo("Sync Thematics");
//            thematicFacade.doBackup();
//
//            final SioTypeServiceFacade sioTypeServiceFacade = new SioTypeServiceFacade();
//            AppLogger.writeInfo("Sync Sio Types");
//            sioTypeServiceFacade.doBackup();

//            final SioTypePromotionServiceFacade sioTypePromotionServiceFacade = new SioTypePromotionServiceFacade();
//            AppLogger.writeInfo("Sync Sio Type Promotions");
//            sioTypePromotionServiceFacade.doBackup();

//            final CategoryPromotionItemServiceFacade categoryPromotionItemServiceFacade = new CategoryPromotionItemServiceFacade();
//            AppLogger.writeInfo("Sync Category Promotion Items");
//            categoryPromotionItemServiceFacade.doBackup();
//
//            final PromotionItemServiceFacade promotionItemServiceFacade = new PromotionItemServiceFacade();
//            AppLogger.writeInfo("Sync Promotion Items");
//            promotionItemServiceFacade.doBackup();
//
//            final UserServiceFacade userServiceFacade = new UserServiceFacade();
//            AppLogger.writeInfo("Sync Erp Users");
//            userServiceFacade.doBackup();

            // staging
//            final OutletServiceFacade outletServiceFacade = new OutletServiceFacade();
//            AppLogger.writeInfo("Sync Contracts");
//            outletServiceFacade.doBackupContract();
//            AppLogger.writeInfo("Sync Outlets");
//            outletServiceFacade.doBackupOutlet();

            AppLogger.writeInfo("=========================================== Finish ===========================================");

            AppLogger.writeInfo("Generate Next Task...");

            // ONE CYCLE
            final TaskServiceFacade taskServiceFacade = new TaskServiceFacade();
            taskServiceFacade.generateNewTask();

            // FULL CYCLE
//            final NewTaskServiceFacade newTaskServiceFacade = new NewTaskServiceFacade();
//            newTaskServiceFacade.generateTask();

            AppLogger.writeInfo("Generate Task Finished!");
        } catch (Exception ex) {
            AppLogger.writeError(ex.getMessage(), ex);
        }
    }

    private static void readProperties() throws IOException {
        properties = new Properties();
        try (final FileInputStream inputStream = new FileInputStream("config.properties")) {
            properties.load(inputStream);
        } catch (Exception ex) {
            System.out.println("No file config !!");
        }
    }

    public static synchronized DataSource getDataSourceRemote() throws SQLException {
        return dataRemote;
    }
}
