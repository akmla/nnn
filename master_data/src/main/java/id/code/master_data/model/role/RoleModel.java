package id.code.master_data.model.role;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;

import java.util.List;

import static id.code.master_data.AliasName._NAME;
import static id.code.master_data.AliasName._TABLE_NAME_MODULES;

@Table(name = AliasName._TABLE_NAME_ROLES)
public class RoleModel extends BaseModel {
	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	private String name;

	@JsonProperty(value = _TABLE_NAME_MODULES)
	private List<String> modules;

	public String getName() {
		return name;
	}
    public List<String> getModules() {
        return modules;
    }

	public void setName(String name) {
		this.name = name;
	}
    public void setModules(List<String> modules) {
        this.modules = modules;
    }
}