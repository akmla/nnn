package id.code.master_data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class BaseModel extends IdModel {

    @TableColumn(name = _MODIFIED)
    @JsonProperty(value = _MODIFIED)
    private long modified;

    @TableColumn(name = _CREATED)
    @JsonProperty(value = _CREATED)
    private long created;

    @TableColumn(name = _CREATED_BY)
    @JsonProperty(value = _CREATED_BY)
    private String createdBy;

    @TableColumn(name = _MODIFIED_BY)
    @JsonProperty(value = _MODIFIED_BY)
    private String modifiedBy;

    public long getCreated() { return this.created; }
    public long getModified() { return this.modified; }
    public String getCreatedBy() { return this.createdBy; }
    public String getModifiedBy() { return this.modifiedBy; }

    public void setCreated(long created) { this.created = created; }
    public void setModified(long modified) { this.modified = modified; }
    public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
    public void setModifiedBy(String modifiedBy) { this.modifiedBy = modifiedBy; }

    protected BaseModel() { }
    public BaseModel(long id) { super(id); }

    public void newModel(String createdBy) {
        this.modified = System.currentTimeMillis();
        this.created = this.modified;
        this.createdBy = createdBy;
        this.modifiedBy = this.createdBy;
    }

    public void modify(String modifiedBy) {
        this.modified = System.currentTimeMillis();
        this.modifiedBy = modifiedBy;
    }

}
