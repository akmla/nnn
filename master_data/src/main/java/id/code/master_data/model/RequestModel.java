package id.code.master_data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

import static id.code.master_data.AliasName._REQUEST_ID;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class RequestModel {

    @ValidateColumn(name = _REQUEST_ID)
    @JsonProperty(_REQUEST_ID)
    private long requestId;

    public void setRequestId(long requestId ) { this.requestId = requestId; }
    public long getRequestId() { return this.requestId; }
}
