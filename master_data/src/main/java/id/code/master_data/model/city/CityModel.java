package id.code.master_data.model.city;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.province.ProvinceViewModel;

import static id.code.master_data.AliasName.*;

@Table(name = AliasName._TABLE_NAME_CITIES)
public class CityModel extends BaseModel {

	@TableColumn(name = _BRANCH_CODE)
	@JsonProperty(value = _BRANCH_CODE)
	@ValidateColumn(name =_BRANCH_CODE)
	private String branchCode;

	@TableColumn(name = _CODE)
	@JsonProperty(value = _CODE)
	@ValidateColumn(name = _CODE)
	private String code;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	@ValidateColumn(name = _NAME)
	private String name;

	@TableColumn(name = _PROVINCE_CODE)
	@JsonProperty(value = _PROVINCE_CODE)
	@ValidateColumn(name = _PROVINCE_CODE)
	private String provinceCode;

	@JsonProperty(value = _PROVINCE)
	private ProvinceViewModel province;

	@JsonProperty(value = _BRANCH)
	private BranchViewModel branch;

	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public BranchViewModel getBranch() {
		return branch;
	}
	public ProvinceViewModel getProvince() {
		return province;
	}



	public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = name;
    }
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }
	public void setBranch(BranchViewModel branch) {
		this.branch = branch;
	}
	public void setProvince(ProvinceViewModel province) {
		this.province = province;
	}
}