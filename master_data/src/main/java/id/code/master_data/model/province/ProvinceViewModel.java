package id.code.master_data.model.province;

import id.code.database.builder.annotation.Table;
import id.code.master_data.AliasName;
import id.code.master_data.model.ViewCodeNameModel;

/**
 * Created by CODE.ID on 8/20/2017.
 */
@Table(name = AliasName._TABLE_NAME_PROVINCES)
public class ProvinceViewModel extends ViewCodeNameModel {
}
