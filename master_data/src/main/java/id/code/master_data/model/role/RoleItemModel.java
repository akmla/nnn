package id.code.master_data.model.role;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.module.ModuleModel;

import java.util.List;

import static id.code.master_data.AliasName.*;

@Table(name = AliasName._TABLE_NAME_ROLE_ITEMS)
public class RoleItemModel extends BaseModel {

	@TableColumn(name = _ROLE_ID)
	@JsonProperty(value = _ROLE_ID)
	@ValidateColumn(name = _ROLE_ID)
	private int roleId;

	@JsonProperty(value = _ROLE_NAME)
	private String roleName;

	@TableColumn(name = _MODULE_ID)
	@JsonProperty(value = _MODULE_ID)
	@ValidateColumn(name = _MODULE_ID)
	private long moduleId;

	@JsonProperty(value = _MODULE_NAME)
	private String moduleName;

	@JsonProperty(value = _TABLE_NAME_MODULES)
	private List<ModuleModel> modules;


	public int getRoleId() {
		return roleId;
	}
    public long getModuleId() {
        return moduleId;
    }
    public String getRoleName() { return roleName; }
    public String getModuleName() {
        return moduleName;
    }
	public List<ModuleModel> getModules() {
        return modules;
    }



    public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
    public void setModuleId(long moduleId) {
        this.moduleId = moduleId;
    }
    public void setRoleName(String roleName) { this.roleName = roleName; }
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }
	public void setModules(List<ModuleModel> modules) {
		this.modules = modules;
	}

}