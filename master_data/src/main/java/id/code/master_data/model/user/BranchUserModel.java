package id.code.master_data.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.branch.BranchModel;

import static id.code.master_data.AliasName.*;

@Table(_TABLE_NAME_BRANCH_USERS)
public class BranchUserModel extends BaseModel {

    @JsonProperty(_BRANCH_CODE)
    @TableColumn(_BRANCH_CODE)
    private String branchCode;

    @JsonProperty(_USER_ID)
    @TableColumn(_USER_ID)
    private long userId;

    @JsonProperty(_TABLE_NAME_BRANCHES)
    private BranchModel branch;

    public String getBranchCode() {
        return branchCode;
    }
    public long getUserId() {
        return userId;
    }
    public BranchModel getBranch() {
        return branch;
    }


    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setUserId(long userId) {
        this.userId = userId;
    }
    public void setBranch(BranchModel branch) {
        this.branch = branch;
    }
}
