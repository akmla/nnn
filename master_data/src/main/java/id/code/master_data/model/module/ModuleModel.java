package id.code.master_data.model.module;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName._NAME;

@Table(name = AliasName._TABLE_NAME_MODULES)
public class ModuleModel extends BaseModel {

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}