package id.code.master_data.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.role.RoleModel;

import java.util.List;

import static id.code.master_data.AliasName.*;

@Table(name = AliasName._TABLE_NAME_USERS)
public class UserModel extends BaseModel {

    @TableColumn(name = _STATUS)
    @JsonProperty(value = _STATUS)
    @ValidateColumn(name = _STATUS, invalidZeroNumber = false)
    private int status;

    @TableColumn(name = _ROLE_ID)
    @JsonProperty(value = _ROLE_ID)
    @ValidateColumn(name = _ROLE_ID)
    private int roleId;

    @TableColumn(name = _CODE)
    @JsonProperty(value = _CODE)
    @ValidateColumn(name = _CODE)
    private String code;

    @TableColumn(name = _NAME)
    @JsonProperty(value = _NAME)
    @ValidateColumn(name = _NAME)
    private String name;

    @TableColumn(name = _EMAIL)
    @JsonProperty(value = _EMAIL)
    @ValidateColumn(name = _EMAIL)
    private String email;

    @TableColumn(name = _PHONE)
    @JsonProperty(value = _PHONE)
    @ValidateColumn(name = _PHONE)
    private String phone;

    @TableColumn(name = _GENDER)
    @JsonProperty(value = _GENDER)
    @ValidateColumn(name = _GENDER)
    private String gender;

    @TableColumn(name = _PASSWORD)
    @JsonProperty(value = _PASSWORD, access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @TableColumn(name = _FCM_TOKEN)
    @JsonProperty(value = _FCM_TOKEN, access = JsonProperty.Access.WRITE_ONLY)
    private String fcmToken;

    @TableColumn(name = _AUTHORIZATION_ID)
    @JsonProperty(value = _AUTHORIZATION_ID, access = JsonProperty.Access.WRITE_ONLY)
    private String authorizationId;

    @TableColumn(name = _BRANCH_CODE)
    @JsonProperty(value = _BRANCH_CODE)
    @ValidateColumn(name = _BRANCH_CODE)
    private String branchCode;

    @TableColumn(name = _SUPERVISOR_ID)
    @JsonProperty(value = _SUPERVISOR_ID)
    private Long supervisorId;

    @TableColumn(name = _LAST_LOGIN_TIME)
    @JsonProperty(value = _LAST_LOGIN_TIME)
    private long lastLoginTime;

    @JsonProperty(value = _PASSWORD_MODIFIED)
    @TableColumn(name = _PASSWORD_MODIFIED)
    private Long passwordModified;

    @JsonProperty(value = _TOKEN)
    private String token;

    @JsonProperty(value = _TABLE_NAME_BRANCHES)
    private List<BranchViewModel> branches;

    @JsonProperty(value = _SUPERVISOR)
    private UserViewModel supervisor;

    @JsonProperty(value = _TABLE_NAME_ROLES)
    private RoleModel role;

    public int getStatus() {
        return status;
    }
    public int getRoleId() {
        return roleId;
    }
    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public String getPhone() { return phone; }
    public String getEmail() {
        return email;
    }
    public String getToken() {
        return token;
    }
    public String getGender() {
        return gender;
    }
    public String getPassword() {
        return password;
    }
    public String getFcmToken() {
        return fcmToken;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getAuthorizationId() { return authorizationId; }
    public Long getSupervisorId() {
        return supervisorId;
    }
    public Long getLastLoginTime() {
        return lastLoginTime;
    }
    public Long getPasswordModified() {
        return passwordModified;
    }
    public RoleModel getRole() {
        return role;
    }
    public UserViewModel getSupervisor() {
        return supervisor;
    }
    public List<BranchViewModel> getBranches() {
        return branches;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
    public void setAuthorizationId(String authorizationId) { this.authorizationId = authorizationId; }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setSupervisorId(Long supervisorId) {
        this.supervisorId = supervisorId;
    }
    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
    public void setPasswordModified(Long passwordModified) {
        this.passwordModified = passwordModified;
    }
    public void setBranches(List<BranchViewModel> branches) {
        this.branches = branches;
    }
    public void setSupervisor(UserViewModel supervisor) {
        this.supervisor = supervisor;
    }
    public void setRole(RoleModel role) {
        this.role = role;
    }


}