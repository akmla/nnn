package id.code.master_data.model.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.branch.BranchViewModel;

import static id.code.master_data.AliasName.*;

@Table(name = AliasName._TABLE_NAME_PRODUCTS)
public class ProductModel extends BaseModel {

	@JsonProperty(value = _BRAND_CODE)
	@TableColumn(name = _BRAND_CODE)
	private String brandCode;

    @JsonProperty(value =_PRODUCT_GROUP)
    @TableColumn(name = _PRODUCT_GROUP)
    private String productGroup;

	@JsonProperty(value = _BRAND_NAME)
	private String brandName;

	@JsonProperty(value = _CODE)
	@TableColumn(name = _CODE)
	private String code;

	@JsonProperty(value = _NAME)
	@TableColumn(name = _NAME)
	private String name;

	@TableColumn(name = _TYPE)
	@JsonProperty(value = _TYPE)
	private String type;

	@JsonProperty(value = _BRAND)
	private BrandViewModel brand;

	@JsonProperty(value = _BRANCH)
	private BranchViewModel branch;


	public String getBrandCode() {
		return brandCode;
	}
	public String getBrandName() {
		return brandName;
	}
	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
    public String getProductGroup() {
        return productGroup;
    }
    public BrandViewModel getBrand() {
		return brand;
	}
	public BranchViewModel getBranch() {
		return branch;
	}


	public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setType(String type) {
        this.type = type;
    }
	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}
	public void setBrand(BrandViewModel brand) {
		this.brand = brand;
	}
	public void setBranch(BranchViewModel branch) {
		this.branch = branch;
	}
}