package id.code.master_data.facade.product;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.ProductFilter;
import id.code.master_data.model.branch.BranchProductModel;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.product.BrandViewModel;
import id.code.master_data.model.product.ProductModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/16/2017.
 */
public class ProductFacade extends BaseFacade {

    public List<ProductModel> getProducts(Filter<ProductFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_PRODUCTS, ProductModel.class)
                .leftJoin(_BRAND, BrandViewModel.class).on(_BRAND, _CODE).isEqual(_TABLE_NAME_PRODUCTS, _BRAND_CODE)
                .orderBy(_TABLE_NAME_PRODUCTS, filter)
                .filter(_TABLE_NAME_PRODUCTS, filter)
                .limitOffset(filter);

        if (filter.getParam().getApplicationType() == null) {
            if (filter.getParam().getType() != null && filter.getParam().getType().equalsIgnoreCase(_EXTERNAL)) {
                sqlSelect.where(_TABLE_NAME_PRODUCTS, _PRODUCT_GROUP).isNot(null);
            } else if (filter.getParam().getType() != null && filter.getParam().getType().equalsIgnoreCase(_INTERNAL)) {

                sqlSelect.where(_TABLE_NAME_PRODUCTS, _BRAND_CODE).isNot(null);

                if (filter.getParam().getBranchCode() != null) {
                    if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL_BRANCH)) {
                        sqlSelect.join(_BRANCH_PRODUCT, BranchProductModel.class)
                                .on(_TABLE_NAME_PRODUCTS, _CODE).isEqual(_BRANCH_PRODUCT, _PRODUCT_CODE)
                                .join(_TABLE_NAME_BRANCHES, BranchViewModel.class)
                                .on(_TABLE_NAME_BRANCHES, _CODE).isEqual(_BRANCH_PRODUCT, _BRANCH_CODE)
                                .where(_BRANCH_PRODUCT, _BRANCH_CODE).in(
                                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE)
                                                .where(_USER_ID).isEqual(filter.getParam().getUserId())
                        );
                    } else { //if (!filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                        sqlSelect.join(_BRANCH_PRODUCT, BranchProductModel.class)
                                .on(_TABLE_NAME_PRODUCTS, _CODE).isEqual(_BRANCH_PRODUCT, _PRODUCT_CODE)
                                .join(_TABLE_NAME_BRANCHES, BranchViewModel.class)
                                .on(_TABLE_NAME_BRANCHES, _CODE).isEqual(_BRANCH_PRODUCT, _BRANCH_CODE)
                                .where(_BRANCH_PRODUCT, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
                    }
                }
//                if (filter.getParam().getBranchCode() != null) {
//                    sqlSelect.join(_BRANCH_PRODUCT, BranchProductModel.class)
//                            .on(_TABLE_NAME_PRODUCTS, _CODE).isEqual(_BRANCH_PRODUCT, _PRODUCT_CODE)
//                            .join(_TABLE_NAME_BRANCHES, BranchViewModel.class)
//                            .on(_TABLE_NAME_BRANCHES, _CODE).isEqual(_BRANCH_PRODUCT, _BRANCH_CODE)
//                            .where(_BRANCH_PRODUCT, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
//                }
            }
        } else if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_MONICA) {
            sqlSelect.leftJoin(_BRANCH_PRODUCT, BranchProductModel.class)
                    .on(_TABLE_NAME_PRODUCTS, _CODE).isEqual(_BRANCH_PRODUCT, _PRODUCT_CODE)
                    .leftJoin(_TABLE_NAME_BRANCHES, BranchViewModel.class)
                    .on(_TABLE_NAME_BRANCHES, _CODE).isEqual(_BRANCH_PRODUCT, _BRANCH_CODE)
                    .where(_BRANCH_PRODUCT, _BRANCH_CODE).isEqualOr(filter.getParam().getBranchCode(),
                    _TABLE_NAME_PRODUCTS, _PRODUCT_GROUP).isNot(null);
        } else {
            sqlSelect.join(_BRANCH_PRODUCT, BranchProductModel.class)
                    .on(_TABLE_NAME_PRODUCTS, _CODE).isEqual(_BRANCH_PRODUCT, _PRODUCT_CODE)
                    .join(_TABLE_NAME_BRANCHES, BranchViewModel.class)
                    .on(_TABLE_NAME_BRANCHES, _CODE).isEqual(_BRANCH_PRODUCT, _BRANCH_CODE)
                    .where(_BRANCH_PRODUCT, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
        }

        sqlSelect.includeAllJoin();

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<ProductModel> listProducts = new ArrayList<>();
            while (result.moveNext()) {
                final ProductModel product = result.getItem(_TABLE_NAME_PRODUCTS, ProductModel.class);
                product.setBrand(result.getItem(_BRAND, BrandViewModel.class));
                product.setBrandName((product.getProductGroup() != null) ? product.getType() : product.getBrand().getName());

                if (filter.getParam().getType() != null && filter.getParam().getType().equalsIgnoreCase(_INTERNAL)) {
                    product.setBranch(result.getItem(_TABLE_NAME_BRANCHES, BranchViewModel.class));
                }

                listProducts.add(product);
            }
            return listProducts;
        }
    }

    public ProductModel getProduct(long productId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_PRODUCTS, ProductModel.class)
                .includeAllJoin()
                .leftJoin(_BRAND, BrandViewModel.class).on(_TABLE_NAME_PRODUCTS, _BRAND_CODE).isEqual(_BRAND, _CODE)
                .where(_TABLE_NAME_PRODUCTS, _ID).isEqual(productId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                ProductModel product = result.getItem(_TABLE_NAME_PRODUCTS, ProductModel.class);
                product.setBrand(result.getItem(_BRAND, BrandViewModel.class));
                return product;
            }
            return null;
        }
    }
}