package id.code.master_data.facade.branch;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.SubBranchFilter;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.branch.SubBranchModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class SubBranchFacade extends BaseFacade {
    public List<SubBranchModel> getSubBranches(Filter<SubBranchFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SUB_BRANCHES, SubBranchModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchViewModel.class).on(_SUB_BRANCHES, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .orderBy(_SUB_BRANCHES, filter)
                .filter(_SUB_BRANCHES, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL_BRANCH)) {
                sqlSelect.where(_SUB_BRANCHES, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE)
                                .where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else if (!filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_SUB_BRANCHES, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<SubBranchModel> listSubBranch = new ArrayList<>();
            while (result.moveNext()) {
                final SubBranchModel subBranch = result.getItem(_SUB_BRANCHES, SubBranchModel.class);
                subBranch.setBranch(result.getItem(_BRANCH, BranchViewModel.class));
                listSubBranch.add(subBranch);
            }
            return listSubBranch;
        }
    }

    public SubBranchModel getSubBranch(long subBranchId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SUB_BRANCHES, SubBranchModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchViewModel.class).on(_SUB_BRANCHES, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .where(_ID).isEqual(subBranchId).limit(1).offset(0);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final SubBranchModel subBranch = result.getItem(_SUB_BRANCHES, SubBranchModel.class);
                subBranch.setBranch(result.getItem(_BRANCH, BranchViewModel.class));
                return subBranch;
            }
            return null;
        }
    }
}
