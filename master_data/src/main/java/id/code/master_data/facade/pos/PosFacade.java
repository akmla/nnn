package id.code.master_data.facade.pos;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.PosFilter;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.pos.PosModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/18/2017.
 */
public class PosFacade extends BaseFacade {

    public List<PosModel> getAllPos(Filter<PosFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_POS, PosModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_BRANCHES, BranchViewModel.class).on(_TABLE_NAME_POS, _BRANCH_CODE).isEqual(_TABLE_NAME_BRANCHES, _CODE)
                .orderBy(_TABLE_NAME_POS, filter)
                .filter(_TABLE_NAME_POS, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_TABLE_NAME_POS, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE)
                                .where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else /*if (!filter.getParam().getBranchCode().equalsIgnoreCase(_ALL))*/ {
                sqlSelect.where(_TABLE_NAME_POS, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<PosModel> listPos = new ArrayList<>();
            while (result.moveNext()) {
                PosModel pos = result.getItem(_TABLE_NAME_POS, PosModel.class);
                pos.setBranch(result.getItem(_TABLE_NAME_BRANCHES, BranchViewModel.class));
                listPos.add(pos);
            }
            return listPos;
        }
    }

    public PosModel getPos(long posId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_POS, PosModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_BRANCHES, BranchViewModel.class).on(_TABLE_NAME_POS, _BRANCH_CODE).isEqual(_TABLE_NAME_BRANCHES, _CODE)
                .where(_TABLE_NAME_POS, _ID).isEqual(posId).limit(1).offset(0);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                PosModel pos = result.getItem(_TABLE_NAME_POS, PosModel.class);
                pos.setBranch(result.getItem(_TABLE_NAME_BRANCHES, BranchViewModel.class));
                return pos;
            }
            return null;
        }
    }

}
