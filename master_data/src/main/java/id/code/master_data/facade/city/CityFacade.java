package id.code.master_data.facade.city;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.CityFilter;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.city.CityModel;
import id.code.master_data.model.province.ProvinceViewModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class CityFacade extends BaseFacade {

    public List<CityModel> getCities(Filter<CityFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_CITIES, CityModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchViewModel.class).on(_BRANCH, _CODE).isEqual(_TABLE_NAME_CITIES, _BRANCH_CODE)
                .join(_PROVINCE, ProvinceViewModel.class).on(_TABLE_NAME_CITIES, _PROVINCE_CODE).isEqual(_PROVINCE, _CODE)
                .orderBy(_TABLE_NAME_CITIES, filter)
                .filter(_TABLE_NAME_CITIES, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_TABLE_NAME_CITIES, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_TABLE_NAME_CITIES, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<CityModel> listCity = new ArrayList<>();
            while (result.moveNext()) {
                CityModel city = result.getItem(_TABLE_NAME_CITIES, CityModel.class);
                city.setProvince(result.getItem(_PROVINCE, ProvinceViewModel.class));
                city.setBranch(result.getItem(_BRANCH, BranchViewModel.class));
                listCity.add(city);
            }
            return listCity;
        }
    }

    public CityModel getCity(long cityId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_CITIES, CityModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchViewModel.class).on(_BRANCH, _CODE).isEqual(_TABLE_NAME_CITIES, _BRANCH_CODE)
                .join(_PROVINCE, ProvinceViewModel.class).on(_TABLE_NAME_CITIES, _PROVINCE_CODE).isEqual(_PROVINCE, _CODE)
                .where(_TABLE_NAME_CITIES, _ID).isEqual(cityId).limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if(result.moveNext()) {
                CityModel city = result.getItem(_TABLE_NAME_CITIES, CityModel.class);
                city.setProvince(result.getItem(_PROVINCE, ProvinceViewModel.class));
                city.setBranch(result.getItem(_BRANCH, BranchViewModel.class));
                return city;
            }
            return null;
        }
    }
}