package id.code.master_data.facade.lookup;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.LookupFilter;
import id.code.master_data.model.setting.LookupDetailModel;
import id.code.master_data.model.setting.LookupModel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class LookupFacade extends BaseFacade {

    public List<LookupModel> getAll(Filter<LookupFilter> filter) throws SQLException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_LOOKUPS, LookupModel.class)
                .orderBy(_TABLE_NAME_LOOKUPS, _GROUP_NAME).asc()
                .orderBy(_TABLE_NAME_LOOKUPS, _KEY).asc()
                .filter(_TABLE_NAME_LOOKUPS, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result != null) ? result.getItems(_TABLE_NAME_LOOKUPS, LookupModel.class) : null;
        }
    }

    public Collection<LookupDetailModel> getAllGrouped(Filter<LookupFilter> filter) throws SQLException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_LOOKUPS, LookupModel.class)
                .orderBy(_TABLE_NAME_LOOKUPS, _GROUP_NAME).asc()
                .orderBy(_TABLE_NAME_LOOKUPS, _KEY).asc()
                .filter(_TABLE_NAME_LOOKUPS, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final HashMap<String, LookupDetailModel> lookupMap = new HashMap<>();

            while (result.moveNext()) {
                LookupModel detail = result.getItem(_TABLE_NAME_LOOKUPS, LookupModel.class);
                LookupDetailModel inMap = lookupMap.get(detail.getGroupName());

                if (inMap == null) {
                    inMap = new LookupDetailModel();
                    inMap.setGroupName(detail.getGroupName());
                    inMap.setDetails(new ArrayList<>());
                }

                inMap.getDetails().add(detail);
                lookupMap.put(inMap.getGroupName(), inMap);
            }
            return lookupMap.values();
        }
    }

    public void insertOrUpdate(LookupModel lookup) throws SQLException {
        try (final Connection connection = super.openConnection()) {
            final LookupModel existing = QueryBuilder.select(LookupModel.class)
                    .where(_GROUP_NAME).isEqual(lookup.getGroupName())
                    .where(_KEY).isEqual(lookup.getKey())
                    .getResult(connection, true)
                    .executeItem(LookupModel.class);

            if (existing == null) {
                QueryBuilder.insert(lookup).execute(connection);
            } else {
                lookup.setId(existing.getId());
                QueryBuilder.update(lookup, _MODIFIED, _MODIFIED_BY, _VALUE).execute(connection);
            }
        }
    }
}