package id.code.master_data.facade.user;

import id.code.component.PasswordHasher;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.UserFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.model.user.ErpUserModel;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.model.user.UserViewModel;
import id.code.master_data.validation.UserRegisterValidation;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.security.Role.*;

/**
 * Created by CODE.ID on 8/9/2017.
 */
public class UserFacade extends BaseFacade {
    public List<UserModel> getAllUser(Filter<UserFilter> filter) throws SQLException, InstantiationException, IllegalAccessException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_USERS, UserModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_ROLES, RoleModel.class).on(_TABLE_NAME_USERS, _ROLE_ID).isEqual(_TABLE_NAME_ROLES, _ID)
                .join(_BRANCH, BranchViewModel.class).on(_TABLE_NAME_USERS, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .leftJoin(_SUPERVISOR, UserViewModel.class).on(_TABLE_NAME_USERS, _SUPERVISOR_ID).isEqual(_SUPERVISOR, _ID)
                .where(_TABLE_NAME_ROLES, _ID).isNotEqual(_ROLE_ID_SPG)
                .orderBy(_TABLE_NAME_USERS, filter)
                .filter(_TABLE_NAME_USERS, filter)
                .limitOffset(filter);

        if (filter.getParam().getApolloUser()) {
            sqlSelect.where(_TABLE_NAME_USERS, _ROLE_ID).in(Arrays.asList(_ROLE_ID_TL_PROMOSI, _ROLE_ID_MERCHANDISER, _ROLE_ID_HO, _ROLE_ID_BRANCH, _ROLE_ID_HO_IT));
        }

        if ((filter.getParam().getApplicationType() != null) && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            filter.getParam().setSupervisorId(null);
        }

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_TABLE_NAME_USERS, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_TABLE_NAME_USERS, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<UserModel> users = new ArrayList<>();
            while (result.moveNext()) {
                final UserModel user = result.getItem(_TABLE_NAME_USERS, UserModel.class);
                final BranchViewModel branch = result.getItem(_BRANCH, BranchViewModel.class);
                user.setBranches(new ArrayList<>());
                user.getBranches().add(branch);
                user.setSupervisor(result.getItem(_SUPERVISOR, UserViewModel.class));
                user.setRole(result.getItem(_TABLE_NAME_ROLES, RoleModel.class));
                users.add(user);
            }
            return users;
        }
    }

    public UserModel getUser(long userId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_USERS, UserModel.class)
                .join(_TABLE_NAME_ROLES, RoleModel.class).on(_TABLE_NAME_USERS, _ROLE_ID).isEqual(_TABLE_NAME_ROLES, _ID)
                .join(_BRANCH, BranchViewModel.class).on(_TABLE_NAME_USERS, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .leftJoin(_SUPERVISOR, UserViewModel.class).on(_TABLE_NAME_USERS, _SUPERVISOR_ID).isEqual(_SUPERVISOR, _ID)
                .includeAllJoin()
                .where(_TABLE_NAME_USERS, _ID).isEqual(userId).limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final UserModel user = result.getItem(_TABLE_NAME_USERS, UserModel.class);

                if (user.getRoleId() == _ROLE_ID_HO_IT) {
                    user.setRoleId(_ROLE_ID_HO);
                }

                final BranchViewModel branch = result.getItem(_BRANCH, BranchViewModel.class);
                user.setBranches(new ArrayList<>());
                user.getBranches().add(branch);
                user.setSupervisor(result.getItem(_SUPERVISOR, UserViewModel.class));
                user.setRole(result.getItem(_TABLE_NAME_ROLES, RoleModel.class));
                return user;
            }
            return null;
        }
    }

    public boolean isEmailAvailable(String email) throws SQLException, IllegalAccessException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(UserModel.class).where(_EMAIL).isEqual(email);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return !result.moveNext();
        }
    }

    public UserModel getUserReal(long userId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_USERS, UserModel.class)
                .join(_TABLE_NAME_ROLES, RoleModel.class).on(_TABLE_NAME_USERS, _ROLE_ID).isEqual(_TABLE_NAME_ROLES, _ID)
                .join(_BRANCH, BranchViewModel.class).on(_TABLE_NAME_USERS, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .leftJoin(_SUPERVISOR, UserViewModel.class).on(_TABLE_NAME_USERS, _SUPERVISOR_ID).isEqual(_SUPERVISOR, _ID)
                .includeAllJoin()
                .where(_TABLE_NAME_USERS, _ID).isEqual(userId).limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final UserModel user = result.getItem(_TABLE_NAME_USERS, UserModel.class);
                final BranchViewModel branch = result.getItem(_BRANCH, BranchViewModel.class);
                user.setBranches(new ArrayList<>());
                user.getBranches().add(branch);
                user.setSupervisor(result.getItem(_SUPERVISOR, UserViewModel.class));
                user.setRole(result.getItem(_TABLE_NAME_ROLES, RoleModel.class));
                return user;
            }
            return null;
        }
    }

    public UserModel login(String code, String password) throws SQLException, IllegalAccessException, InstantiationException, GeneralSecurityException, UnsupportedEncodingException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_USERS, UserModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchViewModel.class).on(_TABLE_NAME_USERS, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .where(_TABLE_NAME_USERS, _CODE).isEqualOr(code, _TABLE_NAME_USERS, _EMAIL).isEqual(code)
                .where(_TABLE_NAME_USERS, _STATUS).isEqual(_STATUS_TRUE)
                .limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            UserModel user = null;

            if (result.moveNext()) {
                user = result.getItem(_TABLE_NAME_USERS, UserModel.class);
                final BranchViewModel branch = result.getItem(_BRANCH, BranchViewModel.class);
                user.setBranches(new ArrayList<>());
                user.getBranches().add(branch);
            }

            return user != null && PasswordHasher.verify(password, user.getPassword()) ? user : null;
        }
    }

    public boolean register(UserRegisterValidation user, AuditTrailModel auditTrail) throws SQLException, IllegalAccessException, GeneralSecurityException, IOException, QueryBuilderException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            user.setPassword(PasswordHasher.computePasswordHash(user.getPassword()));
            final UpdateResult result = QueryBuilder.insert(user).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_USERS, user);

            if (result.isModified() && super.insertAudit(sqlTransaction, auditTrail)) {

                if (user.getRoleId() == _ROLE_ID_HO || user.getRoleId() == _ROLE_ID_BRANCH ||
                        user.getRoleId() == _ROLE_ID_MERCHANDISER || user.getRoleId() == _ROLE_ID_TL_PROMOSI) {

                    final SelectBuilder sqlSelect = QueryBuilder.select(ErpUserModel.class)
                            .where(_CODE).isEqual(user.getCode());

                    try (final ResultBuilder resultErpUser = sqlSelect.execute(sqlTransaction)) {
                        if (resultErpUser.moveNext()) {
                            final ErpUserModel erpUserModel = resultErpUser.getItem(ErpUserModel.class);
                            erpUserModel.modify(_MY_APPLICATION);
                            erpUserModel.setAvailable(_STATUS_FALSE);
                            final UpdateResult resultErpUserUpdate = QueryBuilder.update(erpUserModel).execute(sqlTransaction);
                            if (!resultErpUserUpdate.isModified()) {
                                sqlTransaction.rollbackTransaction();
                                return false;
                            }
                        }
                    }
                }

                sqlTransaction.commitTransaction();
                return true;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public void update(UserModel user) throws SQLException, IllegalAccessException, GeneralSecurityException, IOException, QueryBuilderException {
        try (final UpdateResult result = QueryBuilder.update(user, _LAST_LOGIN_TIME, _FCM_TOKEN, _AUTHORIZATION_ID).execute(super.openConnection())) {
            result.isModified();
        }
    }

    public boolean updateFcmToken(UserModel user) throws SQLException, QueryBuilderException {
        try (final UpdateResult result = QueryBuilder.update(user, _FCM_TOKEN).execute(super.openConnection())) {
            return result.isModified();
        }
    }

    public long getUserIdByEmail(String email) throws SQLException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(UserModel.class, _ID).where(_EMAIL).isEqual(email);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(UserModel.class).getId() : 0;
        }
    }

    public int getRoleIdByEmail(String email) throws SQLException, QueryBuilderException {
        final SelectBuilder sqlSelect = QueryBuilder.select(UserModel.class, _ROLE_ID).where(_EMAIL).isEqual(email);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(UserModel.class).getRoleId() : 0;
        }
    }

    public boolean delete(UserModel user, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult result = QueryBuilder.delete(user).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_USERS, user);

            if ((result.isModified() && this.insertAudit(sqlTransaction, auditTrail))) {
                final ErpUserModel erpUser = QueryBuilder.select(ErpUserModel.class)
                        .where(_CODE).isEqual(user.getCode())
                        .getResult(sqlTransaction)
                        .executeItem(ErpUserModel.class);

                if (erpUser != null) {
                    erpUser.setAvailable(_STATUS_TRUE);
                    erpUser.modify(_MY_APPLICATION);
                    if (!QueryBuilder.update(erpUser).execute(sqlTransaction).isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return true;
                    }
                }

                sqlTransaction.commitTransaction();
                return true;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}
