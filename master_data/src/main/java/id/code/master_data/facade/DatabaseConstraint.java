package id.code.master_data.facade;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class DatabaseConstraint {
    public static final String CONSTRAINT_UNIQUE_USER_EMAIL = "users_email_key";
    public static final String CONSTRAINT_FOREIGN_USER_AREA = "user_areas_to_areas_constraint";
    public static final String CONSTRAINT_FOREIGN_OUTLET_AREA = "outlets_to_areas_constraint";
    public static final String CONSTRAINT_FOREIGN_AREA_SCOPE = "area_scope_to_areas_constraints";
    public static final String CONSTRAINT_FOREIGN_PARENT_SCOPE = "parent_scope_to_areas_constraint";
}
