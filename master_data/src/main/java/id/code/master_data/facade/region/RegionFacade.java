package id.code.master_data.facade.region;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.RegionFilter;
import id.code.master_data.model.region.RegionModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName._ID;
import static id.code.master_data.AliasName._TABLE_NAME_REGIONS;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class RegionFacade extends BaseFacade {
    public List<RegionModel> getRegions(Filter<RegionFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_REGIONS, RegionModel.class)
                .orderBy(_TABLE_NAME_REGIONS, filter)
                .filter(_TABLE_NAME_REGIONS, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result != null) ? result.getItems(_TABLE_NAME_REGIONS, RegionModel.class) : null;
        }
    }

    public RegionModel getRegion(long regionId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_REGIONS, RegionModel.class).where(_ID).isEqual(regionId).limit(1).offset(0);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.moveNext() ? result.getItem(_TABLE_NAME_REGIONS, RegionModel.class) : null;
        }
    }
}
