package id.code.master_data.facade.branch;

import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.BranchProductFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.branch.BranchModel;
import id.code.master_data.model.branch.BranchProductModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class BranchProductFacade extends BaseFacade {

    public List<BranchProductModel> getBranchProducts(Filter<BranchProductFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_BRANCH_PRODUCT, BranchProductModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchModel.class).on(_BRANCH_PRODUCT, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .orderBy(_BRANCH_PRODUCT, filter)
                .filter(_BRANCH_PRODUCT, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_BRANCH_PRODUCT, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_BRANCH_PRODUCT, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<BranchProductModel> listBranchProduct = new ArrayList<>();
            while (result.moveNext()) {
                final BranchProductModel branchProduct = result.getItem(_BRANCH_PRODUCT, BranchProductModel.class);
                final BranchModel branch = result.getItem(_BRANCH, BranchModel.class);
                branchProduct.setBranchName(branch.getName());
                listBranchProduct.add(branchProduct);
            }
            return listBranchProduct;
        }
    }

    public BranchProductModel getBranchProduct(long branchProductId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_BRANCH_PRODUCT, BranchProductModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchModel.class).on(_BRANCH_PRODUCT, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .where(_BRANCH_PRODUCT, _ID).isEqual(branchProductId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final BranchProductModel branchProduct = result.getItem(_BRANCH_PRODUCT, BranchProductModel.class);
                final BranchModel branch = result.getItem(_BRANCH, BranchModel.class);
                branchProduct.setBranchName(branch.getName());
                return branchProduct;
            }
            return null;
        }
    }

//    public boolean insert(BranchProductModel newData, AuditTrailModel auditTrail) throws Exception {
//        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
//            final UpdateResult resultInsert = QueryBuilder.insert(newData).execute(sqlTransaction);
//            auditTrail.prepareAudit(_TABLE_NAME_BRANCH_PRODUCTS, newData);
//            if (resultInsert.isModified() && this.insertAudit(sqlTransaction, auditTrail)) {
//                final ProductModel product = QueryBuilder.select(ProductModel.class)
//                        .where(_CODE).isEqual(newData.getProductCode())
//                        .getResult(sqlTransaction)
//                        .executeItem(ProductModel.class);
//                product.modify(_MY_APPLICATION);
//                final UpdateResult insertAll = QueryBuilder.update(product).execute(sqlTransaction);
//                if (!insertAll.isModified()) {
//                    sqlTransaction.rollbackTransaction();
//                    return false;
//                }
//
//                sqlTransaction.commitTransaction();
//                return true;
//            }
//            sqlTransaction.rollbackTransaction();
//            return false;
//        }
//    }

    public boolean insert(BranchProductModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult resultInsert = QueryBuilder.insert(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_BRANCH_PRODUCTS, newData);
            if (resultInsert.isModified() && this.insertAudit(sqlTransaction, auditTrail)) {
                final BranchProductModel branchProductAll = new BranchProductModel();
                final List<BranchModel> branchModels;
                if (newData.getBranchCode().equalsIgnoreCase(_ALL)) {
                    branchModels = QueryBuilder.select(BranchModel.class)
                            .where(_BRANCH_CODE).isNotEqual(_ALL)
                            .getResult(sqlTransaction)
                            .executeItems(BranchModel.class);
                } else {
                    branchModels = new ArrayList<>();
                }

                branchProductAll.newModel(_MY_APPLICATION);
                branchProductAll.setProductCode(newData.getProductCode());
                branchProductAll.setType(newData.getType());

                for (final BranchModel branch : branchModels) {
                    branchProductAll.setBranchCode(branch.getCode());
                    final UpdateResult insertAll = QueryBuilder.insert(branchProductAll).execute(sqlTransaction);
                    if (!insertAll.isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }

                sqlTransaction.commitTransaction();
                return true;
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }

    }

    public boolean delete(BranchProductModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult resultInsert = QueryBuilder.delete(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_BRANCH_PRODUCTS, newData);
            if (resultInsert.isModified() && this.insertAudit(sqlTransaction, auditTrail)) {
                final BranchProductModel branchProductAll;
                final SelectBuilder selectBranchProductAll = QueryBuilder.select(BranchProductModel.class)
                        .where(_BRANCH_CODE).isEqual(_ALL)
                        .where(_PRODUCT_CODE).isEqual(newData.getProductCode())
                        .where(_TYPE).isEqual(newData.getType());

                try (final ResultBuilder resultBranchProductAll = selectBranchProductAll.execute(sqlTransaction)) {
                    if (resultBranchProductAll.moveNext()) {
                        branchProductAll = resultBranchProductAll.getItem(BranchProductModel.class);
                        final UpdateResult insertAll = QueryBuilder.delete(branchProductAll).execute(sqlTransaction);
                        if (insertAll.isModified()) {
                            sqlTransaction.commitTransaction();
                            return true;
                        }
                    }

                }
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

}