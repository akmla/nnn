package id.code.master_data;

import id.code.server.ApiResponse;

import static id.code.server.ApiHandler.STATUS_INVALID_LOGIN;
import static id.code.server.ApiHttpStatus.STATUS_UNPROCESSABLE_ENTITY;

/**
 * Created by CODE.ID on 8/9/2017.
 */
public final class MonicaResponse {
    private static final int STATUS_DUPLICATE_EMAIL = 2001;
    private static final int STATUS_MULTIPLE_LOGIN = 2002;
    private static final int STATUS_SPG_NOT_FOUND = 2003;
    private static final int STATUS_EVENT_NOT_FOUND = 2004;
    private static final int STATUS_CHECK_IN_NOT_FOUND = 2005;
    private static final int STATUS_INVALID_UNIQUE_CODE = 2006;
    private static final int STATUS_ERROR_UPDATE = 2007;
    private static final int STATUS_PLEASE_WAIT = 2008;
    private static final int STATUS_DUPLICATE_DATA = 2009;
    private static final int STATUS_CONSTRAINT_FOREIGN_KEY = 2010;
    private static final int STATUS_INVALID_EMAIL = 2011;
    private static final int STATUS_EVENT_IS_ACTIVE = 2012;
    private static final int STATUS_ERROR_INSERT = 2013;
    private static final int STATUS_ERROR_DELETE = 2014;
    private static final int STATUS_NO_PARENT_KEY = 2015;
    private static final int STATUS_TASK_RUNNING = 2016;
    private static final int STATUS_FORBIDDEN = 2017;
    private static final int STATUS_ERROR_UPLOAD = 2018;
    private static final int STATUS_NO_USER = 2019;
    private static final int STATUS_CONTRACT_IS_USED = 2020;
    private static final int STATUS_NOT_MONDAY = 2021;

    public static final ApiResponse RESPONSE_INVALID_USER_LOGIN = new ApiResponse(STATUS_INVALID_LOGIN, "Email atau password salah !");
    public static final ApiResponse RESPONSE_DUPLICATE_EMAIL = new ApiResponse(STATUS_DUPLICATE_EMAIL, "Email sudah dipakai !");
    public static final ApiResponse RESPONSE_MULTIPLE_LOGIN = new ApiResponse(STATUS_MULTIPLE_LOGIN, "Anda sudah login di device lain !");
    public static final ApiResponse RESPONSE_SPG_NOT_FOUND = new ApiResponse(STATUS_SPG_NOT_FOUND, "Anda bukan spg !");
    public static final ApiResponse RESPONSE_ERROR_UPDATE_DATA = new ApiResponse(STATUS_ERROR_UPDATE, "Data gagal diubah !");
    public static final ApiResponse RESPONSE_EVENT_NOT_FOUND = new ApiResponse(STATUS_EVENT_NOT_FOUND, "Event tidak ditemukan !");
    public static final ApiResponse RESPONSE_CHECK_IN_NOT_FOUND = new ApiResponse(STATUS_CHECK_IN_NOT_FOUND, "Check in tidak ditemukan !");
    public static final ApiResponse RESPONSE_INVALID_UNIQUE_CODE = new ApiResponse(STATUS_INVALID_UNIQUE_CODE, "Unique code tidak valid !");
    public static final ApiResponse RESPONSE_FAILED_TO_CHANGE_PASSWORD = new ApiResponse(STATUS_ERROR_UPDATE, "Password tidak berhasil diubah !");
    public static final ApiResponse RESPONSE_FAILED_TO_UPDATE_STATUS = new ApiResponse(STATUS_ERROR_UPDATE, "Status tidak berhasil diubah !");
    public static final ApiResponse RESPONSE_PLEASE_WAIT = new ApiResponse(STATUS_PLEASE_WAIT, "Mohon tunggu beberapa saat !");
    public static final ApiResponse RESPONSE_DUPLICATE_DATA = new ApiResponse(STATUS_DUPLICATE_DATA, "Data sudah ada / Unique Constraint !");
    public static final ApiResponse RESPONSE_ERROR_CONSTRAINT = new ApiResponse(STATUS_CONSTRAINT_FOREIGN_KEY, "Data is used in another place !");
    public static final ApiResponse RESPONSE_INVALID_EMAIL = new ApiResponse(STATUS_INVALID_EMAIL, "Email salah !");
    public static final ApiResponse RESPONSE_EVENT_IS_ACTIVE = new ApiResponse(STATUS_EVENT_IS_ACTIVE, "Event masih aktif !");
    public static final ApiResponse RESPONSE_ERROR_INSERT_DATA = new ApiResponse(STATUS_ERROR_INSERT, "Data gagal disimpan !");
    public static final ApiResponse RESPONSE_ERROR_START_DATE = new ApiResponse(STATUS_NOT_MONDAY, "Start date harus hari senin !");
    public static final ApiResponse RESPONSE_ERROR_DELETE_DATA = new ApiResponse(STATUS_ERROR_DELETE, "Gagal delete data !");
    public static final ApiResponse RESPONSE_PARENT_KEY_NOT_FOUND = new ApiResponse(STATUS_NO_PARENT_KEY, "Parent key / Reference Key not found!");
    public static final ApiResponse RESPONSE_ERROR_MODIFY_TASK_RUNNING = new ApiResponse(STATUS_TASK_RUNNING, "Task already running!");
    public static final ApiResponse RESPONSE_ERROR_FORBIDDEN = new ApiResponse(STATUS_FORBIDDEN, "Tidak diperbolehkan!");
    public static final ApiResponse RESPONSE_ERROR_UPLOAD = new ApiResponse(STATUS_ERROR_UPLOAD, "Upload gagal!");
    public static final ApiResponse RESPONSE_ERROR_PARAMETER = new ApiResponse(STATUS_UNPROCESSABLE_ENTITY, "Parameter tidak ditemukan atau parameter salah!");
    public static final ApiResponse RESPONSE_ERROR_NO_USER = new ApiResponse(STATUS_NO_USER, "Tidak ada user!");
    public static final ApiResponse RESPONSE_CONTRACT_IS_USED = new ApiResponse(STATUS_CONTRACT_IS_USED, "Kontrak sudah dipakai!");
}