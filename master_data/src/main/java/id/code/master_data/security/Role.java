package id.code.master_data.security;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.master_data.facade.roles.RoleFacade;
import id.code.master_data.model.role.RoleModel;

import java.sql.SQLException;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by CODE.ID on 8/28/2017.
 */
public class Role {
    public static final int _ROLE_ID_SPG = 1;
    public static final int _ROLE_ID_TL_SPG = 2; // Limit to own spg
    public static final int _ROLE_ID_MERCHANDISER = 3; // Limit to own
    public static final int _ROLE_ID_TL_PROMOSI = 4; // Limit to own
    public static final int _ROLE_ID_BRANCH = 5; // All access in their own branch.
    public static final int _ROLE_ID_HO = 6; // All Access
    public static final int _ROLE_ID_HO_IT = 7; // All Access WITH SETTING

    public static final int _ID_MODULE_USER = 1;
    public static final int _ID_MODULE_BRANCHES = 2;
    public static final int _ID_MODULE_BRANDS = 3;
    public static final int _ID_MODULE_CHANNELS = 4;
    public static final int _ID_MODULE_CITIES = 5;
    public static final int _ID_MODULE_CONTACTS = 6;
    public static final int _ID_MODULE_DISTRICTS = 7;
    public static final int _ID_MODULE_EVENTS = 8;
    public static final int _ID_MODULE_SPG = 9;
    public static final int _ID_MODULE_LOOKUPS = 10;
    public static final int _ID_MODULE_MODULES = 11;
    public static final int _ID_MODULE_PACKAGES = 12;
    public static final int _ID_MODULE_PRODUCTS = 13;
    public static final int _ID_MODULE_POS = 14;
    public static final int _ID_MODULE_REGIONS = 15;
    public static final int _ID_MODULE_ROLES = 16;
    public static final int _ID_MODULE_SETTINGS = 17;
    public static final int _ID_MODULE_SUB_BRANCHES = 18;
    public static final int _ID_MODULE_SUMMARIES = 19;
    public static final int _ID_MODULE_PROVINCES = 20;

    public static void invalidateRole(long userId) { roles.invalidate(userId); }
    private static final Cache<Long, Optional<RoleModel>> roles;
    private static final RoleFacade roleFacade;

    static {
        roleFacade = new RoleFacade();
        roles = CacheBuilder.newBuilder()
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .expireAfterAccess(5, TimeUnit.MINUTES)
                .build();
    }

    public static RoleModel getCompleteRole(long userId) throws SQLException, InstantiationException, QueryBuilderException, IllegalAccessException, ExecutionException {
        final Optional<RoleModel> roleModel = roles.get(userId, () -> Optional.ofNullable(roleFacade.getCompleteRole(userId)));
        return roleModel.orElse(null);
    }
}
