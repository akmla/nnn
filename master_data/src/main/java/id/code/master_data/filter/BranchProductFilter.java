package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName.*;

public class BranchProductFilter extends BaseFilter {

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(value = _PRODUCT_CODE)
    @FilterColumn(columnName = _PRODUCT_CODE)
    private String productCode;

    @JsonProperty(_TYPE)
    @FilterColumn(_TYPE)
    private String type;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;

    public String getBranchCode() {
        return branchCode;
    }
    public String getProductCode() {
        return productCode;
    }
    public String getType() {
        return type;
    }
    public Long getUserId() {
        return userId;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public void setType(String type) {
        this.type = type;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
