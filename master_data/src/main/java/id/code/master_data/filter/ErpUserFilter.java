package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName.*;

public class ErpUserFilter extends BaseFilter {

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    @JsonProperty(value = _AVAILABLE)
    @FilterColumn(columnName = _AVAILABLE)
    private Integer available;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;

    public String getBranchCode() {
        return branchCode;
    }
    public String getName() {
        return name;
    }
    public Integer getAvailable() {
        return available;
    }
    public Long getUserId() {
        return userId;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setAvailable(Integer available) {
        this.available = available;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
