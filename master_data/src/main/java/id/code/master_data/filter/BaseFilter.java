package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class BaseFilter {
    @JsonProperty(value = _ID)
    @FilterColumn(columnName = _ID , comparator = WhereComparator.EQUALS)
    private Long id;

    @JsonProperty(value = _LAST_MODIFIED)
    @FilterColumn(columnName = _MODIFIED, paramName = _LAST_MODIFIED, comparator = WhereComparator.GREATER_THAN)
    private Long lastDateModified;

    // 0 = CMS - FULL
    // 1 = SPG / MONICA - EVENT CURRENT USER; AUDIT TRAIL = EVENT AND SUMMARY
    // 2 = APOLLO / ??

    @JsonProperty(value = _APPLICATION_TYPE)
    @FilterColumn(paramName = _APPLICATION_TYPE, includeInQuery = false)
    private Integer applicationType;

    public Long getId() { return id;}
    public Long getLastDateModified() {
        return lastDateModified;
    }
    public Integer getApplicationType() {
        return applicationType;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public void setApplicationType(Integer applicationType) {
        this.applicationType = applicationType;
    }
    public void setLastDateModified(Long lastDateModified) {
        this.lastDateModified = lastDateModified;
    }


}