package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName._ROLE_ID;

/**
 * Created by CODE.ID on 8/22/2017.
 */
public class RoleItemFilter extends BaseFilter {
    @JsonProperty(value = _ROLE_ID)
    @FilterColumn(columnName = _ROLE_ID)
    private Long roleId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}
