package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.component.utility.StringUtility;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/16/2017.
 */
public class ProductFilter extends BaseFilter {
    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    @JsonProperty(value = _BRAND_CODE)
    @FilterColumn(columnName =_BRAND_CODE)
    private String brandCode;

    @JsonProperty(value = _TYPE)
    @FilterColumn(paramName = _TYPE, includeInQuery = false)
    private String type;

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(paramName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonIgnore
    private Long userId;

    public String getBrandCode() {
        return brandCode;
    }
    public String getName() {
        return name;
    }
    public String getType() {
        return type;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public Long getUserId() {
        return userId;
    }


    public void setBrandCode(String brandCode) {
        this.brandCode = (brandCode != null) ? StringUtility.trimNotNull(brandCode).toUpperCase() : null;
    }
    public void setName(String name) {
        this.name = (name != null) ? StringUtility.trimNotNull(name).toUpperCase() : null;
    }
    public void setType(String type) {
        this.type = (type != null) ? StringUtility.trimNotNull(type).toUpperCase() : null;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
