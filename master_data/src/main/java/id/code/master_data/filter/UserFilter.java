package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.component.utility.StringUtility;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class UserFilter extends BaseFilter {

    @JsonProperty(value = _SUPERVISOR_ID)
    @FilterColumn(columnName = _SUPERVISOR_ID)
    private Long supervisorId;

    @JsonProperty(value = _ROLE_ID)
    @FilterColumn(columnName = _ROLE_ID)
    private Long roleId;

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    @JsonProperty(value = _APOLLO_USER)
    @FilterColumn(columnName = _APOLLO_USER, includeInQuery = false)
    private boolean apolloUser;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;

    public Long getRoleId() {
        return roleId;
    }
    public Long getUserId() {
        return userId;
    }
    public Long getSupervisorId() {
        return supervisorId;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getName() { return name; }
    public boolean isApolloUser() {
        return apolloUser;
    }
    public boolean getApolloUser() {
        return apolloUser;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setSupervisorId(Long supervisorId) {
        this.supervisorId = supervisorId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setName(String name) {
        this.name = (name != null) ? StringUtility.trimNotNull(name).toUpperCase() : null;
    }
    public void setApolloUser(boolean apolloUser) {
        this.apolloUser = apolloUser;
    }
}
