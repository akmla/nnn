package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName._REPORT_NAME;

public class ReportFilter extends BaseFilter{
    @JsonProperty(value = _REPORT_NAME)
    @FilterColumn(value = _REPORT_NAME, includeInQuery = false)
    private String reportName;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }
}
