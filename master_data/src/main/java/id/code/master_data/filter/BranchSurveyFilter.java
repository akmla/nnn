package id.code.master_data.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName.*;

public class BranchSurveyFilter extends BaseFilter {

    @JsonProperty(_BRANCH_CODE)
    @FilterColumn(value = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(_SURVEY_ID)
    @FilterColumn(_SURVEY_ID)
    private Long surveyId;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;

    public Long getUserId() {
        return userId;
    }
    public Long getSurveyId() {
        return surveyId;
    }
    public String getBranchCode() {
        return branchCode;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}
