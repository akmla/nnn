package id.code.master_data.filter;

import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;

import static id.code.master_data.AliasName._APPLICATION_TYPE;
import static id.code.master_data.AliasName._CREATED;

/**
 * Created by CODE.ID on 8/10/2017.
 */
class AuditTrailFilter {
    private static final String _LAST_DATE_CREATED = "last_date_created";
    private static final String _REMOVE_UNNECESSARY = "remove_unnecessary";

    @FilterColumn(columnName = _CREATED, paramName = _LAST_DATE_CREATED, comparator = WhereComparator.GREATER_THAN)
    private Long lastDateCreated;

    @FilterColumn(paramName = _REMOVE_UNNECESSARY, includeInQuery = false)
    private Boolean removeUnnecessary;

    // event, event summary;
    // 0 = CMS - FULL
    // 1 = SPG / MONICA - EVENT CURRENT USER; AUDIT TRAIL = EVENT AND SUMMARY
    // 2 = APOLLO / ??
    @FilterColumn(paramName = _APPLICATION_TYPE, includeInQuery = false)
    private int applicationType;

    public boolean isRemoveUnnecessary() {
        if (this.removeUnnecessary == null) {
            return false;
        } else {
            return this.removeUnnecessary;
        }
    }

    public int getApplicationType() { return this.applicationType; }
}
