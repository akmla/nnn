package id.code.master_data.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

import java.util.List;

import static id.code.master_data.AliasName._GROUP_NAME;
import static id.code.master_data.AliasName._VALUE;

/**
 * Created by CODE.ID on 8/21/2017.
 */
public class LookupValidation {
    @JsonProperty(value = _VALUE)
    @ValidateColumn(name = _VALUE)
    private List<String> value;

    @JsonProperty(value = _GROUP_NAME)
    @ValidateColumn(name = _GROUP_NAME)
    private String groupName;

    public List<String> getValue() {
        return value;
    }
    public String getGroupName() {
        return groupName;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }
}
