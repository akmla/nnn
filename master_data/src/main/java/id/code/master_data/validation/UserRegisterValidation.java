package id.code.master_data.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.user.UserModel;

import static id.code.master_data.AliasName._EMAIL;
import static id.code.master_data.AliasName._REQUEST_ID;

/**
 * Created by CODE.ID on 8/14/2017.
 */
public class UserRegisterValidation extends UserModel {

    @ValidateColumn(name = _REQUEST_ID, required = false)
    @JsonProperty(value = _REQUEST_ID)
    private long requestId;

    @JsonProperty(value = _EMAIL)
    @ValidateColumn(name = _EMAIL, required = false)
    private String email;

}
