package id.code.master_data.api.handler.branch;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.branch.SubBranchFacade;
import id.code.master_data.filter.SubBranchFilter;
import id.code.master_data.model.branch.SubBranchModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL_BRANCH;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/10/2017.
 */

public class SubBranchHandler extends RouteApiHandler<UserClaim> {
    private final SubBranchFacade subBranchFacade = new SubBranchFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<SubBranchFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL_BRANCH)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<SubBranchModel> subBranches = this.subBranchFacade.getSubBranches(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, subBranches, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final SubBranchModel data = this.subBranchFacade.getSubBranch(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

}