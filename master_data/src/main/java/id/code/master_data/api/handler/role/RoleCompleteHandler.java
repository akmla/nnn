package id.code.master_data.api.handler.role;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.roles.RoleItemFacade;
import id.code.master_data.filter.RoleItemFilter;
import id.code.master_data.model.role.RoleItemModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.Collection;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class RoleCompleteHandler extends RouteApiHandler<UserClaim> {
    private final RoleItemFacade roleItemFacade = new RoleItemFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<RoleItemFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final Collection<RoleItemModel> roleDetails = this.roleItemFacade.getRoleCompletes(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, roleDetails, filter).setInfo(role));
    }

}
