package id.code.master_data.api.handler.user;

import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.filter.UserFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerDelete;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.master_data.security.Role._ROLE_ID_TL_PROMOSI;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/9/2017.
 */
public class UserHandler extends RouteApiHandler<UserClaim> {
    private final UserFacade userFacade = new UserFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<UserFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
            if (serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_TL_PROMOSI) {
                filter.getParam().setSupervisorId(serverExchange.getAccessTokenPayload().getUserId());
            }
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<UserModel> items = this.userFacade.getAllUser(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final UserModel data = this.userFacade.getUserReal(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final UserModel data = this.userFacade.getUser(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.userFacade.delete(data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody UserModel newData, Long id) throws Exception {
        final UserModel oldData;
        ApiResponse cache;

        if ((oldData = this.userFacade.getUser(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((cache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, cache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.setPassword(oldData.getPassword());
            newData.setFcmToken(oldData.getFcmToken());
            newData.setCreated(oldData.getCreated());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim and upper data
            newData.setCode(StringUtility.trimNotNull(newData.getCode()).toUpperCase());
            newData.setEmail(StringUtility.trimNotNull(newData.getEmail()).toUpperCase());
            newData.setGender(StringUtility.trimNotNull(newData.getGender()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.userFacade.update(UserModel.class, newData, auditTrail)) {
                super.getAccessTokenValidator().saveClaimCache(new UserClaim(newData));
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }
}