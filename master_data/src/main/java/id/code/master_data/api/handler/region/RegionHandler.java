package id.code.master_data.api.handler.region;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.region.RegionFacade;
import id.code.master_data.filter.RegionFilter;
import id.code.master_data.model.region.RegionModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class RegionHandler extends RouteApiHandler<UserClaim> {
    private final RegionFacade regionFacade = new RegionFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<RegionFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<RegionModel> regions = this.regionFacade.getRegions(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, regions, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final RegionModel data = this.regionFacade.getRegion(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }
}