package id.code.master_data.api.handler.user;

import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.user.BranchUserFacade;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/15/2017.
 */
public class CurrentUserHandler extends RouteApiHandler<UserClaim> {
    private final UserFacade userFacade = new UserFacade();
    private final BranchUserFacade branchUserFacade = new BranchUserFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange) throws Exception {
        final UserModel user = this.userFacade.getUser(serverExchange.getAccessTokenPayload().getUserId());
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (user == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else {
            user.setRole(role);

            if (user.getBranchCode().equalsIgnoreCase(_ALL)) {
                final List<BranchViewModel> branches = branchUserFacade.getBranchUsersByUserId(user.getId());
                user.setBranches(branches);
            }

            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, user).setInfo(role));
        }
    }
}
