package id.code.master_data.api;

import id.code.component.utility.StringUtility;
import id.code.master_data.model.user.UserModel;
import id.code.server.ApiClaim;

import java.util.Map;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/9/2017.
 */
@SuppressWarnings("unchecked")
public class UserClaim extends ApiClaim {
    private final long userId;
    private final long roleId;
    private final String fcmToken;
    private final String authorizationId;
    private final String branchCode;
    private final String code;

    public long getUserId() { return this.userId; }
    public long getRoleId() { return this.roleId; }
    public String getFcmToken() { return fcmToken; }
    public String getAuthorizationId() { return authorizationId; }
    public String getBranchCode() { return branchCode; }
    public String getCode() { return code; }
    @Override public boolean isEmpty() { return this.userId == 0; }

    public UserClaim(UserModel user) {
        super(String.valueOf(user.getId()), user.getName());
        this.branchCode = user.getBranchCode();
        this.fcmToken = user.getFcmToken();
        this.authorizationId = user.getAuthorizationId();
        this.userId = user.getId();
        this.roleId = user.getRoleId();
        this.code = user.getCode();
    }

    public UserClaim(Map claims) {
        super(claims);
        this.branchCode = claims.containsKey(_BRANCH_CODE) ? String.valueOf(claims.get(_BRANCH_CODE)) : null;
        this.fcmToken = claims.containsKey(_FCM_TOKEN) ? String.valueOf(claims.get(_FCM_TOKEN)) : null;
        this.authorizationId = claims.containsKey(_AUTHORIZATION_ID) ? String.valueOf(claims.get(_AUTHORIZATION_ID)) : null;
        this.code = (claims.containsKey(_CODE) ? String.valueOf(claims.get(_CODE)) : null);
        this.userId = StringUtility.getLong(super.getClaimId());
        this.roleId = StringUtility.getLong(claims.containsKey(_ROLE_ID) ? String.valueOf(claims.get(_ROLE_ID)) : null);
    }

    @Override
    public Map toMap() {
        final Map map = super.toMap();
        map.put(_BRANCH_CODE, StringUtility.isNullOrWhiteSpace(this.branchCode) ? null : this.branchCode);
        map.put(_FCM_TOKEN, StringUtility.isNullOrWhiteSpace(this.fcmToken) ? null : this.fcmToken);
        map.put(_AUTHORIZATION_ID, StringUtility.isNullOrWhiteSpace(this.authorizationId) ? null : this.authorizationId);
        map.put(_CODE, StringUtility.isNullOrWhiteSpace(this.code) ? null : this.code);
        map.put(_ROLE_ID, StringUtility.getLong(this.roleId));
        return map;
    }
}