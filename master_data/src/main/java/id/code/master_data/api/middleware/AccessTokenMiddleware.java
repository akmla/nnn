package id.code.master_data.api.middleware;

import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.user.BranchUserFacade;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.user.UserModel;
import id.code.server.AccessTokenValidator;
import id.code.server.ApiHandler;
import id.code.server.ServerExchange;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.MonicaResponse.RESPONSE_MULTIPLE_LOGIN;

/**
 * Created by CODE.ID on 8/9/2017.
 */
public class AccessTokenMiddleware extends AccessTokenValidator<UserClaim> {
    private final UserFacade userFacade;
    private final BranchUserFacade branchUserFacade;

    public AccessTokenMiddleware(Properties properties) {
        super(properties);
        this.userFacade = new UserFacade();
        this.branchUserFacade = new BranchUserFacade();
    }

    @Override
    public UserClaim getValidatedClaim(UserClaim requestedClaim) throws Exception {
        final UserModel user = this.userFacade.getUser(requestedClaim.getUserId());

        if (user.getBranchCode().equalsIgnoreCase(_ALL)) {
            final List<BranchViewModel> branches = branchUserFacade.getBranchUsersByUserId(user.getId());
            user.setBranches(branches);
        }

        return new UserClaim(user);
    }

    @Override
    public UserClaim getRequestedClaim(Map claims) throws Exception {
        return new UserClaim(claims);
    }

    @Override
    public boolean isMatch(UserClaim fromClient, UserClaim fromServer, ApiHandler<UserClaim> handler, ServerExchange<UserClaim> exchange, boolean sendResponse) throws Exception {
        if (fromClient.getAuthorizationId() == null) {
            return super.isMatch(fromClient, fromServer, handler, exchange, sendResponse);
        } else if ((!fromClient.getAuthorizationId().equals(fromServer.getAuthorizationId())) && (!fromClient.getAuthorizationId().equals("null"))) {
            handler.sendResponse(exchange, RESPONSE_MULTIPLE_LOGIN);
            return false;
        } else {
            return super.isMatch(fromClient, fromServer, handler, exchange, sendResponse);
        }
    }
}