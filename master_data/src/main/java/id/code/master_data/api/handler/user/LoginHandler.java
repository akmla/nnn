package id.code.master_data.api.handler.user;

import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.user.BranchUserFacade;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.security.Role;
import id.code.master_data.validation.LoginValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.MonicaResponse.RESPONSE_INVALID_USER_LOGIN;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/9/2017.
 */
public class LoginHandler extends RouteApiHandler<UserClaim> {
    private final UserFacade userFacade = new UserFacade();
    private final BranchUserFacade branchUserFacade = new BranchUserFacade();

    @HandlerPost(authorizeAccess = false, bypassAllMiddleware = true)
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody LoginValidation newData) throws Exception {
        UserModel user;

        if ((((user = this.userFacade.login(newData.getCode().toUpperCase(), newData.getPassword())) == null) || (user.getRoleId() == Role._ROLE_ID_SPG))
                || (user.getRoleId() == Role._ROLE_ID_TL_SPG) // comment it for monica build !!!
                || (user.getRoleId() == Role._ROLE_ID_MERCHANDISER && newData.getAuthorizationId() == null)) {
            super.sendResponse(serverExchange, RESPONSE_INVALID_USER_LOGIN);
        } else {
            Role.invalidateRole(user.getId());
            user.setLastLoginTime(System.currentTimeMillis());
            user.setFcmToken(newData.getFcmToken());
            user.setAuthorizationId(newData.getAuthorizationId());

            userFacade.update(user);

            if (user.getRoleId() == Role._ROLE_ID_HO_IT) {
                user.setRoleId(Role._ROLE_ID_HO);
            }

            final UserClaim userClaim = new UserClaim(user);
            user.setToken(super.getAccessTokenValidator().generateAccessToken(userClaim));
            user.setRole(Role.getCompleteRole(user.getId()));

            if (user.getBranchCode().equalsIgnoreCase(_ALL)) {
                final List<BranchViewModel> branches = branchUserFacade.getBranchUsersByUserId(user.getId());
                user.setBranches(branches);
            }

            super.getAccessTokenValidator().saveClaimCache(userClaim);
            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, user));
        }
    }
}
