package id.code.master_data.api.handler.product;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.product.ProductFacade;
import id.code.master_data.filter.ProductFilter;
import id.code.master_data.model.product.ProductModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.AliasName._ALL_BRANCH;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/19/2017.
 */
public class ProductHandler extends RouteApiHandler<UserClaim> {
    private final ProductFacade productFacade = new ProductFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ProductFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (filter.getParam().getBranchCode() == null && serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_HO) {
            filter.getParam().setBranchCode(_ALL);
        } else if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL_BRANCH)) {
            filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
        } else {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        }

        final List<ProductModel> products = this.productFacade.getProducts(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, products, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ProductModel data = this.productFacade.getProduct(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

}