package id.code.master_data;

import id.code.component.AppLogger;
import id.code.component.utility.DateUtility;
import id.code.database.builder.QueryBuilderException;
import id.code.master_data.facade.setting.SettingFacade;
import id.code.master_data.facade.user.UserFacade;
import id.code.master_data.security.Role;
import org.apache.tika.io.IOUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/18/2017.
 */

public class MailSender {
    private final SettingFacade settingFacade = new SettingFacade();
    private final UserFacade userFacade = new UserFacade();

    private Session session = null;
    private String sender, password;
    private StringBuilder contentEmail;
    private Long lastGetData;

    public void sendEmail(String toAddress, String tokenResetPassword) throws IOException, QueryBuilderException, SQLException {
        final String url = this.settingFacade.getSetting("MAIL_URL_RESET").getValue();

        final String completeUrlResetPassword = url.concat(tokenResetPassword);

        final Properties properties = new Properties();

        properties.put(_PROPERTIES_EMAIL_SMTP_AUTH, true);
        properties.put(_PROPERTIES_EMAIL_SMTP_START_TLS, true);
        properties.put(_PROPERTIES_EMAIL_SUBJECT, this.settingFacade.getSetting("MAIL_SUBJECT").getValue());
        properties.put(_PROPERTIES_EMAIL_SMTP_HOST, this.settingFacade.getSetting("MAIL_SMTP_HOST").getValue());
        properties.put(_PROPERTIES_EMAIL_SMTP_PORT, this.settingFacade.getSetting("MAIL_SMTP_PORT").getValue());
        this.sender = this.settingFacade.getSetting("MAIL_USERNAME").getValue();
        this.password = this.settingFacade.getSetting("MAIL_PASSWORD").getValue();

        final String applicationName;

        if (userFacade.getRoleIdByEmail(toAddress.toUpperCase()) <= Role._ROLE_ID_TL_SPG) {
            applicationName = this.settingFacade.getSetting("MONICA_APPLICATION_NAME").getValue(); //"Application Control Outlet (APOLLO)";
        } else {
            applicationName = this.settingFacade.getSetting("APOLLO_APPLICATION_NAME").getValue();// "Monitoring Customer Application (MONICA)";
        }

        if (this.contentEmail == null || ((this.lastGetData + 3600000) < System.currentTimeMillis())) {
            try (final FileInputStream inputStream = new FileInputStream("content-email.txt")) {
                this.contentEmail = new StringBuilder(IOUtils.toString(inputStream));
                this.lastGetData = System.currentTimeMillis();
                AppLogger.writeInfo("Last get data: " + DateUtility.toLocalDateTime(this.lastGetData));
                AppLogger.writeInfo("Content Email: " + this.contentEmail);
            }
        }

        String finalContentEmail = this.contentEmail.toString().replaceAll("<email_receiver>", toAddress);
        finalContentEmail = finalContentEmail.replaceAll("<url_link>", completeUrlResetPassword);
        finalContentEmail = finalContentEmail.replaceAll("<application_name>", applicationName);

        if (session == null) {
            session = Session.getInstance(properties,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(sender, password);
                        }
                    });
        }

        try {
            session.setDebug(true);
            final Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(sender));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toAddress));
            message.setSubject(properties.getProperty(_PROPERTIES_EMAIL_SUBJECT));

            final MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(finalContentEmail, "text/html");

            final Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);
            message.setContent(multipart);

            Transport.send(message);

            AppLogger.writeInfo("Email sent to: " + toAddress);
            AppLogger.writeInfo("Messages: " + finalContentEmail);
        } catch (MessagingException e) {
            AppLogger.writeError(e.getMessage(), e);
            e.printStackTrace();
        }

    }

}
