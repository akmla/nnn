package id.code.monica.model.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.branch.BranchViewModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_EVENTS)
public class EventModel extends BaseModel {

	@TableColumn(name = _BRANCH_CODE)
	@JsonProperty(value = _BRANCH_CODE)
	@ValidateColumn(name = _BRANCH_CODE)
	private String branchCode;

	@TableColumn(name = _BRAND_CODE)
	@JsonProperty(value = _BRAND_CODE)
	@ValidateColumn(name = _BRAND_CODE)
	private String brandCode;

	@JsonProperty(value = _BRAND_NAME)
	private String brandName;

	@JsonProperty(value = _SUPERVISOR_NAME)
	private String supervisorName;

	@JsonProperty(value = _SUPERVISOR_ID)
    @TableColumn(name = _SUPERVISOR_ID)
	private long supervisorId;

	@TableColumn(name = _PAP_NO)
	@JsonProperty(value = _PAP_NO)
	private String papNo;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	@ValidateColumn(name = _NAME)
	private String name;

	@TableColumn(name = _START_DATE)
	@JsonProperty(value = _START_DATE)
	@ValidateColumn(name = _START_DATE)
	private long startDate;

	@TableColumn(name = _FINISH_DATE)
	@JsonProperty(value = _FINISH_DATE)
	@ValidateColumn(name = _FINISH_DATE)
	private long finishDate;

	@JsonProperty(value = _BRANCH)
	private BranchViewModel branch;

    public String getName() { return name; }
	public String getPapNo() { return papNo; }
	public String getSupervisorName() {
		return supervisorName;
	}
	public String getBrandCode() {
		return brandCode;
	}
	public String getBrandName() {
		return brandName;
	}
	public String getBranchCode() { return branchCode; }
    public long getSupervisorId() {
        return supervisorId;
    }
	public long getStartDate() { return startDate; }
	public long getFinishDate() { return finishDate; }
	public BranchViewModel getBranch() {
		return branch;
	}


	public void setName(String name) { this.name = name; }
    public void setPapNo(String papNo) { this.papNo = papNo; }
	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}
    public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
    public void setBranchCode(String branchCode) { this.branchCode = branchCode; }
    public void setSupervisorId(long supervisorId) {
        this.supervisorId = supervisorId;
    }
    public void setStartDate(long startDate) { this.startDate = startDate; }
    public void setFinishDate(long finishDate) { this.finishDate = finishDate; }
	public void setBranch(BranchViewModel branch) {
		this.branch = branch;
	}

}