package id.code.monica.model.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.AliasName;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/19/2017.
 */
@Table(name = AliasName._TABLE_NAME_PACKAGES_PRODUCTS)
public class PackageProductModel extends BaseModel {

    @JsonProperty(value = _PACKAGE_ID)
    @TableColumn(name = _PACKAGE_ID)
    private long packageId;

    @JsonProperty(value = _PRODUCT_CODE)
    @TableColumn(name = _PRODUCT_CODE)
    private String productCode;

    @JsonProperty(value = _QUANTITY)
    @TableColumn(name = _QUANTITY)
    private int quantity;

    @JsonProperty(value = _PRODUCT_NAME)
    private String productName;

    @JsonProperty(value = _PACKAGE_NAME)
    private String packageName;

    @JsonProperty(value = _EVENT_ID)
    private long eventId;

    public int getQuantity() {
        return quantity;
    }
    public long getEventId() {
        return eventId;
    }
    public long getPackageId() {
        return packageId;
    }
    public String getPackageName() {
        return packageName;
    }
    public String getProductCode() {
        return productCode;
    }
    public String getProductName() {
        return productName;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public void setEventId(long eventId) {
        this.eventId = eventId;
    }
    public void setPackageId(long packageId) {
        this.packageId = packageId;
    }
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
}
