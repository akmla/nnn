package id.code.monica.model.report;

import com.fasterxml.jackson.annotation.JsonProperty;

import static id.code.master_data.AliasName.*;

public class SpgReportSummaryModel {
    @JsonProperty(value = _TEAM)
    private String team;

    @JsonProperty(value = _PAP_NO)
    private String papNo;

    @JsonProperty(value = _BRANCH_NAME)
    private String branchName;

    @JsonProperty(value = _BRAND_NAME)
    private String brandName;

    @JsonProperty(value = _KABUPATEN_KOTA)
    private String kabupaten;

    @JsonProperty(value = _KECAMATAN)
    private String kecamatan;

    @JsonProperty(value = _TANGGAL)
    private String tanggal;

    @JsonProperty(value = _VENUE)
    private String venue;

    @JsonProperty(value = _KAWASAN)
    private String kawasan;

    @JsonProperty(value = _NAMA_TL)
    private String namaTl;

    @JsonProperty(value = _NAMA_SPG)
    private String namaSpg;

    @JsonProperty(value = _BELI)
    private int buy;

    @JsonProperty(value = _TIDAK_BELI)
    private int notBuy;

    @JsonProperty(value = _CONTACT)
    private int contact;

    @JsonProperty(value = _PAKET_TERJUAL)
    private int packageSold;

    @JsonProperty(value = _ROKOK_TERJUAL)
    private int cigaretteSold;

    public String getTeam() {
        return team;
    }
    public String getPapNo() {
        return papNo;
    }
    public String getBranchName() {
        return branchName;
    }
    public String getBrandName() {
        return brandName;
    }
    public String getKabupaten() {
        return kabupaten;
    }
    public String getKecamatan() {
        return kecamatan;
    }
    public String getTanggal() {
        return tanggal;
    }
    public String getVenue() {
        return venue;
    }
    public String getKawasan() {
        return kawasan;
    }
    public String getNamaTl() {
        return namaTl;
    }
    public String getNamaSpg() {
        return namaSpg;
    }
    public int getBuy() {
        return buy;
    }
    public int getNotBuy() {
        return notBuy;
    }
    public int getContact() {
        return contact;
    }
    public int getPackageSold() {
        return packageSold;
    }
    public int getCigaretteSold() {
        return cigaretteSold;
    }

    public void setTeam(String team) {
        this.team = team;
    }
    public void setPapNo(String papNo) {
        this.papNo = papNo;
    }
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }
    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }
    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
    public void setVenue(String venue) {
        this.venue = venue;
    }
    public void setKawasan(String kawasan) {
        this.kawasan = kawasan;
    }
    public void setNamaTl(String namaTl) {
        this.namaTl = namaTl;
    }
    public void setNamaSpg(String namaSpg) {
        this.namaSpg = namaSpg;
    }
    public void setBuy(int buy) {
        this.buy = buy;
    }
    public void setNotBuy(int notBuy) {
        this.notBuy = notBuy;
    }
    public void setContact(int contact) {
        this.contact = contact;
    }
    public void setPackageSold(int packageSold) {
        this.packageSold = packageSold;
    }
    public void setCigaretteSold(int cigaretteSold) {
        this.cigaretteSold = cigaretteSold;
    }

}
