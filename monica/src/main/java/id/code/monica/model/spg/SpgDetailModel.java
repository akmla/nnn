package id.code.monica.model.spg;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;


@Table(name = _TABLE_NAME_SPG_ITEMS)
public class SpgDetailModel extends BaseModel {

	@TableColumn(name = _USER_ID)
	@JsonProperty(value = _USER_ID)
	private long userId;

	@TableColumn(name = _BIRTH_PLACE)
	@JsonProperty(value = _BIRTH_PLACE)
	@ValidateColumn(name = _BIRTH_PLACE)
	private String birthPlace;

	@TableColumn(name = _BIRTH_DATE)
	@JsonProperty(value = _BIRTH_DATE)
	@ValidateColumn(name = _BIRTH_DATE)
	private long birthDate;

	@TableColumn(name = _HEIGHT)
	@JsonProperty(value = _HEIGHT)
	@ValidateColumn(name = _HEIGHT)
	private double height;

	@TableColumn(name = _WEIGHT)
	@JsonProperty(value = _WEIGHT)
	@ValidateColumn(name = _WEIGHT)
	private double weight;

	public long getUserId() {
		return userId;
	}
	public long getBirthDate() {
		return birthDate;
	}
	public double getHeight() {
		return height;
	}
	public double getWeight() {
		return weight;
	}
	public String getBirthPlace() {
		return birthPlace;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	public void setBirthDate(long birthDate) {
		this.birthDate = birthDate;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
}