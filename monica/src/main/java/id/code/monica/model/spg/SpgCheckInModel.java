package id.code.monica.model.spg;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_SPG_CHECK_IN)
public class SpgCheckInModel extends BaseModel {

	@TableColumn(name = _SPG_ID)
	@JsonProperty(value = _SPG_ID)
	private long spgId;

	@TableColumn(name = _EVENT_ID)
	@JsonProperty(value = _EVENT_ID)
	@ValidateColumn(name = _EVENT_ID)
	private long eventId;

	@TableColumn(name = _CHECK_IN)
	@JsonProperty(value = _CHECK_IN)
	private long checkIn;

	@TableColumn(name = _CHECK_OUT)
	@JsonProperty(value = _CHECK_OUT)
	private Long checkOut;

	@JsonProperty(value = _CHECK_IN_LAT)
	@TableColumn(name = _CHECK_IN_LAT)
	private Double checkInLat;

	@JsonProperty(value = _CHECK_IN_LNG)
	@TableColumn(name = _CHECK_IN_LNG)
	private Double checkInLng;

	@JsonProperty(value = _CHECK_OUT_LAT)
	@TableColumn(name = _CHECK_OUT_LAT)
	private Double checkOutLat;

	@JsonProperty(value = _CHECK_OUT_LNG)
	@TableColumn(name = _CHECK_OUT_LNG)
	private Double checkOutLng;

	@TableColumn(name = _VENUE)
	@JsonProperty(value = _VENUE)
	@ValidateColumn(name = _VENUE)
	private String venue;

	@TableColumn(name = _VENUE_CLASS)
	@JsonProperty(value = _VENUE_CLASS)
	@ValidateColumn(name = _VENUE_CLASS)
	private String venueClass;

	@TableColumn(name = _AREA)
	@JsonProperty(value = _AREA)
	@ValidateColumn(name = _AREA)
	private String area;

	@TableColumn(name = _DISTRICT)
	@JsonProperty(value = _DISTRICT)
	@ValidateColumn(name = _DISTRICT)
	private String district;

	@TableColumn(name = _CITY)
	@JsonProperty(value = _CITY)
	@ValidateColumn(name = _CITY)
	private String city;

	@TableColumn(name = _BUY)
	@JsonProperty(value = _BUY)
	@ValidateColumn(name = _BUY, invalidZeroNumber = false)
	private int buy;

	@TableColumn(name = _NOT_BUY)
	@JsonProperty(value = _NOT_BUY)
	@ValidateColumn(name = _NOT_BUY, invalidZeroNumber = false)
	private int notBuy;

	@TableColumn(name = _CONTACT)
	@JsonProperty(value = _CONTACT)
	@ValidateColumn(name = _CONTACT, invalidZeroNumber = false)
	private int contact;

	@TableColumn(name = _PACKAGE_SOLD)
	@JsonProperty(value = _PACKAGE_SOLD)
	@ValidateColumn(name = _PACKAGE_SOLD, invalidZeroNumber = false)
	private int packageSold;

	@TableColumn(name = _CIGARETTE_SOLD)
	@JsonProperty(value = _CIGARETTE_SOLD)
	@ValidateColumn(name = _CIGARETTE_SOLD, invalidZeroNumber = false)
	private int cigaretteSold;

	public int getBuy() {
		return buy;
	}
	public int getNotBuy() {
		return notBuy;
	}
	public int getContact() {
		return contact;
	}
	public int getPackageSold() {
		return packageSold;
	}
	public int getCigaretteSold() {
		return cigaretteSold;
	}
	public long getSpgId() {
		return spgId;
	}
	public long getCheckIn() {
		return checkIn;
	}
	public long getEventId() {
		return eventId;
	}
	public Long getCheckOut() {
		return checkOut;
	}
	public Double getCheckInLat() {
		return checkInLat;
	}
	public Double getCheckInLng() {
		return checkInLng;
	}
	public Double getCheckOutLat() {
		return checkOutLat;
	}
	public Double getCheckOutLng() {
		return checkOutLng;
	}
	public String getVenue() {
		return venue;
	}
	public String getVenueClass() {
		return venueClass;
	}
	public String getArea() {
		return area;
	}
	public String getDistrict() {
		return district;
	}
	public String getCity() {
		return city;
	}

	public void setBuy(int buy) {
		this.buy = buy;
	}
	public void setNotBuy(int notBuy) {
		this.notBuy = notBuy;
	}
	public void setContact(int contact) {
		this.contact = contact;
	}
	public void setPackageSold(int packageSold) {
		this.packageSold = packageSold;
	}
	public void setCigaretteSold(int cigaretteSold) {
		this.cigaretteSold = cigaretteSold;
	}
	public void setSpgId(long spgId) {
		this.spgId = spgId;
	}
	public void setCheckIn(long checkIn) {
		this.checkIn = checkIn;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public void setCheckOut(Long checkOut) {
		this.checkOut = checkOut;
	}
	public void setCheckInLat(Double checkInLat) {
		this.checkInLat = checkInLat;
	}
	public void setCheckInLng(Double checkInLng) {
		this.checkInLng = checkInLng;
	}
	public void setCheckOutLat(Double checkOutLat) {
		this.checkOutLat = checkOutLat;
	}
	public void setCheckOutLng(Double checkOutLng) {
		this.checkOutLng = checkOutLng;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public void setVenueClass(String venueClass) {
		this.venueClass = venueClass;
	}
}