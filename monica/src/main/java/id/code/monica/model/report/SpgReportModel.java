package id.code.monica.model.report;

import com.fasterxml.jackson.annotation.JsonProperty;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 9/4/2017.
 */
public class SpgReportModel {
    @JsonProperty(value = _TEAM)
    private String team;

    @JsonProperty(value = _PAP_NO)
    private String papNo;

    @JsonProperty(value = _BRANCH_NAME)
    private String branchName;

    @JsonProperty(value = _BRAND_NAME)
    private String brandName;

    @JsonProperty(value = _KABUPATEN_KOTA)
    private String kabupaten;

    @JsonProperty(value = _KECAMATAN)
    private String kecamatan;

    @JsonProperty(value = _TANGGAL)
    private String tanggal;

    @JsonProperty(value = _VENUE)
    private String venue;

    @JsonProperty(value = _KAWASAN)
    private String kawasan;

    @JsonProperty(value = _NAMA_TL)
    private String namaTl;

    @JsonProperty(value = _NAMA_SPG)
    private String namaSpg;

    @JsonProperty(value = _LAT)
    private Double lat;

    @JsonProperty(value = _LNG)
    private Double lng;

    @JsonProperty(value = _RESPONDEN)
    private String responden;

    @JsonProperty(value = _NO_TELP)
    private String noTelepon;

    @JsonProperty(value = _ALAMAT_EMAIL)
    private String alamatEmail;

    @JsonProperty(value = _FACEBOOK)
    private String facebook;

    @JsonProperty(value = _TWITTER)
    private String twitter;

    @JsonProperty(value = _LINE)
    private String line;

    @JsonProperty(value = _INSTAGRAM)
    private String instagram;

    @JsonProperty(value = _UMUR)
    private String umur;

    @JsonProperty(value = _PEKERJAAN)
    private String pekerjaan;

    @JsonProperty(value = _PENGELUARAN)
    private String pengeluaran;

    @JsonProperty(value = _TEMPAT_NONGKRONG_FAVORIT)
    private String tempatNongkrongFavorit;

    @JsonProperty(value = _ROKOK_UTAMA)
    private String rokokUtama;

    @JsonProperty(value = _ROKOK_SELINGAN)
    private String rokokSelingan;

    @JsonProperty(value = _ROKOK_METHOL)
    private String rokokMenthol;

    @JsonProperty(value = _PAKET_PEMBELIAN_1)
    private String paketPembelian1;

    @JsonProperty(value = _PAKET_PEMBELIAN_2)
    private String paketPembelian2;

    @JsonProperty(value = _PAKET_TERJUAL_1)
    private int paketTerjual1;

    @JsonProperty(value = _PAKET_TERJUAL_2)
    private int paketTerjual2;

    @JsonProperty(value = _BUNGKUS)
    private int bungkus;


    public String getPapNo() {
        return papNo;
    }
    public String getBrandName() {
        return brandName;
    }
    public String getBranchName() {
        return branchName;
    }
    public String getKabupaten() {
        return kabupaten;
    }
    public String getKecamatan() {
        return kecamatan;
    }
    public String getTeam() {
        return team;
    }
    public String getTanggal() {
        return tanggal;
    }
    public String getVenue() {
        return venue;
    }
    public String getKawasan() {
        return kawasan;
    }
    public String getNamaTl() {
        return namaTl;
    }
    public String getNamaSpg() {
        return namaSpg;
    }
    public Double getLat() {
        return lat;
    }
    public Double getLng() {
        return lng;
    }
    public String getResponden() {
        return responden;
    }
    public String getNoTelepon() {
        return noTelepon;
    }
    public String getAlamatEmail() {
        return alamatEmail;
    }
    public String getFacebook() {
        return facebook;
    }
    public String getTwitter() {
        return twitter;
    }
    public String getLine() {
        return line;
    }
    public String getInstagram() {
        return instagram;
    }
    public String getUmur() {
        return umur;
    }
    public String getPekerjaan() {
        return pekerjaan;
    }
    public String getPengeluaran() {
        return pengeluaran;
    }
    public String getTempatNongkrongFavorit() {
        return tempatNongkrongFavorit;
    }
    public String getRokokUtama() {
        return rokokUtama;
    }
    public String getRokokSelingan() {
        return rokokSelingan;
    }
    public String getRokokMenthol() {
        return rokokMenthol;
    }
    public String getPaketPembelian1() {
        return paketPembelian1;
    }
    public String getPaketPembelian2() {
        return paketPembelian2;
    }
    public int getPaketTerjual1() {
        return paketTerjual1;
    }
    public int getPaketTerjual2() {
        return paketTerjual2;
    }
    public int getBungkus() {
        return bungkus;
    }


    public void setPapNo(String papNo) {
        this.papNo = papNo;
    }
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }
    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }
    public void setUmur(String umur) {
        this.umur = umur;
    }
    public void setTeam(String team) {
        this.team = team;
    }
    public void setVenue(String venue) {
        this.venue = venue;
    }
    public void setNamaTl(String namaTl) {
        this.namaTl = namaTl;
    }
    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
    public void setKawasan(String kawasan) {
        this.kawasan = kawasan;
    }
    public void setNamaSpg(String namaSpg) {
        this.namaSpg = namaSpg;
    }
    public void setLat(Double lat) {
        this.lat = lat;
    }
    public void setLng(Double lng) {
        this.lng = lng;
    }
    public void setResponden(String responden) {
        this.responden = responden;
    }
    public void setNoTelepon(String noTelepon) {
        this.noTelepon = noTelepon;
    }
    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }
    public void setRokokUtama(String rokokUtama) {
        this.rokokUtama = rokokUtama;
    }
    public void setPengeluaran(String pengeluaran) {
        this.pengeluaran = pengeluaran;
    }
    public void setTempatNongkrongFavorit(String tempatNongkrongFavorit) {
        this.tempatNongkrongFavorit = tempatNongkrongFavorit;
    }
    public void setAlamatEmail(String alamatEmail) {
        this.alamatEmail = alamatEmail;
    }
    public void setRokokMenthol(String rokokMenthol) {
        this.rokokMenthol = rokokMenthol;
    }
    public void setRokokSelingan(String rokokSelingan) {
        this.rokokSelingan = rokokSelingan;
    }
    public void setPaketPembelian1(String paketPembelian1) {
        this.paketPembelian1 = paketPembelian1;
    }
    public void setPaketPembelian2(String paketPembelian2) {
        this.paketPembelian2 = paketPembelian2;
    }
    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }
    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }
    public void setLine(String line) {
        this.line = line;
    }
    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }
    public void setBungkus(int bungkus) {
        this.bungkus = bungkus;
    }
    public void setPaketTerjual1(int paketTerjual1) {
        this.paketTerjual1 = paketTerjual1;
    }
    public void setPaketTerjual2(int paketTerjual2) {
        this.paketTerjual2 = paketTerjual2;
    }
}
