package id.code.monica.model.summary;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.user.UserViewModel;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/16/2017.
 */
@Table(name = _TABLE_NAME_DAILY_SUMMARY)
public class DailySummaryModel extends BaseModel {

    @JsonProperty(value = _SUMMARY_DATE)
    @TableColumn(name = _SUMMARY_DATE)
    private long summaryDate;

    @JsonProperty(value = _SPG_ID)
    @TableColumn(name = _SPG_ID)
    @ValidateColumn(name = _SPG_ID)
    private long spgId;

    @JsonProperty(value = _BUY)
    @TableColumn(name = _BUY)
    private int buy;

    @JsonProperty(value = _NOT_BUY)
    @TableColumn(name = _NOT_BUY)
    private int notBuy;

    @JsonProperty(value = _CONTACT)
    @TableColumn(name = _CONTACT)
    private int contact;

    @JsonProperty(value = _PACKAGE_SOLD)
    @TableColumn(name = _PACKAGE_SOLD)
    private int packageSold;

    @JsonProperty(value = _CIGARETTE_SOLD)
    @TableColumn(name = _CIGARETTE_SOLD)
    private int cigaretteSold;

    @JsonProperty(value = _CHECK_IN_TIMES)
    @TableColumn(name = _CHECK_IN_TIMES)
    private int checkInTimes;

    @JsonProperty(value = _SPG)
    private UserViewModel spg;

    public long getSummaryDate() {
        return summaryDate;
    }
    public long getSpgId() {
        return spgId;
    }
    public int getBuy() {
        return buy;
    }
    public int getNotBuy() {
        return notBuy;
    }
    public int getContact() {
        return contact;
    }
    public int getPackageSold() {
        return packageSold;
    }
    public int getCigaretteSold() {
        return cigaretteSold;
    }
    public int getCheckInTimes() {
        return checkInTimes;
    }
    public UserViewModel getSpg() {
        return spg;
    }

    public void setSummaryDate(long summaryDate) {
        this.summaryDate = summaryDate;
    }
    public void setSpgId(long spgId) {
        this.spgId = spgId;
    }
    public void setBuy(int buy) {
        this.buy = buy;
    }
    public void setNotBuy(int notBuy) {
        this.notBuy = notBuy;
    }
    public void setContact(int contact) {
        this.contact = contact;
    }
    public void setPackageSold(int packageSold) {
        this.packageSold = packageSold;
    }
    public void setCigaretteSold(int cigaretteSold) {
        this.cigaretteSold = cigaretteSold;
    }
    public void setCheckInTimes(int checkInTimes) {
        this.checkInTimes = checkInTimes;
    }
    public void setSpg(UserViewModel spg) {
        this.spg = spg;
    }
}
