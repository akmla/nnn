package id.code.monica.model.contact;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_CONTACTS)
public class ContactModel extends BaseModel {
	@JsonProperty(value = _EVENT_ID)
	@ValidateColumn(name = _EVENT_ID)
	private long eventId;

	@JsonProperty(value = _CHECK_IN)
	@ValidateColumn(name = _CHECK_IN)
	private long checkIn;

	@JsonProperty(value = _SPG_ID)
	@TableColumn(name = _SPG_ID)
	private long spgId;

	@JsonProperty(value = _SPG_CHECK_IN_ID)
	@TableColumn(name = _SPG_CHECK_IN_ID)
	private long spgCheckInId;

	@JsonProperty(value = _LAT)
	@TableColumn(name = _LAT)
	private Double lat;

	@JsonProperty(value = _LNG)
	@TableColumn(name = _LNG)
	private Double lng;

	@JsonProperty(value = _NAME)
	@TableColumn(name = _NAME)
	private String name;

	@JsonProperty(value = _NICK_NAME)
	@TableColumn(name = _NICK_NAME)
	private String nickName;

	@JsonProperty(value = _BIRTH_PLACE)
	@TableColumn(name = _BIRTH_PLACE)
	private String birthPlace;

	@JsonProperty(value = _BIRTH_DATE)
	@TableColumn(name = _BIRTH_DATE)
	private Long birthDate;

	@JsonProperty(value = _JOB)
	@TableColumn(name = _JOB)
	private String job;

	@JsonProperty(value = _SPENDING)
	@TableColumn(name = _SPENDING)
	private String spending;

	@JsonProperty(value = _EMAIL)
	@TableColumn(name = _EMAIL)
	private String email;

	@JsonProperty(value = _PHONE)
	@TableColumn(name = _PHONE)
	private String phone;

	@JsonProperty(value = _TWITTER)
	@TableColumn(name = _TWITTER)
	private String twitter;

	@JsonProperty(value = _FACEBOOK)
	@TableColumn(name = _FACEBOOK)
	private String facebook;

	@JsonProperty(value = _INSTAGRAM)
	@TableColumn(name = _INSTAGRAM)
	private String instagram;

	@JsonProperty(value = _LINE)
	@TableColumn(name = _LINE)
	private String line;

	@JsonProperty(value = _LOUNGE)
	@TableColumn(name = _LOUNGE)
	private String lounge;

	@JsonProperty(value = _PRIMARY_CIGARETTE)
	@TableColumn(name = _PRIMARY_CIGARETTE)
	private String primaryCigarette;

	@JsonProperty(value = _SECONDARY_CIGARETTE)
	@TableColumn(name = _SECONDARY_CIGARETTE)
	private String secondaryCigarette;

	@JsonProperty(value = _MENTHOL_CIGARETTE)
	@TableColumn(name = _MENTHOL_CIGARETTE)
	private String mentholCigarette;

	@JsonProperty(value = _PACKAGE_TYPE_1)
	@TableColumn(name = _PACKAGE_TYPE_1)
	private String packageType1;

	@JsonProperty(value = _PACKAGE_TYPE_2)
	@TableColumn(name = _PACKAGE_TYPE_2)
	private String packageType2;

	@JsonProperty(value = _PACKAGE_SOLD_1)
	@TableColumn(name = _PACKAGE_SOLD_1)
	private int packageSold1;

	@JsonProperty(value = _PACKAGE_SOLD_2)
	@TableColumn(name = _PACKAGE_SOLD_2)
	private int packageSold2;

	@JsonProperty(value = _CIGARETTE_SOLD)
	@TableColumn(name = _CIGARETTE_SOLD)
	private int cigaretteSold;

	@JsonProperty(value = _CAN_CONTACT)
	@TableColumn(name = _CAN_CONTACT)
	private int canContact;

	@JsonProperty(value = _REMARK)
	@TableColumn(name = _REMARK)
	private String remarks;

	public long getSpgId() {
		return spgId;
	}
	public long getEventId() { return eventId; }
	public long getCheckIn() { return checkIn; }
	public long getSpgCheckInId() {
		return spgCheckInId;
	}
	public Double getLat() {
		return lat;
	}
	public Double getLng() {
		return lng;
	}
	public Long getBirthDate() {
		return birthDate;
	}
	public String getBirthPlace() {
		return birthPlace;
	}
	public String getName() {
		return name;
	}
	public String getNickName() {
		return nickName;
	}
	public String getJob() {
		return job;
	}
	public String getSpending() {
		return spending;
	}
	public String getEmail() {
		return email;
	}
	public String getTwitter() {
		return twitter;
	}
	public String getFacebook() {
		return facebook;
	}
	public String getInstagram() {
		return instagram;
	}
	public String getLine() {
		return line;
	}
	public String getLounge() {
		return lounge;
	}
	public String getPrimaryCigarette() {
		return primaryCigarette;
	}
	public String getSecondaryCigarette() {
		return secondaryCigarette;
	}
	public String getMentholCigarette() {
		return mentholCigarette;
	}
	public String getPackageType1() {
		return packageType1;
	}
	public String getPackageType2() {
        return packageType2;
    }
	public String getRemarks() {
		return remarks;
	}
	public String getPhone() {
		return phone;
	}
	public int getPackageSold1() {
		return packageSold1;
	}
	public int getPackageSold2() {
		return packageSold2;
	}
	public int getCigaretteSold() {
		return cigaretteSold;
	}
	public int getCanContact() {
		return canContact;
	}


	public void setSpgId(long spgId) {
		this.spgId = spgId;
	}
	public void setSpgCheckInId(long spgCheckInId) {
		this.spgCheckInId = spgCheckInId;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	public void setCanContact(int canContact) {
		this.canContact = canContact;
	}
	public void setPackageSold1(int packageSold1) {
		this.packageSold1 = packageSold1;
	}
	public void setPackageSold2(int packageSold2) {
		this.packageSold2 = packageSold2;
	}
	public void setCigaretteSold(int cigaretteSold) {
		this.cigaretteSold = cigaretteSold;
	}
	public void setBirthDate(Long birthDate) {
		this.birthDate = birthDate;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setLounge(String lounge) {
		this.lounge = lounge;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	public void setSpending(String spending) {
		this.spending = spending;
	}
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}
	public void setPackageType1(String packageType1) {
		this.packageType1 = packageType1;
	}
	public void setPackageType2(String packageType2) {
		this.packageType2 = packageType2;
	}
	public void setPrimaryCigarette(String primaryCigarette) {
		this.primaryCigarette = primaryCigarette;
	}
	public void setSecondaryCigarette(String secondaryCigarette) {
		this.secondaryCigarette = secondaryCigarette;
	}
	public void setMentholCigarette(String mentholCigarette) {
		this.mentholCigarette = mentholCigarette;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public void setCheckIn(long checkIn) {
		this.checkIn = checkIn;
	}
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
}