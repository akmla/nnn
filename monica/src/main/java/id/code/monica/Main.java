package id.code.monica;

import id.code.master_data.api.middleware.AccessTokenMiddleware;
import id.code.monica.api.Route;
import id.code.server.ApiServerApplication;

public class Main {
    public static void main(String[] args) {
        createApplication("config.properties").standaloneStart();
    }

    static ApiServerApplication createApplication(String configPath) {
        return new ApiServerApplication()
                .setPropertiesPath(configPath)
                .setRoutes(Route::createRoutes)
                .setAccessTokenMiddleware(AccessTokenMiddleware::new);
    }
}
