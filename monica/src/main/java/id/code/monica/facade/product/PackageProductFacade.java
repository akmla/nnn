package id.code.monica.facade.product;

import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.builder.UpdateResult;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.PackageProductFilter;
import id.code.master_data.filter.ProductFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.product.PackageViewModel;
import id.code.master_data.model.product.ProductModel;
import id.code.monica.model.product.PackageModel;
import id.code.monica.model.product.PackageProductModel;

import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/19/2017.
 */
public class PackageProductFacade extends BaseFacade {

    public List<PackageProductModel> getPackageProducts(Filter<PackageProductFilter> filter, Filter<ProductFilter> filter2) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_PACKAGES_PRODUCTS, PackageProductModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_PACKAGES, PackageViewModel.class).on(_TABLE_NAME_PACKAGES, _ID).isEqual(_TABLE_NAME_PACKAGES_PRODUCTS, _PACKAGE_ID)
                .join(_TABLE_NAME_PRODUCTS, ProductModel.class).on(_TABLE_NAME_PRODUCTS, _CODE).isEqual(_TABLE_NAME_PACKAGES_PRODUCTS, _PRODUCT_CODE)
                .orderBy(_TABLE_NAME_PACKAGES_PRODUCTS, filter)
                .filter(_TABLE_NAME_PACKAGES_PRODUCTS, filter)
                .filter(_TABLE_NAME_PRODUCTS, filter2)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<PackageProductModel> listPackageProduct = new ArrayList<>();
            while (result.moveNext()) {
                final PackageProductModel packageProduct = result.getItem(_TABLE_NAME_PACKAGES_PRODUCTS, PackageProductModel.class);
                final PackageViewModel packageView = result.getItem(_TABLE_NAME_PACKAGES, PackageViewModel.class);
                packageProduct.setEventId(packageView.getEventId());
                packageProduct.setPackageName(packageView.getName());
                packageProduct.setProductName(result.getItem(_TABLE_NAME_PRODUCTS, ProductModel.class).getName());
                listPackageProduct.add(packageProduct);
            }
            return listPackageProduct;
        }
    }

    public PackageProductModel getPackageProduct(long packageProductId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_PACKAGES_PRODUCTS, PackageProductModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_PACKAGES, PackageViewModel.class).on(_TABLE_NAME_PACKAGES, _ID).isEqual(_TABLE_NAME_PACKAGES_PRODUCTS, _PACKAGE_ID)
                .join(_TABLE_NAME_PRODUCTS, ProductModel.class).on(_TABLE_NAME_PRODUCTS, _CODE).isEqual(_TABLE_NAME_PACKAGES_PRODUCTS, _PRODUCT_CODE)
                .where(_TABLE_NAME_PACKAGES_PRODUCTS, _ID).isEqual(packageProductId).limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final PackageProductModel packageProduct = result.getItem(_TABLE_NAME_PACKAGES_PRODUCTS, PackageProductModel.class);
                packageProduct.setEventId(result.getItem(_TABLE_NAME_PACKAGES, PackageViewModel.class).getEventId());
                packageProduct.setPackageName(result.getItem(_TABLE_NAME_PACKAGES, PackageViewModel.class).getName());
                packageProduct.setProductName(result.getItem(_TABLE_NAME_PRODUCTS, ProductModel.class).getName());
                return packageProduct;
            }
            return null;
        }
    }

    public boolean insert(PackageProductModel packageProduct, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult result = QueryBuilder.insert(packageProduct).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_PACKAGES_PRODUCTS, packageProduct);

            if ((result.isModified()) && (super.insertAudit(sqlTransaction, auditTrail))) {
                final ResultBuilder resultPackage = QueryBuilder.select(PackageModel.class)
                        .where(_ID).isEqual(packageProduct.getPackageId()).execute(sqlTransaction);

                if (resultPackage.moveNext()) {
                    final PackageModel oldPackage = resultPackage.getItem(PackageModel.class);
                    oldPackage.modify(packageProduct.getModifiedBy());
                    oldPackage.setTotal(oldPackage.getTotal() + packageProduct.getQuantity());
                    if (QueryBuilder.update(oldPackage).execute(sqlTransaction).isModified()) {
                        sqlTransaction.commitTransaction();
                        return true;
                    }
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean delete(PackageProductModel packageProduct, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult result = QueryBuilder.delete(packageProduct).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_PACKAGES_PRODUCTS, packageProduct);

            if ((result.isModified()) && (super.insertAudit(sqlTransaction, auditTrail))) {
                final ResultBuilder resultPackage = QueryBuilder.select(PackageModel.class)
                        .where(_ID).isEqual(packageProduct.getPackageId()).execute(sqlTransaction);

                if (resultPackage.moveNext()) {
                    final PackageModel oldPackage = resultPackage.getItem(PackageModel.class);
                    oldPackage.modify(packageProduct.getModifiedBy());
                    oldPackage.setTotal(oldPackage.getTotal() - packageProduct.getQuantity());
                    if (QueryBuilder.update(oldPackage).execute(sqlTransaction).isModified()) {
                        sqlTransaction.commitTransaction();
                        return true;
                    }
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}
