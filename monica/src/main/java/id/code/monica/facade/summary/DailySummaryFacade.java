package id.code.monica.facade.summary;

import id.code.component.utility.DateUtility;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.user.UserViewModel;
import id.code.monica.filter.DailySummaryFilter;
import id.code.monica.model.summary.DailySummaryModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;


/**
 * Created by CODE.ID on 8/16/2017.
 */
public class DailySummaryFacade extends BaseFacade {

    public List<DailySummaryModel> getDailySummaries(long spgId, Filter<DailySummaryFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SUMMARY, DailySummaryModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_USERS, UserViewModel.class).on(_TABLE_NAME_USERS, _ID).isEqual(_SUMMARY, _SPG_ID)
                .orderBy(_SUMMARY, filter)
                .filter(_SUMMARY, filter)
                .limitOffset(filter);

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_MONICA) {
            sqlSelect.where(_SUMMARY, _SPG_ID).isEqual(spgId);
            long twoDaysAgo = DateUtility.toUnixMillis(DateUtility.toLocalDate(System.currentTimeMillis()).minusDays(2));
            sqlSelect.where(_SUMMARY, _SUMMARY_DATE).equalsGreaterThan(twoDaysAgo);
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            List<DailySummaryModel> listEventSpg = new ArrayList<>();
            while (result.moveNext()) {
                DailySummaryModel dailySummary = result.getItem(_SUMMARY, DailySummaryModel.class);
                dailySummary.setSpg(result.getItem(_TABLE_NAME_USERS, UserViewModel.class));
                listEventSpg.add(dailySummary);
            }
            return listEventSpg;
        }
    }

    public DailySummaryModel getDailySummary(long summaryId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SUMMARY, DailySummaryModel.class)
                .includeAllJoin()
                .join(_TABLE_NAME_USERS, UserViewModel.class).on(_TABLE_NAME_USERS, _ID).isEqual(_SUMMARY, _SPG_ID)
                .where(_SUMMARY, _ID).isEqual(summaryId).limit(1);
        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                DailySummaryModel dailySummary = result.getItem(_SUMMARY, DailySummaryModel.class);
                dailySummary.setSpg(result.getItem(_TABLE_NAME_USERS, UserViewModel.class));
                return dailySummary;
            }
            return null;
        }
    }

    public DailySummaryModel find(long spgId, long summaryDate) throws SQLException {
        return QueryBuilder.select(_TABLE_NAME_DAILY_SUMMARY, DailySummaryModel.class)
                .where(_SUMMARY_DATE).isEqual(summaryDate)
                .where(_SPG_ID).isEqual(spgId)
                .orderBy(_ID).desc()
                .limit(1)
                .getResult(openConnection())
                .executeItem(DailySummaryModel.class);
    }
}