package id.code.monica.facade.contact;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.monica.filter.ContactSettingFilter;
import id.code.monica.model.contact.ContactSettingModel;

import java.sql.SQLException;
import java.util.List;

import static id.code.master_data.AliasName._CONTACT_SETTING;
import static id.code.master_data.AliasName._ID;

/**
 * Created by CODE.ID on 8/30/2017.
 */
public class ContactSettingFacade extends BaseFacade {

    public List<ContactSettingModel> getContactSettings(Filter<ContactSettingFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_CONTACT_SETTING, ContactSettingModel.class)
                .orderBy(_CONTACT_SETTING, filter)
                .filter(_CONTACT_SETTING, filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result != null) ? result.getItems(_CONTACT_SETTING, ContactSettingModel.class) : null;
        }
    }

    public ContactSettingModel getContactSetting(long contactSettingId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_CONTACT_SETTING, ContactSettingModel.class)
                .where(_CONTACT_SETTING, _ID).isEqual(contactSettingId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(_CONTACT_SETTING, ContactSettingModel.class) : null;
        }
    }
}