package id.code.monica.facade.product;

import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.PackageFilter;
import id.code.master_data.model.user.BranchUserModel;
import id.code.monica.filter.EventJoinFilter;
import id.code.monica.model.event.EventModel;
import id.code.monica.model.event.EventSpgModel;
import id.code.monica.model.product.PackageModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/16/2017.
 */
public class PackageFacade extends BaseFacade {


    public List<PackageModel> getPackages(Long userId, Filter<PackageFilter> filter, Filter<EventJoinFilter> filter2) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_PACKAGES, PackageModel.class)
                .includeAllJoin()
                .join(_EVENT, EventModel.class).on(_TABLE_NAME_PACKAGES, _EVENT_ID).isEqual(_EVENT, _ID)
                .orderBy(_TABLE_NAME_PACKAGES, filter)
                .filter(_TABLE_NAME_PACKAGES, filter)
                .filter(_EVENT, filter2)
                .limitOffset(filter);

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_MONICA) {
            sqlSelect.where(_TABLE_NAME_PACKAGES, _EVENT_ID).in(
                    QueryBuilder.select(_TABLE_NAME_EVENTS_SPG, EventSpgModel.class, _EVENT_ID)
                            .where(_TABLE_NAME_EVENTS_SPG, _SPG_ID).isEqual(userId));
        }

        if (filter2.getParam().getBranchCode() != null) {
            if (filter2.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_EVENT, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE)
                                .where(_USER_ID).isEqual(filter2.getParam().getUserId())
                );
            } else /*if (!filter2.getParam().getBranchCode().equalsIgnoreCase(_ALL)) */ {
                sqlSelect.where(_EVENT, _BRANCH_CODE).isEqual(filter2.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<PackageModel> listPackages = new ArrayList<>();
            while (result.moveNext()) {
                final PackageModel packageModel = result.getItem(_TABLE_NAME_PACKAGES, PackageModel.class);
                packageModel.setEvent(result.getItem(_EVENT, EventModel.class));
                packageModel.setEdit(packageModel.getEvent().getStartDate() > System.currentTimeMillis());
                listPackages.add(packageModel);
            }
            return listPackages;
        }
    }

    public PackageModel getPackage(long packageId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_PACKAGES, PackageModel.class)
                .includeAllJoin()
                .join(_EVENT, EventModel.class).on(_TABLE_NAME_PACKAGES, _EVENT_ID).isEqual(_EVENT, _ID)
                .where(_TABLE_NAME_PACKAGES, _ID).isEqual(packageId).limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final PackageModel packageModel = result.getItem(_TABLE_NAME_PACKAGES, PackageModel.class);
                packageModel.setEvent(result.getItem(_EVENT, EventModel.class));
                packageModel.setEdit(packageModel.getEvent().getStartDate() > System.currentTimeMillis());
                return packageModel;
            }
            return null;
        }
    }

}