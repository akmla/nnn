package id.code.monica.api.handler.spg;

import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.monica.facade.spg.SpgCheckInFacade;
import id.code.monica.filter.SpgCheckInFilter;
import id.code.monica.model.spg.SpgCheckInModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/15/2017.
 */
public class SpgCheckInHandler extends RouteApiHandler<UserClaim> {
    private final SpgCheckInFacade spgCheckInFacade = new SpgCheckInFacade();

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody SpgCheckInModel newData, Long id) throws Exception {
        final SpgCheckInModel oldData;
        ApiResponse responseCache;

        if ((oldData = this.spgCheckInFacade.getSpgCheckIn(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            this.update(serverExchange, oldData, newData, role);
        }
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<SpgCheckInFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<SpgCheckInModel> eventsSpg = this.spgCheckInFacade.getSpgCheckIns(serverExchange.getAccessTokenPayload().getUserId(), filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, eventsSpg, filter).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody SpgCheckInModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final SpgCheckInModel oldData = this.spgCheckInFacade.find(serverExchange.getAccessTokenPayload().getUserId(), newData.getEventId(), newData.getCheckIn());

            if (oldData == null) {
                newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                newData.setSpgId(serverExchange.getAccessTokenPayload().getUserId());

                // To Upper
                newData.setVenue(StringUtility.trimNotNull(newData.getVenue()).toUpperCase());

                final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

                if (this.spgCheckInFacade.insert(SpgCheckInModel.class, newData, auditTrail)) {
                    super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                }
            } else {
                this.update(serverExchange, oldData, newData, role);
            }
        }
    }

    private void update(ServerExchange<UserClaim> serverExchange, SpgCheckInModel oldData, SpgCheckInModel newData, RoleModel role) throws Exception {
        // Assign Old Value
        newData.setId(oldData.getId());
        newData.setCheckIn(oldData.getCheckIn());
        newData.setSpgId(oldData.getSpgId());
        newData.setCreated(oldData.getCreated());
        newData.setCreatedBy(oldData.getCreatedBy());
        newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

        if (this.spgCheckInFacade.update(SpgCheckInModel.class, newData, auditTrail)) {
            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
        }
    }
}