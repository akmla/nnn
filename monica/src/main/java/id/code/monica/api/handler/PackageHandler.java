package id.code.monica.api.handler;

import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.filter.PackageFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.monica.facade.product.PackageFacade;
import id.code.monica.filter.EventJoinFilter;
import id.code.monica.model.product.PackageModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.security.Role.*;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/19/2017.
 */
public class PackageHandler extends RouteApiHandler<UserClaim> {
    private final PackageFacade packageFacade = new PackageFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<PackageFilter> filter) throws Exception {
        final Filter<EventJoinFilter> eventFilter = super.getRequestedFilter(serverExchange, EventJoinFilter.class);

        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final long userId = serverExchange.getAccessTokenPayload().getUserId();
        final long roleId = serverExchange.getAccessTokenPayload().getRoleId();

        if (roleId == _ROLE_ID_TL_SPG) {
            eventFilter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
            eventFilter.getParam().setSupervisorId(userId);
        } else if (roleId == _ROLE_ID_BRANCH || roleId == _ROLE_ID_SPG) {
            eventFilter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (eventFilter.getParam().getBranchCode() != null && eventFilter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                eventFilter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<PackageModel> packages = this.packageFacade.getPackages(userId, filter, eventFilter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, packages, filter, eventFilter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final PackageModel data = this.packageFacade.getPackage(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody PackageModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim and upper data
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());
            newData.setBonus(StringUtility.trimNotNull(newData.getBonus()).toUpperCase());

            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if (this.packageFacade.insert(PackageModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final PackageModel data = this.packageFacade.getPackage(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.packageFacade.delete(PackageModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody PackageModel newData, Long id) throws Exception {
        final PackageModel oldData;
        ApiResponse responseCache;

        if ((oldData = this.packageFacade.getPackage(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.setTotal(oldData.getTotal());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim and upper data
            newData.setBonus(StringUtility.trimNotNull(newData.getBonus()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.packageFacade.update(PackageModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }
}