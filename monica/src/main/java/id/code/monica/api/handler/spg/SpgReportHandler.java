package id.code.monica.api.handler.spg;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.monica.facade.report.SpgReportFacade;
import id.code.monica.filter.EventFilter;
import id.code.monica.filter.SpgCheckInFilter;
import id.code.monica.model.report.CompleteSpgReport;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.master_data.security.Role._ROLE_ID_TL_SPG;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 9/4/2017.
 */
public class SpgReportHandler extends RouteApiHandler<UserClaim> {
    private final SpgReportFacade spgReportFacade = new SpgReportFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<EventFilter> filter) throws Exception {
        final Filter<SpgCheckInFilter> sciFilter = super.getRequestedFilter(serverExchange, SpgCheckInFilter.class);

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else if (serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_TL_SPG) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
            filter.getParam().setSupervisorId(serverExchange.getAccessTokenPayload().getUserId());
        }

        final CompleteSpgReport reports = this.spgReportFacade.getCompleteReports(filter, sciFilter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, reports, filter, sciFilter));
    }
}