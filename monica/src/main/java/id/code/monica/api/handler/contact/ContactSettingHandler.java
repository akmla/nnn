package id.code.monica.api.handler.contact;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.monica.facade.contact.ContactSettingFacade;
import id.code.monica.filter.ContactSettingFilter;
import id.code.monica.model.contact.ContactSettingModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by CODE.ID on 8/31/2017.
 */
public class ContactSettingHandler extends RouteApiHandler<UserClaim> {
    private final ContactSettingFacade contactSettingFacade = new ContactSettingFacade();

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ContactSettingModel data = this.contactSettingFacade.getContactSetting(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody ContactSettingModel newData, Long id) throws Exception {
        final ContactSettingModel oldData;
        ApiResponse responseCache;

        if ((oldData = this.contactSettingFacade.getContactSetting(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.contactSettingFacade.update(ContactSettingModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ContactSettingFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<ContactSettingModel> contactSettings = this.contactSettingFacade.getContactSettings(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, contactSettings, filter).setInfo(role));
    }
}