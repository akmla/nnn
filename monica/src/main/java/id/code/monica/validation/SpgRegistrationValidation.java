package id.code.monica.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.user.UserModel;
import id.code.monica.model.spg.SpgDetailModel;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/18/2017.
 */
public class SpgRegistrationValidation extends UserModel {

    @ValidateColumn(name = _REQUEST_ID, required = false)
    @JsonProperty(value = _REQUEST_ID)
    private long requestId;

    @ValidateColumn(name = _CODE, required = false)
    @JsonProperty(value = _CODE)
    private String code;

    @ValidateColumn(name = _ROLE_ID, required = false)
    @JsonProperty(value = _ROLE_ID)
    private int roleId;

    @JsonProperty(value = _DETAIL)
    private SpgDetailModel detail;

    public SpgDetailModel getDetail() {
        return detail;
    }

    public void setDetail(SpgDetailModel detail) {
        this.detail = detail;
    }

}
