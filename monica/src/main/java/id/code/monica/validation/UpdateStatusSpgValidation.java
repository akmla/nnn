package id.code.monica.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.user.UserModel;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/20/2017.
 */
class UpdateStatusSpgValidation extends UserModel {

    @JsonProperty(value = _SUPERVISOR_ID)
    @ValidateColumn(name = _SUPERVISOR_ID, required = false)
    private long supervisorId;

    @ValidateColumn(name = _CODE, required = false)
    @JsonProperty(value = _CODE)
    private String code;

    @ValidateColumn(name = _CONTACT, required = false)
    @JsonProperty(value = _CONTACT)
    private String contact;

    @JsonProperty(value = _EMAIL)
    @ValidateColumn(name = _EMAIL, required = false)
    private String email;

    @JsonProperty(value = _ROLE_ID)
    @ValidateColumn(name = _ROLE_ID, required = false)
    private int roleId;

    @JsonProperty(value = _BRANCH_CODE)
    @ValidateColumn(name = _BRANCH_CODE, required = false)
    private String branchCode;

    @JsonProperty(value = _NAME)
    @ValidateColumn(name = _NAME, required = false)
    private String name;

    @JsonProperty(value = _GENDER)
    @ValidateColumn(name = _GENDER, required = false)
    private String gender;

}