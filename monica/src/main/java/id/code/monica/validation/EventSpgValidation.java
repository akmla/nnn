package id.code.monica.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.IdModel;

import java.util.List;

import static id.code.master_data.AliasName._EVENT_ID;
import static id.code.master_data.AliasName._SPG_ID;

/**
 * Created by CODE.ID on 8/23/2017.
 */
class EventSpgValidation extends IdModel {
    @JsonProperty(value = _EVENT_ID)
    @ValidateColumn(name = _EVENT_ID)
    private long eventId;

    @JsonProperty(value = _SPG_ID)
    @ValidateColumn(name = _SPG_ID)
    private List<Long> spgId;

    public List<Long> getSpgId() { return this.spgId; }

    public long getEventId() { return this.eventId; }

    public EventSpgValidation() { }
    public EventSpgValidation(List<Long> spgId) { this.spgId = spgId; }
}
