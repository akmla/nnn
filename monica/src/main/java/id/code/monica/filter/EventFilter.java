package id.code.monica.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.component.utility.StringUtility;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/10/2017.
 */
public class EventFilter extends BaseFilter {
    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    @JsonProperty(value = _SUPERVISOR_ID)
    @FilterColumn(columnName =_SUPERVISOR_ID)
    private Long supervisorId;

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(value = _BRAND_CODE)
    @FilterColumn(columnName = _BRAND_CODE)
    private String brandCode;

    @JsonProperty(value = _PAP_NO)
    @FilterColumn(columnName = _PAP_NO, comparator = WhereComparator.START_WITH)
    private String papNo;

    @JsonIgnore
    private Long userId;

    public Long getUserId() {
        return userId;
    }
    public Long getSupervisorId() {
        return supervisorId;
    }
    public String getBrandCode() {
        return brandCode;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getName() {
        return name;
    }
    public String getPapNo() {
        return papNo;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setSupervisorId(Long supervisorId) {
        this.supervisorId = supervisorId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }
    public void setName(String name) {
        this.name = (name != null) ? StringUtility.trimNotNull(name).toUpperCase() : null;
    }
    public void setPapNo(String papNo) {
        this.papNo = (papNo != null) ? StringUtility.trimNotNull(papNo).toUpperCase() : null;
    }
}
