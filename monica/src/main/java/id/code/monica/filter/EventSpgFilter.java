package id.code.monica.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName._EVENT_ID;

/**
 * Created by CODE.ID on 8/15/2017.
 */
public class EventSpgFilter extends BaseFilter {

    @JsonProperty(value = _EVENT_ID)
    @FilterColumn(columnName = _EVENT_ID)
    private String eventCode;

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }
}
