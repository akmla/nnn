package id.code.apollo;

import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.AuditTrailFacade;
import id.code.master_data.filter.BaseFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class TracingHandler extends RouteApiHandler<UserClaim> {
    private final AuditTrailFacade auditTrailFacade = new AuditTrailFacade();

    @HandlerGet(authorizeAccess = false, bypassAllMiddleware = true)
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<BaseFilter> filter) throws Exception {
        final List<AuditTrailModel> items = auditTrailFacade.getAuditTrails(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

}
