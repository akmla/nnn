package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class ThematicFilter extends BaseFilter {
    @JsonProperty(value = _CODE)
    @FilterColumn(columnName = _CODE)
    private String code;

    @JsonProperty(value = _PRODUCT_CODE)
    @FilterColumn(columnName = _PRODUCT_CODE)
    private String productCode;

    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    @JsonProperty(_BRANCH_CODE)
    @FilterColumn(value = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonIgnore
    private Long userId;

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public String getProductCode() {
        return productCode;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public Long getUserId() {
        return userId;
    }


    public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

}
