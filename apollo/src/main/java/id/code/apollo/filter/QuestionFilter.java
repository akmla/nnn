package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName._BRANCH_CODE;
import static id.code.master_data.AliasName._SURVEY_ID;

public class QuestionFilter extends BaseFilter {
    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(_SURVEY_ID)
    @FilterColumn(_SURVEY_ID)
    private Long surveyId;

    @JsonIgnore
    private Long userId;

    public String getBranchCode() {
        return branchCode;
    }
    public Long getSurveyId() {
        return surveyId;
    }
    public Long getUserId() {
        return userId;
    }


    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
