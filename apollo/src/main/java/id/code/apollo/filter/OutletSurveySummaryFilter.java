package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;


public class OutletSurveySummaryFilter extends BaseFilter {

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(value = _OUTLET_ID)
    @FilterColumn(columnName = _OUTLET_ID)
    private Long outletId;

    @JsonProperty(_SURVEY_NAME)
    @FilterColumn(value = _SURVEY_NAME, includeInQuery = false)
    private String surveyName;

    @JsonIgnore
    private Long userId;

    public String getBranchCode() {
        return branchCode;
    }
    public Long getOutletId() {
        return outletId;
    }
    public String getSurveyName() {
        return surveyName;
    }
    public Long getUserId() {
        return userId;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setOutletId(Long outletId) {
        this.outletId = outletId;
    }
    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName;
    }
    public void setUserId(Long userId){
        this.userId = userId;
    }
}
