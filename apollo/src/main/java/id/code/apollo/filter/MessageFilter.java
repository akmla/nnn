package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName._BRANCH_CODE;
import static id.code.master_data.AliasName._USER_ID;

public class MessageFilter extends BaseFilter {

    @JsonProperty(value = _USER_ID)
    @FilterColumn(columnName = _USER_ID)
    private Long userId;

    @FilterColumn(value = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonIgnore
    private Long senderId;

    public Long getUserId() {
        return userId;
    }
    public Long getSenderId() {
        return senderId;
    }
    public String getBranchCode() {
        return branchCode;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}
