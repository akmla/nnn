package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;


public class PromotionItemFilter extends BaseFilter {

    // OUTLET ID, MD ID, TASK DATE

    @JsonProperty(value = _OUTLET_ID)
    @FilterColumn(columnName = _OUTLET_ID)
    private Long outletId;

    @JsonProperty(value = _MD_ID)
    @FilterColumn(columnName = _MD_ID)
    private Long mdId;

    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(_TASK_ITEM_ID)
    @FilterColumn(_TASK_ITEM_ID)
    private Long taskItemId;

    @JsonIgnore
    private Long userId;

    public Long getOutletId() {
        return outletId;
    }
    public Long getMdId() {
        return mdId;
    }
    public Long getTaskItemId() {
        return taskItemId;
    }
    public Long getUserId() {
        return userId;
    }
    public String getName() {
        return name;
    }
    public String getBranchCode() {
        return branchCode;
    }

    public void setOutletId(Long outletId) {
        this.outletId = outletId;
    }
    public void setMdId(Long mdId) {
        this.mdId = mdId;
    }
    public void setTaskItemId(Long taskItemId) {
        this.taskItemId = taskItemId;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

}
