package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class TaskFilter extends BaseFilter {

    @JsonProperty(value = _BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(value = _LATEST)
    @FilterColumn(columnName = _LATEST, includeInQuery = false)
    private Boolean latest;

    @JsonProperty(value = _MD_NAME)
    @FilterColumn(columnName = _MD_NAME, includeInQuery = false)
    private String mdName;

    @JsonProperty(value = _START_DATE)
    @FilterColumn(columnName = _START_DATE, comparator = WhereComparator.LESS_THAN_OR_EQUALS)
    private Long startDate;

    @JsonProperty(_SUPERVISOR_ID)
    @FilterColumn(value = _SUPERVISOR_ID, includeInQuery = false)
    private Long supervisorId;

    @JsonIgnore
    private Long userId;

    public String getBranchCode() {
        return branchCode;
    }
    public Boolean getLatest() {
        return latest;
    }
    public String getMdName() {
        return mdName;
    }
    public Long getStartDate() {
        return startDate;
    }
    public Long getSupervisorId() {
        return supervisorId;
    }
    public Long getUserId() {
        return userId;
    }

    public void setMdName(String mdName) {
        this.mdName = mdName;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setLatest(Boolean latest) {
        this.latest = latest;
    }
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }
    public void setSupervisorId(Long supervisorId) {
        this.supervisorId = supervisorId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
