package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class TaskItemFilter extends BaseFilter {

    @JsonProperty(value = _STATUS)
    @FilterColumn(columnName = _STATUS)
    private String status;

    @JsonProperty(value = _OUTLET_ID)
    @FilterColumn(columnName = _OUTLET_ID)
    private Long outletId;

    @JsonProperty(value = _TASK_ID)
    @FilterColumn(columnName = _TASK_ID)
    private Long taskId;

    @JsonProperty(value = _TASK_DATE)
    @FilterColumn(columnName = _TASK_DATE)
    private Long taskDate;

    @JsonProperty(value = _MD_ID)
    @FilterColumn(columnName = _MD_ID, includeInQuery = false)
    private Long mdId;

    @JsonProperty(_BRANCH_CODE)
    @FilterColumn(columnName = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    @JsonProperty(_OUTLET_NAME)
    @FilterColumn(value = _OUTLET_NAME, includeInQuery = false)
    private String outletName;

    @JsonProperty(_SIO_TYPE_CODE)
    @FilterColumn(value = _SIO_TYPE_CODE, includeInQuery = false)
    private String sioTypeCode;

    @JsonIgnore
    private Long userId;

    public Long getMdId() {
        return mdId;
    }
    public Long getOutletId() {
        return outletId;
    }
    public Long getTaskId() {
        return taskId;
    }
    public Long getTaskDate() {
        return taskDate;
    }
    public Long getUserId() {
        return userId;
    }
    public String getStatus() {
        return status;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getOutletName() {
        return outletName;
    }
    public String getSioTypeCode() {
        return sioTypeCode;
    }


    public void setMdId(Long mdId) {
        this.mdId = mdId;
    }
    public void setTaskDate(Long taskDate) {
        this.taskDate = taskDate;
    }
    public void setOutletId(Long outletId) {
        this.outletId = outletId;
    }
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }
    public void setSioTypeCode(String sioTypeCode) {
        this.sioTypeCode = sioTypeCode;
    }
}
