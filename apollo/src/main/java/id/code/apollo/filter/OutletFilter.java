package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class OutletFilter extends BaseFilter {

    @JsonProperty(value = _CODE)
    @FilterColumn(columnName = _CODE)
    private String code;

    @JsonProperty(value = _CONTRACT_CODE)
    @FilterColumn(columnName = _CONTRACT_CODE)
    private String contractCode;

    @JsonProperty(value = _DISTRICT_CODE)
    @FilterColumn(columnName = _DISTRICT_CODE)
    private String districtCode;

    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.CONTAINS)
    private String name;

    @JsonProperty(value = _VERIFIED)
    @FilterColumn(columnName = _VERIFIED, includeInQuery = false)
    private String verified;

    @JsonProperty(value = _CONTRACT)
    @FilterColumn(value = _CONTRACT, includeInQuery = false)
    private boolean contract;

    @JsonProperty(_ROLE_ID)
    @FilterColumn(value = _ROLE_ID, includeInQuery = false)
    private Long roleId;

    @JsonProperty(_USER_ID)
    @FilterColumn(value = _USER_ID, includeInQuery = false)
    private Long userId;

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public String getContractCode() {
        return contractCode;
    }
    public String getDistrictCode() {
        return districtCode;
    }
    public String getVerified() {
        return verified;
    }
    public boolean isContract() {
        return contract;
    }
    public Long getRoleId() {
        return roleId;
    }
    public Long getUserId() { return userId; }


    public void setCode(String code) {
        this.code = code;
    }
    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }
    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setVerified(String verified) {
        this.verified = verified;
    }
    public void setContract(boolean contract) {
        this.contract = contract;
    }
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public void setUserId(Long userId) { this.userId = userId; }
}
