package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName._CODE;
import static id.code.master_data.AliasName._NAME;

public class SioTypeFilter extends BaseFilter {
    @JsonProperty(value = _CODE)
    @FilterColumn(columnName = _CODE)
    private String code;

    @JsonProperty(value = _NAME)
    @FilterColumn(columnName = _NAME, comparator = WhereComparator.START_WITH)
    private String name;

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = name;
    }
}
