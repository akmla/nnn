package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class TlPromotionVerificationFilter extends BaseFilter {
    @JsonProperty(value = _USER_ID)
    @FilterColumn(columnName = _USER_ID)
    private Long userId;

    @JsonProperty(value = _OUTLET_ID)
    @FilterColumn(columnName = _OUTLET_ID)
    private Long outletId;

    @JsonProperty(value = _VERIFICATION_TIME)
    @FilterColumn(columnName = _VERIFICATION_TIME)
    private Long verificationTime;

    @JsonProperty(value = _DISTRICT_CODE)
    @FilterColumn(columnName = _DISTRICT_CODE)
    private String districtCode;

    @JsonProperty(_BRANCH_CODE)
    @FilterColumn(value = _BRANCH_CODE, includeInQuery = false)
    private String branchCode;

    public Long getUserId() {
        return userId;
    }
    public Long getOutletId() {
        return outletId;
    }
    public Long getVerificationTime() {
        return verificationTime;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getDistrictCode() {
        return districtCode;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setOutletId(Long outletId) {
        this.outletId = outletId;
    }
    public void setVerificationTime(Long verificationTime) {
        this.verificationTime = verificationTime;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }
}
