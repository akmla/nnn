package id.code.apollo.filter;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.filter.annotation.FilterColumn;
import id.code.master_data.filter.BaseFilter;

import static id.code.master_data.AliasName.*;

public class ScheduleOutletFilter extends BaseFilter {

    @JsonProperty(value = _SCHEDULE_ID)
    @FilterColumn(columnName = _SCHEDULE_ID)
    private Long scheduleId;

    @JsonProperty(value = _CYCLE_SEQ)
    @FilterColumn(columnName = _CYCLE_SEQ)
    private Integer cycleSeq;

    @JsonProperty(value = _DAY)
    @FilterColumn(columnName = _DAY)
    private Integer day;

    @JsonProperty(_CITY_NAME)
    @FilterColumn(value = _CITY_NAME, includeInQuery = false)
    private String cityName;

    @JsonProperty(_DISTRICT_NAME)
    @FilterColumn(value = _DISTRICT_NAME, includeInQuery = false)
    private String districtName;

    @JsonProperty(_OUTLET_NAME)
    @FilterColumn(value = _OUTLET_NAME, includeInQuery = false)
    private String outletName;


    public Integer getCycleSeq() {
        return cycleSeq;
    }
    public Integer getDay() {
        return day;
    }
    public Long getScheduleId() {
        return scheduleId;
    }
    public String getCityName() {
        return cityName;
    }
    public String getDistrictName() {
        return districtName;
    }
    public String getOutletName() {
        return outletName;
    }

    public void setCycleSeq(Integer cycleSeq) {
        this.cycleSeq = cycleSeq;
    }
    public void setDay(Integer day) {
        this.day = day;
    }
    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }
}
