package id.code.apollo.api.handler.outlet;

import id.code.apollo.facade.contract.ContractFacade;
import id.code.apollo.facade.outlet.OutletFacade;
import id.code.apollo.filter.OutletFilter;
import id.code.apollo.filter.OutletSummaryFilter;
import id.code.apollo.model.outlet.OutletModel;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.MonicaResponse.RESPONSE_CONTRACT_IS_USED;
import static id.code.master_data.security.Role.*;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 11/09/2017.
 */

@SuppressWarnings("Duplicates")
public class OutletHandler extends RouteApiHandler<UserClaim> {
    private final OutletFacade outletFacade = new OutletFacade();
    private final ContractFacade contractFacade = new ContractFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<OutletFilter> filter) throws Exception {
        final Filter<OutletSummaryFilter> outletSummaryFilter = super.getRequestedFilter(serverExchange, OutletSummaryFilter.class);
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            outletSummaryFilter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
            if (serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_MERCHANDISER) {
                filter.getParam().setRoleId(serverExchange.getAccessTokenPayload().getRoleId());
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        } else {
            if (outletSummaryFilter.getParam().getBranchCode() != null && outletSummaryFilter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        if (filter.getOrderColumn() == null) {
            outletSummaryFilter.getParam().setDefaultSort(true);
        }

        final List<OutletModel> items = this.outletFacade.getOutlets(filter, outletSummaryFilter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter, outletSummaryFilter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final OutletModel data = this.outletFacade.getOutlet(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, long id) throws Exception {
        final OutletModel data = this.outletFacade.getOutlet(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.outletFacade.delete(OutletModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody OutletModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            newData.setVerified(_STATUS_NOT_TAGGED);

            if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
                newData.setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
            }

            // Trim and upper data
            newData.setAddress(StringUtility.trimNotNull(newData.getAddress()).toUpperCase());
            newData.setArea(StringUtility.trimNotNull(newData.getArea()).toUpperCase());
            newData.setDimension(StringUtility.trimNotNull(newData.getDimension()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());

            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
            if (this.outletFacade.insert(newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody OutletModel newData, long id) throws Exception {
        final OutletModel oldData;
        ApiResponse cache;

        if ((oldData = this.outletFacade.getOutlet(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if (newData.getOldContractStatus() != null &&
                !newData.getOldContractStatus().equalsIgnoreCase(_STATUS_TERMINATE) &&
                this.contractFacade.getContract(newData.getContractCode()).getIsUsed() > 0) {
            super.sendResponse(serverExchange, RESPONSE_CONTRACT_IS_USED);
        } else if ((cache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, cache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCode(oldData.getCode());
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.setPics(oldData.getPics());
            newData.setOldContractCode(oldData.getContractCode());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
            newData.setBranchCode(oldData.getBranchCode());
            newData.setStatus((newData.getOldContractStatus() != null && newData.getOldContractStatus().equalsIgnoreCase(_STATUS_TERMINATE)) ? _STATUS_FALSE : oldData.getStatus());

            if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
                newData.setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
                newData.setVerified(oldData.getVerified());
                if (serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_TL_PROMOSI) {
                    newData.setVerified(StringUtility.trimNotNull(newData.getVerified()).toUpperCase());
                } else {
                    newData.setLat(oldData.getLat());
                    newData.setLng(oldData.getLng());
                }
            } else {
                newData.setVerified(oldData.getVerified());
                newData.setLat(oldData.getLat());
                newData.setLng(oldData.getLng());
            }

            // Trim and upper data
            newData.setAddress(StringUtility.trimNotNull(newData.getAddress()).toUpperCase());
            newData.setDimension(StringUtility.trimNotNull(newData.getDimension()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());
            newData.setArea(StringUtility.trimNotNull(newData.getArea()).toUpperCase());

            boolean changeContract;

            if (newData.getContractCode() == null && oldData.getContractCode() == null) {
                changeContract = false;
            } else if (newData.getContractCode() != null && oldData.getContractCode() != null) {
                changeContract = !(newData.getContractCode().equalsIgnoreCase(oldData.getContractCode()));
            } else {
                changeContract = !(StringUtility.trimNotNull(oldData.getContractCode()).equalsIgnoreCase(newData.getContractCode()));
            }

            if (changeContract) {
                newData.setLokasi(oldData.getLokasi());
                newData.setPosisi(oldData.getPosisi());
            }

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.outletFacade.update(newData, auditTrail, changeContract)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }
}