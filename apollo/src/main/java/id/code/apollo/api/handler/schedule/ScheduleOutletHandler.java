package id.code.apollo.api.handler.schedule;

import id.code.apollo.facade.schedule.ScheduleOutletFacade;
import id.code.apollo.filter.ScheduleOutletFilter;
import id.code.apollo.model.schedule.ScheduleOutletModel;
import id.code.apollo.validation.ScheduleOutletValidation;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class ScheduleOutletHandler extends RouteApiHandler<UserClaim> {
    private final ScheduleOutletFacade scheduleOutletFacade = new ScheduleOutletFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ScheduleOutletFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<ScheduleOutletModel> items = this.scheduleOutletFacade.getScheduleOutlets(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ScheduleOutletModel data = this.scheduleOutletFacade.getScheduleOutlet(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody ScheduleOutletValidation newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
            if (this.scheduleOutletFacade.insert(newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final ScheduleOutletModel data = this.scheduleOutletFacade.getScheduleOutlet(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.scheduleOutletFacade.delete(ScheduleOutletModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody ScheduleOutletModel newData, Long id) throws Exception {
        final ScheduleOutletModel oldData;
        ApiResponse responseCache;

        if ((oldData = this.scheduleOutletFacade.getScheduleOutlet(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.scheduleOutletFacade.update(ScheduleOutletModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }
}