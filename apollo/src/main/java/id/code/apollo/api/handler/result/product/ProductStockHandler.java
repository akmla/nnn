package id.code.apollo.api.handler.result.product;

import id.code.apollo.facade.result.product.ProductStockFacade;
import id.code.apollo.facade.task.TaskItemFacade;
import id.code.apollo.model.Error.ErrorProductStockModel;
import id.code.apollo.model.result.product.ProductStockModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.filter.ProductStockFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_UPDATE_DATA;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 11/09/2017.
 */

public class ProductStockHandler extends RouteApiHandler<UserClaim> {
    private final ProductStockFacade productStockFacade = new ProductStockFacade();
    private final TaskItemFacade taskItemFacade = new TaskItemFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ProductStockFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<ProductStockModel> items = this.productStockFacade.getProductStocks(filter, serverExchange.getAccessTokenPayload().getRoleId());
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ProductStockModel data = this.productStockFacade.getProductStock(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody ProductStockModel newData, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final ErrorProductStockModel errorProductStockModel = new ErrorProductStockModel();
                errorProductStockModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                errorProductStockModel.setProductCode(newData.getProductCode());
                errorProductStockModel.setQuantity(newData.getQuantity());
                errorProductStockModel.setRemark(newData.getRemark());
                errorProductStockModel.setTaskItemId(newData.getTaskItemId());
                errorProductStockModel.setType(newData.getType());
                final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorProductStockModel);
                if (this.productStockFacade.insert(ErrorProductStockModel.class, errorProductStockModel, auditTrail2)) {
                    super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorProductStockModel).setInfo(role));
                } else {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                }
            } else {
                final ProductStockModel oldData;
                newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

                // Trim and upper data
                newData.setType(StringUtility.trimNotNull(newData.getType()).toUpperCase());
                newData.setRemark(StringUtility.trimNotNull(newData.getRemark()).toUpperCase());

                if ((oldData = this.productStockFacade.getProductStockByUnique(newData.getTaskItemId(), newData.getProductCode(), newData.getType())) == null) {
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.productStockFacade.insert(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    } else {
                        final ErrorProductStockModel errorProductStockModel = new ErrorProductStockModel();
                        errorProductStockModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                        errorProductStockModel.setProductCode(newData.getProductCode());
                        errorProductStockModel.setQuantity(newData.getQuantity());
                        errorProductStockModel.setRemark(newData.getRemark());
                        errorProductStockModel.setTaskItemId(newData.getTaskItemId());
                        errorProductStockModel.setType(newData.getType());
                        final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorProductStockModel);
                        if (this.productStockFacade.insert(ErrorProductStockModel.class, errorProductStockModel, auditTrail2)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorProductStockModel).setInfo(role));
                        } else {
                            super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                        }
                    }
                } else {
                    newData.setId(oldData.getId());
                    newData.setCreated(oldData.getCreated());
                    newData.setCreatedBy(oldData.getCreatedBy());
                    newData.setTaskItemId(oldData.getTaskItemId());
                    newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.productStockFacade.update(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                    }
                }
            }
        }
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody ProductStockModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final ErrorProductStockModel errorProductStockModel = new ErrorProductStockModel();
                errorProductStockModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                errorProductStockModel.setProductCode(newData.getProductCode());
                errorProductStockModel.setQuantity(newData.getQuantity());
                errorProductStockModel.setRemark(newData.getRemark());
                errorProductStockModel.setTaskItemId(newData.getTaskItemId());
                errorProductStockModel.setType(newData.getType());
                final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorProductStockModel);
                if (this.productStockFacade.insert(ErrorProductStockModel.class, errorProductStockModel, auditTrail2)) {
                    super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorProductStockModel).setInfo(role));
                } else {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                }
            } else {
                final ProductStockModel oldData;
                newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

                // Trim and upper data
                newData.setType(StringUtility.trimNotNull(newData.getType()).toUpperCase());
                newData.setRemark(StringUtility.trimNotNull(newData.getRemark()).toUpperCase());

                if ((oldData = this.productStockFacade.getProductStockByUnique(newData.getTaskItemId(), newData.getProductCode(), newData.getType())) == null) {
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.productStockFacade.insert(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    } else {
                        final ErrorProductStockModel errorProductStockModel = new ErrorProductStockModel();
                        errorProductStockModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                        errorProductStockModel.setProductCode(newData.getProductCode());
                        errorProductStockModel.setQuantity(newData.getQuantity());
                        errorProductStockModel.setRemark(newData.getRemark());
                        errorProductStockModel.setTaskItemId(newData.getTaskItemId());
                        errorProductStockModel.setType(newData.getType());
                        final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorProductStockModel);
                        if (this.productStockFacade.insert(ErrorProductStockModel.class, errorProductStockModel, auditTrail2)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorProductStockModel).setInfo(role));
                        } else {
                            super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                        }
                    }
                } else {
                    newData.setId(oldData.getId());
                    newData.setCreated(oldData.getCreated());
                    newData.setCreatedBy(oldData.getCreatedBy());
                    newData.setTaskItemId(oldData.getTaskItemId());
                    newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.productStockFacade.update(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                    }
                }
            }
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final ProductStockModel data = this.productStockFacade.getProductStock(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.productStockFacade.delete(ProductStockModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }
}