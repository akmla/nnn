package id.code.apollo.api.handler.result.competitor;

import id.code.apollo.facade.result.competitor.CompetitorActivityFacade;
import id.code.apollo.facade.task.TaskItemFacade;
import id.code.apollo.filter.CompetitorActivityFilter;
import id.code.apollo.model.Error.ErrorCompetitorActivityModel;
import id.code.apollo.model.result.competitor.CompetitorActivityModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_INSERT_DATA;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_UPDATE_DATA;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class CompetitorActivityHandler extends RouteApiHandler<UserClaim> {
    private final CompetitorActivityFacade competitorActivityFacade = new CompetitorActivityFacade();
    private final TaskItemFacade taskItemFacade = new TaskItemFacade();

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final CompetitorActivityModel data = this.competitorActivityFacade.getCompetitorActivity(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final CompetitorActivityModel data = this.competitorActivityFacade.getCompetitorActivity(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.competitorActivityFacade.delete(CompetitorActivityModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<CompetitorActivityFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());


        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            if (role != null && role.getId() == Role._ROLE_ID_MERCHANDISER) {
                filter.getParam().setMdId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<CompetitorActivityModel> items = this.competitorActivityFacade.getCompetitorActivities(filter, serverExchange.getAccessTokenPayload().getRoleId());
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody CompetitorActivityModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            // Trim and upper data
            newData.setActivity((newData.getActivity() == null) ? _DASH : StringUtility.trimNotNull(newData.getActivity()).toUpperCase());

            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final ErrorCompetitorActivityModel errorCompetitorActivityModel = new ErrorCompetitorActivityModel();
                errorCompetitorActivityModel.setActivity(newData.getActivity());
                errorCompetitorActivityModel.setProductCode(newData.getProductCode());
                errorCompetitorActivityModel.setTaskItemId(newData.getTaskItemId());
                errorCompetitorActivityModel.setType(newData.getType());
                final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorCompetitorActivityModel);
                if (this.competitorActivityFacade.insert(ErrorCompetitorActivityModel.class, errorCompetitorActivityModel, auditTrail2)) {
                    super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorCompetitorActivityModel).setInfo(role));
                } else {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                }
            } else {
                final CompetitorActivityModel oldData;

                if((oldData = competitorActivityFacade.getCompetitorActivityByUnique(newData.getTaskItemId(), newData.getProductCode(), newData.getType())) == null) {
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.competitorActivityFacade.insert(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    } else {
                        final ErrorCompetitorActivityModel errorCompetitorActivityModel = new ErrorCompetitorActivityModel();
                        errorCompetitorActivityModel.setActivity(newData.getActivity());
                        errorCompetitorActivityModel.setProductCode(newData.getProductCode());
                        errorCompetitorActivityModel.setTaskItemId(newData.getTaskItemId());
                        errorCompetitorActivityModel.setType(newData.getType());
                        final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorCompetitorActivityModel);
                        if (this.competitorActivityFacade.insert(ErrorCompetitorActivityModel.class, errorCompetitorActivityModel, auditTrail2)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorCompetitorActivityModel).setInfo(role));
                        } else {
                            super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                        }
                    }
                } else {
                    newData.setId(oldData.getId());
                    newData.setCreated(oldData.getCreated());
                    newData.setCreatedBy(oldData.getCreatedBy());
                    newData.setTaskItemId(oldData.getTaskItemId());
                    newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.competitorActivityFacade.update(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                    }
                }
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody CompetitorActivityModel newData, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            // Trim and upper data
            newData.setActivity((newData.getActivity() == null) ? _DASH : StringUtility.trimNotNull(newData.getActivity()).toUpperCase());

            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final ErrorCompetitorActivityModel errorCompetitorActivityModel = new ErrorCompetitorActivityModel();
                errorCompetitorActivityModel.setActivity(newData.getActivity());
                errorCompetitorActivityModel.setProductCode(newData.getProductCode());
                errorCompetitorActivityModel.setTaskItemId(newData.getTaskItemId());
                errorCompetitorActivityModel.setType(newData.getType());
                final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorCompetitorActivityModel);
                if (this.competitorActivityFacade.insert(ErrorCompetitorActivityModel.class, errorCompetitorActivityModel, auditTrail2)) {
                    super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorCompetitorActivityModel).setInfo(role));
                } else {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                }
            } else {
                final CompetitorActivityModel oldData;

                if((oldData = competitorActivityFacade.getCompetitorActivityByUnique(newData.getTaskItemId(), newData.getProductCode(), newData.getType())) == null) {
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.competitorActivityFacade.insert(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    } else {
                        final ErrorCompetitorActivityModel errorCompetitorActivityModel = new ErrorCompetitorActivityModel();
                        errorCompetitorActivityModel.setActivity(newData.getActivity());
                        errorCompetitorActivityModel.setProductCode(newData.getProductCode());
                        errorCompetitorActivityModel.setTaskItemId(newData.getTaskItemId());
                        errorCompetitorActivityModel.setType(newData.getType());
                        final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorCompetitorActivityModel);
                        if (this.competitorActivityFacade.insert(ErrorCompetitorActivityModel.class, errorCompetitorActivityModel, auditTrail2)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorCompetitorActivityModel).setInfo(role));
                        } else {
                            super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                        }
                    }
                } else {
                    newData.setId(oldData.getId());
                    newData.setCreated(oldData.getCreated());
                    newData.setCreatedBy(oldData.getCreatedBy());
                    newData.setTaskItemId(oldData.getTaskItemId());
                    newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.competitorActivityFacade.update(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                    }
                }
            }
        }
    }

}