package id.code.apollo.api.handler.tl_promosi;

import id.code.apollo.facade.tl_promotion.TlPromotionCheckInFacade;
import id.code.apollo.filter.TlPromotionCheckInFilter;
import id.code.apollo.model.tl_promotion.TlPromotionCheckInModel;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_INSERT_DATA;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_UPDATE_DATA;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

@SuppressWarnings("Duplicates")
public class TlPromotionCheckInHandler extends RouteApiHandler<UserClaim> {
    private final TlPromotionCheckInFacade tlPromotionCheckInFacade = new TlPromotionCheckInFacade();

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody TlPromotionCheckInModel newData, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final TlPromotionCheckInModel oldData;
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else if (serverExchange.getAccessTokenPayload().getRoleId() != Role._ROLE_ID_TL_PROMOSI) {
            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
        } else if ((oldData = this.tlPromotionCheckInFacade.getCheckIn(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else {
            // Assign Old Value
            newData.setId(id);
            newData.setCheckIn(oldData.getCheckIn());
            newData.setUserId(oldData.getUserId());
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.tlPromotionCheckInFacade.update(TlPromotionCheckInModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            } else {
                super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
            }
        }
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<TlPromotionCheckInFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<TlPromotionCheckInModel> eventsSpg = this.tlPromotionCheckInFacade.getCheckIns(serverExchange.getAccessTokenPayload().getUserId(), filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, eventsSpg, filter).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody TlPromotionCheckInModel newData) throws Exception {
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            newData.setUserId(serverExchange.getAccessTokenPayload().getUserId());

            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if (serverExchange.getAccessTokenPayload().getRoleId() != Role._ROLE_ID_TL_PROMOSI) {
                newData.setId(System.currentTimeMillis());
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            } else if (this.tlPromotionCheckInFacade.insert(TlPromotionCheckInModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            } else {
                super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
            }
        }
    }

}
