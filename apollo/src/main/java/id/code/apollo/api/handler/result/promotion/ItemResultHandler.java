package id.code.apollo.api.handler.result.promotion;

import id.code.apollo.facade.result.promotion.ItemResultFacade;
import id.code.apollo.facade.task.TaskItemFacade;
import id.code.apollo.filter.ItemResultFilter;
import id.code.apollo.model.Error.ErrorItemResultModel;
import id.code.apollo.model.result.promotion.ItemResultModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.component.utility.DateUtility;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.time.format.DateTimeFormatter;
import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.MonicaResponse.*;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.master_data.security.Role._ROLE_ID_MERCHANDISER;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

@SuppressWarnings("Duplicates")
public class ItemResultHandler extends RouteApiHandler<UserClaim> {
    private final ItemResultFacade itemResultFacade = new ItemResultFacade();
    private final TaskItemFacade taskItemFacade = new TaskItemFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ItemResultFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
            if (serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_MERCHANDISER)
                filter.getParam().setMdId(serverExchange.getAccessTokenPayload().getUserId());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<ItemResultModel> items = this.itemResultFacade.getItemResults(filter, serverExchange.getAccessTokenPayload().getRoleId());
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ItemResultModel data = this.itemResultFacade.getItemResult(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final ItemResultModel data = this.itemResultFacade.getItemResult(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.itemResultFacade.delete(ItemResultModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody ItemResultModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_TASK_RESULT);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append("ERROR");
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if (!this.itemResultFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_TASK_RESULTS_IMAGES, filename.toString())) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final ErrorItemResultModel errorItemResultModel = new ErrorItemResultModel();
                    errorItemResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                    errorItemResultModel.setActionToItem(newData.getActionToItem());
                    errorItemResultModel.setCategoryItemCode(newData.getCategoryItemCode());
                    errorItemResultModel.setCustom(newData.getCustom());
                    errorItemResultModel.setItemCode(newData.getItemCode());
                    errorItemResultModel.setItemCondition(newData.getItemCondition());
                    errorItemResultModel.setItemName(newData.getItemName());
                    errorItemResultModel.setItemSize(newData.getItemSize());
                    errorItemResultModel.setPics(filename.toString());
                    errorItemResultModel.setSioTypeCode(newData.getSioTypeCode());
                    errorItemResultModel.setTaskItemId(newData.getTaskItemId());
                    errorItemResultModel.setThematicCode(newData.getThematicCode());
                    final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorItemResultModel);
                    if (this.itemResultFacade.insert(ErrorItemResultModel.class, errorItemResultModel, auditTrail2)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorItemResultModel).setInfo(role));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                    }
                }
            } else {
                newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

                // Trim and upper data
                newData.setItemName(StringUtility.trimNotNull(newData.getItemName()).toUpperCase());
                newData.setItemCondition(StringUtility.trimNotNull(newData.getItemCondition()).toUpperCase());
                newData.setActionToItem(StringUtility.trimNotNull(newData.getActionToItem()).toUpperCase());

                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_TASK_RESULT);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(taskItem.getOutlet().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if (!this.itemResultFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_TASK_RESULTS_IMAGES, filename.toString())) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final ItemResultModel oldData;
                    newData.setPics(filename.toString());

                    if ((oldData = itemResultFacade.getItemResultByTaskItemIdAndItemCode(newData.getTaskItemId(), newData.getItemCode())) == null) {
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.itemResultFacade.insert(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                        } else {
                            final ErrorItemResultModel errorItemResultModel = new ErrorItemResultModel();
                            errorItemResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                            errorItemResultModel.setActionToItem(newData.getActionToItem());
                            errorItemResultModel.setCategoryItemCode(newData.getCategoryItemCode());
                            errorItemResultModel.setCustom(newData.getCustom());
                            errorItemResultModel.setItemCode(newData.getItemCode());
                            errorItemResultModel.setItemCondition(newData.getItemCondition());
                            errorItemResultModel.setItemName(newData.getItemName());
                            errorItemResultModel.setItemSize(newData.getItemSize());
                            errorItemResultModel.setPics(filename.toString());
                            errorItemResultModel.setSioTypeCode(newData.getSioTypeCode());
                            errorItemResultModel.setTaskItemId(newData.getTaskItemId());
                            errorItemResultModel.setThematicCode(newData.getThematicCode());
                            final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorItemResultModel);
                            if (this.itemResultFacade.insert(ErrorItemResultModel.class, errorItemResultModel, auditTrail2)) {
                                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorItemResultModel).setInfo(role));
                            } else {
                                super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                            }
                        }
                    } else {
                        newData.setId(oldData.getId());
                        newData.setCreated(oldData.getCreated());
                        newData.setCreatedBy(oldData.getCreatedBy());
                        newData.setTaskItemId(oldData.getTaskItemId());
                        newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.itemResultFacade.update(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                        } else {
                            super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                        }
                    }
                }
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody ItemResultModel newData, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_TASK_RESULT);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append("ERROR");
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if (!this.itemResultFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_TASK_RESULTS_IMAGES, filename.toString())) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final ErrorItemResultModel errorItemResultModel = new ErrorItemResultModel();
                    errorItemResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                    errorItemResultModel.setActionToItem(newData.getActionToItem());
                    errorItemResultModel.setCategoryItemCode(newData.getCategoryItemCode());
                    errorItemResultModel.setCustom(newData.getCustom());
                    errorItemResultModel.setItemCode(newData.getItemCode());
                    errorItemResultModel.setItemCondition(newData.getItemCondition());
                    errorItemResultModel.setItemName(newData.getItemName());
                    errorItemResultModel.setItemSize(newData.getItemSize());
                    errorItemResultModel.setPics(filename.toString());
                    errorItemResultModel.setSioTypeCode(newData.getSioTypeCode());
                    errorItemResultModel.setTaskItemId(newData.getTaskItemId());
                    errorItemResultModel.setThematicCode(newData.getThematicCode());
                    final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorItemResultModel);
                    if (this.itemResultFacade.insert(ErrorItemResultModel.class, errorItemResultModel, auditTrail2)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorItemResultModel).setInfo(role));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                    }
                }
            } else {
                newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

                // Trim and upper data
                newData.setItemName(StringUtility.trimNotNull(newData.getItemName()).toUpperCase());
                newData.setItemCondition(StringUtility.trimNotNull(newData.getItemCondition()).toUpperCase());
                newData.setActionToItem(StringUtility.trimNotNull(newData.getActionToItem()).toUpperCase());

                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_TASK_RESULT);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(taskItem.getOutlet().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if (!this.itemResultFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_TASK_RESULTS_IMAGES, filename.toString())) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final ItemResultModel oldData;
                    newData.setPics(filename.toString());

                    if ((oldData = itemResultFacade.getItemResultByTaskItemIdAndItemCode(newData.getTaskItemId(), newData.getItemCode())) == null) {
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.itemResultFacade.insert(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                        } else {
                            final ErrorItemResultModel errorItemResultModel = new ErrorItemResultModel();
                            errorItemResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                            errorItemResultModel.setActionToItem(newData.getActionToItem());
                            errorItemResultModel.setCategoryItemCode(newData.getCategoryItemCode());
                            errorItemResultModel.setCustom(newData.getCustom());
                            errorItemResultModel.setItemCode(newData.getItemCode());
                            errorItemResultModel.setItemCondition(newData.getItemCondition());
                            errorItemResultModel.setItemName(newData.getItemName());
                            errorItemResultModel.setItemSize(newData.getItemSize());
                            errorItemResultModel.setPics(filename.toString());
                            errorItemResultModel.setSioTypeCode(newData.getSioTypeCode());
                            errorItemResultModel.setTaskItemId(newData.getTaskItemId());
                            errorItemResultModel.setThematicCode(newData.getThematicCode());
                            final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorItemResultModel);
                            if (this.itemResultFacade.insert(ErrorItemResultModel.class, errorItemResultModel, auditTrail2)) {
                                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorItemResultModel).setInfo(role));
                            } else {
                                super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                            }
                        }
                    } else {
                        newData.setId(oldData.getId());
                        newData.setCreated(oldData.getCreated());
                        newData.setCreatedBy(oldData.getCreatedBy());
                        newData.setTaskItemId(oldData.getTaskItemId());
                        newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.itemResultFacade.update(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                        } else {
                            super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                        }
                    }
                }
            }
        }
    }

}
