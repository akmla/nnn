package id.code.apollo.api.handler.tl_promosi;

import id.code.apollo.facade.outlet.OutletFacade;
import id.code.apollo.facade.tl_promotion.TlPromotionVerificationFacade;
import id.code.apollo.filter.TlPromotionVerificationFilter;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.tl_promotion.TlPromotionVerificationModel;
import id.code.component.utility.DateUtility;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.time.format.DateTimeFormatter;
import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.MonicaResponse.*;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.master_data.security.Role._ROLE_ID_TL_PROMOSI;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

@SuppressWarnings("Duplicates")
public class TlPromotionVerificationHandler extends RouteApiHandler<UserClaim> {
    private final TlPromotionVerificationFacade tlPromotionVerificationFacade = new TlPromotionVerificationFacade();
    private final OutletFacade outletFacade = new OutletFacade();

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody TlPromotionVerificationModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final OutletModel outlet = outletFacade.getOutlet(newData.getOutletId());
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            newData.setUserId(serverExchange.getAccessTokenPayload().getUserId());

            final StringBuilder filename = new StringBuilder();

            filename.append(_FORMAT_VALID);
            filename.append(_SEPARATOR_PICTURE_NAME);
            filename.append(outlet.getCode());
            filename.append(_SEPARATOR_PICTURE_NAME);
            filename.append(serverExchange.getAccessTokenPayload().getCode());
            filename.append(_SEPARATOR_PICTURE_NAME);
            filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                    .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
            filename.append(_EXTENSION_PICTURE);

            if (!tlPromotionVerificationFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_OUTLETS_IMAGES, filename.toString())) {
                super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
            } else {
                newData.setPics(filename.toString());
                final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                if (this.tlPromotionVerificationFacade.insert(newData, auditTrail)) {
                    super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                } else {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                }
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody TlPromotionVerificationModel newData, Long id) throws Exception {
        final TlPromotionVerificationModel oldData;
        ApiResponse responseCache;

        if ((oldData = this.tlPromotionVerificationFacade.getTlPromotionItem(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            final OutletModel outlet = outletFacade.getOutlet(newData.getOutletId());
            newData.setId(id);
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim and upper data
            newData.setRemark(StringUtility.trimNotNull(newData.getRemark()).toUpperCase());

            if (!oldData.getPics().equalsIgnoreCase(newData.getPics())) {
                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_VALID);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(outlet.getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDate(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if (!tlPromotionVerificationFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_OUTLETS_IMAGES, filename.toString())) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    newData.setPics(filename.toString());
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);
                    if (this.tlPromotionVerificationFacade.update(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                    }
                }
            }
        }
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final TlPromotionVerificationModel data = this.tlPromotionVerificationFacade.getTlPromotionItem(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<TlPromotionVerificationFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
            if (serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_TL_PROMOSI) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<TlPromotionVerificationModel> items = this.tlPromotionVerificationFacade.getTlPromotionItems(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }
}
