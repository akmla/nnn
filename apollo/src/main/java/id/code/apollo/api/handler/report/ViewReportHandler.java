package id.code.apollo.api.handler.report;

import id.code.component.utility.StringUtility;
import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.facade.setting.SettingFacade;
import id.code.master_data.filter.ReportFilter;
import id.code.master_data.model.report.ReportJasper;
import id.code.master_data.model.setting.SettingModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;

import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_PARAMETER;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class ViewReportHandler extends RouteApiHandler<UserClaim> {
    private final SettingFacade settingFacade = new SettingFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ReportFilter> filter) throws IOException, SQLException, QueryBuilderException {

        // SAMPLE URL = http://localhost:8080/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=/reports/interactive/User_Report&standAlone=true&j_username=apollo&j_password=apollo&decorate=no
        // NEW SAMPLE URL http://{ip}:{port}/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=/reports/NNA/{folder}/{report_name}&standAlone=true&j_username={user_name}&j_password={password}&decorate=no

        if (filter.getParam().getReportName() != null) {
            final ReportJasper report = new ReportJasper();
            final SettingModel jasper_url = this.settingFacade.getSetting("JASPER_URL");
            final SettingModel jasper_port = this.settingFacade.getSetting("JASPER_PORT");

            final SettingModel jasper_user = this.settingFacade.getSetting("JASPER_USER");
            final SettingModel jasper_password = this.settingFacade.getSetting("JASPER_PASSWORD");

            final String host = jasper_url != null ? jasper_url.getValue() : null;
            final long port = jasper_port != null ? StringUtility.getLong(jasper_port.getValue()) : 8080;
            final String username = jasper_user != null ? jasper_user.getValue() : null;
            final String password = jasper_password != null ? jasper_password.getValue() : null;

            report.setUrl(String.format("%s:%s/jasperserver/flow.html?_flowId=viewReportFlow&decorate=no&reportUnit=/reports/interactive/%s&standAlone=true&j_username=%s&j_password=%s",
                    host, port, filter.getParam().getReportName(), username, password));

            super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, report, filter));
        } else {
            super.sendResponse(serverExchange, RESPONSE_ERROR_PARAMETER);
        }
    }


}
