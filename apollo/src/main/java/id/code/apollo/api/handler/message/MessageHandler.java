package id.code.apollo.api.handler.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.facade.message.MessageFacade;
import id.code.apollo.filter.MessageFilter;
import id.code.apollo.model.message.MessageModel;
import id.code.apollo.validation.SendMessageValidation;
import id.code.component.AppLogger;
import id.code.component.JsonMapper;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_NO_USER;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 11/09/2017.
 */

public class MessageHandler extends RouteApiHandler<UserClaim> {
    private final MessageFacade messageFacade = new MessageFacade();
    private String pushNotificationAddress;

    @Override
    public void initializeBeforeServerStarted(Properties properties) {
        super.initializeBeforeServerStarted(properties);
        this.pushNotificationAddress = properties.getProperty("pushNotification.url");
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final MessageModel data = this.messageFacade.getMessage(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final MessageModel data = this.messageFacade.getMessage(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

            final boolean deleted = this.messageFacade.delete(MessageModel.class, data, auditTrail);
            super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody MessageModel newData, Long id) throws Exception {
        final MessageModel oldData;
        ApiResponse responseCache;

        if ((oldData = this.messageFacade.getMessage(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.messageFacade.update(MessageModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<MessageFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setSenderId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<MessageModel> items = this.messageFacade.getMessages(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody SendMessageValidation newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim and upper data
            newData.setMessage(StringUtility.trimNotNull(newData.getMessage()).toUpperCase());
            newData.setSendDate(System.currentTimeMillis());
            newData.setSenderId(serverExchange.getAccessTokenPayload().getUserId());

            final List<MessageModel> messages = this.messageFacade.insertMessage(newData);

            if (messages != null) {
                if (messages.size() == 0) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_NO_USER);
                }
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                this.sendMessages(messages);
            }
        }
    }

    private void sendMessages(List<MessageModel> messages) throws IOException {
        try (final CloseableHttpClient httpClient = HttpClients.createDefault()) {
            final HttpPost post = new HttpPost(this.pushNotificationAddress);

            for (final MessageModel message : messages) {
                if (StringUtility.isNullOrWhiteSpace(message.getFcmToken())) {
                    continue;
                } else if (message.getFcmToken().startsWith("fake")) {
                    continue;
                }

                String body = JsonMapper.serializeAsString(new NotificationWrapper(message));
                post.setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));

                try (final CloseableHttpResponse execute = httpClient.execute(post)) {
                    final StringBuilder stringBuilder = new StringBuilder();
                    final HttpEntity entity = execute.getEntity();
                    String line;

                    try (final BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()))) {
                        while ((line = reader.readLine()) != null) {
                            stringBuilder.append(line);
                        }
                    }

                    AppLogger.writeDebug("Push Response: " + stringBuilder);
                    // push notification is not reliable, ignore any error. Fuck this particular response
                }
            }
        }
    }

    private static class NotificationWrapper {
        @JsonProperty
        private final String token;

        @JsonProperty
        private final MessageModel data;

        public String getToken() {
            return token;
        }

        public MessageModel getData() {
            return data;
        }

        NotificationWrapper(MessageModel data) {
            this.token = data.getFcmToken();
            this.data = data;
        }
    }
}