package id.code.apollo.api.handler.result.customer;

import id.code.apollo.facade.result.customer.CustomerFeedbackFacade;
import id.code.apollo.facade.task.TaskItemFacade;
import id.code.apollo.filter.CustomerFeedbackFilter;
import id.code.apollo.model.Error.ErrorCustomerFeedbackModel;
import id.code.apollo.model.result.customer.CustomerFeedbackModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_INSERT_DATA;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_UPDATE_DATA;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 11/09/2017.
 */

public class CustomerFeedbackHandler extends RouteApiHandler<UserClaim> {
    private final CustomerFeedbackFacade customerFeedbackFacade = new CustomerFeedbackFacade();
    private final TaskItemFacade taskItemFacade = new TaskItemFacade();

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final CustomerFeedbackModel data = this.customerFeedbackFacade.getCustomerFeedback(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final CustomerFeedbackModel data = this.customerFeedbackFacade.getCustomerFeedback(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.customerFeedbackFacade.delete(CustomerFeedbackModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<CustomerFeedbackFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<CustomerFeedbackModel> items = this.customerFeedbackFacade.getAllCustomerFeedback(filter, serverExchange.getAccessTokenPayload().getRoleId());
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody CustomerFeedbackModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim and upper data
            newData.setFeedback(StringUtility.trimNotNull(newData.getFeedback()).toUpperCase());

            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final ErrorCustomerFeedbackModel errorCustomerFeedbackModel = new ErrorCustomerFeedbackModel();
                errorCustomerFeedbackModel.setFeedback(newData.getFeedback());
                errorCustomerFeedbackModel.setTaskItemId(newData.getTaskItemId());
                final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorCustomerFeedbackModel);
                if (this.customerFeedbackFacade.insert(ErrorCustomerFeedbackModel.class, errorCustomerFeedbackModel, auditTrail2)) {
                    super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorCustomerFeedbackModel).setInfo(role));
                } else {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                }
            } else {
                final CustomerFeedbackModel oldData;

                if((oldData = customerFeedbackFacade.getCustomerFeedbackByTaskItemId(newData.getTaskItemId())) == null) {
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.customerFeedbackFacade.insert(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    } else {
                        final ErrorCustomerFeedbackModel errorCustomerFeedbackModel = new ErrorCustomerFeedbackModel();
                        errorCustomerFeedbackModel.setFeedback(newData.getFeedback());
                        errorCustomerFeedbackModel.setTaskItemId(newData.getTaskItemId());
                        final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorCustomerFeedbackModel);
                        if (this.customerFeedbackFacade.insert(ErrorCustomerFeedbackModel.class, errorCustomerFeedbackModel, auditTrail2)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorCustomerFeedbackModel).setInfo(role));
                        } else {
                            super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                        }
                    }
                } else {
                    newData.setId(oldData.getId());
                    newData.setCreated(oldData.getCreated());
                    newData.setCreatedBy(oldData.getCreatedBy());
                    newData.setTaskItemId(oldData.getTaskItemId());
                    newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.customerFeedbackFacade.update(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                    }
                }
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody CustomerFeedbackModel newData, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim and upper data
            newData.setFeedback(StringUtility.trimNotNull(newData.getFeedback()).toUpperCase());

            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final ErrorCustomerFeedbackModel errorCustomerFeedbackModel = new ErrorCustomerFeedbackModel();
                errorCustomerFeedbackModel.setFeedback(newData.getFeedback());
                errorCustomerFeedbackModel.setTaskItemId(newData.getTaskItemId());
                final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorCustomerFeedbackModel);
                if (this.customerFeedbackFacade.insert(ErrorCustomerFeedbackModel.class, errorCustomerFeedbackModel, auditTrail2)) {
                    super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorCustomerFeedbackModel).setInfo(role));
                } else {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                }
            } else {
                final CustomerFeedbackModel oldData;

                if((oldData = customerFeedbackFacade.getCustomerFeedbackByTaskItemId(newData.getTaskItemId())) == null) {
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.customerFeedbackFacade.insert(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    } else {
                        final ErrorCustomerFeedbackModel errorCustomerFeedbackModel = new ErrorCustomerFeedbackModel();
                        errorCustomerFeedbackModel.setFeedback(newData.getFeedback());
                        errorCustomerFeedbackModel.setTaskItemId(newData.getTaskItemId());
                        final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorCustomerFeedbackModel);
                        if (this.customerFeedbackFacade.insert(ErrorCustomerFeedbackModel.class, errorCustomerFeedbackModel, auditTrail2)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorCustomerFeedbackModel).setInfo(role));
                        } else {
                            super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                        }
                    }
                } else {
                    newData.setId(oldData.getId());
                    newData.setCreated(oldData.getCreated());
                    newData.setCreatedBy(oldData.getCreatedBy());
                    newData.setTaskItemId(oldData.getTaskItemId());
                    newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.customerFeedbackFacade.update(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                    }
                }
            }
        }
    }

}