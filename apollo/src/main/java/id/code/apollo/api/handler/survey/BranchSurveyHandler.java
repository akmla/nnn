package id.code.apollo.api.handler.survey;

import id.code.apollo.facade.survey.BranchSurveyFacade;
import id.code.apollo.model.survey.BranchSurveyModel;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.filter.BranchSurveyFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_FORBIDDEN;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

@SuppressWarnings("Duplicates")
public class BranchSurveyHandler extends RouteApiHandler<UserClaim> {
    private final BranchSurveyFacade branchSurveyFacade = new BranchSurveyFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<BranchSurveyFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<BranchSurveyModel> branchSurveys = this.branchSurveyFacade.getBranchSurveys(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, branchSurveys, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final BranchSurveyModel data = this.branchSurveyFacade.getBranchSurvey(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final BranchSurveyModel data = this.branchSurveyFacade.getBranchSurvey(id);
        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.branchSurveyFacade.delete(data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody BranchSurveyModel newData, Long id) throws Exception {
        final BranchSurveyModel oldData;
        ApiResponse responseCache;

        if ((oldData = this.branchSurveyFacade.getBranchSurvey(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.branchSurveyFacade.update(newData, auditTrail)) {
                super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody BranchSurveyModel newData) throws Exception {
        ApiResponse responseCache;
        Long lastFinishDate;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else if ((lastFinishDate = branchSurveyFacade.getLastFinishDate(newData.getBranchCode(), newData.getSurveyId())) != null && lastFinishDate > newData.getStartDate()) {
            super.sendResponse(serverExchange, RESPONSE_ERROR_FORBIDDEN);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if (this.branchSurveyFacade.insert(newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

}
