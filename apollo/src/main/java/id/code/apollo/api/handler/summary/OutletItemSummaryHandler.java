package id.code.apollo.api.handler.summary;

import id.code.apollo.facade.summary.OutletItemSummaryFacade;
import id.code.apollo.filter.OutletItemSummaryFilter;
import id.code.apollo.model.summary.OutletItemSummaryModel;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.master_data.security.Role._ROLE_ID_MERCHANDISER;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 11/09/2017.
 */

public class OutletItemSummaryHandler extends RouteApiHandler<UserClaim> {
    private final OutletItemSummaryFacade outletItemSummaryFacade = new OutletItemSummaryFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<OutletItemSummaryFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
            if (serverExchange.getAccessTokenPayload().getRoleId() == _ROLE_ID_MERCHANDISER) {
                filter.getParam().setRoleId(serverExchange.getAccessTokenPayload().getRoleId());
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<OutletItemSummaryModel> items = this.outletItemSummaryFacade.getOutletItemSummaries(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }
}