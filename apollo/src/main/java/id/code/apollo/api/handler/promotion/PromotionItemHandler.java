package id.code.apollo.api.handler.promotion;

import id.code.apollo.facade.promotion.PromotionItemFacade;
import id.code.apollo.filter.PromotionItemFilter;
import id.code.apollo.model.promotion.PromotionItemModel;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL_BRANCH;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 11/09/2017.
 */

public class PromotionItemHandler extends RouteApiHandler<UserClaim> {
    private final PromotionItemFacade promotionItemFacade = new PromotionItemFacade();

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final PromotionItemModel data = this.promotionItemFacade.getPromotionItem(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final PromotionItemModel data = this.promotionItemFacade.getPromotionItem(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.promotionItemFacade.delete(PromotionItemModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody PromotionItemModel newData, Long id) throws Exception {
        final PromotionItemModel oldData;
        ApiResponse responseCache;

        if ((oldData = this.promotionItemFacade.getPromotionItem(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim and upper data
            newData.setCode(StringUtility.trimNotNull(newData.getCode()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());
            newData.setProductCode(StringUtility.trimNotNull(newData.getProductCode()).toUpperCase());
            newData.setItemSize(StringUtility.trimNotNull(newData.getItemSize()).toUpperCase());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.promotionItemFacade.update(PromotionItemModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<PromotionItemFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL_BRANCH)) {
            filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
        }

        final List<PromotionItemModel> items = this.promotionItemFacade.getPromotionItems(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody PromotionItemModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());

            // Trim and upper data
            newData.setCode(StringUtility.trimNotNull(newData.getCode()).toUpperCase());
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());
            newData.setProductCode(StringUtility.trimNotNull(newData.getProductCode()).toUpperCase());
            newData.setItemSize(StringUtility.trimNotNull(newData.getItemSize()).toUpperCase());

            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if (this.promotionItemFacade.insert(PromotionItemModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }
}