package id.code.apollo.api.handler.result.survey;

import id.code.apollo.facade.result.survey.SurveyResultFacade;
import id.code.apollo.facade.task.TaskItemFacade;
import id.code.apollo.filter.SurveyResultFilter;
import id.code.apollo.model.Error.ErrorSurveyResultModel;
import id.code.apollo.model.result.survey.SurveyResultModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName._ALL;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 11/09/2017.
 */

public class SurveyResultHandler extends RouteApiHandler<UserClaim> {
    private final SurveyResultFacade surveyResultFacade = new SurveyResultFacade();
    private final TaskItemFacade taskItemFacade = new TaskItemFacade();

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final SurveyResultModel data = this.surveyResultFacade.getSurveyResult(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<SurveyResultFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<SurveyResultModel> items = this.surveyResultFacade.getSurveyResults(filter, serverExchange.getAccessTokenPayload().getRoleId());
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody SurveyResultModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final ErrorSurveyResultModel errorSurveyResultModel = new ErrorSurveyResultModel();
                errorSurveyResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                errorSurveyResultModel.setQuestionId(newData.getQuestionId());
                errorSurveyResultModel.setSurveyId(newData.getSurveyId());
                errorSurveyResultModel.setTaskItemId(newData.getTaskItemId());
                errorSurveyResultModel.setValue(newData.getValue());
                final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorSurveyResultModel);
                if (this.surveyResultFacade.insert(ErrorSurveyResultModel.class, errorSurveyResultModel, auditTrail2)) {
                    super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorSurveyResultModel).setInfo(role));
                }
            } else {
                final SurveyResultModel oldData;

                // Trim and upper data
                newData.setValue(StringUtility.trimNotNull(newData.getValue()).toUpperCase());

                if ((oldData = this.surveyResultFacade.getSurveyResultByUnique(newData.getTaskItemId(), newData.getSurveyId(), newData.getQuestionId())) == null) {
                    newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                    newData.setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.surveyResultFacade.insert(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    } else {
                        final ErrorSurveyResultModel errorSurveyResultModel = new ErrorSurveyResultModel();
                        errorSurveyResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                        errorSurveyResultModel.setQuestionId(newData.getQuestionId());
                        errorSurveyResultModel.setSurveyId(newData.getSurveyId());
                        errorSurveyResultModel.setTaskItemId(newData.getTaskItemId());
                        errorSurveyResultModel.setValue(newData.getValue());
                        final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorSurveyResultModel);
                        if (this.surveyResultFacade.insert(ErrorSurveyResultModel.class, errorSurveyResultModel, auditTrail2)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorSurveyResultModel).setInfo(role));
                        }
                    }
                } else {
                    newData.setId(oldData.getId());
                    newData.setCreated(oldData.getCreated());
                    newData.setCreatedBy(oldData.getCreatedBy());
                    newData.setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
                    newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

                    if (this.surveyResultFacade.update(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    }
                }
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody SurveyResultModel newData, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final ErrorSurveyResultModel errorSurveyResultModel = new ErrorSurveyResultModel();
                errorSurveyResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                errorSurveyResultModel.setQuestionId(newData.getQuestionId());
                errorSurveyResultModel.setSurveyId(newData.getSurveyId());
                errorSurveyResultModel.setTaskItemId(newData.getTaskItemId());
                errorSurveyResultModel.setValue(newData.getValue());
                final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorSurveyResultModel);
                if (this.surveyResultFacade.insert(ErrorSurveyResultModel.class, errorSurveyResultModel, auditTrail2)) {
                    super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorSurveyResultModel).setInfo(role));
                }
            } else {
                final SurveyResultModel oldData;

                // Trim and upper data
                newData.setValue(StringUtility.trimNotNull(newData.getValue()).toUpperCase());

                if ((oldData = this.surveyResultFacade.getSurveyResultByUnique(newData.getTaskItemId(), newData.getSurveyId(), newData.getQuestionId())) == null) {
                    newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                    newData.setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                    if (this.surveyResultFacade.insert(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    } else {
                        final ErrorSurveyResultModel errorSurveyResultModel = new ErrorSurveyResultModel();
                        errorSurveyResultModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                        errorSurveyResultModel.setQuestionId(newData.getQuestionId());
                        errorSurveyResultModel.setSurveyId(newData.getSurveyId());
                        errorSurveyResultModel.setTaskItemId(newData.getTaskItemId());
                        errorSurveyResultModel.setValue(newData.getValue());
                        final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorSurveyResultModel);
                        if (this.surveyResultFacade.insert(ErrorSurveyResultModel.class, errorSurveyResultModel, auditTrail2)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorSurveyResultModel).setInfo(role));
                        }
                    }
                } else {
                    newData.setId(oldData.getId());
                    newData.setCreated(oldData.getCreated());
                    newData.setCreatedBy(oldData.getCreatedBy());
                    newData.setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
                    newData.modify(serverExchange.getAccessTokenPayload().getClaimName());

                    final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

                    if (this.surveyResultFacade.update(newData, auditTrail)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                    }
                }
            }
        }
    }
}