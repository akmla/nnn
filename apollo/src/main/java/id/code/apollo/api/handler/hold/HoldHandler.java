package id.code.apollo.api.handler.hold;

import id.code.apollo.facade.task.HoldFacade;
import id.code.apollo.facade.task.TaskItemFacade;
import id.code.apollo.filter.HoldFilter;
import id.code.apollo.model.Error.ErrorHoldModel;
import id.code.apollo.model.task.HoldModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.component.utility.DateUtility;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.time.format.DateTimeFormatter;
import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.MonicaResponse.*;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 11/09/2017.
 */

@SuppressWarnings("Duplicates")
public class HoldHandler extends RouteApiHandler<UserClaim> {
    private final HoldFacade holdFacade = new HoldFacade();
    private final TaskItemFacade taskItemFacade = new TaskItemFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<HoldFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final List<HoldModel> items = this.holdFacade.getHolds(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final HoldModel data = this.holdFacade.getHold(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody HoldModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        final ApiResponse responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_HOLD);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append("ERROR");
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if ((newData.getPics() != null) && (!this.holdFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_HOLD_IMAGES, filename.toString()))) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final ErrorHoldModel errorHoldModel = new ErrorHoldModel();
                    errorHoldModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                    errorHoldModel.setPics(filename.toString());
                    errorHoldModel.setReasons(newData.getReasons());
                    errorHoldModel.setTaskItemId(newData.getTaskItemId());
                    final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorHoldModel);
                    if (this.holdFacade.insert(ErrorHoldModel.class, errorHoldModel, auditTrail2)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorHoldModel).setInfo(role));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                    }
                }
            } else {

                // Trim and upper data
                newData.setReasons(StringUtility.trimNotNull(newData.getReasons()).toUpperCase());

                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_HOLD);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(taskItem.getOutlet().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if ((newData.getPics() != null) && (!this.holdFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_HOLD_IMAGES, filename.toString()))) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final HoldModel oldData;

                    newData.setPics((newData.getPics() != null) ? filename.toString() : "NO IMAGE");

                    if ((oldData = holdFacade.getHoldByTaskItemId(newData.getTaskItemId())) == null) {
                        newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.holdFacade.insert(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                        } else {
                            final ErrorHoldModel errorHoldModel = new ErrorHoldModel();
                            errorHoldModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                            errorHoldModel.setPics(newData.getPics());
                            errorHoldModel.setReasons(newData.getReasons());
                            errorHoldModel.setTaskItemId(newData.getTaskItemId());
                            final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorHoldModel);
                            if (this.holdFacade.insert(ErrorHoldModel.class, errorHoldModel, auditTrail2)) {
                                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorHoldModel).setInfo(role));
                            } else {
                                super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                            }
                        }
                    } else {
                        newData.setId(oldData.getId());
                        newData.setCreated(oldData.getCreated());
                        newData.setCreatedBy(oldData.getCreatedBy());
                        newData.setTaskItemId(oldData.getTaskItemId());
                        newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.holdFacade.update(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                        } else {
                            super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                        }
                    }
                }
            }
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody HoldModel newData, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        final ApiResponse responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final TaskItemModel taskItem = taskItemFacade.getTaskItem(newData.getTaskItemId());

            if (taskItem == null) {
                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_HOLD);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append("ERROR");
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if ((newData.getPics() != null) && (!this.holdFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_HOLD_IMAGES, filename.toString()))) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final ErrorHoldModel errorHoldModel = new ErrorHoldModel();
                    errorHoldModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                    errorHoldModel.setPics(filename.toString());
                    errorHoldModel.setReasons(newData.getReasons());
                    errorHoldModel.setTaskItemId(newData.getTaskItemId());
                    final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorHoldModel);
                    if (this.holdFacade.insert(ErrorHoldModel.class, errorHoldModel, auditTrail2)) {
                        super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorHoldModel).setInfo(role));
                    } else {
                        super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                    }
                }
            } else {

                // Trim and upper data
                newData.setReasons(StringUtility.trimNotNull(newData.getReasons()).toUpperCase());

                final StringBuilder filename = new StringBuilder();

                filename.append(_FORMAT_HOLD);
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(taskItem.getOutlet().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(serverExchange.getAccessTokenPayload().getCode());
                filename.append(_SEPARATOR_PICTURE_NAME);
                filename.append(DateUtility.toLocalDateTime(System.currentTimeMillis())
                        .format(DateTimeFormatter.ofPattern(_DATE_FORMAT_DATABASE_NO_DASH)));
                filename.append(_EXTENSION_PICTURE);

                if ((newData.getPics() != null) && (!this.holdFacade.uploadFile(newData.getPics(), _UPLOAD_PATH_HOLD_IMAGES, filename.toString()))) {
                    super.sendResponse(serverExchange, RESPONSE_ERROR_UPLOAD);
                } else {
                    final HoldModel oldData;

                    newData.setPics((newData.getPics() != null) ? filename.toString() : "NO IMAGE");

                    if ((oldData = holdFacade.getHoldByTaskItemId(newData.getTaskItemId())) == null) {
                        newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.holdFacade.insert(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
                        } else {
                            final ErrorHoldModel errorHoldModel = new ErrorHoldModel();
                            errorHoldModel.newModel(serverExchange.getAccessTokenPayload().getClaimName());
                            errorHoldModel.setPics(newData.getPics());
                            errorHoldModel.setReasons(newData.getReasons());
                            errorHoldModel.setTaskItemId(newData.getTaskItemId());
                            final AuditTrailModel auditTrail2 = new AuditTrailModel(serverExchange, errorHoldModel);
                            if (this.holdFacade.insert(ErrorHoldModel.class, errorHoldModel, auditTrail2)) {
                                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, errorHoldModel).setInfo(role));
                            } else {
                                super.sendResponse(serverExchange, RESPONSE_ERROR_INSERT_DATA);
                            }
                        }
                    } else {
                        newData.setId(oldData.getId());
                        newData.setCreated(oldData.getCreated());
                        newData.setCreatedBy(oldData.getCreatedBy());
                        newData.setTaskItemId(oldData.getTaskItemId());
                        newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
                        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);
                        if (this.holdFacade.update(newData, auditTrail)) {
                            super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                        } else {
                            super.sendResponse(serverExchange, RESPONSE_ERROR_UPDATE_DATA);
                        }
                    }
                }
            }
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final HoldModel data = this.holdFacade.getHold(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        final boolean deleted = this.holdFacade.delete(HoldModel.class, data, auditTrail);
        super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }
}