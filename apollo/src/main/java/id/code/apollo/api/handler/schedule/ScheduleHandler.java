package id.code.apollo.api.handler.schedule;

import id.code.apollo.facade.schedule.ScheduleFacade;
import id.code.apollo.filter.ScheduleFilter;
import id.code.apollo.model.schedule.ScheduleModel;
import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.master_data.api.UserClaim;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.role.RoleModel;
import id.code.master_data.security.Role;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.master_data.AliasName.*;
import static id.code.master_data.MonicaResponse.RESPONSE_ERROR_DELETE_DATA;
import static id.code.master_data.security.Role._ROLE_ID_HO;
import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 11/09/2017.
 */

public class ScheduleHandler extends RouteApiHandler<UserClaim> {
    private final ScheduleFacade scheduleFacade = new ScheduleFacade();

    @HandlerGet
    public void handleGET(ServerExchange<UserClaim> serverExchange, Filter<ScheduleFilter> filter) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());

        if (serverExchange.getAccessTokenPayload().getRoleId() != _ROLE_ID_HO) {
            filter.getParam().setBranchCode(serverExchange.getAccessTokenPayload().getBranchCode());
        } else {
            if (filter.getParam().getBranchCode() != null && filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                filter.getParam().setUserId(serverExchange.getAccessTokenPayload().getUserId());
            }
        }

        final List<ScheduleModel> items = this.scheduleFacade.getSchedules(filter);
        super.sendResponse(serverExchange, new ApiResponse(HTTP_STATUS_OK, items, filter).setInfo(role));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        final ScheduleModel data = this.scheduleFacade.getSchedule(id);
        super.sendResponse(serverExchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data).setInfo(role));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<UserClaim> serverExchange, @RequestBody ScheduleModel newData) throws Exception {
        final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
        ApiResponse responseCache;

        if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            newData.newModel(serverExchange.getAccessTokenPayload().getClaimName());
            newData.setStatus(_STATUS_FALSE);
            newData.setIsUsed(_STATUS_FALSE);
            newData.setDraft(_STATUS_TRUE);
            newData.setCycleNum(_DEFAULT_CYCLE_NUM);

            // Trim and upper data
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());

            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, newData);

            if (this.scheduleFacade.insert(newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<UserClaim> serverExchange, Long id) throws Exception {
        final ScheduleModel data = this.scheduleFacade.getSchedule(id);

        // Audit Trail
        final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, data);

        if (data == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else {
            final boolean deleted = this.scheduleFacade.delete(ScheduleModel.class, data, auditTrail);
            super.sendResponse(serverExchange, deleted ? RESPONSE_OK : RESPONSE_ERROR_DELETE_DATA);
        }
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<UserClaim> serverExchange, @RequestBody ScheduleModel newData, Long id) throws Exception {
        final ScheduleModel oldData;
        ApiResponse responseCache;

        if ((oldData = this.scheduleFacade.getSchedule(id)) == null) {
            super.sendResponse(serverExchange, RESPONSE_DATA_NOT_FOUND);
        } else if ((responseCache = super.getRequestIdReferenceCache(serverExchange.getAccessTokenPayload(), newData.getRequestId())) != null) {
            super.sendResponse(serverExchange, responseCache);
        } else {
            final RoleModel role = Role.getCompleteRole(serverExchange.getAccessTokenPayload().getUserId());
            newData.setId(id);
            newData.setCreated(oldData.getCreated());
            newData.setCreatedBy(oldData.getCreatedBy());
            newData.modify(serverExchange.getAccessTokenPayload().getClaimName());
            newData.setDraft(_STATUS_FALSE);
            newData.setIsUsed(oldData.getIsUsed());

            // Trim and upper data
            newData.setName(StringUtility.trimNotNull(newData.getName()).toUpperCase());

            // Audit Trail
            final AuditTrailModel auditTrail = new AuditTrailModel(serverExchange, oldData);

            if (this.scheduleFacade.update(ScheduleModel.class, newData, auditTrail)) {
                super.sendResponse(serverExchange, serverExchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData).setInfo(role));
            }
        }
    }
}