package id.code.apollo.api;

import id.code.apollo.api.handler.FileContractHandler;
import id.code.apollo.api.handler.FilePictureHandler;
import id.code.apollo.api.handler.contract.ContractFileHandler;
import id.code.apollo.api.handler.contract.ContractHandler;
import id.code.apollo.api.handler.contract.ContractHistoryHandler;
import id.code.apollo.api.handler.contract.ContractItemHandler;
import id.code.apollo.api.handler.hold.HoldHandler;
import id.code.apollo.api.handler.message.MessageHandler;
import id.code.apollo.api.handler.outlet.OutletHandler;
import id.code.apollo.api.handler.outlet.OutletPictureHandler;
import id.code.apollo.api.handler.pos.PosHandler;
import id.code.apollo.api.handler.promotion.CategoryPromotionItemHandler;
import id.code.apollo.api.handler.promotion.PromotionItemHandler;
import id.code.apollo.api.handler.question.QuestionHandler;
import id.code.apollo.api.handler.report.JasperReportHandler;
import id.code.apollo.api.handler.report.ViewReportHandler;
import id.code.apollo.api.handler.result.competitor.CompetitorActivityHandler;
import id.code.apollo.api.handler.result.customer.CustomerFeedbackHandler;
import id.code.apollo.api.handler.result.product.ProductStockHandler;
import id.code.apollo.api.handler.result.promotion.ItemResultHandler;
import id.code.apollo.api.handler.result.promotion.PlanogramResultHandler;
import id.code.apollo.api.handler.result.selling_out.SellingOutHandler;
import id.code.apollo.api.handler.result.survey.SurveyResultHandler;
import id.code.apollo.api.handler.schedule.ScheduleDuplicateHandler;
import id.code.apollo.api.handler.schedule.ScheduleHandler;
import id.code.apollo.api.handler.schedule.ScheduleOutletHandler;
import id.code.apollo.api.handler.sio_type.SioTypeHandler;
import id.code.apollo.api.handler.sio_type.SioTypesPromotionHandler;
import id.code.apollo.api.handler.summary.OutletItemSummaryHandler;
import id.code.apollo.api.handler.summary.OutletSummaryHandler;
import id.code.apollo.api.handler.summary.OutletSurveySummaryHandler;
import id.code.apollo.api.handler.survey.BranchSurveyHandler;
import id.code.apollo.api.handler.survey.SurveyHandler;
import id.code.apollo.api.handler.task.TaskHandler;
import id.code.apollo.api.handler.task.TaskItemHandler;
import id.code.apollo.api.handler.thematic.ThematicHandler;
import id.code.apollo.api.handler.tl_promosi.TlPromotionCheckInHandler;
import id.code.apollo.api.handler.tl_promosi.TlPromotionVerificationHandler;
import id.code.master_data.api.handler.DateHandler;
import id.code.master_data.api.handler.branch.BranchHandler;
import id.code.master_data.api.handler.branch.BranchProductHandler;
import id.code.master_data.api.handler.branch.SubBranchHandler;
import id.code.master_data.api.handler.channel.ChannelHandler;
import id.code.master_data.api.handler.city.CityHandler;
import id.code.master_data.api.handler.district.DistrictHandler;
import id.code.master_data.api.handler.lookup.LookupHandler;
import id.code.master_data.api.handler.module.ModuleHandler;
import id.code.master_data.api.handler.product.BrandHandler;
import id.code.master_data.api.handler.product.ProductHandler;
import id.code.master_data.api.handler.province.ProvinceHandler;
import id.code.master_data.api.handler.region.RegionHandler;
import id.code.master_data.api.handler.role.RoleCompleteHandler;
import id.code.master_data.api.handler.role.RoleHandler;
import id.code.master_data.api.handler.role.RoleItemHandler;
import id.code.master_data.api.handler.setting.SettingHandler;
import id.code.master_data.api.handler.user.*;
import id.code.server.ApiHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static id.code.master_data.AliasName.*;

/**
 * Created by CODE.ID on 8/9/2017.
 */
public class Route {

    public static Map<String, ApiHandler> createRoutes(Properties properties) throws Exception {
        final Map<String, ApiHandler> routes = new HashMap<>();

        // General

        routes.put("date", new DateHandler());
        routes.put("file/contract/{file}", new FileContractHandler(_UPLOAD_PATH_CONTRACTS));
        routes.put("file/hold_image/{file}", new FilePictureHandler(_UPLOAD_PATH_HOLD_IMAGES));
        routes.put("file/outlet_image/{file}", new FilePictureHandler(_UPLOAD_PATH_OUTLETS_IMAGES));
        routes.put("file/task_result_image/{file}", new FilePictureHandler(_UPLOAD_PATH_TASK_RESULTS_IMAGES));

        routes.put("users", new UserHandler());
        routes.put("users/current", new CurrentUserHandler());
        routes.put("users/login", new LoginHandler());
        routes.put("users/register", new RegisterHandler());
        routes.put("users/password", new PasswordHandler());
        routes.put("users/forgot", new RequestForgotPasswordHandler());
        routes.put("users/erp", new ErpUserHandler());
        routes.put("users/branches", new BranchUserHandler());
        routes.put("user/change_fcm", new ChangeFcmTokenHandler());

        routes.put("branches", new BranchHandler());
        routes.put("branches/products", new BranchProductHandler());
        routes.put("branches/surveys", new BranchSurveyHandler());

        routes.put("brands", new BrandHandler());

        routes.put("channels", new ChannelHandler());

        routes.put("cities", new CityHandler());

        routes.put("districts", new DistrictHandler());

        routes.put("lookups", new LookupHandler());

        routes.put("modules", new ModuleHandler());

        routes.put("products", new ProductHandler());

        routes.put("provinces", new ProvinceHandler());

        routes.put("regions", new RegionHandler());

        routes.put("roles", new RoleHandler());

        routes.put("roles/items", new RoleItemHandler());

        routes.put("roles/completes", new RoleCompleteHandler());

        routes.put("settings", new SettingHandler());

        routes.put("sub_branches", new SubBranchHandler());

        // Apollo

        routes.put("contracts", new ContractHandler());
        routes.put("contracts/{contract_id}/document", new ContractFileHandler(_UPLOAD_PATH_CONTRACTS));
        routes.put("contracts/histories", new ContractHistoryHandler());

        routes.put("contracts/items", new ContractItemHandler());

        routes.put("holds", new HoldHandler());

        routes.put("messages", new MessageHandler());

        routes.put("outlets", new OutletHandler());
        routes.put("outlets/{outlet_id}/picture", new OutletPictureHandler(_UPLOAD_PATH_OUTLETS_IMAGES));

        routes.put("pos", new PosHandler());

        routes.put("promotions/categories", new CategoryPromotionItemHandler());

        routes.put("promotions/items", new PromotionItemHandler());

        routes.put("questions", new QuestionHandler());

        routes.put("schedules", new ScheduleHandler());

        routes.put("schedules/outlets", new ScheduleOutletHandler());
        routes.put("schedules/outlets/duplicate", new ScheduleDuplicateHandler());

        routes.put("sio_types", new SioTypeHandler());

        routes.put("sio_types/promotions", new SioTypesPromotionHandler());

        routes.put("surveys", new SurveyHandler());

        routes.put("tasks", new TaskHandler());

        routes.put("tasks/items", new TaskItemHandler());

        routes.put("thematics", new ThematicHandler());

        routes.put("tl_verifications", new TlPromotionVerificationHandler());

        routes.put("tl_promotion/check_in", new TlPromotionCheckInHandler());

        // Summary and Result

        routes.put("outlets_summaries", new OutletSummaryHandler());
        routes.put("outlets_items_summaries", new OutletItemSummaryHandler());
        routes.put("outlets_surveys_summaries", new OutletSurveySummaryHandler());

        routes.put("competitor_activities", new CompetitorActivityHandler());
        routes.put("customer_feedback", new CustomerFeedbackHandler());
        routes.put("items_results", new ItemResultHandler());
        routes.put("planograms_results", new PlanogramResultHandler());
        routes.put("products_stocks", new ProductStockHandler());
        routes.put("selling_out", new SellingOutHandler());
        routes.put("surveys_results", new SurveyResultHandler());

        routes.put("jasper_reports", new JasperReportHandler());
        routes.put("view_reports", new ViewReportHandler());

        return routes;
    }
}