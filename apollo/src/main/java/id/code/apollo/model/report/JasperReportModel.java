package id.code.apollo.model.report;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(_TABLE_NAME_JASPER_REPORTS)
public class JasperReportModel extends BaseModel {

    @JsonProperty(_REPORT_NAME)
    @TableColumn(_REPORT_NAME)
    @ValidateColumn(_REPORT_NAME)
    private String reportName;

    @JsonProperty(_FILE_NAME)
    @TableColumn(_FILE_NAME)
    @ValidateColumn(_FILE_NAME)
    private String fileName;

    @JsonProperty(_DESCRIPTION)
    @TableColumn(_DESCRIPTION)
    private String description;

    public String getReportName() {
        return reportName;
    }
    public String getFileName() {
        return fileName;
    }
    public String getDescription() {
        return description;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
