package id.code.apollo.model.result.promotion;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_PLANOGRAM_RESULT)
public class PlanogramResultModel extends BaseModel{

    @TableColumn(name = _TASK_ITEM_ID)
    @JsonProperty(value = _TASK_ITEM_ID)
    @ValidateColumn(name = _TASK_ITEM_ID)
    private long taskItemId;

    @JsonProperty(value = _PICS)
    @TableColumn(name = _PICS)
    @ValidateColumn(name = _PICS)
    private String pics;

    @JsonProperty(value = _PLANOGRAM_CONDITION)
    @TableColumn(name = _PLANOGRAM_CONDITION)
    @ValidateColumn(name = _PLANOGRAM_CONDITION)
    private String planogramCondition;

    @JsonProperty(_MD_ID)
    private long mdId;

    @JsonProperty(_OUTLET_ID)
    private long outletId;

    @JsonProperty(_BRANCH_CODE)
    private String branchCode;

    public long getTaskItemId() { return taskItemId; }
    public String getPics() {
        return pics;
    }
    public String getPlanogramCondition() {
        return planogramCondition;
    }
    public long getMdId() {
        return mdId;
    }
    public long getOutletId() {
        return outletId;
    }
    public String getBranchCode() {
        return branchCode;
    }

    public void setMdId(long mdId) {
        this.mdId = mdId;
    }
    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setTaskItemId(long taskItemId) { this.taskItemId = taskItemId; }
    public void setPics(String pics) {
        this.pics = pics;
    }
    public void setPlanogramCondition(String planogramCondition) {
        this.planogramCondition = planogramCondition;
    }

}
