package id.code.apollo.model.outlet;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.facade.outlet.OutletFacade;
import id.code.apollo.model.contract.ContractModel;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_OUTLETS)
public class OutletModel extends BaseModel {

    @TableColumn(name = _VERIFIED)
    @JsonProperty(value = _VERIFIED)
    private String verified;

    @JsonProperty(value = _VERIFIED_BY)
    private Long verifiedBy;

    @TableColumn(name = _LAT)
	@JsonProperty(value = _LAT)
	private Double lat;

	@TableColumn(name = _LNG)
	@JsonProperty(value = _LNG)
	private Double lng;

    @JsonProperty(value = _BRANCH_CODE)
    @TableColumn(name = _BRANCH_CODE)
    @ValidateColumn(name = _BRANCH_CODE)
    private String branchCode;

    @TableColumn(name = _CHANNEL_CODE)
	@JsonProperty(value = _CHANNEL_CODE)
    @ValidateColumn(name = _CHANNEL_CODE)
	private String channelCode;

    @TableColumn(name = _CODE, frameworkIncrement = true, frameworkIncrementProvider = OutletFacade.class)
    @JsonProperty(value = _CODE)
    private String code;

	@TableColumn(name = _PHONE)
	@JsonProperty(value = _PHONE)
    @ValidateColumn(name = _PHONE)
	private String phone;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
    @ValidateColumn(name = _NAME)
	private String name;

	@TableColumn(name = _AREA)
	@JsonProperty(value = _AREA)
    @ValidateColumn(name = _AREA)
	private String area;

	@TableColumn(name = _CONTRACT_CODE)
	@JsonProperty(value = _CONTRACT_CODE)
	private String contractCode;

	@TableColumn(name = _DIMENSION)
	@JsonProperty(value = _DIMENSION)
    @ValidateColumn(name = _DIMENSION)
	private String dimension;

	@TableColumn(name = _ADDRESS)
	@JsonProperty(value = _ADDRESS)
    @ValidateColumn(name = _ADDRESS)
	private String address;

	@TableColumn(name = _DISTRICT_CODE)
	@JsonProperty(value = _DISTRICT_CODE)
    @ValidateColumn(name = _DISTRICT_CODE)
	private String districtCode;

    @TableColumn(name = _PICS)
    @JsonProperty(value = _PICS)
    private String pics;

    @TableColumn(_WS_BNS)
    @JsonProperty(_WS_BNS)
    private String wsBns;

    @TableColumn(_LOKASI)
    @JsonProperty(_LOKASI)
//    @ValidateColumn(_LOKASI)
    private String lokasi;

    @TableColumn(_POSISI)
    @JsonProperty(_POSISI)
//    @ValidateColumn(_POSISI)
    private String posisi;

    @TableColumn(_JUMLAH_SKU)
    @JsonProperty(_JUMLAH_SKU)
    private long jumlahSku;

    @TableColumn(_JUMLAH_STOCK)
    @JsonProperty(_JUMLAH_STOCK)
    private long jumlahStock;

    @TableColumn(_STATUS)
    @JsonProperty(_STATUS)
    private int status;

	// Must set while load data
	@JsonProperty(value = _DISTRICT_NAME)
	private String districtName;

    @JsonProperty(value = _CITY_CODE)
    private String cityCode;

    @JsonProperty(value = _CITY_NAME)
    private String cityName;

    @JsonProperty(value = _BRANCH_NAME)
    private String branchName;

    @JsonProperty(value = _CHANNEL_NAME)
    private String channelName;

    @JsonProperty(value = _REGION_CODE)
    private String regionCode;

    @JsonProperty(value = _REGION_NAME)
    private String regionName;

    @JsonProperty(value = _SIO_TYPE_CODE)
    private String sioTypeCode;

    @JsonProperty(value = _SIO_TYPE_NAME)
    private String sioTypeName;

    @JsonProperty(value = _LINK)
    private String link;

    @JsonProperty(value = _FILE_SIZE)
    private long fileSize;

    @JsonProperty(value = _CONTRACT)
    private ContractModel contract;

    @JsonProperty(value = _OLD_CONTRACT)
    private String oldContractCode;

    @JsonProperty(value = _OLD_CONTRACT_STATUS)
    private String oldContractStatus;

    @JsonProperty(value = _VERIFIED_BY_NAME)
    private String verifiedByName;

    public String getVerified() {
        return verified;
    }
    public Double getLat() {
        return lat;
    }
    public Double getLng() {
        return lng;
    }
    public String getCode() {
        return code;
    }
    public String getChannelCode() {
        return channelCode;
    }
    public String getPhone() {
        return phone;
    }
    public String getName() {
        return name;
    }
    public String getArea() {
        return area;
    }
    public String getContractCode() {
        return contractCode;
    }
    public String getDimension() {
        return dimension;
    }
    public String getAddress() {
        return address;
    }
    public String getDistrictCode() {
        return districtCode;
    }
    public String getDistrictName() {
        return districtName;
    }
    public String getPics() {
        return pics;
    }
    public String getCityCode() {
        return cityCode;
    }
    public String getCityName() {
        return cityName;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getBranchName() {
        return branchName;
    }
    public String getChannelName() {
        return channelName;
    }
    public String getRegionCode() {
        return regionCode;
    }
    public String getRegionName() {
        return regionName;
    }
    public String getSioTypeCode() {
        return sioTypeCode;
    }
    public String getSioTypeName() {
        return sioTypeName;
    }
    public String getLink() {
        return link;
    }
    public Long getVerifiedBy() {
        return verifiedBy;
    }
    public long getFileSize() {
        return fileSize;
    }
    public ContractModel getContract() {
        return contract;
    }
    public String getOldContractCode() {
        return oldContractCode;
    }
    public String getOldContractStatus() {
        return oldContractStatus;
    }
    public String getWsBns() {
        return wsBns;
    }
    public String getLokasi() {
        return lokasi;
    }
    public String getPosisi() {
        return posisi;
    }
    public int getStatus() {
        return status;
    }
    public long getJumlahSku() {
        return jumlahSku;
    }
    public long getJumlahStock() {
        return jumlahStock;
    }
    public String getVerifiedByName() {
        return verifiedByName;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }
    public void setLat(Double lat) {
        this.lat = lat;
    }
    public void setLng(Double lng) {
        this.lng = lng;
    }
    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setArea(String area) {
        this.area = area;
    }
    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }
    public void setDimension(String dimension) {
        this.dimension = dimension;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public void setPics(String pics) {
        this.pics = pics;
    }
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
    public void setSioTypeCode(String sioTypeCode) {
        this.sioTypeCode = sioTypeCode;
    }
    public void setSioTypeName(String sioTypeName) {
        this.sioTypeName = sioTypeName;
    }
    public void setVerifiedBy(Long verifiedBy) {
        this.verifiedBy = verifiedBy;
    }
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public void setContract(ContractModel contract) {
        this.contract = contract;
    }
    public void setOldContractCode(String oldContractCode) {
        this.oldContractCode = oldContractCode;
    }
    public void setOldContractStatus(String oldContractStatus) {
        this.oldContractStatus = oldContractStatus;
    }
    public void setWsBns(String wsBns) {
        this.wsBns = wsBns;
    }
    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }
    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public void setJumlahSku(long jumlahSku) {
        this.jumlahSku = jumlahSku;
    }
    public void setJumlahStock(long jumlahStock) {
        this.jumlahStock = jumlahStock;
    }
    public void setVerifiedByName(String verifiedByName) {
        this.verifiedByName = verifiedByName;
    }
}