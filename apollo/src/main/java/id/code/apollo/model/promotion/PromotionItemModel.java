package id.code.apollo.model.promotion;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.product.ProductViewModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_PROMOTION_ITEMS)
public class PromotionItemModel extends BaseModel {

	@TableColumn(name = _CATEGORY_CODE)
	@JsonProperty(value = _CATEGORY_CODE)
	private String categoryCode;

	@TableColumn(name = _PRODUCT_CODE)
	@JsonProperty(value = _PRODUCT_CODE)
	private String productCode;

	@TableColumn(name = _CODE)
	@JsonProperty(value = _CODE)
	private String code;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	private String name;

	@TableColumn(name = _ITEM_SIZE)
	@JsonProperty(value = _ITEM_SIZE)
	private String itemSize;

	@JsonProperty(value = _PRODUCT)
	private ProductViewModel product;

	@JsonProperty(value = _CATEGORY)
	private CategoryPromotionItemModel category;


	public String getCategoryCode() {
		return categoryCode;
	}
	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public String getItemSize() {
		return itemSize;
	}
	public String getProductCode() {
		return productCode;
	}
    public ProductViewModel getProduct() {
        return product;
    }
    public CategoryPromotionItemModel getCategory() {
        return category;
    }


    public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
    public void setProduct(ProductViewModel product) {
        this.product = product;
    }
    public void setCategory(CategoryPromotionItemModel category) {
        this.category = category;
    }
}