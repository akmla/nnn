package id.code.apollo.model.tl_promotion;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_TL_PROMOTION_CHECK_IN)
public class TlPromotionCheckInModel extends BaseModel {

    @JsonProperty(_USER_ID)
    @TableColumn(name = _USER_ID)
    private long userId;

    @JsonProperty(_OUTLET_ID)
    @TableColumn(name = _OUTLET_ID)
    @ValidateColumn(name = _OUTLET_ID)
    private long outletId;

    @JsonProperty(_CHECK_IN)
    @TableColumn(name = _CHECK_IN)
    @ValidateColumn(name = _CHECK_IN)
    private Long checkIn;

    @JsonProperty(_CHECK_OUT)
    @TableColumn(name = _CHECK_OUT)
    private Long checkOut;

    @JsonProperty(_CHECK_IN_LAT)
    @TableColumn(name = _CHECK_IN_LAT)
    private Double checkInLat;

    @JsonProperty(_CHECK_IN_LNG)
    @TableColumn(name = _CHECK_IN_LNG)
    private Double checkInLng;

    @JsonProperty(_CHECK_OUT_LAT)
    @TableColumn(name = _CHECK_OUT_LAT)
    private Double checkOutLat;

    @JsonProperty(_CHECK_OUT_LNG)
    @TableColumn(name = _CHECK_OUT_LNG)
    private Double checkOutLng;

//    @JsonProperty(_TASK_ITEM_ID)
//    private Long taskItemId;
//

    public long getUserId() {
        return userId;
    }

    public long getOutletId() {
        return outletId;
    }

    public Long getCheckIn() {
        return checkIn;
    }

    public Long getCheckOut() {
        return checkOut;
    }

    public Double getCheckInLat() {
        return checkInLat;
    }

    public Double getCheckInLng() {
        return checkInLng;
    }

    public Double getCheckOutLat() {
        return checkOutLat;
    }

    public Double getCheckOutLng() {
        return checkOutLng;
    }

//    public Long getTaskItemId() {
//        return taskItemId;
//    }


    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }

    public void setCheckIn(Long checkIn) {
        this.checkIn = checkIn;
    }

    public void setCheckOut(Long checkOut) {
        this.checkOut = checkOut;
    }

    public void setCheckInLat(Double checkInLat) {
        this.checkInLat = checkInLat;
    }

    public void setCheckInLng(Double checkInLng) {
        this.checkInLng = checkInLng;
    }

    public void setCheckOutLat(Double checkOutLat) {
        this.checkOutLat = checkOutLat;
    }

    public void setCheckOutLng(Double checkOutLng) {
        this.checkOutLng = checkOutLng;
    }

//    public void setTaskItemId(Long taskItemId) {
//        this.taskItemId = taskItemId;
//    }
}
