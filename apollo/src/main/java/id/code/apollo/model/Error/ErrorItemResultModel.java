package id.code.apollo.model.Error;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table("ITEM_RESULTS_ERROR")
public class ErrorItemResultModel extends BaseModel {

    @TableColumn(name = _TASK_ITEM_ID)
    @JsonProperty(value = _TASK_ITEM_ID)
    @ValidateColumn(name = _TASK_ITEM_ID)
    private long taskItemId;

    @JsonProperty(_ITEM_CODE)
    @TableColumn(_ITEM_CODE)
    @ValidateColumn(_ITEM_CODE)
    private String itemCode;

    @JsonProperty(_SIO_TYPE_CODE)
    @TableColumn(_SIO_TYPE_CODE)
    private String sioTypeCode;

    @JsonProperty(_CATEGORY_ITEM_CODE)
    @TableColumn(_CATEGORY_ITEM_CODE)
    private String categoryItemCode;

    @JsonProperty(_THEMATIC_CODE)
    @TableColumn(_THEMATIC_CODE)
    private String thematicCode;

    @JsonProperty(_ITEM_NAME)
    @TableColumn(_ITEM_NAME)
    @ValidateColumn(_ITEM_NAME)
    private String itemName;

    @JsonProperty(_PICS)
    @TableColumn(_PICS)
    @ValidateColumn(_PICS)
    private String pics;

    @JsonProperty(_ITEM_CONDITION)
    @TableColumn(_ITEM_CONDITION)
    @ValidateColumn(_ITEM_CONDITION)
    private String itemCondition;

    @JsonProperty(_ACTION_TO_ITEM)
    @TableColumn(_ACTION_TO_ITEM)
    @ValidateColumn(_ACTION_TO_ITEM)
    private String actionToItem;

    @JsonProperty(_CUSTOM)
    @TableColumn(_CUSTOM)
    private Long custom;

    @JsonProperty(_ITEM_SIZE)
    @TableColumn(_ITEM_SIZE)
    private String itemSize;

    public long getTaskItemId() {
        return taskItemId;
    }
    public String getItemCode() {
        return itemCode;
    }
    public String getSioTypeCode() {
        return sioTypeCode;
    }
    public String getCategoryItemCode() {
        return categoryItemCode;
    }
    public String getThematicCode() {
        return thematicCode;
    }
    public String getItemName() {
        return itemName;
    }
    public String getPics() {
        return pics;
    }
    public String getItemCondition() {
        return itemCondition;
    }
    public String getActionToItem() {
        return actionToItem;
    }
    public String getItemSize() {
        return itemSize;
    }
    public Long getCustom() {
        return custom;
    }

    public void setTaskItemId(long taskItemId) {
        this.taskItemId = taskItemId;
    }
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }
    public void setSioTypeCode(String sioTypeCode) {
        this.sioTypeCode = sioTypeCode;
    }
    public void setCategoryItemCode(String categoryItemCode) {
        this.categoryItemCode = categoryItemCode;
    }
    public void setThematicCode(String thematicCode) {
        this.thematicCode = thematicCode;
    }
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
    public void setPics(String pics) {
        this.pics = pics;
    }
    public void setItemCondition(String itemCondition) {
        this.itemCondition = itemCondition;
    }
    public void setActionToItem(String actionToItem) {
        this.actionToItem = actionToItem;
    }
    public void setItemSize(String itemSize) {
        this.itemSize = itemSize;
    }
    public void setCustom(Long custom) {
        this.custom = custom;
    }

}
