package id.code.apollo.model.task;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.model.schedule.ScheduleModel;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.user.UserModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_TASKS)
public class TaskModel extends BaseModel {
	@TableColumn(name = _CYCLE_NUM)
	@JsonProperty(value = _CYCLE_NUM)
	@ValidateColumn(name = _CYCLE_NUM)
	private long cycleNum;

	@TableColumn(name = _SCHEDULE_ID)
	@JsonProperty(value = _SCHEDULE_ID)
    @ValidateColumn(name = _SCHEDULE_ID)
	private long scheduleId;

	@TableColumn(name = _USER_ID)
	@JsonProperty(value = _USER_ID)
    @ValidateColumn(name = _USER_ID)
	private long userId;

	@TableColumn(name = _START_DATE)
	@JsonProperty(value = _START_DATE)
	@ValidateColumn(name = _START_DATE)
	private Long startDate;

	@TableColumn(name = _FINISH_DATE)
	@JsonProperty(value = _FINISH_DATE)
//	@ValidateColumn(name = _FINISH_DATE)
	private Long finishDate;

	@TableColumn(name = _BRANCH_CODE)
    @JsonProperty(value = _BRANCH_CODE)
    @ValidateColumn(name = _BRANCH_CODE)
	private String branchCode;

	@JsonProperty(value = _BRANCH)
	private BranchViewModel branch;

	@JsonProperty(value = _MD)
	private UserModel merchandiser;

	@JsonProperty(value = _SCHEDULE)
	private ScheduleModel schedule;


    public long getUserId() {
		return userId;
	}
	public long getCycleNum() {
		return cycleNum;
	}
	public long getScheduleId() {
		return scheduleId;
	}
    public Long getStartDate() {
        return startDate;
    }
    public Long getFinishDate() {
		return finishDate;
	}
    public String getBranchCode() {
        return branchCode;
    }
    public BranchViewModel getBranch() {
        return branch;
    }
    public UserModel getMerchandiser() {
        return merchandiser;
    }
    public ScheduleModel getSchedule() {
        return schedule;
    }


    public void setUserId(long userId) {
		this.userId = userId;
	}
	public void setCycleNum(long cycleNum) {
		this.cycleNum = cycleNum;
	}
	public void setScheduleId(long scheduleId) {
		this.scheduleId = scheduleId;
	}
	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}
	public void setFinishDate(Long finishDate) {
		this.finishDate = finishDate;
	}
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setBranch(BranchViewModel branch) {
        this.branch = branch;
    }
    public void setMerchandiser(UserModel merchandiser) {
        this.merchandiser = merchandiser;
    }
    public void setSchedule(ScheduleModel schedule) {
        this.schedule = schedule;
    }

}