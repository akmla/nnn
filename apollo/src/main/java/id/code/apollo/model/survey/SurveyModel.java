package id.code.apollo.model.survey;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_SURVEYS)
public class SurveyModel extends BaseModel {

	@TableColumn(value = _NAME)
	@JsonProperty(value = _NAME)
	private String name;

	@JsonProperty(value = _IS_USED)
	@TableColumn(value = _IS_USED)
	private int isUsed;

	public int getIsUsed() {
		return isUsed;
	}
	public String getName() {
		return name;
	}

	public void setIsUsed(int isUsed) {
		this.isUsed = isUsed;
	}
	public void setName(String name) {
		this.name = name;
	}

}