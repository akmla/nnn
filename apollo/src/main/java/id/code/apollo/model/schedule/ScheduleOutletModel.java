package id.code.apollo.model.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.model.outlet.OutletModel;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_SCHEDULE_OUTLETS)
public class ScheduleOutletModel extends BaseModel {
	@TableColumn(name = _SCHEDULE_ID)
	@JsonProperty(value = _SCHEDULE_ID)
	private long scheduleId;

	@TableColumn(name = _OUTLET_ID)
	@JsonProperty(value = _OUTLET_ID)
	private long outletId;

	@TableColumn(name = _CYCLE_SEQ)
	@JsonProperty(value = _CYCLE_SEQ)
	private int cycleSeq;

	@TableColumn(name = _DAY)
	@JsonProperty(value = _DAY)
	private int day;

	@TableColumn(name = _REMARK)
	@JsonProperty(value = _REMARK)
	private String remark;

	@JsonProperty(value = _OUTLET)
	private OutletModel outlet;

	public long getScheduleId() {
		return scheduleId;
	}
	public long getOutletId() {
		return outletId;
	}
	public int getCycleSeq() {
		return cycleSeq;
	}
	public int getDay() {
		return day;
	}
	public OutletModel getOutlet() {
		return outlet;
	}
	public String getRemark() {
		return remark;
	}


	public void setScheduleId(long scheduleId) {
		this.scheduleId = scheduleId;
	}
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	public void setCycleSeq(int cycleSeq) {
		this.cycleSeq = cycleSeq;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public void setOutlet(OutletModel outlet) {
		this.outlet = outlet;
	}
    public void setRemark(String remark) {
        this.remark = remark;
    }
}