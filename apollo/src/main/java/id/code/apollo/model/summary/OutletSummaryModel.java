package id.code.apollo.model.summary;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.user.UserModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_OUTLET_SUMMARY)
public class OutletSummaryModel extends BaseModel {

    @TableColumn(name = _OUTLET_ID)
    @JsonProperty(value = _OUTLET_ID)
    private long outletId;

    @TableColumn(name = _LAST_VISITED_BY)
    @JsonProperty(value = _LAST_VISITED_BY)
    private Long lastVisitedBy;

    @TableColumn(name = _VERIFIED_BY)
    @JsonProperty(value = _VERIFIED_BY)
    private Long verifiedBy;

    @TableColumn(name = _VERIFIED_DATE)
    @JsonProperty(value = _VERIFIED_DATE)
    private Long verifiedDate;

    @TableColumn(name = _LAST_VISITED_DATE)
    @JsonProperty(value = _LAST_VISITED_DATE)
    private Long lastVisitedDate;

    @TableColumn(name = _CHANNEL_CODE)
    @JsonProperty(value = _CHANNEL_CODE)
    private String channelCode;

    @TableColumn(name = _CHANNEL_NAME)
    @JsonProperty(value = _CHANNEL_NAME)
    private String channelName;

    @TableColumn(name = _CONTRACT_CODE)
    @JsonProperty(value = _CONTRACT_CODE)
    private String contractCode;

    @TableColumn(name = _DISTRICT_CODE)
    @JsonProperty(value = _DISTRICT_CODE)
    private String districtCode;

    @TableColumn(name = _DISTRICT_NAME)
    @JsonProperty(value = _DISTRICT_NAME)
    private String districtName;

    @TableColumn(name = _BRANCH_CODE)
    @JsonProperty(value = _BRANCH_CODE)
    private String branchCode;

    @TableColumn(name = _BRANCH_NAME)
    @JsonProperty(value = _BRANCH_NAME)
    private String branchName;

    @TableColumn(name = _CITY_CODE)
    @JsonProperty(value = _CITY_CODE)
    private String cityCode;

    @TableColumn(name = _CITY_NAME)
    @JsonProperty(value = _CITY_NAME)
    private String cityName;

    @TableColumn(name = _REGION_CODE)
    @JsonProperty(value = _REGION_CODE)
    private String regionCode;

    @TableColumn(name = _REGION_NAME)
    @JsonProperty(value = _REGION_NAME)
    private String regionName;

    @TableColumn(name = _SIO_TYPE_CODE)
    @JsonProperty(value = _SIO_TYPE_CODE)
    private String sioTypeCode;

    @TableColumn(name = _SIO_TYPE_NAME)
    @JsonProperty(value = _SIO_TYPE_NAME)
    private String sioTypeName;

    @TableColumn(name = _ITEM_CHECK)
    @JsonProperty(value = _ITEM_CHECK)
    private Integer itemCheck;

    @TableColumn(name = _PLANOGRAM_CHECK)
    @JsonProperty(value = _PLANOGRAM_CHECK)
    private Integer planogramCheck;

    @TableColumn(name = _NNA_STOCK_CHECK)
    @JsonProperty(value = _NNA_STOCK_CHECK)
    private Integer nnaStockCheck;

    @TableColumn(name = _NNA_SALES_CHECK)
    @JsonProperty(value = _NNA_SALES_CHECK)
    private Integer nnaSalesCheck;

    @TableColumn(name = _NON_STOCK_CHECK)
    @JsonProperty(value = _NON_STOCK_CHECK)
    private Integer nonStockCheck;

    @TableColumn(name = _NON_SALES_CHECK)
    @JsonProperty(value = _NON_SALES_CHECK)
    private Integer nonSalesCheck;

    @TableColumn(name = _SURVEY_CHECK)
    @JsonProperty(value = _SURVEY_CHECK)
    private Integer surveyCheck;

    @TableColumn(name = _COMPETITORS_CHECK)
    @JsonProperty(value = _COMPETITORS_CHECK)
    private Integer competitorCheck;

    @TableColumn(name = _PAYMENT_CHECK)
    @JsonProperty(value = _PAYMENT_CHECK)
    private Integer paymentCheck;

    @JsonProperty(_FEEDBACK_CHECK)
    @TableColumn(_FEEDBACK_CHECK)
    private Integer feedbackCheck;

    @TableColumn(name = _LAST_TASK_ITEM_ID)
    @JsonProperty(value = _LAST_TASK_ITEM_ID)
    private Long lastTaskItemId;

    @JsonProperty(value = _MD_NAME)
    private String mdName;

    @JsonProperty(value = _MD)
    private UserModel merchandiser;

    public long getOutletId() {
        return outletId;
    }
    public Long getVerifiedBy() {
        return verifiedBy;
    }
    public Long getLastVisitedBy() {
        return lastVisitedBy;
    }
    public Long getVerifiedDate() {
        return verifiedDate;
    }
    public Long getLastVisitedDate() {
        return lastVisitedDate;
    }
    public String getChannelCode() {
        return channelCode;
    }
    public String getChannelName() {
        return channelName;
    }
    public String getContractCode() {
        return contractCode;
    }
    public String getDistrictCode() {
        return districtCode;
    }
    public String getDistrictName() {
        return districtName;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public String getBranchName() {
        return branchName;
    }
    public String getCityCode() {
        return cityCode;
    }
    public String getCityName() {
        return cityName;
    }
    public String getRegionCode() {
        return regionCode;
    }
    public String getRegionName() {
        return regionName;
    }
    public String getSioTypeCode() {
        return sioTypeCode;
    }
    public String getSioTypeName() {
        return sioTypeName;
    }
    public Integer getItemCheck() {
        return itemCheck;
    }
    public Integer getPlanogramCheck() {
        return planogramCheck;
    }
    public Integer getNnaStockCheck() {
        return nnaStockCheck;
    }
    public Integer getNnaSalesCheck() {
        return nnaSalesCheck;
    }
    public Integer getNonStockCheck() {
        return nonStockCheck;
    }
    public Integer getNonSalesCheck() {
        return nonSalesCheck;
    }
    public Integer getCompetitorCheck() {
        return competitorCheck;
    }
    public Integer getSurveyCheck() {
        return surveyCheck;
    }
    public Integer getPaymentCheck() {
        return paymentCheck;
    }
    public Integer getFeedbackCheck() {
        return feedbackCheck;
    }
    public Long getLastTaskItemId() {
        return lastTaskItemId;
    }
    public UserModel getMerchandiser() { return merchandiser; }
    public String getMdName() {
        return mdName;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }
    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
    public void setSioTypeCode(String sioTypeCode) {
        this.sioTypeCode = sioTypeCode;
    }
    public void setSioTypeName(String sioTypeName) {
        this.sioTypeName = sioTypeName;
    }
    public void setItemCheck(Integer itemCheck) {
        this.itemCheck = itemCheck;
    }
    public void setPlanogramCheck(Integer planogramCheck) {
        this.planogramCheck = planogramCheck;
    }
    public void setNnaStockCheck(Integer nnaStockCheck) {
        this.nnaStockCheck = nnaStockCheck;
    }
    public void setNnaSalesCheck(Integer nnaSalesCheck) {
        this.nnaSalesCheck = nnaSalesCheck;
    }
    public void setNonStockCheck(Integer nonStockCheck) {
        this.nonStockCheck = nonStockCheck;
    }
    public void setNonSalesCheck(Integer nonSalesCheck) {
        this.nonSalesCheck = nonSalesCheck;
    }
    public void setSurveyCheck(Integer surveyCheck) {
        this.surveyCheck = surveyCheck;
    }
    public void setCompetitorCheck(Integer competitorCheck) {
        this.competitorCheck = competitorCheck;
    }
    public void setPaymentCheck(Integer paymentCheck) {
        this.paymentCheck = paymentCheck;
    }
    public void setFeedbackCheck(Integer feedbackCheck) {
        this.feedbackCheck = feedbackCheck;
    }
    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }
    public void setVerifiedBy(Long verifiedBy) {
        this.verifiedBy = verifiedBy;
    }
    public void setLastVisitedBy(Long lastVisitedBy) {
        this.lastVisitedBy = lastVisitedBy;
    }
    public void setVerifiedDate(Long verifiedDate) {
        this.verifiedDate = verifiedDate;
    }
    public void setLastVisitedDate(Long lastVisitedDate) {
        this.lastVisitedDate = lastVisitedDate;
    }
    public void setLastTaskItemId(Long lastTaskItemId) {
        this.lastTaskItemId = lastTaskItemId;
    }
    public void setMerchandiser(UserModel merchandiser) {
        this.merchandiser = merchandiser;
    }
    public void setMdName(String mdName) {
        this.mdName = mdName;
    }
}
