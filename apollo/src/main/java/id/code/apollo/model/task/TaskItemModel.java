package id.code.apollo.model.task;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.model.contract.ContractItemModel;
import id.code.apollo.model.outlet.OutletModel;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_TASK_ITEMS)
public class TaskItemModel extends BaseModel {

	@TableColumn(name = _CHECK_OUT_LNG)
	@JsonProperty(value = _CHECK_OUT_LNG)
	private Double checkOutLng;

	@TableColumn(name = _CHECK_OUT_LAT)
	@JsonProperty(value = _CHECK_OUT_LAT)
	private Double checkOutLat;

	@TableColumn(name = _CHECK_IN_LAT)
	@JsonProperty(value = _CHECK_IN_LAT)
	private Double checkInLat;

	@TableColumn(name = _CHECK_IN_LNG)
	@JsonProperty(value = _CHECK_IN_LNG)
	private Double checkInLng;

	@TableColumn(name = _TASK_DATE)
	@JsonProperty(value = _TASK_DATE)
	private Long taskDate;

	@TableColumn(name = _CHECK_IN)
	@JsonProperty(value = _CHECK_IN)
	private Long checkIn;

	@TableColumn(name = _CHECK_OUT)
	@JsonProperty(value = _CHECK_OUT)
	private Long checkOut;

	@TableColumn(name = _OUTLET_ID)
	@JsonProperty(value = _OUTLET_ID)
	@ValidateColumn(name = _OUTLET_ID)
	private long outletId;

	@TableColumn(name = _TASK_ID)
	@JsonProperty(value = _TASK_ID)
	@ValidateColumn(name = _TASK_ID)
	private long taskId;

	@TableColumn(name = _REMARK)
	@JsonProperty(value = _REMARK)
	private String remark;

	@TableColumn(name = _STATUS)
	@JsonProperty(value = _STATUS)
	@ValidateColumn(name = _STATUS)
	private String status;

	@JsonProperty(value = _ROLE_MD)
	private String mdName;

	@JsonProperty(value = _OUTLET)
	private OutletModel outlet;

	@JsonProperty(value = _CONTRACT_ITEM)
	private ContractItemModel contractItem;

	public Double getCheckOutLng() {
		return checkOutLng;
	}
	public Double getCheckOutLat() {
		return checkOutLat;
	}
	public Double getCheckInLat() {
		return checkInLat;
	}
	public Double getCheckInLng() {
		return checkInLng;
	}
	public Long getTaskDate() {
		return taskDate;
	}
	public Long getCheckIn() {
		return checkIn;
	}
	public Long getCheckOut() {
		return checkOut;
	}
	public long getOutletId() {
		return outletId;
	}
	public long getTaskId() {
		return taskId;
	}
	public String getRemark() {
		return remark;
	}
	public String getStatus() {
		return status;
	}
	public OutletModel getOutlet() {
		return outlet;
	}
    public String getMdName() {
        return mdName;
    }
	public ContractItemModel getContractItem() {
		return contractItem;
	}

	public void setCheckOutLng(Double checkOutLng) {
		this.checkOutLng = checkOutLng;
	}
	public void setCheckOutLat(Double checkOutLat) {
		this.checkOutLat = checkOutLat;
	}
	public void setCheckInLat(Double checkInLat) {
		this.checkInLat = checkInLat;
	}
	public void setCheckInLng(Double checkInLng) {
		this.checkInLng = checkInLng;
	}
	public void setTaskDate(Long taskDate) {
		this.taskDate = taskDate;
	}
	public void setCheckIn(Long checkIn) {
		this.checkIn = checkIn;
	}
	public void setCheckOut(Long checkOut) {
		this.checkOut = checkOut;
	}
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setOutlet(OutletModel outlet) {
		this.outlet = outlet;
	}
    public void setMdName(String mdName) {
        this.mdName = mdName;
    }
	public void setContractItem(ContractItemModel contractItem) {
		this.contractItem = contractItem;
	}
}