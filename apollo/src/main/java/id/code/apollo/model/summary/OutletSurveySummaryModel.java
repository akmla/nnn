package id.code.apollo.model.summary;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.model.survey.SurveyModel;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_OUTLET_SURVEY_SUMMARY)
public class OutletSurveySummaryModel extends BaseModel {
    @JsonProperty(value = _OUTLET_ID)
    @TableColumn(name = _OUTLET_ID)
    private long outletId;

    @JsonProperty(value = _SURVEY_ID)
    @TableColumn(name = _SURVEY_ID)
    private long surveyId;

    @JsonProperty(value = _START_DATE)
    @TableColumn(name = _START_DATE)
    private Long startDate;

    @JsonProperty(value = _FINISH_DATE)
    @TableColumn(name = _FINISH_DATE)
    private Long finishDate;

    @JsonProperty(_SURVEY)
    private SurveyModel survey;

    public long getOutletId() {
        return outletId;
    }
    public long getSurveyId() {
        return surveyId;
    }
    public Long getStartDate() {
        return startDate;
    }
    public Long getFinishDate() {
        return finishDate;
    }

    public SurveyModel getSurvey() {
        return survey;
    }


    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }
    public void setSurveyId(long surveyId) {
        this.surveyId = surveyId;
    }
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }
    public void setFinishDate(Long finishDate) {
        this.finishDate = finishDate;
    }

    public void setSurvey(SurveyModel survey) {
        this.survey = survey;
    }
}
