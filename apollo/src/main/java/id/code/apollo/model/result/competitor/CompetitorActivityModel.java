package id.code.apollo.model.result.competitor;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.product.ProductViewModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_COMPETITOR_ACTIVITIES)
public class CompetitorActivityModel extends BaseModel {

	@TableColumn(name = _TASK_ITEM_ID)
	@JsonProperty(value = _TASK_ITEM_ID)
	@ValidateColumn(name = _TASK_ITEM_ID)
	private long taskItemId;

	@TableColumn(name = _ACTIVITY)
	@JsonProperty(value = _ACTIVITY)
//    @ValidateColumn(name = _ACTIVITY)
	private String activity;

	@TableColumn(name = _TYPE)
	@JsonProperty(value = _TYPE)
	@ValidateColumn(name = _TYPE)
	private String type;

	@TableColumn(name = _PRODUCT_CODE)
	@JsonProperty(value = _PRODUCT_CODE)
    @ValidateColumn(name = _PRODUCT_CODE)
	private String productCode;

	@JsonProperty(value = _PRODUCT)
	private ProductViewModel product;

	@JsonProperty(_BRANCH_CODE)
	private String branchCode;

	@JsonProperty(_MD_ID)
	private long mdId;

	@JsonProperty(_OUTLET_ID)
	private long outletId;


    public long getTaskItemId() {
        return taskItemId;
    }
	public String getActivity() {
		return activity;
	}
	public String getProductCode() {
		return productCode;
	}
	public String getType() {
		return type;
	}
    public String getBranchCode() {
        return branchCode;
    }
    public long getMdId() {
        return mdId;
    }
    public long getOutletId() {
        return outletId;
    }
    public ProductViewModel getProduct() {
        return product;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setMdId(long mdId) {
        this.mdId = mdId;
    }
    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }
    public void setTaskItemId(long taskItemId) {
        this.taskItemId = taskItemId;
    }
    public void setActivity(String activity) {
		this.activity = activity;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public void setProduct(ProductViewModel product) {
		this.product = product;
	}
}