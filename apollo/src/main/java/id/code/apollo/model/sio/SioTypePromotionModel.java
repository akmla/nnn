package id.code.apollo.model.sio;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_SIO_PROMOTION_TYPES)
public class SioTypePromotionModel extends BaseModel {

	@TableColumn(name = _CODE)
	@JsonProperty(value = _CODE)
	private String code;

	@TableColumn(name = _CATEGORY_ITEM_CODE)
	@JsonProperty(value = _CATEGORY_ITEM_CODE)
	private String categoryItemCode;

	@TableColumn(name = _SIO_TYPE_CODE)
	@JsonProperty(value = _SIO_TYPE_CODE)
	private String sioTypeCode;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	private String name;

    public String getCode() {
        return code;
    }
    public String getCategoryItemCode() {
        return categoryItemCode;
    }
    public String getSioTypeCode() {
        return sioTypeCode;
    }
    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public void setCategoryItemCode(String categoryItemCode) {
        this.categoryItemCode = categoryItemCode;
    }
    public void setSioTypeCode(String sioTypeCode) {
        this.sioTypeCode = sioTypeCode;
    }
    public void setName(String name) {
        this.name = name;
    }
}