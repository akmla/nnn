package id.code.apollo.model.sio;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_SIO_TYPES)
public class SioTypeModel extends BaseModel {

	@TableColumn(name = _CODE)
	@JsonProperty(value = _CODE)
	@ValidateColumn(name = _CODE)
	private String code;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	@ValidateColumn(name = _NAME)
	private String name;

	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}

    public void setCode(String code) {
        this.code = code;
    }
	public void setName(String name) {
		this.name = name;
	}
}