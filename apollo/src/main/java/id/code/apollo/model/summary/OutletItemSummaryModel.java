package id.code.apollo.model.summary;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_OUTLET_ITEM_SUMMARY)
public class OutletItemSummaryModel extends BaseModel {
	@TableColumn(name = _OUTLET_ID)
	@JsonProperty(value = _OUTLET_ID)
	private long outletId;

	@TableColumn(name = _ITEM_CODE)
	@JsonProperty(value = _ITEM_CODE)
	private String itemCode;

	@TableColumn(name = _CATEGORY_CODE)
	@JsonProperty(value = _CATEGORY_CODE)
	private String categoryCode;

	@JsonProperty(value = _LAST_ITEM_CONDITION)
    @TableColumn(name = _LAST_ITEM_CONDITION)
	private String lastItemCondition;

    @JsonProperty(value = _LAST_ACTION_TO_ITEM)
    @TableColumn(name = _LAST_ACTION_TO_ITEM)
	private String lastActionToItem;

    @JsonProperty(value = _ITEM_NAME)
    private String itemName;

    @JsonProperty(value = _SIO_TYPE_CODE)
    private String sioTypeCode;

    @JsonProperty(value = _THEMATIC_CODE)
    private String thematicCode;

	public long getOutletId() {
		return outletId;
	}
	public String getItemCode() {
		return itemCode;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public String getLastItemCondition() {
        return lastItemCondition;
    }
    public String getLastActionToItem() {
        return lastActionToItem;
    }
	public String getItemName() {
		return itemName;
	}
    public String getSioTypeCode() {
        return sioTypeCode;
    }
    public String getThematicCode() {
        return thematicCode;
    }


    public void setLastItemCondition(String lastItemCondition) {
        this.lastItemCondition = lastItemCondition;
    }
    public void setLastActionToItem(String lastActionToItem) {
        this.lastActionToItem = lastActionToItem;
    }
    public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
    public void setSioTypeCode(String sioTypeCode) {
        this.sioTypeCode = sioTypeCode;
    }
    public void setThematicCode(String thematicCode) {
        this.thematicCode = thematicCode;
    }

}