package id.code.apollo.model.thematic;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.product.ProductViewModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_THEMATICS)
public class ThematicModel extends BaseModel {

	@TableColumn(name = _CODE)
	@JsonProperty(value = _CODE)
    @ValidateColumn(name = _CODE)
	private String code;

	@TableColumn(name = _PRODUCT_CODE)
	@JsonProperty(value = _PRODUCT_CODE)
    @ValidateColumn(name = _PRODUCT_CODE)
	private String productCode;

	@TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
    @ValidateColumn(name = _NAME)
	private String name;

	@JsonProperty(value = _PRODUCT)
	private ProductViewModel product;

	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public String getProductCode() {
		return productCode;
	}
    public ProductViewModel getProduct() {
        return product;
    }

    public void setCode(String code) {
		this.code = code;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
    public void setProduct(ProductViewModel product) {
        this.product = product;
    }

}