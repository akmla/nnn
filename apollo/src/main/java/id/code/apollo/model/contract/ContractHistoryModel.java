package id.code.apollo.model.contract;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_CONTRACT_HISTORY)
public class ContractHistoryModel extends BaseModel {
    @JsonProperty(_OUTLET_ID)
    @TableColumn(_OUTLET_ID)
    private long outletId;

    @JsonProperty(_CONTRACT_CODE)
    @TableColumn(_CONTRACT_CODE)
    private String contractCode;

    @JsonProperty(_START_DATE)
    @TableColumn(_START_DATE)
    private Long startDate;

    @JsonProperty(_FINISH_DATE)
    @TableColumn(_FINISH_DATE)
    private Long finishDate;

    @JsonProperty(_REAL_CODE)
    private String realCode;

    public long getOutletId() {
        return outletId;
    }
    public String getRealCode() {
        return realCode;
    }
    public String getContractCode() {
        return contractCode;
    }
    public Long getStartDate() {
        return startDate;
    }
    public Long getFinishDate() {
        return finishDate;
    }

    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }
    public void setRealCode(String realCode){
        this.realCode = realCode;
    }
    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }
    public void setFinishDate(Long finishDate) {
        this.finishDate = finishDate;
    }
}
