package id.code.apollo.model.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_MESSAGES)
public class MessageModel extends BaseModel {
    @JsonProperty(_FCM_TOKEN)
    private String fcmToken;

	@TableColumn(name = _SENDER_ID)
	@JsonProperty(value = _SENDER_ID)
	private long senderId;

	@TableColumn(name = _USER_ID)
	@JsonProperty(value = _USER_ID)
	private long userId;

	@TableColumn(name = _SEND_DATE)
	@JsonProperty(value = _SEND_DATE)
	private Long sendDate;

	@TableColumn(name = _MESSAGE)
	@JsonProperty(value = _MESSAGE)
	private String message;

	@JsonProperty(_SENDER_NAME)
	private String senderName;

	@JsonProperty(_USER_NAME)
	private String userName;

	public long getUserId() {
		return userId;
	}
	public long getSenderId() {
		return senderId;
	}
	public Long getSendDate() {
		return sendDate;
	}
	public String getMessage() {
		return message;
	}
    public String getFcmToken() {
        return fcmToken;
    }
    public String getSenderName() {
        return senderName;
    }
    public String getUserName() {
        return userName;
    }


    public void setUserId(long userId) {
		this.userId = userId;
	}
	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}
	public void setSendDate(Long sendDate) {
		this.sendDate = sendDate;
	}
	public void setMessage(String message) {
		this.message = message;
	}
    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
}