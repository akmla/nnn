package id.code.apollo.model.summary;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_TASK_SUMMARY)
public class TaskSummaryModel extends BaseModel {

    @JsonProperty(value = _USER_ID)
    @TableColumn(name = _USER_ID)
    private long userId;

    @JsonProperty(value = _SCHEDULE_ID)
    @TableColumn(name = _SCHEDULE_ID)
    private long scheduleId;

    @JsonProperty(value = _CYCLE_NUM)
    @TableColumn(name = _CYCLE_NUM)
    private long cycleNum;

    @JsonProperty(value = _NEXT_CYCLE_SEQ)
    @TableColumn(name = _NEXT_CYCLE_SEQ)
    private long nextCycleSeq;

    @JsonProperty(value = _TOTAL_WEEK)
    @TableColumn(name = _TOTAL_WEEK)
    private long totalWeek;

    @JsonProperty(value = _BRANCH_CODE)
    @TableColumn(name = _BRANCH_CODE)
    private String branchCode;


    public long getUserId() {
        return userId;
    }
    public long getScheduleId() {
        return scheduleId;
    }
    public long getCycleNum() {
        return cycleNum;
    }
    public long getNextCycleSeq() {
        return nextCycleSeq;
    }
    public long getTotalWeek() {
        return totalWeek;
    }
    public String getBranchCode() {
        return branchCode;
    }


    public void setUserId(long userId) {
        this.userId = userId;
    }
    public void setScheduleId(long scheduleId) {
        this.scheduleId = scheduleId;
    }
    public void setCycleNum(long cycleNum) {
        this.cycleNum = cycleNum;
    }
    public void setNextCycleSeq(long nextCycleSeq) {
        this.nextCycleSeq = nextCycleSeq;
    }
    public void setTotalWeek(long totalWeek) {
        this.totalWeek = totalWeek;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}
