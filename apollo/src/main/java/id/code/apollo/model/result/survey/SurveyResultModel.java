package id.code.apollo.model.result.survey;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.model.question.QuestionModel;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_SURVEY_RESULT)
public class SurveyResultModel extends BaseModel {

    @TableColumn(name = _TASK_ITEM_ID)
    @JsonProperty(value = _TASK_ITEM_ID)
    @ValidateColumn(name = _TASK_ITEM_ID)
    private long taskItemId;

	@TableColumn(name = _SURVEY_ID)
	@JsonProperty(value = _SURVEY_ID)
	private long surveyId;

	@TableColumn(name = _QUESTION_ID)
	@JsonProperty(value = _QUESTION_ID)
	private long questionId;

    @TableColumn(name = _VALUE)
    @JsonProperty(value = _VALUE)
    private String value;

    @JsonProperty(value = _BRANCH_CODE)
    private String branchCode;

    @JsonProperty(value = _MD_ID)
    private long mdId;

    @JsonProperty(value = _OUTLET_ID)
    private long outletId;

    @JsonProperty(value = _QUESTION)
    private QuestionModel question;

    public long getQuestionId() {
		return questionId;
	}
    public long getSurveyId() {
		return surveyId;
	}
	public long getTaskItemId() { return taskItemId; }
    public String getValue() {
        return value;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public long getMdId() {
        return mdId;
    }
    public long getOutletId() {
        return outletId;
    }
    public QuestionModel getQuestion() {
        return question;
    }


    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }
    public void setSurveyId(long surveyId) {
		this.surveyId = surveyId;
	}
    public void setTaskItemId(long taskItemId) { this.taskItemId = taskItemId; }
    public void setValue(String value) {
        this.value = value;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public void setMdId(long mdId) {
        this.mdId = mdId;
    }
    public void setOutletId(long outletId) {
        this.outletId = outletId;
    }
    public void setQuestion(QuestionModel question) {
        this.question = question;
    }
}