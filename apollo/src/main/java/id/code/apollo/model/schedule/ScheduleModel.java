package id.code.apollo.model.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;
import id.code.master_data.model.branch.BranchModel;

import static id.code.master_data.AliasName.*;

@Table(name = _TABLE_NAME_SCHEDULES)
public class ScheduleModel extends BaseModel {

	@TableColumn(name = _CYCLE_NUM)
	@JsonProperty(value = _CYCLE_NUM)
	private int cycleNum;

	@TableColumn(name = _STATUS)
	@JsonProperty(value = _STATUS)
	private int status;

    @TableColumn(name = _DRAFT)
    @JsonProperty(value = _DRAFT)
    private int draft;

    @TableColumn(name = _NAME)
	@JsonProperty(value = _NAME)
	private String name;

    @TableColumn(name = _BRANCH_CODE)
    @JsonProperty(value = _BRANCH_CODE)
    @ValidateColumn(name = _BRANCH_CODE)
    private String branchCode;

    @TableColumn(name = _IS_USED)
    @JsonProperty(value = _IS_USED)
    private int isUsed;

    @JsonProperty(value = _BRANCH)
    private BranchModel branch;


    public int getIsUsed() {
        return isUsed;
    }
    public int getDraft() {
        return draft;
    }
    public int getStatus() {
        return status;
    }
    public long getCycleNum() {
		return cycleNum;
	}
    public String getBranchCode() {
        return branchCode;
    }
    public String getName() {
		return name;
	}
    public BranchModel getBranch() {
        return branch;
    }

    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }
    public void setDraft(int draft) {
        this.draft = draft;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public void setCycleNum(int cycleNum) {
        this.cycleNum = cycleNum;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
	public void setName(String name) {
		this.name = name;
	}
    public void setBranch(BranchModel branch) {
        this.branch = branch;
    }
}