package id.code.apollo.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.apollo.model.contract.ContractItemModel;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import java.util.List;

import static id.code.master_data.AliasName.*;

public class ContractItemValidation extends BaseModel {

    @JsonProperty(_CONTRACT_YEAR)
    @ValidateColumn(_CONTRACT_YEAR)
    private int contractYear;

    @JsonProperty(_TOP)
    @ValidateColumn(_TOP)
    private int top;

    @JsonProperty(_CONTRACT_CODE)
    private String contractCode;

    @JsonProperty(_START_DATE)
    @ValidateColumn(_START_DATE)
    private Long startDate;

    @JsonProperty(_FINISH_DATE)
    @ValidateColumn(_FINISH_DATE)
    private Long finishDate;

    @JsonProperty(_ITEMS)
    @ValidateColumn(_ITEMS)
    private List<ContractItemModel> items;

    public int getContractYear() {
        return contractYear;
    }
    public int getTop() {
        return top;
    }
    public String getContractCode() {
        return contractCode;
    }
    public Long getStartDate() {
        return startDate;
    }
    public Long getFinishDate() {
        return finishDate;
    }
    public List<ContractItemModel> getItems() {
        return items;
    }

    public void setContractYear(int contractYear) {
        this.contractYear = contractYear;
    }
    public void setTop(int top) {
        this.top = top;
    }
    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }
    public void setFinishDate(Long finishDate) {
        this.finishDate = finishDate;
    }
    public void setItems(List<ContractItemModel> items) {
        this.items = items;
    }
}
