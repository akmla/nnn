package id.code.apollo.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.master_data.model.BaseModel;

import java.util.List;

import static id.code.master_data.AliasName.*;

class SurveyValidation extends BaseModel {

    @JsonProperty(_NAME)
    @ValidateColumn(_NAME)
    private String name;

    @JsonProperty(_START_DATE)
    @ValidateColumn(_START_DATE)
    private Long startDate;

    @JsonProperty(_FINISH_DATE)
    @ValidateColumn(_FINISH_DATE)
    private Long finishDate;

    @JsonProperty(_TABLE_NAME_BRANCHES)
    @ValidateColumn(_TABLE_NAME_BRANCHES)
    private List<String> branches;


    public String getName() {
        return name;
    }
    public Long getStartDate() {
        return startDate;
    }
    public Long getFinishDate() {
        return finishDate;
    }
    public List<String> getBranches() {
        return branches;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }
    public void setFinishDate(Long finishDate) {
        this.finishDate = finishDate;
    }
    public void setBranches(List<String> branches) {
        this.branches = branches;
    }
}
