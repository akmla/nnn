package id.code.apollo;

import id.code.apollo.api.Route;
import id.code.master_data.api.middleware.AccessTokenMiddleware;
import id.code.server.ApiServerApplication;

/**
 * Created by CODE.ID on 8/9/2017.
 */

public class Main {
    public static void main(String[] args) {
        createApplication("config.properties").standaloneStart();
    }

    static ApiServerApplication createApplication(String configPath) {
        return new ApiServerApplication()
                .setPropertiesPath(configPath)
                .setRoutes(Route::createRoutes)
                .setAccessTokenMiddleware(AccessTokenMiddleware::new);
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }
}