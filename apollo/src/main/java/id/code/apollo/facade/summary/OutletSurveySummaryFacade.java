package id.code.apollo.facade.summary;

import id.code.apollo.filter.OutletSurveySummaryFilter;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.summary.OutletSurveySummaryModel;
import id.code.apollo.model.survey.SurveyModel;
import id.code.component.utility.DateUtility;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.user.BranchUserModel;

import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class OutletSurveySummaryFacade extends BaseFacade {

    public List<OutletSurveySummaryModel> getOutletSurveySummary(Filter<OutletSurveySummaryFilter> filter) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SUMMARY, OutletSurveySummaryModel.class)
                .includeAllJoin()
                .join(_OUTLET, OutletSummaryModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_OUTLET, _OUTLET_ID)
                .join(_SURVEY, SurveyModel.class).on(_SUMMARY, _SURVEY_ID).isEqual(_SURVEY, _ID)
                .orderBy(_SUMMARY, filter)
                .filter(_SUMMARY, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if(filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        if (filter.getParam().getSurveyName() != null) {
            sqlSelect.where(_SURVEY, _NAME).startWith(filter.getParam().getSurveyName());
        }

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            sqlSelect.where(_SUMMARY, _FINISH_DATE).equalsGreaterThan(
                    DateUtility.toUnixMillis(DateUtility.toLocalDate(System.currentTimeMillis())));
            sqlSelect.where(_SUMMARY, _START_DATE).equalsLessThan(
                    DateUtility.toUnixMillis(DateUtility.toLocalDate(System.currentTimeMillis())));
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<OutletSurveySummaryModel> listOutletSurveySummary = new ArrayList<>();
            while (result.moveNext()) {
                final OutletSurveySummaryModel outletSurveySummary = result.getItem(_SUMMARY, OutletSurveySummaryModel.class);
                final SurveyModel survey = result.getItem(_SURVEY, SurveyModel.class);
                outletSurveySummary.setSurvey(survey);
                listOutletSurveySummary.add(outletSurveySummary);
            }
            return listOutletSurveySummary;
        }
    }
}
