package id.code.apollo.facade.thematic;

import id.code.apollo.filter.ThematicFilter;
import id.code.apollo.model.thematic.ThematicModel;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.branch.BranchProductModel;
import id.code.master_data.model.product.ProductViewModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class ThematicFacade extends BaseFacade {

    public List<ThematicModel> getThematics(Filter<ThematicFilter> filter) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_THEMATIC, ThematicModel.class)
                .includeAllJoin()
                .join(_PRODUCT, ProductViewModel.class).on(_THEMATIC, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .orderBy(_THEMATIC, filter)
                .filter(_THEMATIC, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL_BRANCH)) {
                sqlSelect.where(_THEMATIC, _PRODUCT_CODE).in(
                        QueryBuilder.select(BranchProductModel.class, _PRODUCT_CODE)
                                .where(_BRANCH_CODE).in(
                                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE)
                                                .where(_USER_ID).isEqual(filter.getParam().getUserId())
                        )
                );
            } else if (!filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_THEMATIC, _PRODUCT_CODE).in(
                        QueryBuilder.select(BranchProductModel.class, _PRODUCT_CODE)
                                .where(_BRANCH_CODE).isEqual(filter.getParam().getBranchCode())
                );
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<ThematicModel> listThematics = new ArrayList<>();
            while (result.moveNext()) {
                final ThematicModel thematic = result.getItem(_THEMATIC, ThematicModel.class);
                thematic.setProduct(result.getItem(_PRODUCT, ProductViewModel.class));
                listThematics.add(thematic);
            }
            return listThematics;
        }
    }

    public ThematicModel getThematic(long thematicId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_THEMATIC, ThematicModel.class)
                .includeAllJoin()
                .join(_PRODUCT, ProductViewModel.class).on(_THEMATIC, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .where(_THEMATIC, _ID).isEqual(thematicId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final ThematicModel thematic = result.getItem(_THEMATIC, ThematicModel.class);
                thematic.setProduct(result.getItem(_PRODUCT, ProductViewModel.class));
                return thematic;
            }
            return null;
        }
    }

}