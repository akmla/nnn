package id.code.apollo.facade.schedule;

import id.code.apollo.filter.ScheduleOutletFilter;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.schedule.ScheduleOutletModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.validation.ScheduleOutletValidation;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

@SuppressWarnings("Duplicates")
public class ScheduleOutletFacade extends BaseFacade {

    public List<ScheduleOutletModel> getScheduleOutlets(Filter<ScheduleOutletFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SCHEDULE_OUTLET, ScheduleOutletModel.class)
                .includeAllJoin()
                .join(_OUTLET, OutletModel.class).on(_SCHEDULE_OUTLET, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .join(_SUMMARY, OutletSummaryModel.class).on(_OUTLET, _ID).isEqual(_SUMMARY, _OUTLET_ID)
//                .orderBy(_SCHEDULE_OUTLET, filter)
                .filter(_SCHEDULE_OUTLET, filter)
                .limitOffset(filter);

        if (filter.getOrderColumn() != null) {

            if (filter.getOrderColumn().equalsIgnoreCase(_ID)) {
                if (filter.isOrderAscending()) {
                    sqlSelect.orderBy(_SCHEDULE_OUTLET, _ID).asc();
                } else {
                    sqlSelect.orderBy(_SCHEDULE_OUTLET, _ID).desc();
                }
            }

            if (filter.getOrderColumn().equalsIgnoreCase(_LAST_MODIFIED)) {
                if (filter.isOrderAscending()) {
                    sqlSelect.orderBy(_SCHEDULE_OUTLET, _LAST_MODIFIED).asc();
                } else {
                    sqlSelect.orderBy(_SCHEDULE_OUTLET, _LAST_MODIFIED).desc();
                }
            }

            if (filter.getOrderColumn().equalsIgnoreCase(_DAY)) {
                if (filter.isOrderAscending()) {
                    sqlSelect.orderBy(_SCHEDULE_OUTLET, _DAY).asc();
                } else {
                    sqlSelect.orderBy(_SCHEDULE_OUTLET, _DAY).desc();
                }
            }

            if (filter.getOrderColumn().equalsIgnoreCase(_SCHEDULE_ID)) {
                if (filter.isOrderAscending()) {
                    sqlSelect.orderBy(_SCHEDULE_OUTLET, _SCHEDULE_ID).asc();
                } else {
                    sqlSelect.orderBy(_SCHEDULE_OUTLET, _SCHEDULE_ID).desc();
                }
            }

            if (filter.getOrderColumn().equalsIgnoreCase(_CYCLE_SEQ)) {
                if (filter.isOrderAscending()) {
                    sqlSelect.orderBy(_SCHEDULE_OUTLET, _CYCLE_SEQ).asc();
                } else {
                    sqlSelect.orderBy(_SCHEDULE_OUTLET, _CYCLE_SEQ).desc();
                }
            }

            if (filter.getOrderColumn().equalsIgnoreCase(_CITY_NAME)) {
                if (filter.isOrderAscending()) {
                    sqlSelect.orderBy(_SUMMARY, _CITY_NAME).asc();
                } else {
                    sqlSelect.orderBy(_SUMMARY, _CITY_NAME).desc();
                }
            }

            if (filter.getOrderColumn().equalsIgnoreCase(_DISTRICT_NAME)) {
                if (filter.isOrderAscending()) {
                    sqlSelect.orderBy(_SUMMARY, _DISTRICT_NAME).asc();
                } else {
                    sqlSelect.orderBy(_SUMMARY, _DISTRICT_NAME).desc();
                }
            }

            if (filter.getOrderColumn().equalsIgnoreCase(_OUTLET_NAME)) {
                if (filter.isOrderAscending()) {
                    sqlSelect.orderBy(_OUTLET, _NAME).asc();
                } else {
                    sqlSelect.orderBy(_OUTLET, _NAME).desc();
                }
            }

        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<ScheduleOutletModel> listScheduleOutlets = new ArrayList<>();
            while (result.moveNext()) {
                final ScheduleOutletModel scheduleOutlet = result.getItem(_SCHEDULE_OUTLET, ScheduleOutletModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                final OutletSummaryModel summary = result.getItem(_SUMMARY, OutletSummaryModel.class);

                outlet.setBranchCode(summary.getBranchCode());
                outlet.setBranchName(summary.getBranchName());
                outlet.setChannelCode(summary.getChannelCode());
                outlet.setChannelName(summary.getChannelName());
                outlet.setCityCode(summary.getCityCode());
                outlet.setCityName(summary.getCityName());
                outlet.setDistrictName(summary.getDistrictName());
                outlet.setRegionCode(summary.getRegionCode());
                outlet.setRegionName(summary.getRegionName());
                outlet.setSioTypeCode(summary.getSioTypeCode());
                outlet.setSioTypeName(summary.getSioTypeName());
                outlet.setVerifiedBy(summary.getVerifiedBy());

                scheduleOutlet.setOutlet(outlet);
                listScheduleOutlets.add(scheduleOutlet);
            }
            return listScheduleOutlets;
        }
    }

    List<ScheduleOutletModel> getScheduleOutletsByScheduleId(long scheduleId, SimpleTransaction connection) throws
            SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(ScheduleOutletModel.class)
                .where(_SCHEDULE_ID).isEqual(scheduleId)
                .orderBy(_CYCLE_SEQ).asc();

        try (final ResultBuilder result = sqlSelect.execute(connection)) {
            return (result != null) ? result.getItems(ScheduleOutletModel.class) : null;
        }
    }

    public ScheduleOutletModel getScheduleOutlet(long scheduleOutletId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SCHEDULE_OUTLET, ScheduleOutletModel.class)
                .includeAllJoin()
                .join(_OUTLET, OutletModel.class).on(_SCHEDULE_OUTLET, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .where(_SCHEDULE_OUTLET, _ID).isEqual(scheduleOutletId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                ScheduleOutletModel scheduleOutlet = result.getItem(_SCHEDULE_OUTLET, ScheduleOutletModel.class);
                scheduleOutlet.setOutlet(result.getItem(_OUTLET, OutletModel.class));
                return scheduleOutlet;
            }
            return null;
        }
    }

    public boolean insert(ScheduleOutletValidation scheduleOutletValidation, AuditTrailModel auditTrail) throws SQLException, IOException, QueryBuilderException, IllegalAccessException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final ScheduleOutletModel scheduleOutlet = new ScheduleOutletModel();
            scheduleOutlet.newModel(auditTrail.getCreatedBy());
            scheduleOutlet.setCycleSeq(scheduleOutletValidation.getCycleSeq());
            scheduleOutlet.setDay(scheduleOutletValidation.getDay());
            scheduleOutlet.setScheduleId(scheduleOutletValidation.getScheduleId());
            scheduleOutlet.setRemark(scheduleOutletValidation.getRemark());

            for (int i = 0; scheduleOutletValidation.getOutletId().size() > i; i++) {
                scheduleOutlet.setOutletId(scheduleOutletValidation.getOutletId().get(i));

                final ResultBuilder resultOutlet = QueryBuilder.select(OutletModel.class)
                        .where(_ID).isEqual(scheduleOutletValidation.getOutletId().get(i))
                        .execute(sqlTransaction);

                if (resultOutlet.moveNext()) {
                    final OutletModel outlet = resultOutlet.getItem(OutletModel.class);
                    if (outlet != null && outlet.getContractCode() != null) {
                        final UpdateResult result = QueryBuilder.insert(scheduleOutlet).execute(sqlTransaction);
                        auditTrail.prepareAudit(_TABLE_NAME_SCHEDULE_OUTLETS, scheduleOutletValidation);

                        if (!result.isModified() || (!super.insertAudit(sqlTransaction, auditTrail))) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }
                }
            }

            sqlTransaction.commitTransaction();
            return true;
        }
    }

}