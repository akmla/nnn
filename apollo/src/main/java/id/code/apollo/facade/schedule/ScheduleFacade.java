package id.code.apollo.facade.schedule;

import id.code.apollo.Main;
import id.code.apollo.filter.ScheduleFilter;
import id.code.apollo.model.schedule.ScheduleDuplicateModel;
import id.code.apollo.model.schedule.ScheduleModel;
import id.code.apollo.model.schedule.ScheduleOutletModel;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.branch.BranchModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class ScheduleFacade extends BaseFacade {
    private final ScheduleOutletFacade scheduleOutletFacade = new ScheduleOutletFacade();

    public List<ScheduleModel> getSchedules(Filter<ScheduleFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SCHEDULE, ScheduleModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchModel.class).on(_SCHEDULE, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .orderBy(_SCHEDULE, filter)
                .filter(_SCHEDULE, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_SCHEDULE, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_SCHEDULE, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<ScheduleModel> listSchedules = new ArrayList<>();
            while (result.moveNext()) {
                final ScheduleModel schedule = result.getItem(_SCHEDULE, ScheduleModel.class);
                schedule.setBranch(result.getItem(_BRANCH, BranchModel.class));
                listSchedules.add(schedule);
            }
            return listSchedules;
        }
    }

    public ScheduleModel getSchedule(long scheduleId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SCHEDULE, ScheduleModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchModel.class).on(_SCHEDULE, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .where(_SCHEDULE, _ID).isEqual(scheduleId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final ScheduleModel schedule = result.getItem(_SCHEDULE, ScheduleModel.class);
                schedule.setBranch(result.getItem(_BRANCH, BranchModel.class));
                return schedule;
            }
            return null;
        }
    }

    public ScheduleModel getSchedule(long scheduleId, SimpleTransaction sqlTransaction) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SCHEDULE, ScheduleModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchModel.class).on(_SCHEDULE, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .where(_SCHEDULE, _ID).isEqual(scheduleId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
            if (result.moveNext()) {
                final ScheduleModel schedule = result.getItem(_SCHEDULE, ScheduleModel.class);
                schedule.setBranch(result.getItem(_BRANCH, BranchModel.class));
                return schedule;
            }
            return null;
        }
    }

    public ScheduleModel duplicate(ScheduleModel newSchedule, ScheduleDuplicateModel scheduleDuplicate, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            auditTrail.prepareAudit(_TABLE_NAME_SCHEDULES, newSchedule);

            if (super.insert(ScheduleModel.class, newSchedule, auditTrail)) {
                final List<ScheduleOutletModel> scheduleOutlets = scheduleOutletFacade.getScheduleOutletsByScheduleId(scheduleDuplicate.getScheduleId(), sqlTransaction);

                if (scheduleDuplicate.getAction().equalsIgnoreCase(_ADD)) {
                    for (int i = 0; i < scheduleDuplicate.getOutlets().size(); i++) {
                        final ScheduleOutletModel scheduleOutlet = new ScheduleOutletModel();
                        scheduleOutlet.newModel(newSchedule.getCreatedBy());
                        scheduleOutlet.setScheduleId(newSchedule.getId());
                        scheduleOutlet.setOutletId(scheduleDuplicate.getOutlets().get(i));
                        scheduleOutlet.setCycleSeq(scheduleDuplicate.getCycleSeq());
                        scheduleOutlet.setDay(scheduleDuplicate.getDay());
                        scheduleOutlets.add(scheduleOutlet);
                    }
                }

                for (ScheduleOutletModel scheduleOutlet : scheduleOutlets) {
                    if (!(scheduleOutlet.getCycleSeq() == scheduleDuplicate.getCycleSeq() &&
                            scheduleOutlet.getDay() == scheduleDuplicate.getDay() &&
                            scheduleOutlet.getOutletId() == scheduleDuplicate.getOutletId() &&
                            scheduleDuplicate.getAction().equalsIgnoreCase(_DELETE))) {

                        scheduleOutlet.newModel(auditTrail.getCreatedBy());
                        scheduleOutlet.setScheduleId(newSchedule.getId());

                        final UpdateResult result = QueryBuilder.insert(scheduleOutlet).execute(sqlTransaction);
                        auditTrail.prepareAudit(_TABLE_NAME_SCHEDULE_OUTLETS, scheduleOutlet);

                        if (!result.isModified() || (!super.insertAudit(sqlTransaction, auditTrail))) {
                            sqlTransaction.rollbackTransaction();
                            return null;
                        }
                    }
                }

                sqlTransaction.commitTransaction();
                return newSchedule;
            }

            sqlTransaction.rollbackTransaction();
            return null;
        }
    }

    public boolean insert(ScheduleModel newSchedule, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final String keyword = _SCHEDULE.concat(_DASH).concat(newSchedule.getBranchCode()).concat(_DASH);
            String generatedName = keyword;

            final ScheduleModel lastSchedule = QueryBuilder.select(ScheduleModel.class)
                    .where(_NAME).startWith(keyword)
                    .orderBy(_NAME).desc()
                    .limit(1)
                    .getResult(sqlTransaction)
                    .executeItem(ScheduleModel.class);

            if (lastSchedule != null) {
                final String seqNo = lastSchedule.getName().substring(keyword.length());
                if (Main.isNumeric(seqNo)) {
                    final long numSeqNo = Long.parseLong(seqNo) + 1;
                    final String trimSeqNo = String.valueOf(numSeqNo);
                    if (trimSeqNo.length() == 1) {
                        generatedName = generatedName.concat("0000").concat(String.valueOf(numSeqNo));
                    } else if (trimSeqNo.length() == 2) {
                        generatedName = generatedName.concat("000").concat(String.valueOf(numSeqNo));
                    } else if (trimSeqNo.length() == 3) {
                        generatedName = generatedName.concat("00").concat(String.valueOf(numSeqNo));
                    } else if (trimSeqNo.length() == 4) {
                        generatedName = generatedName.concat("0").concat(String.valueOf(numSeqNo));
                    } else if (trimSeqNo.length() == 5) {
                        generatedName = generatedName.concat(String.valueOf(numSeqNo));
                    }
                } else {
                    generatedName = keyword.concat(String.valueOf(System.currentTimeMillis()));
                }
            } else {
                generatedName = keyword.concat(_DEFAULT_SEQ_NO);
            }

            newSchedule.setName(generatedName);
            final UpdateResult resultInsert = QueryBuilder.insert(newSchedule).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_SCHEDULES, newSchedule);

            if (resultInsert.isModified() && this.insertAudit(sqlTransaction, auditTrail)) {
                sqlTransaction.commitTransaction();
                return true;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}