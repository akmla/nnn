package id.code.apollo.facade.outlet;

import id.code.apollo.filter.OutletFilter;
import id.code.apollo.filter.OutletSummaryFilter;
import id.code.apollo.model.contract.ContractHistoryModel;
import id.code.apollo.model.contract.ContractModel;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.sio.SioTypeModel;
import id.code.apollo.model.sio.SioTypePromotionModel;
import id.code.apollo.model.summary.OutletItemSummaryModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.summary.OutletSurveySummaryModel;
import id.code.apollo.model.survey.BranchSurveyModel;
import id.code.apollo.model.survey.SurveyModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.component.utility.StringUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.builder.increment.FrameworkIncrementProvider;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.channel.ChannelViewModel;
import id.code.master_data.model.city.CityViewModel;
import id.code.master_data.model.district.DistrictViewModel;
import id.code.master_data.model.region.RegionViewModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

@SuppressWarnings("Duplicates")
public class OutletFacade extends BaseFacade implements FrameworkIncrementProvider<OutletModel, String> {

    public List<OutletModel> getOutlets(Filter<OutletFilter> filter, Filter<OutletSummaryFilter> outletSummaryFilter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_OUTLETS, OutletModel.class)
                .includeAllJoin()
                .join(_SUMMARY, OutletSummaryModel.class).on(_TABLE_NAME_OUTLETS, _ID).isEqual(_SUMMARY, _OUTLET_ID)
                .leftJoin(_CONTRACT, ContractModel.class).on(_TABLE_NAME_OUTLETS, _CONTRACT_CODE).isEqual(_CONTRACT, _CODE)
                .orderBy(_TABLE_NAME_OUTLETS, filter)
                .filter(_TABLE_NAME_OUTLETS, filter)
                .filter(_SUMMARY, outletSummaryFilter)
                .limitOffset(filter);

        if (filter.getParam().getVerified() != null) {
            if (filter.getParam().getVerified().equalsIgnoreCase(_VERIFIED)) {
                sqlSelect.where(_TABLE_NAME_OUTLETS, _VERIFIED).isEqual(_VERIFIED);
            } else {
                sqlSelect.where(_TABLE_NAME_OUTLETS, _VERIFIED).isNotEqual(_VERIFIED);
            }
        }

        if (outletSummaryFilter.getParam().getLastDateModified() != null) {
            outletSummaryFilter.getParam().setLastDateModified(null);
        }

        if (filter.getParam().isContract()) {
            sqlSelect.where(_TABLE_NAME_OUTLETS, _CONTRACT_CODE).isNot(null);
        }

        if (filter.getParam().getRoleId() != null && filter.getParam().getUserId() != null) {
            sqlSelect.join(_TASK_ITEM, TaskItemModel.class).on(_TABLE_NAME_OUTLETS, _ID).isEqual(_TASK_ITEM, _OUTLET_ID)
                    .join(_TASK, TaskModel.class).on(_TASK_ITEM, _TASK_ID).isEqual(_TASK, _ID)
                    .where(_TASK, _USER_ID).isEqual(filter.getParam().getUserId());
        }

        if (outletSummaryFilter.getParam().getBranchCode() != null) {
            if (outletSummaryFilter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_TABLE_NAME_OUTLETS, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_TABLE_NAME_OUTLETS, _BRANCH_CODE).isEqual(outletSummaryFilter.getParam().getBranchCode());
            }
        }

        if (outletSummaryFilter.getParam().getDefaultSort()) {
            sqlSelect.orderBy(_SUMMARY, _REGION_NAME).asc()
                    .orderBy(_SUMMARY, _BRANCH_NAME).asc()
                    .orderBy(_SUMMARY, _DISTRICT_NAME).asc()
                    .orderBy(_TABLE_NAME_OUTLETS, _NAME).asc();
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<OutletModel> listOutlets = new ArrayList<>();
            while (result.moveNext()) {
                final OutletModel outlet = result.getItem(_TABLE_NAME_OUTLETS, OutletModel.class);
                final ContractModel contract = result.getItem(_CONTRACT, ContractModel.class);
                final OutletSummaryModel summary = result.getItem(_SUMMARY, OutletSummaryModel.class);

                outlet.setBranchCode(summary.getBranchCode());
                outlet.setBranchName(summary.getBranchName());
                outlet.setChannelCode(summary.getChannelCode());
                outlet.setChannelName(summary.getChannelName());
                outlet.setCityCode(summary.getCityCode());
                outlet.setCityName(summary.getCityName());
                outlet.setDistrictName(summary.getDistrictName());
                outlet.setRegionCode(summary.getRegionCode());
                outlet.setRegionName(summary.getRegionName());
                outlet.setSioTypeCode(summary.getSioTypeCode());
                outlet.setSioTypeName(summary.getSioTypeName());
                outlet.setVerifiedBy(summary.getVerifiedBy());

                outlet.setContract(contract);
                outlet.setFileSize(contract.getFileSize());
                outlet.setLink(contract.getLink());

                listOutlets.add(outlet);
            }
            return listOutlets;
        }
    }

    public OutletModel getOutlet(long outletId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TABLE_NAME_OUTLETS, OutletModel.class)
                .includeAllJoin()
                .join(_SUMMARY, OutletSummaryModel.class).on(_TABLE_NAME_OUTLETS, _ID).isEqual(_SUMMARY, _OUTLET_ID)
                .leftJoin(_CONTRACT, ContractModel.class).on(_TABLE_NAME_OUTLETS, _CONTRACT_CODE).isEqual(_CONTRACT, _CODE)
                .where(_TABLE_NAME_OUTLETS, _ID).isEqual(outletId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final OutletModel outlet = result.getItem(_TABLE_NAME_OUTLETS, OutletModel.class);
                final ContractModel contract = result.getItem(_CONTRACT, ContractModel.class);
                final OutletSummaryModel summary = result.getItem(_SUMMARY, OutletSummaryModel.class);

                outlet.setBranchCode(summary.getBranchCode());
                outlet.setBranchName(summary.getBranchName());
                outlet.setChannelCode(summary.getChannelCode());
                outlet.setChannelName(summary.getChannelName());
                outlet.setCityCode(summary.getCityCode());
                outlet.setCityName(summary.getCityName());
                outlet.setDistrictName(summary.getDistrictName());
                outlet.setRegionCode(summary.getRegionCode());
                outlet.setRegionName(summary.getRegionName());
                outlet.setSioTypeCode(summary.getSioTypeCode());
                outlet.setSioTypeName(summary.getSioTypeName());
                outlet.setVerifiedBy(summary.getVerifiedBy());

                outlet.setContract(contract);
                outlet.setFileSize(contract.getFileSize());
                outlet.setLink(contract.getLink());

                return outlet;
            }
            return null;
        }
    }

    public boolean insert(OutletModel outlet, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.insert(outlet).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_OUTLETS, outlet);

            if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
                final OutletSummaryModel outletSummary = new OutletSummaryModel();
                outletSummary.newModel(outlet.getCreatedBy());
                outletSummary.setOutletId(outlet.getId());

                final ResultBuilder sqlSelectLocationResult = QueryBuilder.select(_DISTRICT, DistrictViewModel.class)
                        .includeAllJoin()
                        .join(_BRANCH, BranchViewModel.class).on(_DISTRICT, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                        .join(_CITY, CityViewModel.class).on(_DISTRICT, _CITY_CODE).isEqual(_CITY, _CODE)
                        .join(_REGION, RegionViewModel.class).on(_BRANCH, _REGION_CODE).isEqual(_REGION, _CODE)
                        .where(_DISTRICT, _CODE).isEqual(outlet.getDistrictCode()).execute(sqlTransaction);

                if (!sqlSelectLocationResult.moveNext()) {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }

                final BranchViewModel branch = sqlSelectLocationResult.getItem(_BRANCH, BranchViewModel.class);
                final CityViewModel city = sqlSelectLocationResult.getItem(_CITY, CityViewModel.class);
                final DistrictViewModel district = sqlSelectLocationResult.getItem(_DISTRICT, DistrictViewModel.class);
                final RegionViewModel region = sqlSelectLocationResult.getItem(_REGION, RegionViewModel.class);

                outletSummary.setBranchCode(branch.getCode());
                outletSummary.setBranchName(branch.getName());
                outletSummary.setDistrictCode(district.getCode());
                outletSummary.setDistrictName(district.getName());
                outletSummary.setRegionCode(region.getCode());
                outletSummary.setRegionName(region.getName());
                outletSummary.setCityCode(city.getCode());
                outletSummary.setCityName(city.getName());

                final ResultBuilder channelResult = QueryBuilder.select(ChannelViewModel.class)
                        .where(_CODE).isEqual(outlet.getChannelCode()).execute(sqlTransaction);

                if (!channelResult.moveNext()) {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }

                final ChannelViewModel channel = channelResult.getItem(ChannelViewModel.class);
                outletSummary.setChannelCode(channel.getCode());
                outletSummary.setChannelName(channel.getName());
                outletSummary.modify(_MY_APPLICATION);

                if (!QueryBuilder.insert(outletSummary).execute(sqlTransaction).isModified()) {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }

                final ResultBuilder resultSelectSurvey = QueryBuilder.select(_SURVEY, SurveyModel.class)
                        .includeAllJoin()
                        .join(_BRANCH_SURVEY, BranchSurveyModel.class).on(_SURVEY, _ID).isEqual(_BRANCH_SURVEY, _SURVEY_ID)
                        .where(_BRANCH_SURVEY, _BRANCH_CODE).isEqual(outlet.getBranchCode()).execute(sqlTransaction);

                while (resultSelectSurvey.moveNext()) {
                    final SurveyModel survey = resultSelectSurvey.getItem(_SURVEY, SurveyModel.class);
                    final BranchSurveyModel branchSurvey = resultSelectSurvey.getItem(_BRANCH_SURVEY, BranchSurveyModel.class);
                    final OutletSurveySummaryModel outletSurveySummary = new OutletSurveySummaryModel();
                    outletSurveySummary.newModel(_MY_APPLICATION);
                    outletSurveySummary.setOutletId(outlet.getId());
                    outletSurveySummary.setSurveyId(survey.getId());
                    outletSurveySummary.setStartDate(branchSurvey.getStartDate());
                    outletSurveySummary.setFinishDate(branchSurvey.getFinishDate());

                    if (!QueryBuilder.insert(outletSurveySummary).execute(sqlTransaction).isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }

                sqlTransaction.commitTransaction();
                return true;
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean update(OutletModel outlet, AuditTrailModel auditTrail, boolean changeContract) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final UpdateResult result = QueryBuilder.update(outlet).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_OUTLETS, outlet);

            if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
                final OutletSummaryModel outletSummary;
                final ResultBuilder resultOutletSummary = QueryBuilder.select(OutletSummaryModel.class)
                        .where(_OUTLET_ID).isEqual(outlet.getId()).execute(sqlTransaction);

                if (resultOutletSummary.moveNext()) {
                    outletSummary = resultOutletSummary.getItem(OutletSummaryModel.class);
                    outletSummary.modify(outlet.getModifiedBy());
                } else {
                    outletSummary = new OutletSummaryModel();
                    outletSummary.newModel(outlet.getModifiedBy());
                    outletSummary.setOutletId(outlet.getId());
                    outletSummary.setContractCode(outlet.getContractCode());
                }

                final ResultBuilder sqlSelectLocationResult = QueryBuilder.select(_DISTRICT, DistrictViewModel.class)
                        .includeAllJoin()
                        .join(_BRANCH, BranchViewModel.class).on(_DISTRICT, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                        .join(_CITY, CityViewModel.class).on(_DISTRICT, _CITY_CODE).isEqual(_CITY, _CODE)
                        .join(_REGION, RegionViewModel.class).on(_BRANCH, _REGION_CODE).isEqual(_REGION, _CODE)
                        .where(_DISTRICT, _CODE).isEqual(outlet.getDistrictCode()).execute(sqlTransaction);

                if (!sqlSelectLocationResult.moveNext()) {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }

                final BranchViewModel branch = sqlSelectLocationResult.getItem(_BRANCH, BranchViewModel.class);
                final CityViewModel city = sqlSelectLocationResult.getItem(_CITY, CityViewModel.class);
                final DistrictViewModel district = sqlSelectLocationResult.getItem(_DISTRICT, DistrictViewModel.class);
                final RegionViewModel region = sqlSelectLocationResult.getItem(_REGION, RegionViewModel.class);

                outletSummary.setBranchCode(branch.getCode());
                outletSummary.setBranchName(branch.getName());
                outletSummary.setDistrictCode(district.getCode());
                outletSummary.setDistrictName(district.getName());
                outletSummary.setRegionCode(region.getCode());
                outletSummary.setRegionName(region.getName());
                outletSummary.setCityCode(city.getCode());
                outletSummary.setCityName(city.getName());

                final ResultBuilder channelResult = QueryBuilder.select(ChannelViewModel.class)
                        .where(_CODE).isEqual(outlet.getChannelCode()).execute(sqlTransaction);

                if (!channelResult.moveNext()) {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }

                final ChannelViewModel channel = channelResult.getItem(ChannelViewModel.class);
                outletSummary.setChannelCode(channel.getCode());
                outletSummary.setChannelName(channel.getName());

                if (!StringUtility.isNullOrWhiteSpace(outlet.getContractCode())) {
                    final ResultBuilder contractResult = QueryBuilder.select(_SIO_TYPE, SioTypeModel.class)
                            .join(_CONTRACT, ContractModel.class).on(_CONTRACT, _SIO_TYPE_CODE).isEqual(_SIO_TYPE, _CODE)
                            .where(_CONTRACT, _CODE).isEqual(outlet.getContractCode()).execute(sqlTransaction);

                    if (!contractResult.moveNext()) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }

                    final SioTypeModel sioType = contractResult.getItem(_SIO_TYPE, SioTypeModel.class);
                    outletSummary.setSioTypeCode(sioType.getCode());
                    outletSummary.setSioTypeName(sioType.getName());
                    outletSummary.modify(outlet.getModifiedBy());

                    if (!QueryBuilder.update(outletSummary).execute(sqlTransaction).isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }

                if (changeContract) {

                    // Old Contract

                    if (!StringUtility.isNullOrWhiteSpace(outlet.getOldContractCode())) {

                        final ResultBuilder resultOldContract = QueryBuilder.select(ContractModel.class)
                                .where(_CODE).isEqual(outlet.getOldContractCode()).execute(sqlTransaction);

                        if (!resultOldContract.moveNext()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }

                        final ContractModel oldContract = resultOldContract.getItem(ContractModel.class);
                        oldContract.setStatus((outlet.getOldContractStatus() == null) ? _STATUS_NOT_ACTIVE : _STATUS_TERMINATE);
                        oldContract.modify(outlet.getModifiedBy());
                        final UpdateResult resultUpdateOldContract = QueryBuilder.update(oldContract).execute(sqlTransaction);

                        if (!resultUpdateOldContract.isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }

                        final ContractHistoryModel historyOldContract = QueryBuilder.select(ContractHistoryModel.class)
                                .where(_CONTRACT_CODE).isEqual(oldContract.getCode())
                                .where(_OUTLET_ID).isEqual(outlet.getId())
                                .getResult(sqlTransaction)
                                .executeItem(ContractHistoryModel.class);

                        if (historyOldContract != null) {
                            historyOldContract.modify(outlet.getModifiedBy());
                            if (!QueryBuilder.update(historyOldContract).execute(sqlTransaction).isModified()) {
                                sqlTransaction.rollbackTransaction();
                                return false;
                            }
                        }

                    }

                    // New Contract

                    if (!StringUtility.isNullOrWhiteSpace(outlet.getContractCode())) {
                        final ResultBuilder resultNewContract = QueryBuilder.select(ContractModel.class)
                                .where(_CODE).isEqual(outlet.getContractCode()).execute(sqlTransaction);

                        if (!resultNewContract.moveNext() && (!outlet.getOldContractStatus().equalsIgnoreCase(_STATUS_TERMINATE))) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }

                        final ContractModel newContract = resultNewContract.getItem(ContractModel.class);
                        newContract.setStatus(_STATUS_ACTIVE);
                        newContract.setIsUsed(_STATUS_TRUE);
                        newContract.modify(_MY_APPLICATION);
                        final UpdateResult resultUpdateNewContract = QueryBuilder.update(newContract).execute(sqlTransaction);

                        if (!resultUpdateNewContract.isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }


                        final ContractHistoryModel historyNewContract = new ContractHistoryModel();
                        historyNewContract.newModel(outlet.getModifiedBy());
                        historyNewContract.setStartDate(newContract.getStartDate());
                        historyNewContract.setFinishDate(newContract.getFinishDate());
                        historyNewContract.setContractCode(newContract.getCode());
                        historyNewContract.setOutletId(outlet.getId());
                        if (!QueryBuilder.insert(historyNewContract).execute(sqlTransaction).isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }

                    // DELETE OUTLET ITEM SUMMARY

                    final List<OutletItemSummaryModel> allOutletItemSummaryItems = QueryBuilder.select(OutletItemSummaryModel.class)
                            .where(_OUTLET_ID).isEqual(outlet.getId())
                            .getResult(sqlTransaction)
                            .executeItems(OutletItemSummaryModel.class);

                    for (final OutletItemSummaryModel outletItemSummary : allOutletItemSummaryItems) {
                        outletItemSummary.modify(outlet.getModifiedBy());

                        if (!QueryBuilder.delete(outletItemSummary).execute(sqlTransaction).isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }

                    // INSERT NEW OUTLET ITEM SUMMARY

                    final List<SioTypePromotionModel> listSioTypePromotions = QueryBuilder.select(_SIO_TYPE_PROMOTION, SioTypePromotionModel.class)
                            .join(_SIO_TYPE, SioTypeModel.class).on(_SIO_TYPE, _CODE).isEqual(_SIO_TYPE_PROMOTION, _SIO_TYPE_CODE)
                            .join(_CONTRACT, ContractModel.class).on(_CONTRACT, _SIO_TYPE_CODE).isEqual(_SIO_TYPE, _CODE)
                            .where(_CONTRACT, _CODE).isEqual(outlet.getContractCode())
                            .getResult(sqlTransaction)
                            .executeItems(_SIO_TYPE_PROMOTION, SioTypePromotionModel.class);

                    for (final SioTypePromotionModel sioTypePromotion : listSioTypePromotions) {
                        final OutletItemSummaryModel outletItemSummary = new OutletItemSummaryModel();
                        outletItemSummary.newModel(outlet.getModifiedBy());
                        outletItemSummary.setOutletId(outlet.getId());
                        outletItemSummary.setLastItemCondition(_DEFAULT_LAST_CONDITION);
                        outletItemSummary.setLastActionToItem(_DEFAULT_ACTION);
                        outletItemSummary.setItemCode(sioTypePromotion.getCategoryItemCode());
                        outletItemSummary.setCategoryCode(sioTypePromotion.getCategoryItemCode());

                        final UpdateResult resultOutletItemSummary = QueryBuilder.insert(outletItemSummary).execute(sqlTransaction);

                        if (!resultOutletItemSummary.isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }
                }
                sqlTransaction.commitTransaction();
                return true;
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    @Override
    public void increment(OutletModel outlet, String lastMaxValue) throws Exception {
        final String seqNo = lastMaxValue == null ? null : lastMaxValue.substring(lastMaxValue.length() - 5);
        final int numSeqNo = seqNo == null ? 1 : Integer.parseInt(seqNo) + 1;
        outlet.setCode(String.format("%s%s%05d", outlet.getBranchCode(), outlet.getChannelCode(), numSeqNo));
    }

    @Override
    public SelectBuilder createLastIncrementValueQuery(OutletModel outlet) {
        final String keyword = outlet.getBranchCode().concat(outlet.getChannelCode());
        return QueryBuilder.select(OutletModel.class, _CODE)
                .where(_CODE).startWith(keyword)
                .limit(1)
                .orderBy(_CODE).desc();

    }

    @Override
    public boolean ignoreIncrementValue(OutletModel o) throws QueryBuilderException {
        return !StringUtility.isNullOrWhiteSpace(o.getCode());
    }
}