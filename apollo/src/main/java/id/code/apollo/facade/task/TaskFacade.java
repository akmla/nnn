package id.code.apollo.facade.task;

import id.code.apollo.facade.schedule.ScheduleFacade;
import id.code.apollo.filter.TaskFilter;
import id.code.apollo.model.schedule.ScheduleModel;
import id.code.apollo.model.summary.TaskSummaryModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.component.utility.DateUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.model.user.UserModel;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

@SuppressWarnings("Duplicates")
public class TaskFacade extends BaseFacade {
    private final ScheduleFacade scheduleFacade = new ScheduleFacade();
    private final TaskSummaryFacade taskSummaryFacade = new TaskSummaryFacade();

    public List<TaskModel> getTasks(Filter<TaskFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_TASK, TaskModel.class)
                .includeAllJoin()
                .join(_MD, UserModel.class).on(_TASK, _USER_ID).isEqual(_MD, _ID)
                .join(_BRANCH, BranchViewModel.class).on(_TASK, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .join(_SCHEDULE, ScheduleModel.class).on(_TASK, _SCHEDULE_ID).isEqual(_SCHEDULE, _ID)
                .orderBy(_TASK, filter)
                .filter(_TASK, filter)
                .limitOffset(filter);

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            final LocalDate today = DateUtility.toLocalDate(System.currentTimeMillis());
            final Long dayStart = DateUtility.toUnixMillis(today.minusDays(7));//--.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)));
            final Long dayFinish = DateUtility.toUnixMillis(today.plusDays(7)); //.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)));

            sqlSelect.where(_TASK, _START_DATE).equalsGreaterThan(dayStart)
                    .where(_TASK, _FINISH_DATE).isEqualOr(null, _TASK, _FINISH_DATE)
                    .equalsLessThan(dayFinish);
        }

        if (filter.getParam().getMdName() != null) {
            sqlSelect.where(_MD, _NAME).startWith(filter.getParam().getMdName());
        }

        if (filter.getParam().getSupervisorId() != null) {
            sqlSelect.where(_MD, _SUPERVISOR_ID).isEqual(filter.getParam().getSupervisorId());
        }

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_TASK, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_TASK, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<TaskModel> listTasks = new ArrayList<>();
            while (result.moveNext()) {
                final TaskModel task = result.getItem(_TASK, TaskModel.class);
                final UserModel md = result.getItem(_MD, UserModel.class);
                final BranchViewModel branch = result.getItem(_BRANCH, BranchViewModel.class);
                final ScheduleModel schedule = result.getItem(_SCHEDULE, ScheduleModel.class);
                task.setSchedule(schedule);
                task.setBranch(branch);
                task.setMerchandiser(md);
                listTasks.add(task);
            }
            return listTasks;
        }
    }

    public TaskModel getTask(long taskId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(TaskModel.class)
                .where(_ID).isEqual(taskId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(TaskModel.class) : null;
        }
    }

    public TaskModel getTaskByUserId(long userId, Long taskDate) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        return QueryBuilder.select(TaskModel.class)
                .where(_USER_ID).isEqual(userId)
                .where(_START_DATE).equalsLessThan(taskDate)
                .orderBy(_START_DATE).desc()
                .limit(1)
                .getResult(super.openConnection())
                .executeItem(TaskModel.class);
    }

    public TaskModel getLastTaskByUserId(long userId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(TaskModel.class)
                .where(_USER_ID).isEqual(userId)
                .where(_FINISH_DATE).isNot(null)
                .orderBy(_FINISH_DATE).desc()
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(TaskModel.class) : null;
        }
    }

    public boolean insert(TaskModel newData, AuditTrailModel auditTrail) throws SQLException, IllegalAccessException, IOException, InstantiationException, QueryBuilderException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {

            final TaskModel taskBefore = QueryBuilder.select(TaskModel.class)
                    .where(_USER_ID).isEqual(newData.getUserId())
                    .where(_START_DATE).equalsLessThan(newData.getStartDate())
                    .orderBy(_START_DATE).desc()
                    .limit(1)
                    .getResult(sqlTransaction)
                    .executeItem(TaskModel.class);

            if (taskBefore != null) {
                taskBefore.modify(newData.getModifiedBy());
                taskBefore.setFinishDate(newData.getStartDate() - _UNIX_MILLIS_ONE_DAY_VALUE);

                auditTrail.prepareAudit(_TABLE_NAME_TASKS, taskBefore);

                if (!QueryBuilder.update(taskBefore).execute(sqlTransaction).isModified() || !super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }

            final UpdateResult result = QueryBuilder.insert(newData).execute(sqlTransaction);

            auditTrail.prepareAudit(_TABLE_NAME_TASKS, newData);

            if ((result.isModified()) && (super.insertAudit(sqlTransaction, auditTrail))) {

                TaskSummaryModel taskSummary = QueryBuilder.select(TaskSummaryModel.class)
                        .where(_USER_ID).isEqual(newData.getUserId())
                        .getResult(sqlTransaction)
                        .executeItem(TaskSummaryModel.class);

                if (taskSummary == null) {
                    taskSummary = new TaskSummaryModel();
                    taskSummary.newModel(_MY_APPLICATION);
                    taskSummary.setBranchCode(newData.getBranchCode());
                    taskSummary.setCycleNum(newData.getCycleNum());
                    taskSummary.setScheduleId(newData.getScheduleId());
                    taskSummary.setUserId(newData.getUserId());
                    taskSummary.setNextCycleSeq(_FIRST_CYCLE);
                    taskSummary.setTotalWeek(_DEFAULT_CYCLE_NUM);

                    if ((!QueryBuilder.insert(taskSummary).execute(sqlTransaction).isModified())) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                } else {
                    taskSummary.modify(newData.getModifiedBy());
                    taskSummary.setBranchCode(newData.getBranchCode());
                    taskSummary.setCycleNum(newData.getCycleNum());
                    taskSummary.setUserId(newData.getUserId());
                    taskSummary.setScheduleId(newData.getScheduleId());
                    taskSummary.setTotalWeek(_DEFAULT_CYCLE_NUM);
                    taskSummary.setNextCycleSeq(_FIRST_CYCLE);

                    if ((!QueryBuilder.update(taskSummary).execute(sqlTransaction).isModified())) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }

                // Get Future Task

                final List<TaskModel> futureTasks = QueryBuilder.select(TaskModel.class)
                        .where(_USER_ID).isEqual(newData.getUserId())
                        .where(_START_DATE).greaterThan(newData.getStartDate())
                        .getResult(sqlTransaction)
                        .executeItems(TaskModel.class);

                // Delete Future Task

                for (final TaskModel taskModel : futureTasks) {
                    taskModel.modify(_MY_APPLICATION);
                    if (!QueryBuilder.delete(taskModel).execute(sqlTransaction).isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }

                // Get Last Task

                final TaskModel lastTask = QueryBuilder.select(TaskModel.class)
                        .where(_USER_ID).isEqual(newData.getUserId())
                        .where(_START_DATE).lessThan(newData.getStartDate())
                        .orderBy(_START_DATE).desc().limit(1)
                        .getResult(sqlTransaction)
                        .executeItem(TaskModel.class);

                if (lastTask != null) {
                    lastTask.modify(_MY_APPLICATION);
                    lastTask.setFinishDate(newData.getStartDate() - _UNIX_MILLIS_ONE_DAY_VALUE);

                    // Update Last Task

                    if (QueryBuilder.update(lastTask).execute(sqlTransaction).isModified()) {
                        final List<TaskItemModel> futureTaskItem = QueryBuilder.select(TaskItemModel.class)
                                .where(_TASK_ID).isEqual(lastTask.getId())
                                .where(_TASK_DATE).greaterThan(lastTask.getFinishDate())
                                .getResult(sqlTransaction)
                                .executeItems(TaskItemModel.class);

                        for (final TaskItemModel aFutureTaskItem : futureTaskItem) {
                            if (!QueryBuilder.delete(aFutureTaskItem).execute(sqlTransaction).isModified()) {
                                sqlTransaction.rollbackTransaction();
                                return false;
                            }
                        }
                    } else {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }

                final ScheduleModel schedule = QueryBuilder.select(ScheduleModel.class)
                        .where(_ID).isEqual(newData.getScheduleId())
                        .getResult(sqlTransaction)
                        .executeItem(ScheduleModel.class);

                if (schedule != null) {
                    schedule.setIsUsed(_STATUS_TRUE);
                    schedule.modify(_MY_APPLICATION);
                    if (QueryBuilder.update(schedule).execute(sqlTransaction).isModified()) {
                        sqlTransaction.commitTransaction();
                        return true;
                    }
                }

            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

//    public boolean insert(TaskModel currentTask, TaskModel task, AuditTrailModel auditTrail) throws Exception {
//        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
//            final UpdateResult result = QueryBuilder.insert(task).execute(sqlTransaction);
//
//            auditTrail.prepareAudit(_TABLE_NAME_TASKS, task);
//
//            if ((result.isModified()) && (super.insertAudit(sqlTransaction, auditTrail))) {
//                TaskSummaryModel taskSummary = this.taskSummaryFacade.getTaskSummary(task.getUserId(), sqlTransaction);
//
//                if (taskSummary == null) {
//                    taskSummary = new TaskSummaryModel();
//                    taskSummary.newModel(_MY_APPLICATION);
//                    taskSummary.setBranchCode(task.getBranchCode());
//                    taskSummary.setCycleNum(task.getCycleNum());
//                    taskSummary.setScheduleId(task.getScheduleId());
//                    taskSummary.setUserId(task.getUserId());
//                    taskSummary.setNextCycleSeq(_FIRST_CYCLE);
//                    taskSummary.setTotalWeek(_DEFAULT_CYCLE_NUM);
//
//                    if ((!QueryBuilder.insert(taskSummary).execute(sqlTransaction).isModified())) {
//                        sqlTransaction.rollbackTransaction();
//                        return false;
//                    }
//                } else {
//                    taskSummary.modify(task.getModifiedBy());
//                    taskSummary.setBranchCode(task.getBranchCode());
//                    taskSummary.setCycleNum(task.getCycleNum());
//                    taskSummary.setUserId(task.getUserId());
//                    taskSummary.setScheduleId(task.getScheduleId());
//                    taskSummary.setTotalWeek(_DEFAULT_CYCLE_NUM);
//                    taskSummary.setNextCycleSeq(_FIRST_CYCLE);
//
//                    if ((!QueryBuilder.update(taskSummary).execute(sqlTransaction).isModified())) {
//                        sqlTransaction.rollbackTransaction();
//                        return false;
//                    }
//                }
//
//                final List<TaskModel> taskModels = QueryBuilder.select(TaskModel.class)
//                        .where(_USER_ID).isEqual(task.getUserId())
//                        .where(_START_DATE).greaterThan(task.getStartDate())
//                        .getResult(sqlTransaction)
//                        .executeItems(TaskModel.class);
//
//                for (final TaskModel taskModel : taskModels) {
//                    taskModel.modify(_MY_APPLICATION);
//                    if (!QueryBuilder.delete(taskModel).execute(sqlTransaction).isModified()) {
//                        sqlTransaction.rollbackTransaction();
//                        return false;
//                    }
//                }
//
//                final ScheduleModel schedule = scheduleFacade.getSchedule(task.getScheduleId(), sqlTransaction);
//                if (schedule != null) {
//                    schedule.setIsUsed(_STATUS_TRUE);
//                    schedule.modify(_MY_APPLICATION);
//                    if (QueryBuilder.update(schedule).execute(sqlTransaction).isModified()) {
//                        sqlTransaction.commitTransaction();
//                        return true;
//                    }
//                }
//
//            }
//            sqlTransaction.rollbackTransaction();
//            return false;
//        }
//    }

    public boolean update(TaskModel task, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult result = QueryBuilder.update(task).execute(sqlTransaction);

            auditTrail.prepareAudit(_TABLE_NAME_TASKS, task);

            if ((result.isModified()) && (super.insertAudit(sqlTransaction, auditTrail))) {
                TaskSummaryModel taskSummary = this.taskSummaryFacade.getTaskSummary(task.getUserId(), sqlTransaction);

                if (taskSummary == null) {
                    taskSummary = new TaskSummaryModel();
                    taskSummary.newModel(_MY_APPLICATION);
                    taskSummary.setBranchCode(task.getBranchCode());
                    taskSummary.setCycleNum(task.getCycleNum());
                    taskSummary.setScheduleId(task.getScheduleId());
                    taskSummary.setUserId(task.getUserId());
                    taskSummary.setTotalWeek(_DEFAULT_CYCLE_NUM);
                    taskSummary.setNextCycleSeq(_FIRST_CYCLE);

                    if ((!QueryBuilder.insert(taskSummary).execute(sqlTransaction).isModified())) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                } else {
                    taskSummary.modify(task.getModifiedBy());
                    taskSummary.setBranchCode(task.getBranchCode());
                    taskSummary.setCycleNum(task.getCycleNum());
                    taskSummary.setUserId(task.getUserId());
                    taskSummary.setScheduleId(task.getScheduleId());
                    taskSummary.setTotalWeek(_DEFAULT_CYCLE_NUM);
                    taskSummary.setNextCycleSeq(_FIRST_CYCLE);

                    if ((!QueryBuilder.update(taskSummary).execute(sqlTransaction).isModified())) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }

                // Get All future tasks

                final List<TaskModel> taskModels = QueryBuilder.select(TaskModel.class)
                        .where(_USER_ID).isEqual(task.getUserId())
                        .where(_START_DATE).greaterThan(task.getStartDate())
                        .getResult(sqlTransaction)
                        .executeItems(TaskModel.class);

                // Delete All future tasks

                for (final TaskModel taskModel : taskModels) {
                    taskModel.modify(_MY_APPLICATION);
                    if (!QueryBuilder.delete(taskModel).execute(sqlTransaction).isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }

                // Get Last Task

                final TaskModel lastTask = QueryBuilder.select(TaskModel.class)
                        .where(_USER_ID).isEqual(task.getUserId())
                        .where(_START_DATE).lessThan(task.getStartDate())
                        .orderBy(_START_DATE).desc().limit(1)
                        .getResult(sqlTransaction)
                        .executeItem(TaskModel.class);

                lastTask.modify(_MY_APPLICATION);
                lastTask.setFinishDate(task.getStartDate() - _UNIX_MILLIS_ONE_DAY_VALUE);

                // Update Last Task

                if (QueryBuilder.update(lastTask).execute(sqlTransaction).isModified()) {
                    final List<TaskItemModel> futureTaskItem = QueryBuilder.select(TaskItemModel.class)
                            .where(_TASK_ID).isEqual(lastTask.getId())
                            .where(_TASK_DATE).greaterThan(lastTask.getFinishDate())
                            .getResult(sqlTransaction)
                            .executeItems(TaskItemModel.class);

                    for (final TaskItemModel aFutureTaskItem : futureTaskItem) {
                        if (!QueryBuilder.delete(aFutureTaskItem).execute(sqlTransaction).isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }
                } else {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }

                final ScheduleModel schedule = scheduleFacade.getSchedule(task.getScheduleId(), sqlTransaction);
                if (schedule != null) {
                    schedule.setIsUsed(_STATUS_TRUE);
                    schedule.modify(_MY_APPLICATION);
                    if (scheduleFacade.update(schedule, sqlTransaction)) {
                        sqlTransaction.commitTransaction();
                        return true;
                    }
                }
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean delete(TaskModel task, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult result = QueryBuilder.delete(task).execute(sqlTransaction);

            auditTrail.prepareAudit(_TABLE_NAME_TASKS, task);

            if ((result.isModified()) && (super.insertAudit(sqlTransaction, auditTrail))) {

                final List<TaskModel> futureTask = QueryBuilder.select(TaskModel.class)
                        .where(_USER_ID).isEqual(task.getUserId())
                        .where(_START_DATE).equalsGreaterThan(task.getStartDate())
                        .orderBy(_ID).asc()
                        .getResult(sqlTransaction)
                        .executeItems(TaskModel.class);

                for (final TaskModel taskModel : futureTask) {
                    taskModel.modify(_MY_APPLICATION);
                    if ((!QueryBuilder.delete(taskModel).execute(sqlTransaction).isModified())) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }

                final TaskModel lastTask = QueryBuilder.select(TaskModel.class)
                        .where(_USER_ID).isEqual(task.getUserId())
                        .where(_START_DATE).lessThan(task.getStartDate())
                        .orderBy(_START_DATE).desc().limit(1)
                        .getResult(sqlTransaction)
                        .executeItem(TaskModel.class);

                if (lastTask != null) {
                    lastTask.modify(_MY_APPLICATION);
                    lastTask.setFinishDate(null);

                    if (QueryBuilder.update(lastTask).execute(sqlTransaction).isModified()) {
                        sqlTransaction.commitTransaction();
                        return true;
                    }
                } else {
                    sqlTransaction.commitTransaction();
                    return true;
                }

            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}