package id.code.apollo.facade.contract;

import id.code.apollo.filter.ContractItemFilter;
import id.code.apollo.model.contract.ContractHistoryModel;
import id.code.apollo.model.contract.ContractItemModel;
import id.code.apollo.model.contract.ContractModel;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.validation.ContractItemValidation;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class ContractItemFacade extends BaseFacade {

    public List<ContractItemModel> getContractItems(Filter<ContractItemFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_CONTRACT_ITEM, ContractItemModel.class)
                .includeAllJoin()
                .join(_OUTLET, OutletModel.class).on(_OUTLET, _CONTRACT_CODE).isEqual(_CONTRACT_ITEM, _CONTRACT_CODE)
                .leftJoin(_TASK_ITEM, TaskItemModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqualChain(_OUTLET, _ID)
                .and(_TASK_ITEM
                        , _ID).isEqual(_CONTRACT_ITEM, _TASK_ITEM_ID)
                .orderBy(_CONTRACT_ITEM, filter)
                .filter(_CONTRACT_ITEM, filter)
                .limitOffset(filter);

        if (filter.getParam().getTaskDate() != null) {
            sqlSelect.where(_TASK_ITEM, _TASK_DATE).isEqual(filter.getParam().getTaskDate());
        }

        if (filter.getParam().getBranchCode() != null) {
            sqlSelect.where(_OUTLET, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<ContractItemModel> listContractItems = new ArrayList<>();
            while (result.moveNext()) {
                final ContractItemModel contractItem = result.getItem(_CONTRACT_ITEM, ContractItemModel.class);
                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
                final OutletModel outlet = result.getItem(OutletModel.class);

                contractItem.setPaymentDate(taskItem.getTaskDate());
                contractItem.setOutletId(outlet.getId());
                listContractItems.add(contractItem);
            }
            return listContractItems;
        }

    }

    public ContractItemModel getContractItem(long contractDetailId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(ContractItemModel.class)
                .where(_ID).isEqual(contractDetailId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return result.moveNext() ? result.getItem(ContractItemModel.class) : null;
        }
    }

    public boolean insert(ContractItemValidation newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final int itemSize = newData.getItems().size();

            for (int i = 0; i < itemSize; i++) {
                final ContractItemModel contractItem = newData.getItems().get(i);
                contractItem.newModel(newData.getCreatedBy());
                contractItem.setContractCode(newData.getContractCode());
                contractItem.setStatus(_UNPAID);
                final UpdateResult resultItem = QueryBuilder.insert(contractItem).execute(sqlTransaction);
                auditTrail.prepareAudit(_TABLE_NAME_CONTRACT_ITEMS, contractItem);
                if (!((resultItem.isModified()) && (this.insertAudit(sqlTransaction, auditTrail)))) {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }

            final ResultBuilder result = QueryBuilder.select(ContractModel.class)
                    .where(_CODE).isEqual(newData.getContractCode()).execute(sqlTransaction);
            if (result.moveNext()) {
                final ContractModel contract = result.getItem(ContractModel.class);
                contract.setTop(newData.getTop());
                contract.setStartDate(newData.getStartDate());
                contract.setFinishDate(newData.getFinishDate());
                contract.setCode(newData.getContractCode());
                contract.setContractYear(newData.getContractYear());
                contract.modify(newData.getCreatedBy());

                if (!QueryBuilder.update(contract).execute(sqlTransaction).isModified()) {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }

                // Contract History
                final ContractHistoryModel contractHistory = QueryBuilder.select(ContractHistoryModel.class)
                        .where(_CONTRACT_CODE).isEqual(contract.getCode())
                        .getResult(sqlTransaction)
                        .executeItem(ContractHistoryModel.class);

                if (contractHistory != null) {
                    contractHistory.setStartDate(contract.getStartDate());
                    contractHistory.setFinishDate(contract.getFinishDate());
                    contractHistory.modify(_MY_APPLICATION);
                    if (!QueryBuilder.update(contractHistory).execute(sqlTransaction).isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }
                }

                sqlTransaction.commitTransaction();
                return true;

            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean update(ContractItemModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult result = QueryBuilder.update(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_CONTRACT_ITEMS, newData);

            if (result.isModified() && (super.insertAudit(sqlTransaction, auditTrail))) {
                final ContractModel contract = QueryBuilder.select(ContractModel.class).where(_CODE).isEqual(newData.getContractCode())
                        .getResult(sqlTransaction).executeItem(ContractModel.class);
                contract.setPaymentAmount(contract.getPaymentAmount() + newData.getInsentif());
                contract.modify(_MY_APPLICATION);

                if (QueryBuilder.update(contract).execute(sqlTransaction).isModified()) {
                    sqlTransaction.commitTransaction();
                    return true;
                }
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}