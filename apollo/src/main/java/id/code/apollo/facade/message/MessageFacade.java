package id.code.apollo.facade.message;

import id.code.apollo.filter.MessageFilter;
import id.code.apollo.model.message.MessageModel;
import id.code.apollo.validation.SendMessageValidation;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.model.user.UserModel;
import id.code.master_data.model.user.UserViewModel;
import id.code.master_data.security.Role;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class MessageFacade extends BaseFacade {

    public List<MessageModel> getMessages(Filter<MessageFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_MESSAGE, MessageModel.class)
                .includeAllJoin()
                .join(_USER, UserModel.class).on(_MESSAGE, _SENDER_ID).isEqual(_USER, _ID)
                .join(_USER2, UserModel.class).on(_MESSAGE, _USER_ID).isEqual(_USER2, _ID)
                .orderBy(_MESSAGE, filter)
                .filter(_MESSAGE, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_USER2, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE)
                                .where(_USER_ID).isEqual(filter.getParam().getSenderId())
                );
            } else /*if (!filter.getParam().getBranchCode().equalsIgnoreCase(_ALL))*/ {
                sqlSelect.where(_USER2, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<MessageModel> listMessages = new ArrayList<>();
            while (result.moveNext()) {
                final MessageModel message = result.getItem(_MESSAGE, MessageModel.class);
                final UserModel sender = result.getItem(_USER, UserModel.class);
                final UserModel receiver = result.getItem(_USER2, UserModel.class);
                message.setSenderName(sender.getName());
                message.setUserName(receiver.getName());
                listMessages.add(message);
            }
            return listMessages;
        }
    }

    public MessageModel getMessage(long messageId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_MESSAGE, MessageModel.class)
                .includeAllJoin()
                .join(_USER, UserModel.class).on(_MESSAGE, _SENDER_ID).isEqual(_USER, _ID)
                .join(_USER2, UserViewModel.class).on(_MESSAGE, _USER_ID).isEqual(_USER2, _ID)
                .where(_MESSAGE, _ID).isEqual(messageId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final MessageModel message = result.getItem(_MESSAGE, MessageModel.class);
                final UserModel sender = result.getItem(_USER, UserModel.class);
                final UserViewModel receiver = result.getItem(_USER2, UserViewModel.class);
                message.setSenderName(sender.getName());
                message.setUserName(receiver.getName());
                return message;
            }
            return null;
        }
    }

    public List<MessageModel> insertMessage(SendMessageValidation validation) throws SQLException, QueryBuilderException {
        try (final SimpleTransaction transaction = super.openTransaction()) {
            final SelectBuilder sqlSelectUser = QueryBuilder.select(UserModel.class, _ID, _FCM_TOKEN)
                    .where(_ROLE_ID).isEqualOr(Role._ROLE_ID_TL_PROMOSI, _ROLE_ID).isEqual(Role._ROLE_ID_MERCHANDISER);

            if (validation.getBranchCode().equalsIgnoreCase(_ALL_BRANCH)) {
                sqlSelectUser.where(_BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE)
                                .where(_USER_ID).isEqual(validation.getSenderId())
                );
            } else if (!validation.getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelectUser.where(_BRANCH_CODE).isEqual(validation.getBranchCode());
            }

            try (final ResultBuilder resultSelectUser = sqlSelectUser.execute(transaction)) {
                final List<UserModel> listUsers = resultSelectUser.getItems(UserModel.class);
                final List<MessageModel> messages = new ArrayList<>();

                for (UserModel user : listUsers) {
                    final MessageModel message = new MessageModel();
                    message.newModel(validation.getCreatedBy());
                    message.setUserId(user.getId());
                    message.setFcmToken(user.getFcmToken());
                    message.setSendDate(validation.getSendDate());
                    message.setSenderId(validation.getSenderId());
                    message.setMessage(validation.getMessage());

                    final UpdateResult resultInsert = QueryBuilder.insert(message).execute(transaction);

                    if (!resultInsert.isModified()) {
                        transaction.rollbackTransaction();
                        return null;
                    }

                    messages.add(message);
                }

                transaction.commitTransaction();
                return messages;
            }
        }
    }
}