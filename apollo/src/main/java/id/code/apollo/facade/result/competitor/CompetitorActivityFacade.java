package id.code.apollo.facade.result.competitor;

import id.code.apollo.filter.CompetitorActivityFilter;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.result.competitor.CompetitorActivityModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.component.utility.DateUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.product.ProductViewModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.security.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class CompetitorActivityFacade extends BaseFacade {

    public List<CompetitorActivityModel> getCompetitorActivities(Filter<CompetitorActivityFilter> filter, Long roleId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_COMPETITOR, CompetitorActivityModel.class)
                .includeAllJoin()
                .join(_TASK_ITEM, TaskItemModel.class).on(_TASK_ITEM, _ID).isEqual(_COMPETITOR, _TASK_ITEM_ID)
                .join(_TASK, TaskModel.class).on(_TASK, _ID).isEqual(_TASK_ITEM, _TASK_ID)
                .join(_OUTLET, OutletModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .join(_PRODUCT, ProductViewModel.class).on(_COMPETITOR, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .orderBy(_COMPETITOR, filter)
                .filter(_COMPETITOR, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        if (filter.getParam().getOutletId() != null) {
            sqlSelect.where(_TASK_ITEM, _OUTLET_ID).isEqual(filter.getParam().getOutletId());
        }

        if (filter.getParam().getMdId() != null) {
            sqlSelect.where(_TASK, _USER_ID).isEqual(filter.getParam().getMdId());
        }

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            if (roleId == Role._ROLE_ID_MERCHANDISER) {
                final LocalDate today = DateUtility.toLocalDate(System.currentTimeMillis());
                final Long dayStart = DateUtility.toUnixMillis(today.minusDays(1));
                final Long dayFinish = DateUtility.toUnixMillis(today.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)));

                sqlSelect.where(_TASK_ITEM, _TASK_DATE)
                        .equalsLessThanAnd(dayFinish, _TASK_ITEM, _TASK_DATE)
                        .equalsGreaterThan(dayStart);
            } else {
                sqlSelect.where(_COMPETITOR, _TASK_ITEM_ID).in(
                        QueryBuilder.select(OutletSummaryModel.class, _LAST_TASK_ITEM_ID)
                                .where(_LAST_TASK_ITEM_ID).isNot(null)
                                .where(_BRANCH_CODE).isEqual(filter.getParam().getBranchCode())
                );
            }
        }

        if (filter.getParam().getProductName() != null) {
            sqlSelect.where(_PRODUCT, _NAME).startWith(filter.getParam().getProductName());
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<CompetitorActivityModel> listCompetitorActivities = new ArrayList<>();
            while (result.moveNext()) {
                final CompetitorActivityModel competitorActivity = result.getItem(_COMPETITOR, CompetitorActivityModel.class);
                final ProductViewModel product = result.getItem(_PRODUCT, ProductViewModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
                final TaskModel task = result.getItem(_TASK, TaskModel.class);
                competitorActivity.setOutletId(taskItem.getOutletId());
                competitorActivity.setBranchCode(outlet.getBranchCode());
                competitorActivity.setMdId(task.getUserId());
                competitorActivity.setProduct(product);
                listCompetitorActivities.add(competitorActivity);
            }
            return listCompetitorActivities;
        }

    }

    public CompetitorActivityModel getCompetitorActivity(long competitorActivityId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_COMPETITOR, CompetitorActivityModel.class)
                .includeAllJoin()
                .join(_PRODUCT, ProductViewModel.class).on(_COMPETITOR, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .where(_COMPETITOR, _ID).isEqual(competitorActivityId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final CompetitorActivityModel competitorActivity = result.getItem(_COMPETITOR, CompetitorActivityModel.class);
                final ProductViewModel product = result.getItem(_PRODUCT, ProductViewModel.class);
                competitorActivity.setProduct(product);
                return competitorActivity;
            }
            return null;
        }
    }

    public CompetitorActivityModel getCompetitorActivityByUnique(long taskItemId, String productCode, String type) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_COMPETITOR, CompetitorActivityModel.class)
                .includeAllJoin()
                .join(_PRODUCT, ProductViewModel.class).on(_COMPETITOR, _PRODUCT_CODE).isEqual(_PRODUCT, _CODE)
                .where(_COMPETITOR, _TASK_ITEM_ID).isEqual(taskItemId)
                .where(_COMPETITOR, _PRODUCT_CODE).isEqual(productCode)
                .where(_COMPETITOR, _TYPE).isEqual(type)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final CompetitorActivityModel competitorActivity = result.getItem(_COMPETITOR, CompetitorActivityModel.class);
                final ProductViewModel product = result.getItem(_PRODUCT, ProductViewModel.class);
                competitorActivity.setProduct(product);
                return competitorActivity;
            }
            return null;
        }
    }

    private List<Long> getLastTaskItemId(String branchCode) throws Exception {
        try (final ResultBuilder result = QueryBuilder.select(OutletSummaryModel.class, _LAST_TASK_ITEM_ID)
                .where(_LAST_TASK_ITEM_ID).isNot(null)
                .where(_BRANCH_CODE).isEqual(branchCode)
                .execute(super.openConnection())) {

            final List<Long> listTaskItemId = new ArrayList<>();
            while (result.moveNext()) {
                listTaskItemId.add(result.getItem(OutletSummaryModel.class).getLastTaskItemId());
            }
            return listTaskItemId;
        }
    }

    public boolean insert(CompetitorActivityModel newData, AuditTrailModel auditTrail) throws QueryBuilderException, SQLException, IllegalAccessException, IOException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final TaskItemModel taskItem = QueryBuilder.select(TaskItemModel.class)
                    .where(_ID).isEqual(newData.getTaskItemId())
                    .getResult(sqlTransaction)
                    .executeItem(TaskItemModel.class);

            if (taskItem != null) {
                auditTrail.prepareAudit(_TABLE_NAME_COMPETITOR_ACTIVITIES, newData);
                if (QueryBuilder.insert(newData).execute(sqlTransaction).isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                } else if (QueryBuilder.update(newData).execute(sqlTransaction).isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                } else {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean update(CompetitorActivityModel newData, AuditTrailModel auditTrail) throws QueryBuilderException, SQLException, IllegalAccessException, IOException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final TaskItemModel taskItem = QueryBuilder.select(TaskItemModel.class)
                    .where(_ID).isEqual(newData.getTaskItemId())
                    .getResult(sqlTransaction)
                    .executeItem(TaskItemModel.class);

            if (taskItem != null) {
                auditTrail.prepareAudit(_TABLE_NAME_COMPETITOR_ACTIVITIES, newData);
                if (QueryBuilder.update(newData).execute(sqlTransaction).isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                } else {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}