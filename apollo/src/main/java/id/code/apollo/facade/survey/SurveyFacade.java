package id.code.apollo.facade.survey;

import id.code.apollo.filter.SurveyFilter;
import id.code.apollo.model.survey.BranchSurveyModel;
import id.code.apollo.model.survey.SurveyModel;
import id.code.component.utility.DateUtility;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class SurveyFacade extends BaseFacade {


    public List<SurveyModel> getSurveys(Filter<SurveyFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SURVEY, SurveyModel.class)
                .orderBy(_SURVEY, filter)
                .filter(_SURVEY, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            sqlSelect.leftJoin(_BRANCH_SURVEY, BranchSurveyModel.class).on(_SURVEY, _ID).isEqual(_BRANCH_SURVEY, _SURVEY_ID);
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_BRANCH_SURVEY, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_BRANCH_SURVEY, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }

            if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
                sqlSelect.where(_BRANCH_SURVEY, _FINISH_DATE).equalsGreaterThan(
                        DateUtility.toUnixMillis(DateUtility.toLocalDate(System.currentTimeMillis())));
                sqlSelect.where(_BRANCH_SURVEY, _START_DATE).equalsLessThan(
                        DateUtility.toUnixMillis(DateUtility.toLocalDate(System.currentTimeMillis())));
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<SurveyModel> listSurveys = new ArrayList<>();
            while (result.moveNext()) {
                final SurveyModel survey = result.getItem(_SURVEY, SurveyModel.class);
                listSurveys.add(survey);
            }
            return listSurveys;
        }
    }

    public SurveyModel getSurvey(long surveysId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(SurveyModel.class)
                .where(_ID).isEqual(surveysId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(SurveyModel.class) : null;
        }
    }

//    public boolean insert(SurveyModel data, AuditTrailModel auditTrail) throws SQLException, QueryBuilderException, IOException, IllegalAccessException {
//        try (final SimpleTransaction sqlTransaction = openTransaction()) {
//            final SurveyModel newSurvey = new SurveyModel();
//            newSurvey.newModel(data.getCreatedBy());
//            newSurvey.setName(data.getName());
//
//            final UpdateResult result = QueryBuilder.insert(newSurvey).execute(sqlTransaction);
//            auditTrail.prepareAudit(_TABLE_NAME_OUTLETS, data);
//
//            if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
//                int size;
//                if (data.getBranches().get(0).equalsIgnoreCase(_ALL)) {
//                    final ResultBuilder resultBranch = QueryBuilder.select(_BRANCH, BranchModel.class).execute(sqlTransaction);
//                    final List<String> items = new ArrayList<>();Nfz
//                    while (resultBranch.moveNext()) {
//                        items.add(resultBranch.getItem(_BRANCH, BranchModel.class).getCode());
//                    }
//                    data.setBranches(items);
//                }
//
//                size = data.getBranches().size();
//                for (int i = 0; i < size; i++) {
//                    final BranchSurveyModel newBranchSurvey = new BranchSurveyModel();
//                    newBranchSurvey.newModel(data.getCreatedBy());
//                    newBranchSurvey.setStartDate(data.getStartDate());
//                    newBranchSurvey.setFinishDate(data.getFinishDate());
//                    newBranchSurvey.setSurveyId(newSurvey.getId());
//                    newBranchSurvey.setBranchCode(data.getBranches().get(i));
//                    try (final UpdateResult newResult = QueryBuilder.insert(newBranchSurvey).execute(sqlTransaction)) {
//                        if (!newResult.isModified()) {
//                            sqlTransaction.rollbackTransaction();
//                            return false;
//                        }
//                    }
//                }
//
//                sqlTransaction.commitTransaction();
//                return true;
//            }
//
//            sqlTransaction.rollbackTransaction();
//            return false;
//        }
//    }
}