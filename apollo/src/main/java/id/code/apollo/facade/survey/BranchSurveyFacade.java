package id.code.apollo.facade.survey;

import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.summary.OutletSurveySummaryModel;
import id.code.apollo.model.survey.BranchSurveyModel;
import id.code.apollo.model.survey.SurveyModel;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.filter.BranchSurveyFilter;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.branch.BranchModel;
import id.code.master_data.model.branch.BranchViewModel;
import id.code.master_data.model.user.BranchUserModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class BranchSurveyFacade extends BaseFacade {

    public List<BranchSurveyModel> getBranchSurveys(Filter<BranchSurveyFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_BRANCH_SURVEY, BranchSurveyModel.class)
                .includeAllJoin()
                .join(_BRANCH, BranchViewModel.class).on(_BRANCH_SURVEY, _BRANCH_CODE).isEqual(_BRANCH, _CODE)
                .join(_SURVEY, SurveyModel.class).on(_BRANCH_SURVEY, _SURVEY_ID).isEqual(_SURVEY, _ID)
                .orderBy(_BRANCH_SURVEY, filter)
                .filter(_BRANCH_SURVEY, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_BRANCH_SURVEY, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_BRANCH_SURVEY, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<BranchSurveyModel> listBranchesSurveys = new ArrayList<>();
            while (result.moveNext()) {
                final BranchSurveyModel branchesSurveys = result.getItem(_BRANCH_SURVEY, BranchSurveyModel.class);
                branchesSurveys.setBranchName(result.getItem(_BRANCH, BranchViewModel.class).getName());
                branchesSurveys.setSurvey(result.getItem(_SURVEY, SurveyModel.class));
                listBranchesSurveys.add(branchesSurveys);
            }
            return listBranchesSurveys;
        }
    }

    public BranchSurveyModel getBranchSurvey(long branchSurveyId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(BranchSurveyModel.class)
                .where(_ID).isEqual(branchSurveyId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(BranchSurveyModel.class) : null;
        }
    }

    public Long getLastFinishDate(String branch, long surveyId) throws Exception {
        try (final ResultBuilder result = QueryBuilder.select(BranchSurveyModel.class, _FINISH_DATE)
                .where(_BRANCH_CODE).isEqual(branch)
                .where(_SURVEY_ID).isEqual(surveyId)
                .orderBy(_FINISH_DATE).desc().limit(1)
                .execute(super.openConnection())) {
            if (result.moveNext()) {
                return result.getItem(BranchSurveyModel.class).getFinishDate();
            }
            return null;
        }
    }

    public boolean insert(BranchSurveyModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            if (newData.getBranchCode().equalsIgnoreCase(_ALL)) {
                auditTrail.prepareAudit(_TABLE_NAME_BRANCH_SURVEYS, newData);

                final List<BranchModel> listBranches = QueryBuilder.select(BranchModel.class)
                        .where(_CODE).isNotEqual(_ALL)
                        .getResult(sqlTransaction)
                        .executeItems(BranchModel.class);

                for (final BranchModel branch : listBranches) {
                    newData.setBranchCode(branch.getCode());
                    if (!QueryBuilder.insert(newData).execute(sqlTransaction).isModified()) {
                        sqlTransaction.rollbackTransaction();
                        return false;
                    }

                    final List<OutletModel> listOutlets = QueryBuilder.select(OutletModel.class)
                            .where(_BRANCH_CODE).isEqual(newData.getBranchCode())
                            .getResult(sqlTransaction)
                            .executeItems(OutletModel.class);

                    for (final OutletModel outlet : listOutlets) {
                        final OutletSurveySummaryModel outletSurveySummary = new OutletSurveySummaryModel();
                        outletSurveySummary.newModel(_MY_APPLICATION);
                        outletSurveySummary.setOutletId(outlet.getId());
                        outletSurveySummary.setStartDate(newData.getStartDate());
                        outletSurveySummary.setFinishDate(newData.getFinishDate());
                        outletSurveySummary.setSurveyId(newData.getSurveyId());
                        try (final UpdateResult insertOutletSurveySummary = QueryBuilder.insert(outletSurveySummary).execute(sqlTransaction)) {
                            if (!insertOutletSurveySummary.isModified()) {
                                sqlTransaction.rollbackTransaction();
                                return false;
                            }
                        }
                    }
                }

                if (this.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                }

                sqlTransaction.rollbackTransaction();
                return false;

            } else {
                auditTrail.prepareAudit(_TABLE_NAME_BRANCH_SURVEYS, newData);

                if (!QueryBuilder.insert(newData).execute(sqlTransaction).isModified()) {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }

                final List<OutletModel> listOutlets = QueryBuilder.select(OutletModel.class)
                        .where(_BRANCH_CODE).isEqual(newData.getBranchCode())
                        .getResult(sqlTransaction)
                        .executeItems(OutletModel.class);

                for (final OutletModel outlet : listOutlets) {
                    final OutletSurveySummaryModel outletSurveySummary = new OutletSurveySummaryModel();
                    outletSurveySummary.newModel(_MY_APPLICATION);
                    outletSurveySummary.setOutletId(outlet.getId());
                    outletSurveySummary.setStartDate(newData.getStartDate());
                    outletSurveySummary.setFinishDate(newData.getFinishDate());
                    outletSurveySummary.setSurveyId(newData.getSurveyId());
                    try (final UpdateResult insertOutletSurveySummary = QueryBuilder.insert(outletSurveySummary).execute(sqlTransaction)) {
                        if (!insertOutletSurveySummary.isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }
                }
                if (this.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                }
                sqlTransaction.rollbackTransaction();
                return false;
            }
        }
    }

    public boolean update(BranchSurveyModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult resultInsert = QueryBuilder.update(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_BRANCH_SURVEYS, newData);
            if (resultInsert.isModified() && this.insertAudit(sqlTransaction, auditTrail)) {
                // Get Outlet by branch code
                final SelectBuilder sqlSelectOutlet = QueryBuilder.select(OutletModel.class)
                        .where(_BRANCH_CODE).isEqual(newData.getBranchCode());
                try (final ResultBuilder resultOutlets = sqlSelectOutlet.execute(sqlTransaction)) {
                    while (resultOutlets.moveNext()) {
                        final OutletModel outlet = resultOutlets.getItem(OutletModel.class);
                        // Get Outlet Survey Summary
                        final SelectBuilder sqlSelectOutletSurvey = QueryBuilder.select(OutletSurveySummaryModel.class)
                                .where(_SURVEY_ID).isEqual(newData.getSurveyId())
                                .where(_OUTLET_ID).isEqual(outlet.getId());
                        try (final ResultBuilder resultSelectOutletSurvey = sqlSelectOutletSurvey.execute(sqlTransaction)) {
                            if (resultSelectOutletSurvey.moveNext()) {
                                final OutletSurveySummaryModel outletSurveySummary = resultSelectOutletSurvey.getItem(OutletSurveySummaryModel.class);
                                outletSurveySummary.setStartDate(newData.getStartDate());
                                outletSurveySummary.setFinishDate(newData.getFinishDate());
                                outletSurveySummary.modify(_MY_APPLICATION);

                                // Update Outlet Survey Summary
                                try (final UpdateResult updateOutletSurveySummary = QueryBuilder.update(outletSurveySummary).execute(sqlTransaction)) {
                                    if (!updateOutletSurveySummary.isModified()) {
                                        sqlTransaction.rollbackTransaction();
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
                sqlTransaction.commitTransaction();
                return true;
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean delete(BranchSurveyModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final UpdateResult resultInsert = QueryBuilder.delete(newData).execute(sqlTransaction);
            auditTrail.prepareAudit(_TABLE_NAME_BRANCH_SURVEYS, newData);
            if (resultInsert.isModified() && this.insertAudit(sqlTransaction, auditTrail)) {
                final List<OutletSurveySummaryModel> items;

                // Get Outlet Survey Summary
                final SelectBuilder sqlSelectOutletSurvey = QueryBuilder.select(_SUMMARY, OutletSurveySummaryModel.class)
                        .join(_OUTLET, OutletModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_OUTLET, _ID)
                        .where(_OUTLET, _BRANCH_CODE).isEqual(newData.getBranchCode())
                        .where(_SUMMARY, _SURVEY_ID).isEqual(newData.getSurveyId());
                try (final ResultBuilder resultSelectOutletSurvey = sqlSelectOutletSurvey.execute(sqlTransaction)) {
                    items = resultSelectOutletSurvey.getItems(_SUMMARY, OutletSurveySummaryModel.class);
                }

                for (final OutletSurveySummaryModel item : items) {
                    try (final UpdateResult deleteOutletSurvey = QueryBuilder.delete(item).execute(sqlTransaction)) {
                        if (!deleteOutletSurvey.isModified()) {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }
                }

                sqlTransaction.commitTransaction();
                return true;
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}