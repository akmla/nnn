package id.code.apollo.facade.task;

import id.code.apollo.filter.HoldFilter;
import id.code.apollo.model.task.HoldModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;


public class HoldFacade extends BaseFacade {

    public List<HoldModel> getHolds(Filter<HoldFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(HoldModel.class)
                .orderBy(filter)
                .filter(filter)
                .limitOffset(filter);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<HoldModel> listHold = new ArrayList<>();
            while (result.moveNext()) {
                HoldModel hold = result.getItem(HoldModel.class);
                listHold.add(hold);
            }
            return listHold;
        }
    }

    public HoldModel getHold(long holdId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(HoldModel.class)
                .where(_ID).isEqual(holdId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(HoldModel.class) : null;
        }
    }

    public HoldModel getHoldByTaskItemId(long taskItemId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(HoldModel.class)
                .where(_TASK_ITEM_ID).isEqual(taskItemId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(HoldModel.class) : null;
        }
    }

    public boolean insert(HoldModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final TaskItemModel taskItem = QueryBuilder.select(_TASK_ITEM, TaskItemModel.class)
                    .where(_TASK_ITEM, _ID).isEqual(newData.getTaskItemId())
                    .getResult(sqlTransaction).executeItem(TaskItemModel.class);

            if (taskItem != null) {
                final UpdateResult result = QueryBuilder.insert(newData).execute(sqlTransaction);
                auditTrail.prepareAudit(_TABLE_NAME_HOLD, newData);

                if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
                    taskItem.modify(newData.getCreatedBy());
                    taskItem.setStatus(_PASS);

                    final UpdateResult taskItemUpdate = QueryBuilder.update(taskItem).execute(sqlTransaction);

                    if (taskItemUpdate.isModified()) {
                        sqlTransaction.commitTransaction();
                        return true;
                    }

                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean update(HoldModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = openTransaction()) {
            final TaskItemModel taskItem = QueryBuilder.select(_TASK_ITEM, TaskItemModel.class)
                    .where(_TASK_ITEM, _ID).isEqual(newData.getTaskItemId())
                    .getResult(sqlTransaction).executeItem(TaskItemModel.class);

            if (taskItem != null) {
                final UpdateResult result = QueryBuilder.update(newData).execute(sqlTransaction);
                auditTrail.prepareAudit(_TABLE_NAME_HOLD, newData);

                if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
                    taskItem.modify(_MY_APPLICATION);
                    taskItem.setStatus(_PASS);
                    final UpdateResult taskItemUpdate = QueryBuilder.update(taskItem).execute(sqlTransaction);
                    if (taskItemUpdate.isModified()) {
                        sqlTransaction.commitTransaction();
                        return true;
                    }
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            } else {
                auditTrail.prepareAudit(_TABLE_NAME_HOLD, newData);
                auditTrail.setOldData("NO-DATA");

                if (super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                } else {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}