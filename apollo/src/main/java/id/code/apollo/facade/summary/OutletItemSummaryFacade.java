package id.code.apollo.facade.summary;

import id.code.apollo.filter.OutletItemSummaryFilter;
import id.code.apollo.model.promotion.CategoryPromotionItemModel;
import id.code.apollo.model.summary.OutletItemSummaryModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class OutletItemSummaryFacade extends BaseFacade {

    // Promotion Item

//    public List<OutletItemSummaryModel> getOutletItemSummaries(Filter<OutletItemSummaryFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
//        final SelectBuilder sqlSelect = QueryBuilder.select(_SUMMARY, OutletItemSummaryModel.class)
//                .includeAllJoin()
//                .join(_PROMOTION_ITEM, PromotionItemModel.class).on(_PROMOTION_ITEM, _CODE).isEqual(_SUMMARY, _ITEM_CODE)
//                .join(_THEMATIC, ThematicModel.class).on(_THEMATIC, _PRODUCT_CODE).isEqual(_PROMOTION_ITEM, _PRODUCT_CODE)
//                .join(_OUTLET, OutletSummaryModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_OUTLET, _OUTLET_ID)
//                .orderBy(_SUMMARY, filter)
//                .filter(_SUMMARY, filter)
//                .limitOffset(filter);
//
//        if (filter.getParam().getBranchCode() != null) {
//            sqlSelect.where(_OUTLET, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
//        }
//
//        if (filter.getParam().getRoleId() != null && filter.getParam().getUserId() != null) {
//            sqlSelect.join(_TASK_ITEM, TaskItemModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_TASK_ITEM, _OUTLET_ID)
//                    .join(_TASK, TaskModel.class).on(_TASK_ITEM, _TASK_ID).isEqual(_TASK, _ID)
//                    .where(_TASK, _USER_ID).isEqual(filter.getParam().getUserId());
//        }
//
//
//        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
//            final List<OutletItemSummaryModel> listOutletItemSummary = new ArrayList<>();
//            while (result.moveNext()) {
//                final OutletItemSummaryModel outletItemSummary = result.getItem(_SUMMARY, OutletItemSummaryModel.class);
//                final OutletSummaryModel outletSummary = result.getItem(_OUTLET, OutletSummaryModel.class);
//                final PromotionItemModel promotionItem = result.getItem(_PROMOTION_ITEM, PromotionItemModel.class);
//                final ThematicModel thematic = result.getItem(_THEMATIC, ThematicModel.class);
//                outletItemSummary.setItemName(promotionItem.getName());
//                outletItemSummary.setCategoryCode(promotionItem.getCategoryCode());
//                outletItemSummary.setSioTypeCode(outletSummary.getSioTypeCode());
//                outletItemSummary.setThematicCode(thematic.getCode());
//                listOutletItemSummary.add(outletItemSummary);
//            }
//            return listOutletItemSummary;
//        }
//    }

    // Category

    public List<OutletItemSummaryModel> getOutletItemSummaries(Filter<OutletItemSummaryFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_SUMMARY, OutletItemSummaryModel.class)
                .includeAllJoin()
                .join(_CATEGORY, CategoryPromotionItemModel.class).on(_CATEGORY, _CODE).isEqual(_SUMMARY, _ITEM_CODE)
                .join(_OUTLET, OutletSummaryModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_OUTLET, _OUTLET_ID)
                .orderBy(_SUMMARY, filter)
                .filter(_SUMMARY, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            sqlSelect.where(_OUTLET, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
        }

        if (filter.getParam().getRoleId() != null && filter.getParam().getUserId() != null) {
            sqlSelect.join(_TASK_ITEM, TaskItemModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_TASK_ITEM, _OUTLET_ID)
                    .join(_TASK, TaskModel.class).on(_TASK_ITEM, _TASK_ID).isEqual(_TASK, _ID)
                    .where(_TASK, _USER_ID).isEqual(filter.getParam().getUserId());
        }


        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<OutletItemSummaryModel> listOutletItemSummary = new ArrayList<>();
            while (result.moveNext()) {
                final OutletItemSummaryModel outletItemSummary = result.getItem(_SUMMARY, OutletItemSummaryModel.class);
                final OutletSummaryModel outletSummary = result.getItem(_OUTLET, OutletSummaryModel.class);
                final CategoryPromotionItemModel category = result.getItem(_CATEGORY, CategoryPromotionItemModel.class);
                outletItemSummary.setItemName(category.getName());
                outletItemSummary.setCategoryCode(category.getCode());
                outletItemSummary.setSioTypeCode(outletSummary.getSioTypeCode());
                listOutletItemSummary.add(outletItemSummary);
            }
            return listOutletItemSummary;
        }
    }


}