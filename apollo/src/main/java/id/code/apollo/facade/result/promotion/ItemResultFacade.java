package id.code.apollo.facade.result.promotion;

import id.code.apollo.filter.ItemResultFilter;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.promotion.CategoryPromotionItemModel;
import id.code.apollo.model.result.promotion.ItemResultModel;
import id.code.apollo.model.summary.OutletItemSummaryModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.component.utility.DateUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.security.Role;

import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

@SuppressWarnings("Duplicates")
public class ItemResultFacade extends BaseFacade {
    // CATEGORY

    public List<ItemResultModel> getItemResults(Filter<ItemResultFilter> filter, Long roleId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_RESULT, ItemResultModel.class)
                .includeAllJoin()
                .join(_TASK_ITEM, TaskItemModel.class).on(_TASK_ITEM, _ID).isEqual(_RESULT, _TASK_ITEM_ID)
                .join(_TASK, TaskModel.class).on(_TASK, _ID).isEqual(_TASK_ITEM, _TASK_ID)
                .join(_OUTLET, OutletModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .orderBy(_RESULT, filter)
                .filter(_RESULT, filter)
                .limitOffset(filter);

        // SIO TYPE CODE = NULL
        // CATEGORY ITEM CODE = NULL
        // THEMATIC CODE = NULL

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        if (filter.getParam().getOutletId() != null) {
            sqlSelect.where(_TASK_ITEM, _OUTLET_ID).isEqual(filter.getParam().getOutletId());
        }

        if (filter.getParam().getMdId() != null) {
            sqlSelect.where(_TASK, _USER_ID).isEqual(filter.getParam().getMdId());
        }

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            if (roleId == Role._ROLE_ID_MERCHANDISER) {
                final LocalDate today = DateUtility.toLocalDate(System.currentTimeMillis());
                final Long dayStart = DateUtility.toUnixMillis(today.minusDays(1).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)));
                final Long dayFinish = DateUtility.toUnixMillis(today.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)));

                sqlSelect.where(_TASK_ITEM, _TASK_DATE)
                        .equalsLessThanAnd(dayFinish, _TASK_ITEM, _TASK_DATE)
                        .equalsGreaterThan(dayStart);
            } else {
                sqlSelect.where(_RESULT, _TASK_ITEM_ID).in(
                        QueryBuilder.select(OutletSummaryModel.class, _LAST_TASK_ITEM_ID)
                                .where(_LAST_TASK_ITEM_ID).isNot(null)
                                .where(_BRANCH_CODE).isEqual(filter.getParam().getBranchCode())
                );
            }
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<ItemResultModel> listItemResult = new ArrayList<>();
            while (result.moveNext()) {
                final ItemResultModel itemResult = result.getItem(_RESULT, ItemResultModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
                final TaskModel task = result.getItem(_TASK, TaskModel.class);
                itemResult.setOutletId(taskItem.getOutletId());
                itemResult.setBranchCode(outlet.getBranchCode());
                itemResult.setMdId(task.getUserId());
                listItemResult.add(itemResult);
            }
            return listItemResult;
        }
    }

    public ItemResultModel getItemResult(long itemResultId) throws SQLException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_RESULT, ItemResultModel.class)
                .includeAllJoin()
                .join(_TASK_ITEM, TaskItemModel.class).on(_TASK_ITEM, _ID).isEqual(_RESULT, _TASK_ITEM_ID)
                .join(_TASK, TaskModel.class).on(_TASK, _ID).isEqual(_TASK_ITEM, _TASK_ID)
                .join(_OUTLET, OutletModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .where(_RESULT, _ID).isEqual(itemResultId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final ItemResultModel itemResult = result.getItem(_RESULT, ItemResultModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
                final TaskModel task = result.getItem(_TASK, TaskModel.class);
                itemResult.setOutletId(taskItem.getOutletId());
                itemResult.setBranchCode(outlet.getBranchCode());
                itemResult.setMdId(task.getUserId());
                return itemResult;
            }
            return null;
        }
    }

    public ItemResultModel getItemResultByTaskItemIdAndItemCode(long taskItemId, String itemCode) throws SQLException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_RESULT, ItemResultModel.class)
                .includeAllJoin()
                .join(_TASK_ITEM, TaskItemModel.class).on(_TASK_ITEM, _ID).isEqual(_RESULT, _TASK_ITEM_ID)
                .join(_TASK, TaskModel.class).on(_TASK, _ID).isEqual(_TASK_ITEM, _TASK_ID)
                .join(_OUTLET, OutletModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .where(_RESULT, _TASK_ITEM_ID).isEqual(taskItemId)
                .where(_RESULT, _ITEM_CODE).isEqual(itemCode)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            if (result.moveNext()) {
                final ItemResultModel itemResult = result.getItem(_RESULT, ItemResultModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
                final TaskModel task = result.getItem(_TASK, TaskModel.class);
                itemResult.setOutletId(taskItem.getOutletId());
                itemResult.setBranchCode(outlet.getBranchCode());
                itemResult.setMdId(task.getUserId());
                return itemResult;
            }
            return null;
        }
    }

    public boolean update(ItemResultModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final OutletSummaryModel outletSummaryModel = QueryBuilder.select(_TASK_ITEM, TaskItemModel.class)
                    .includeAllJoin()
                    .join(_SUMMARY, OutletSummaryModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_SUMMARY, _OUTLET_ID)
                    .join(_SUMMARIES, OutletItemSummaryModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_SUMMARIES, _OUTLET_ID)
                    .join(_CATEGORY, CategoryPromotionItemModel.class).on(_CATEGORY, _CODE).isEqual(_SUMMARIES, _ITEM_CODE)
                    .where(_TASK_ITEM, _ID).isEqual(newData.getTaskItemId())
                    .getResult(sqlTransaction).executeItem(OutletSummaryModel.class, (resultNeeded, outletSummary) -> {
                                final CategoryPromotionItemModel category = resultNeeded.getItem(CategoryPromotionItemModel.class);
                                newData.setCategoryItemCode(category.getCode());
                                newData.setSioTypeCode(outletSummary.getSioTypeCode());
                            }
                    );

            if (outletSummaryModel != null) {

                final UpdateResult result = QueryBuilder.update(newData).execute(sqlTransaction);
                auditTrail.prepareAudit(_TABLE_NAME_ITEM_RESULT, newData);

                // SIO TYPE CODE = NULL
                // CATEGORY ITEM CODE = NULL
                // THEMATIC CODE = NULL

                if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
                    final ResultBuilder resultSummary = QueryBuilder.select(_SUMMARY, OutletItemSummaryModel.class)
                            .join(_TASK_ITEM, TaskItemModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_TASK_ITEM, _OUTLET_ID)
                            .where(_TASK_ITEM, _ID).isEqual(newData.getTaskItemId())
                            .where(_SUMMARY, _ITEM_CODE).isEqual(newData.getItemCode())
                            .execute(sqlTransaction);

                    if (resultSummary.moveNext()) {
                        final OutletItemSummaryModel newSummary = resultSummary.getItem(_SUMMARY, OutletItemSummaryModel.class);
                        newSummary.setLastActionToItem(newData.getActionToItem());
                        newSummary.setLastItemCondition(newData.getItemCondition());
                        newSummary.modify(_MY_APPLICATION);

                        final UpdateResult resultUpdate = QueryBuilder.update(newSummary).execute(sqlTransaction);
                        if (resultUpdate.isModified()) {
                            sqlTransaction.commitTransaction();
                            return true;
                        }
                    }

                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }
            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean insert(ItemResultModel newData, AuditTrailModel auditTrail) throws Exception {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final OutletSummaryModel outletSummaryModel = QueryBuilder.select(_TASK_ITEM, TaskItemModel.class)
                    .includeAllJoin()
                    .join(_SUMMARY, OutletSummaryModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_SUMMARY, _OUTLET_ID)
                    .join(_SUMMARIES, OutletItemSummaryModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_SUMMARIES, _OUTLET_ID)
                    .join(_CATEGORY, CategoryPromotionItemModel.class).on(_CATEGORY, _CODE).isEqual(_SUMMARIES, _ITEM_CODE)
                    .where(_TASK_ITEM, _ID).isEqual(newData.getTaskItemId())
                    .getResult(sqlTransaction).executeItem(OutletSummaryModel.class, (resultNeeded, outletSummary) -> {
                                final CategoryPromotionItemModel category = resultNeeded.getItem(CategoryPromotionItemModel.class);
                                newData.setCategoryItemCode(category.getCode());
                                newData.setSioTypeCode(outletSummary.getSioTypeCode());
                            }
                    );

            if (outletSummaryModel != null) {

                final UpdateResult result = QueryBuilder.insert(newData).execute(sqlTransaction);
                auditTrail.prepareAudit(_TABLE_NAME_ITEM_RESULT, newData);

                // SIO TYPE CODE = NULL
                // CATEGORY ITEM CODE = NULL
                // THEMATIC CODE = NULL

                if ((result.isModified()) && (this.insertAudit(sqlTransaction, auditTrail))) {
                    final ResultBuilder resultSummary = QueryBuilder.select(_SUMMARY, OutletItemSummaryModel.class)
                            .join(_TASK_ITEM, TaskItemModel.class).on(_SUMMARY, _OUTLET_ID).isEqual(_TASK_ITEM, _OUTLET_ID)
                            .where(_TASK_ITEM, _ID).isEqual(newData.getTaskItemId())
                            .where(_SUMMARY, _ITEM_CODE).isEqual(newData.getItemCode())
                            .execute(sqlTransaction);

                    if (resultSummary.moveNext()) {
                        final OutletItemSummaryModel newSummary = resultSummary.getItem(_SUMMARY, OutletItemSummaryModel.class);
                        newSummary.setLastActionToItem(newData.getActionToItem());
                        newSummary.setLastItemCondition(newData.getItemCondition());
                        newSummary.modify(_MY_APPLICATION);

                        final UpdateResult resultUpdate = QueryBuilder.update(newSummary).execute(sqlTransaction);
                        if (resultUpdate.isModified()) {
                            sqlTransaction.commitTransaction();
                            return true;
                        } else {
                            sqlTransaction.rollbackTransaction();
                            return false;
                        }
                    }

                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}