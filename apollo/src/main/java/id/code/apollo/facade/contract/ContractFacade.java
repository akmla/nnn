package id.code.apollo.facade.contract;

import id.code.apollo.filter.ContractFilter;
import id.code.apollo.model.contract.ContractModel;
import id.code.component.utility.StringUtility;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.builder.increment.FrameworkIncrementProvider;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName._CODE;
import static id.code.master_data.AliasName._ID;

public class ContractFacade extends BaseFacade implements FrameworkIncrementProvider<ContractModel, String> {

    public List<ContractModel> getContracts(Filter<ContractFilter> filter) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(ContractModel.class)
                .orderBy(filter)
                .filter(filter)
                .limitOffset(filter);
//
//        if(filter.getParam().getBranchCode() != null && !filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
//            sqlSelect.where(_CODE).contains(filter.getParam().getBranchCode());
//        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<ContractModel> listContracts = new ArrayList<>();
            while (result.moveNext()) {
                final ContractModel contracts = result.getItem(ContractModel.class);
                listContracts.add(contracts);
            }
            return listContracts;
        }
    }

    public ContractModel getContract(long contractId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(ContractModel.class)
                .where(_ID).isEqual(contractId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(ContractModel.class) : null;
        }
    }

    public ContractModel getContract(String contractCode) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(ContractModel.class)
                .where(_CODE).isEqual(contractCode)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(ContractModel.class) : null;
        }
    }

    @Override
    public void increment(ContractModel contract, String lastMaxValue) throws Exception {
        final String seqNo = lastMaxValue == null ? null : lastMaxValue.substring(lastMaxValue.length() - 5);
        final int numSeqNo = seqNo == null ? 1 : Integer.parseInt(seqNo) + 1;
        contract.setCode(String.format("%s/%s/%05d", contract.getBrandCode(), contract.getSioTypeCode(), numSeqNo));
    }

    @Override
    public SelectBuilder createLastIncrementValueQuery(ContractModel contract) {
        final String keyword = String.format("%s/%s/", contract.getBrandCode(), contract.getSioTypeCode());
        return QueryBuilder.select(ContractModel.class, _CODE)
                .where(_CODE).startWith(keyword)
                .limit(1)
                .orderBy(_CODE).desc();
    }

    @Override
    public boolean ignoreIncrementValue(ContractModel o) throws QueryBuilderException {
        return !StringUtility.isNullOrWhiteSpace(o.getCode());
    }




}