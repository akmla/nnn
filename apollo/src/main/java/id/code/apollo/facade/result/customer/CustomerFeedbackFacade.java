package id.code.apollo.facade.result.customer;

import id.code.apollo.filter.CustomerFeedbackFilter;
import id.code.apollo.model.outlet.OutletModel;
import id.code.apollo.model.result.customer.CustomerFeedbackModel;
import id.code.apollo.model.summary.OutletSummaryModel;
import id.code.apollo.model.task.TaskItemModel;
import id.code.apollo.model.task.TaskModel;
import id.code.component.utility.DateUtility;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.master_data.facade.BaseFacade;
import id.code.master_data.model.AuditTrailModel;
import id.code.master_data.model.user.BranchUserModel;
import id.code.master_data.security.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import static id.code.master_data.AliasName.*;

public class CustomerFeedbackFacade extends BaseFacade {

    public List<CustomerFeedbackModel> getAllCustomerFeedback(Filter<CustomerFeedbackFilter> filter, Long roleId) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(_FEEDBACK, CustomerFeedbackModel.class)
                .includeAllJoin()
                .join(_TASK_ITEM, TaskItemModel.class).on(_TASK_ITEM, _ID).isEqual(_FEEDBACK, _TASK_ITEM_ID)
                .join(_TASK, TaskModel.class).on(_TASK, _ID).isEqual(_TASK_ITEM, _TASK_ID)
                .join(_OUTLET, OutletModel.class).on(_TASK_ITEM, _OUTLET_ID).isEqual(_OUTLET, _ID)
                .orderBy(_FEEDBACK, filter)
                .filter(_FEEDBACK, filter)
                .limitOffset(filter);

        if (filter.getParam().getBranchCode() != null) {
            if (filter.getParam().getBranchCode().equalsIgnoreCase(_ALL)) {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).in(
                        QueryBuilder.select(BranchUserModel.class, _BRANCH_CODE).where(_USER_ID).isEqual(filter.getParam().getUserId())
                );
            } else {
                sqlSelect.where(_OUTLET, _BRANCH_CODE).isEqual(filter.getParam().getBranchCode());
            }
        }

        if (filter.getParam().getOutletId() != null) {
            sqlSelect.where(_TASK_ITEM, _OUTLET_ID).isEqual(filter.getParam().getOutletId());
        }

        if (filter.getParam().getMdId() != null) {
            sqlSelect.where(_TASK, _USER_ID).isEqual(filter.getParam().getMdId());
        }

        if (filter.getParam().getApplicationType() != null && filter.getParam().getApplicationType() == _APPLICATION_TYPE_APOLLO) {
            if (roleId == Role._ROLE_ID_MERCHANDISER) {
                final LocalDate today = DateUtility.toLocalDate(System.currentTimeMillis());
                final Long dayStart = DateUtility.toUnixMillis(today.minusDays(1));
                final Long dayFinish = DateUtility.toUnixMillis(today.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)));

                sqlSelect.where(_TASK_ITEM, _TASK_DATE)
                        .equalsLessThanAnd(dayFinish, _TASK_ITEM, _TASK_DATE)
                        .equalsGreaterThan(dayStart);
            } else {
                sqlSelect.where(_FEEDBACK, _TASK_ITEM_ID).in(
                        QueryBuilder.select(OutletSummaryModel.class, _LAST_TASK_ITEM_ID)
                                .where(_LAST_TASK_ITEM_ID).isNot(null)
                                .where(_BRANCH_CODE).isEqual(filter.getParam().getBranchCode())
                );
            }
        }

        if (filter.getParam().getTaskDate() != null) {
            sqlSelect.join(_TASK_ITEM, TaskItemModel.class).on(_TASK_ITEM, _ID).isEqual(_FEEDBACK, _TASK_ITEM_ID)
                    .where(_TASK_ITEM, _TASK_DATE).isEqual(filter.getParam().getTaskDate());
        }

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            final List<CustomerFeedbackModel> listCustomerFeedback = new ArrayList<>();
            while (result.moveNext()) {
                final CustomerFeedbackModel customerFeedback = result.getItem(_FEEDBACK, CustomerFeedbackModel.class);
                final OutletModel outlet = result.getItem(_OUTLET, OutletModel.class);
                final TaskItemModel taskItem = result.getItem(_TASK_ITEM, TaskItemModel.class);
                final TaskModel task = result.getItem(_TASK, TaskModel.class);
                customerFeedback.setOutletId(taskItem.getOutletId());
                customerFeedback.setBranchCode(outlet.getBranchCode());
                customerFeedback.setMdId(task.getUserId());
                listCustomerFeedback.add(customerFeedback);
            }
            return listCustomerFeedback;
        }
    }

    public CustomerFeedbackModel getCustomerFeedback(long customerFeedbackId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_FEEDBACK, CustomerFeedbackModel.class)
                .where(_FEEDBACK, _ID).isEqual(customerFeedbackId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(_FEEDBACK, CustomerFeedbackModel.class) : null;
        }
    }

    public CustomerFeedbackModel getCustomerFeedbackByTaskItemId(long taskItemId) throws SQLException, QueryBuilderException, IllegalAccessException, InstantiationException {
        final SelectBuilder sqlSelect = QueryBuilder.select(_FEEDBACK, CustomerFeedbackModel.class)
                .where(_FEEDBACK, _TASK_ITEM_ID).isEqual(taskItemId)
                .limit(1);

        try (final ResultBuilder result = sqlSelect.execute(super.openConnection())) {
            return (result.moveNext()) ? result.getItem(_FEEDBACK, CustomerFeedbackModel.class) : null;
        }
    }

    public boolean insert(CustomerFeedbackModel newData, AuditTrailModel auditTrail) throws QueryBuilderException, SQLException, IllegalAccessException, IOException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final TaskItemModel taskItem = QueryBuilder.select(TaskItemModel.class)
                    .where(_ID).isEqual(newData.getTaskItemId())
                    .getResult(sqlTransaction)
                    .executeItem(TaskItemModel.class);


            if (taskItem != null) {
                auditTrail.prepareAudit(_TABLE_NAME_CUSTOMER_FEEDBACK, newData);
                if (QueryBuilder.insert(newData).execute(sqlTransaction).isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                } else if (QueryBuilder.update(newData).execute(sqlTransaction).isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                } else {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }

    public boolean update(CustomerFeedbackModel newData, AuditTrailModel auditTrail) throws QueryBuilderException, SQLException, IllegalAccessException, IOException {
        try (final SimpleTransaction sqlTransaction = super.openTransaction()) {
            final TaskItemModel taskItem = QueryBuilder.select(TaskItemModel.class)
                    .where(_ID).isEqual(newData.getTaskItemId())
                    .getResult(sqlTransaction)
                    .executeItem(TaskItemModel.class);


            if (taskItem != null) {
                auditTrail.prepareAudit(_TABLE_NAME_CUSTOMER_FEEDBACK, newData);
                if (QueryBuilder.update(newData).execute(sqlTransaction).isModified() && super.insertAudit(sqlTransaction, auditTrail)) {
                    sqlTransaction.commitTransaction();
                    return true;
                } else {
                    sqlTransaction.rollbackTransaction();
                    return false;
                }
            }

            sqlTransaction.rollbackTransaction();
            return false;
        }
    }
}
