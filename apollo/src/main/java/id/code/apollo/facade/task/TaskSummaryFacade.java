package id.code.apollo.facade.task;

import id.code.apollo.model.summary.TaskSummaryModel;
import id.code.database.SimpleTransaction;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.master_data.facade.BaseFacade;

import static id.code.master_data.AliasName._USER_ID;

@SuppressWarnings("WeakerAccess")
public class TaskSummaryFacade extends BaseFacade {

    public TaskSummaryModel getTaskSummary(long userId, SimpleTransaction sqlTransaction) throws Exception {
        final SelectBuilder sqlSelect = QueryBuilder.select(TaskSummaryModel.class)
                .where(_USER_ID).isEqual(userId);
        try (final ResultBuilder result = sqlSelect.execute(sqlTransaction)) {
            return result.moveNext() ? result.getItem(TaskSummaryModel.class) : null;
        }
    }
}
